package technologies.angular;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import technologies.angular.lazylad.OfferBannerFragment;
import technologies.angular.lazylad.Snippets.CouponBannerSnippet;
import technologies.angular.lazylad.Snippets.TabSnippet;

/**
 * Created by Amud on 03/02/16.
 */

public class MainScreenTabsAdapter extends FragmentStatePagerAdapter {
    // This will Store the Titles of the Tabs which are Going to be passed when CartPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the CartPagerAdapter is created

    Context m_context;
    private ArrayList<String> m_tabSnippet;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public MainScreenTabsAdapter(FragmentManager fm, int mNumbOfTabsumb, Context context, ArrayList<String> tabSnippets) {
        super(fm);
        this.NumbOfTabs = mNumbOfTabsumb;
        this.m_context = context;
        m_tabSnippet = tabSnippets;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if (!m_tabSnippet.isEmpty()) {
            TabFragment tabFragment = new TabFragment();
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("m_tabSnippet", m_tabSnippet);
            bundle.putInt("position", position);
            tabFragment.setArguments(bundle);

            return tabFragment;
        } else
            return null;
    }


    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }



    @Override
    public CharSequence getPageTitle(int position) {
        return  "City";
       // return m_tabSnippet.get(position).coupon_id;
    }
}
