package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sakshigupta on 28/10/15.
 */
public class LastActivityAdapter extends BaseAdapter {

    private Activity m_activity;
    private ArrayList<LastActivitySnippet> m_totalItemsList;

    public LastActivityAdapter(Activity activity, ArrayList<LastActivitySnippet> totalItemslist) {
        m_activity = activity;
        m_totalItemsList = totalItemslist;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_totalItemsList != null) {
            count = m_totalItemsList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_totalItemsList != null) {
            return m_totalItemsList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_totalItemsList != null) {
            return position;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (m_totalItemsList != null) {

            String spCode = ((LastActivitySnippet) getItem(position)).sp_code;
            String sellerName = "";
            String[] mProjection = {LazyContract.OrderDetailsEntry.COLUMN_SERV_PROV_NAME};
            String whereClause = LazyContract.OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
            String[] selectionArgs = new String[]{spCode};
            Cursor cursor = m_activity.getContentResolver().query(
                    LazyContract.OrderDetailsEntry.CONTENT_URI,
                    mProjection,
                    whereClause,
                    selectionArgs,
                    null);

            if (cursor.moveToFirst()) {
                sellerName = cursor.getString(cursor.getColumnIndex("serv_prov_name"));
            }
                String orderCode = ((LastActivitySnippet) getItem(position)).order_code;
            double amtPayable = ((LastActivitySnippet) getItem(position)).tot_amount;
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.last_activity_display_list, (ViewGroup) null);
            }

            TextView m_spCodeFinalTextView = (TextView) convertView.findViewById(R.id.spCode);
            TextView m_orderCodeFinalBillTextView = (TextView) convertView.findViewById(R.id.order_code);
            TextView m_amtPayFinalTextView = (TextView) convertView.findViewById(R.id.amt_pay);
            TextView m_delChargeFinalTextView = (TextView) convertView.findViewById(R.id.del_time);

            m_spCodeFinalTextView.setText(sellerName);
            m_orderCodeFinalBillTextView.setText(orderCode);
            m_amtPayFinalTextView.setText("₹" + Double.toString(amtPayable));
        }
        return convertView;
    }
}
