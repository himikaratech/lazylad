package technologies.angular.lazylad;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import technologies.angular.backgroundsync.SyncAdapter;
import technologies.angular.lazylad.Snippets.CouponBannerSnippet;

/**
 * Created by Amud on 25/01/16.
 */
public class OfferBannerFragment extends Fragment {

    int position;
    private ArrayList<CouponBannerSnippet> m_bannerImageAddress;

    private static RecyclerView cartRecycleView;
    private static RecyclerView.Adapter mAdapter;
    private static RecyclerView.LayoutManager mLayoutManager;

    public OfferBannerFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        position = getArguments().getInt("position");
        m_bannerImageAddress = getArguments().getParcelableArrayList("m_bannerImageList");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.coupon_banner, container, false);
        ImageView itemImage = (ImageView) v.findViewById(R.id.bannerCoupon);
        final String coupon_id = m_bannerImageAddress.get(position).coupon_id;
        final String redirect_type=m_bannerImageAddress.get(position).redirect_type;


        int views_count = 0;
        String[] viewsSelectionValues = new String[]{LazyContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT};
        String viewsWhereClause = LazyContract.CouponsStatistics.COLUMN_COUPON_ID + " = ? and " +
                LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " = ? ";
        String[] viewsSelectionArgs = new String[]{coupon_id, String.valueOf(0)};


        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyContract.CouponsViewsDetails.CONTENT_URI,
                viewsSelectionValues,
                viewsWhereClause,
                viewsSelectionArgs,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            views_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT));
        }
        views_count = views_count + 1;
        ContentValues viewsValues = new ContentValues();
        viewsValues.put(LazyContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT, views_count);
        int views_update = getActivity().getContentResolver().update(LazyContract.CouponsViewsDetails.CONTENT_URI, viewsValues, viewsWhereClause, viewsSelectionArgs);
        if (views_update < 1) {
            viewsValues.put(LazyContract.CouponsStatistics.COLUMN_COUPON_ID, coupon_id);
            viewsValues.put(LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 0);
            getActivity().getContentResolver().insert(LazyContract.CouponsViewsDetails.CONTENT_URI, viewsValues);
        }

        int image_flag = m_bannerImageAddress.get(position).coupon_img_flag;
        String address = m_bannerImageAddress.get(position).coupon_img_add;

        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;


        if (image_flag == 1) {
            Picasso.with(getActivity())
                    .load(address)
                    .placeholder(R.drawable.loadingscreen)
                    .error(R.drawable.no_image)
                    .fit()
                    .into(itemImage);
        }


        itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int clicks_count = 0;
                String[] clicksSelectionValues = new String[]{LazyContract.CouponsClicksDetails.COLUMN_COUNT};
                String clicksWhereClause = LazyContract.CouponsStatistics.COLUMN_COUPON_ID + " = ? and " +
                        LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " = ? ";
                String[] clicksSelectionArgs = new String[]{coupon_id, String.valueOf(0)};


                Cursor m_cursor = getActivity().getContentResolver().query(
                        LazyContract.CouponsClicksDetails.CONTENT_URI,
                        clicksSelectionValues,
                        clicksWhereClause,
                        clicksSelectionArgs,
                        null);

                if (m_cursor != null && m_cursor.moveToFirst()) {
                    clicks_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.CouponsClicksDetails.COLUMN_COUNT));
                }
                clicks_count = clicks_count + 1;
                ContentValues clicksValues = new ContentValues();
                clicksValues.put(LazyContract.CouponsClicksDetails.COLUMN_COUNT, clicks_count);
                int clicks_update = getActivity().getContentResolver().update(LazyContract.CouponsClicksDetails.CONTENT_URI, clicksValues, clicksWhereClause, clicksSelectionArgs);
                if (clicks_update < 1) {
                    clicksValues.put(LazyContract.CouponsStatistics.COLUMN_COUPON_ID, coupon_id);
                    clicksValues.put(LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 0);
                    getActivity().getContentResolver().insert(LazyContract.CouponsClicksDetails.CONTENT_URI, clicksValues);
                }

                SyncAdapter.syncImmediately(getContext());


                if(redirect_type.equals("item"))
                {
                    String redirect_link=m_bannerImageAddress.get(position).redirect_link;
                    deepLink(redirect_link);
                }
                else if(redirect_type.equals("coupon_add"))
                {
                    String package_name = "com.lazylad.coupons.lazycoupons";

                    boolean exist = isPackageExisted(package_name);
                    if (exist)
                        openApp(getContext(), package_name);
                    else {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + package_name)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + package_name)));
                        }
                    }

                }
                else if(redirect_type.equals("webpage"))
                {
                    String redirect_link=m_bannerImageAddress.get(position).redirect_link;
                    deepLink(redirect_link);
                }



            }
        });


        return v;
    }

    private void openlink() {
        Uri uri = Uri.parse("http://lazylad.com/items?sp_code=138/st_code=1/sp_del_time=90/sp_min_order=500/sp_name=Khatana/time_interval=90/sp_open_time=540/sp_close_time=1880/first_sc_code=6"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


    public boolean isPackageExisted(String targetPackage) {
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = getActivity().getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }


    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
        if (i == null) {
            return false;
            //throw new PackageManager.NameNotFoundException();
        }
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
        return true;
    }

    public void deepLink(String redirect_link)
    {
        Uri uri = Uri.parse(redirect_link);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


}
