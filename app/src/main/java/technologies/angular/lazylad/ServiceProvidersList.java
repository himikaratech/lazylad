package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.ServiceTypeCategoriesEntry;


public class ServiceProvidersList extends ActionBarActivity {

    private static final String TAG = "ServiceProvidersList";

    public String m_stCode;
    private String[] m_variablesFromLastActivity;
    private String m_areaName;
    private String m_areaCode;
    private ArrayList<ServiceProvidersListSnippet> m_servProvList;
    private ServiceProvidersListAdapter m_servProvAdapter;
    private ListView m_servProvDetails;
    private Toolbar toolbar;
    private AppBarLayout appBar;

    ImageView image_sp;
    private ImageButton cartImage;
    CollapsingToolbarLayout collapsingToolbarLayout;
    RecyclerView recyclerView;
    int vibrant;
    ProgressDialog progressDialog;
    Boolean isInternetPresent = true;
    int m_servTypeFlag;
    SharedPreferences prefs;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isInternetPresent = isNetworkAvailable();
         prefs = this.getSharedPreferences(
                 "technologies.angular.lazylad", Context.MODE_PRIVATE);

        m_servTypeFlag = getIntent().getIntExtra("servTypeFlag", 0);
        setContentView(R.layout.service_providers_list);
        m_servTypeFlag = 1;
        if (m_servTypeFlag == 1)
            overridePendingTransition(0, R.anim.anim_zoom_out);
        else
            overridePendingTransition(R.anim.anim_in, R.anim.anim_zoom_out);

        recyclerView = (RecyclerView) findViewById(R.id.servprov_list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(ServiceProvidersList.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        appBar = (AppBarLayout)findViewById(R.id.appbar);

        cartImage = (ImageButton) findViewById(R.id.shopping_cart_image);

        cartImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(getApplicationContext(), ShoppingCartOnMainScreen.class);
                startActivity(intent);
            }
        });

        setupToolbar();
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        if (m_servTypeFlag == 0)
            collapsingToolbarLayout.setVisibility(View.VISIBLE);

        if (isInternetPresent) {


            m_stCode = prefs.getString("ServTypeCode", "Not Available");//m_variablesFromLastActivity[0];

            if (m_servTypeFlag == 0) {
                recyclerView.setVisibility(View.VISIBLE);
            }

            m_areaCode = prefs.getString(PreferencesStore.AREA_SELECTED_CONSTANT, "Not Available");
            // Get tracker.

            Tracker area = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
            area.send(new HitBuilders.EventBuilder()
                    .setCategory(m_stCode)
                    .setAction(m_areaCode)
                    .build());

            Cursor m_cursor = getContentResolver().query(
                    ServiceTypeCategoriesEntry.CONTENT_URI,
                    null,
                    null,
                    null,
                    null);
            if (m_cursor != null && m_cursor.moveToFirst()) {
                if (m_cursor.getCount() > 0) {
                    getContentResolver().delete(
                            ServiceTypeCategoriesEntry.CONTENT_URI,
                            null,
                            null
                    );
                }
            }

            image_sp = (ImageView) findViewById(R.id.backdrop);

            if (m_servTypeFlag == 0)
                image_sp.setVisibility(View.VISIBLE);

            final int service_type = Integer.parseInt(m_stCode);

            Picasso.with(this)
                    .load("http://angulartechnologies.com/st_images/" + service_type)
                    .placeholder(R.drawable.loading)
                    .fit().centerCrop()
                    .into(image_sp, new Callback() {
                        @Override
                        public void onSuccess() {
                            Picasso.with(ServiceProvidersList.this)
                                    .load("http://angulartechnologies.com/st_images/" + service_type) // image url goes here
                                    .placeholder(image_sp.getDrawable())
                                    .fit().centerCrop()
                                    .into(image_sp);
                            vibrant = R.color.opaque;
                            setupCollapsingToolbarLayout();
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            Toast.makeText(this, "Connection problem , Try again", Toast.LENGTH_LONG).show();
        }

        mHelper = new MoEHelper(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);

        if (isNetworkAvailable())
            loadProvidersForAreaAndType();

        setTracker();
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
        mHelper.onPause(this);
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null)
            setSupportActionBar(toolbar);

        if (m_servTypeFlag == 0) {
            toolbar.setVisibility(View.VISIBLE);
        }

        // Show menu icon
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayShowTitleEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    private void setupCollapsingToolbarLayout() {
        if (collapsingToolbarLayout != null) {
            collapsingToolbarLayout.setTitle("Select Store");
            collapsingToolbarLayout.setBackgroundColor(getResources().getColor(R.color.backgroundcolor));
            Log.e("vibrant", vibrant + "");
            ColorDrawable colorDrawable = new ColorDrawable();
            colorDrawable.setColor(vibrant);
            collapsingToolbarLayout.setContentScrim(colorDrawable);
        }
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.ServiceProvidersList");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }


    private void loadProvidersForAreaAndType() {
        Log.e("st_code",m_stCode);
        progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        final Double latitude = Double.longBitsToDouble(prefs.getLong(PreferencesStore.LATITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LATITUDE_SELECTED_CONSTANT));
        final Double longitude = Double.longBitsToDouble(prefs.getLong(PreferencesStore.LONGITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LONGITUDE_SELECTED_CONSTANT));
        final String city_code = prefs.getString(PreferencesStore.CITY_CODE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT);

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.ServiceTypeRequestModel serviceTypeRequestModel = requestModel.new ServiceTypeRequestModel();
       serviceTypeRequestModel.st_code=m_stCode;
        serviceTypeRequestModel.city_code = city_code;
        serviceTypeRequestModel.lat = String.valueOf(latitude);
        serviceTypeRequestModel.longi = String.valueOf(longitude);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.loadProvidersForAreaAndType(serviceTypeRequestModel, new retrofit.Callback<APIResonseModel.ServiceTypeResponseModel>() {
            @Override
            public void success(APIResonseModel.ServiceTypeResponseModel serviceTypeResponseModel, retrofit.client.Response response) {
                    Log.d("Store","success");
                    m_servProvList = new ArrayList<ServiceProvidersListSnippet>();
                    m_servProvList = serviceTypeResponseModel.serviceProvidersListSnippetArrayList;
                    if (m_servProvList.isEmpty()) {
                        m_servProvList = new ArrayList<ServiceProvidersListSnippet>();
                        ServiceProvidersListSnippet serviceProvidersListSnippet = new ServiceProvidersListSnippet(0, "", "Not available", "", "", "", "", 0, 0, 0);
                        m_servProvList.add(0, serviceProvidersListSnippet);
                        recyclerView.setAdapter(new ServiceProvidersRecycleAdapter(m_servProvList, R.layout.service_provider_display_error, ServiceProvidersList.this, 1, 0));

                    } else
                        recyclerView.setAdapter(new ServiceProvidersRecycleAdapter(m_servProvList, R.layout.service_provider_display_list_view, ServiceProvidersList.this, 0, m_servTypeFlag));

                    progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                m_servProvList = new ArrayList<ServiceProvidersListSnippet>();
                ServiceProvidersListSnippet serviceProvidersListSnippet = new ServiceProvidersListSnippet(0, "", "Not available", "", "", "", "", 0, 0, 0);
                m_servProvList.add(0, serviceProvidersListSnippet);
                recyclerView.setAdapter(new ServiceProvidersRecycleAdapter(m_servProvList, R.layout.service_provider_display_error, ServiceProvidersList.this, 1, 0));
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_zoom_out_back, R.anim.anim_in_back);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_service_providers_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

    public void ShoppingCartOnServiceProviderListScreen() {
        Intent intent = new Intent(ServiceProvidersList.this, ShoppingCartOnMainScreen.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
