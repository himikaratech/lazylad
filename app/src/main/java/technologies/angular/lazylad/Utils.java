package technologies.angular.lazylad;

/**
 * Created by Saurabh on 28/01/15.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import technologies.angular.lazylad.LazyContract.UserDetailsEntry;

public class Utils {

    public static int LOLLIPOP = 21;

    public static boolean isUserSignedIn(Context context) {
        return true;//SessionManager.getInstance(context).isLoggedIn();
    }

    public static String classToJson(Object ob) {
        return new Gson().toJson(ob);
    }

    public static Object jsonToClass(String jStr, Class cls) {
        return new Gson().fromJson(jStr, cls);
    }

    public static void ShowPickerDialog(AlertDialog.Builder dialogBuilder, String title, String[] items, int selected, DialogInterface.OnClickListener listener) {
        dialogBuilder.setTitle(title);
        dialogBuilder.setSingleChoiceItems(items, selected, listener);
        AlertDialog alert = dialogBuilder.create();
        alert.show();
    }

    public static void verify_num(Context context, Bundle dataIntent, int situation, boolean skippable, String prefSkippedString, String prefVerifiedString) {
        SharedPreferences prefs = context.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        boolean skipped = prefs.getBoolean(prefSkippedString, false);
        int varified_flag_pref = prefs.getInt(PreferencesStore.VARIFIED_FLAG_PREF_CONSTANT, 0);
        int email_verify_flag = prefs.getInt(PreferencesStore.VARIFIED_EMAIL_FLAG_PREF_CONSTANT, 0);
        int shallverfiy_flag_pref = prefs.getInt(PreferencesStore.SHALL_NUMBER_VERIFY_CONSTANT, 1);

        if (shallverfiy_flag_pref == 1 && skipped == false) {

            if (varified_flag_pref == 0) {
                Intent intent = new Intent();
                NumberClass.setContext(context);
                intent.putExtra(NumberParameters.SITUATION_CONTANT, situation);
                intent.putExtra(NumberParameters.SKIPPABLE_CONSTANT, skippable);
                intent.putExtra(NumberParameters.PREF_STRING_CONTANT, prefSkippedString);
                intent.putExtra(NumberParameters.PREF_VERIFIED_CONSTANT, prefVerifiedString);
                intent.putExtras(dataIntent);
                intent.setClass(context, NumberClass.class);
                if (!skippable)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            } else {
                Utils.verify_email(context, dataIntent, skippable, prefSkippedString, prefVerifiedString);

            }
        }
    }

    public static void verify_email(Context context, Bundle dataIntent, boolean skippable, String prefSkippedString, String prefVerifiedString) {
        int varified_flag = 0;

        Cursor m_cursor = context.getContentResolver().query(
                UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                varified_flag = m_cursor.getInt(4);
                if (varified_flag == 0) {
                    Log.d("verify", "verify");
                    Intent intent = new Intent();
                    EmailVerify.setContext(context);
                    intent.putExtras(dataIntent);
                    intent.putExtra(NumberParameters.PREF_VERIFIED_CONSTANT, prefVerifiedString);
                    intent.putExtra(NumberParameters.PREF_STRING_CONTANT, prefSkippedString);
                    intent.putExtra(NumberParameters.SKIPPABLE_CONSTANT, skippable);
                    intent.setClass(context, EmailVerify.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                } else {
                    SharedPreferences prefs = context.getSharedPreferences(
                            "technologies.angular.lazylad", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean(prefVerifiedString, true);
                    editor.commit();
                }
            }
        }
    }

    public static void ShowMessageBox(AlertDialog.Builder dialogBuilder, String title, String message) {
        Utils.ShowMessageBox(dialogBuilder, title, message, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    public static void ShowMessageBox(AlertDialog.Builder dialogBuilder, String title, String message, DialogInterface.OnClickListener listener) {
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Close", listener);

        if (!alertDialog.isShowing())
            alertDialog.show();
    }


    public static void TrustInvalidSslCertificates() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isCompatible(int apiLevel) {
        return android.os.Build.VERSION.SDK_INT >= apiLevel;
    }

    public static String strForSqlite(String str) {
        return str.replace("'", "''");
    }

    public interface Handler {
        public void execute(Object data);
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();
            Log.i("no of items", numberOfItems + "");
            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                float px = 300 * (listView.getResources().getDisplayMetrics().density);
                item.measure(View.MeasureSpec.makeMeasureSpec((int) px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            // Get padding
            int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + totalPadding;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }


    public static int getUserCode(Context context) {
        int m_userCode;
        Cursor m_cursor = context.getContentResolver().query(
                UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                m_userCode = m_cursor.getInt(1);


            } else
                m_userCode = 0;
        } else
            m_userCode = 0;//TODO error handling

        return m_userCode;
    }

    public static boolean isValidJson(String result) {
        boolean isValid = true;
        try {
            new JSONObject(result);
        } catch (JSONException ex) {
            try {
                new JSONArray(result);
            } catch (JSONException ex1) {
                isValid = false;
            }
        }
        return isValid;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static RestAdapter providesRestAdapter(String url) {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(120, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(120, TimeUnit.SECONDS);

        return new RestAdapter.Builder()
                .setEndpoint(url)
                .setClient(new OkClient(okHttpClient))
                .build();
    }

    public static long getEpochTimeByTimeSlot(String timeSlot) {
        if (timeSlot.contains("\n")) {
            timeSlot = timeSlot.replace("\n", " ");
        }
        String[] timeSplits = timeSlot.split(" ");
        //timeSplits[0] - Current date, timeSplits[4] - Expected Final Delivery Time, timeSplits[5] - AM or PM
        String finalTime = timeSplits[0] + " " + timeSplits[4] + " " + timeSplits[5];
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);

        long epochTime = -1;
        try {
            Date date = format.parse(finalTime);
            epochTime = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return epochTime;
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}