package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.ShoppingCartEntry;
import technologies.angular.lazylad.LazyContract.ServiceTypeCategoriesEntry;
import technologies.angular.lazylad.LazyContract.ItemDetailsEntry;
import technologies.angular.lazylad.LazyContract.SearchResultEntry;
import technologies.angular.lazylad.LazyContract.OrderDetailsEntry;

public class ItemsActivity extends ActionBarActivity {

    private static final String TAG = "ItemsActivity";
    private ViewPager m_viewPager;
    private Toolbar m_toolbar;
    private ItemsTabPagerAdapter m_itemsTabPagerAdapter;
    private SlidingUpPanelLayout mLayout;
    TextView m_totalCostTextView;
    TextView m_totalItemsTextView;
    private Button m_checkOutButton;

    double totalCost = 0.0;
    int numOfItemsInShoppingCart = 0;

    private ListView m_shoppingCartListView;
    private ShoppingCartAdapter m_shoppingCartAdapter;
    private ArrayList<ItemSnippet> m_shoppingCartItemsList;
    private ArrayList<ItemSnippet> m_shoppingCartItemsCheckOutList;

    public static ArrayList<String> m_itemsCategoryNames;
    public static ArrayList<Integer> m_itemsCategoryCode;

    private static String m_servProvCode;
    private static String m_stCode;
    private String prov_name;
    private AsyncTask m_async5;

    private SearchView searchView;
    public int m_images_available;

    public static String getM_stCode() {
        return m_stCode;
    }

    public static String getM_servProvCode() {
        return m_servProvCode;
    }

    private MoEHelper mHelper;
    int first_sc_code;
    int m_currentPage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_example);
        setFirstTab(0);
        onNewIntent(getIntent());
        m_currentPage = 0;


        SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        totalCost = Double.parseDouble(prefs.getString("totalCost", "0.0"));//m_variablesFromLastActivity[0];
        numOfItemsInShoppingCart = prefs.getInt("noOfItemsInShoppingCart", 0);
        m_servProvCode = prefs.getString("ServiceProviderCode", "Not Available");
        m_stCode = prefs.getString("ServTypeCode", "Not Available");
        m_images_available = prefs.getInt("images_available", 1);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        loadServiceCategoriesfromServer();

        setTracker();

        mHelper = new MoEHelper(this);

        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("St Code", m_stCode);
        builder.putAttrString("Sp Code", m_servProvCode);
        builder.putAttrString("Activity Name", this.getClass().getCanonicalName());
        MoEHelper.getInstance(ItemsActivity.this).trackEvent("Items List", builder.build());

    }

    private void loadServiceCategoriesfromServer() {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.ServiceCategoryRequestModel serviceCategoryRequestModel = requestModel.new ServiceCategoryRequestModel();
        serviceCategoryRequestModel.st_code = m_stCode;
        serviceCategoryRequestModel.sp_code = m_servProvCode;
        apiSuggestionsService.postServiceCategory(serviceCategoryRequestModel, new retrofit.Callback<APIResonseModel.ServiceCategoryResponseModel>() {

            @Override
            public void success(APIResonseModel.ServiceCategoryResponseModel serviceCategoryResponseModel, retrofit.client.Response response) {
                int success = serviceCategoryResponseModel.error;
                if (success == 1) {
                    int m_numberOfServiceCategory = serviceCategoryResponseModel.serviceCategoryArrayResponseModelArrayList.size();

                    getContentResolver().delete(
                            ServiceTypeCategoriesEntry.CONTENT_URI,
                            null,
                            null
                    );

                    Vector<ContentValues> cVVector = new Vector<ContentValues>(m_numberOfServiceCategory);

                    for (int i = 0; i < m_numberOfServiceCategory; i++) {
                        String stcode = serviceCategoryResponseModel.serviceCategoryArrayResponseModelArrayList.get(i).st_code;
                        String sccode = serviceCategoryResponseModel.serviceCategoryArrayResponseModelArrayList.get(i).sc_code;
                        String scname = serviceCategoryResponseModel.serviceCategoryArrayResponseModelArrayList.get(i).sc_name;

                        ContentValues stCatValues = new ContentValues();
                        stCatValues.put(ServiceTypeCategoriesEntry.COLUMN_ST_CODE, stcode);
                        stCatValues.put(ServiceTypeCategoriesEntry.COLUMN_SC_CODE, sccode);
                        stCatValues.put(ServiceTypeCategoriesEntry.COLUMN_SC_NAME, scname);

                        cVVector.add(stCatValues);
                    }

                    if (cVVector.size() > 0) {
                        ContentValues[] cvArray = new ContentValues[cVVector.size()];
                        cVVector.toArray(cvArray);
                        getContentResolver().bulkInsert(ServiceTypeCategoriesEntry.CONTENT_URI, cvArray);
                    }
                } else {
                    Toast.makeText(ItemsActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
                loadItemScreen();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
                loadItemScreen();
            }
        });
    }

    private void loadItemScreen() {

        m_shoppingCartItemsList = new ArrayList<ItemSnippet>();
        m_shoppingCartItemsCheckOutList = new ArrayList<ItemSnippet>();

        m_checkOutButton = (Button) findViewById(R.id.checkout_button);
        m_toolbar = (Toolbar) findViewById(R.id.items_toolbar);

        m_shoppingCartListView = (ListView) findViewById(R.id.shoping_cart_items_list_view);
        m_totalCostTextView = (TextView) findViewById(R.id.total_shopping_cost);
        m_totalItemsTextView = (TextView) findViewById(R.id.items_in_cart);

        String whereClause = ServiceTypeCategoriesEntry.COLUMN_ST_CODE + " = ? ";
        String[] selectionArgs = new String[]{m_stCode};
        Cursor cursor = getContentResolver().query(
                ServiceTypeCategoriesEntry.CONTENT_URI,
                null,
                whereClause,
                selectionArgs,
                null);

        m_itemsCategoryNames = null;
        m_itemsCategoryCode = null;
        if (cursor != null && cursor.moveToFirst()) {
            m_itemsCategoryNames = new ArrayList<String>(cursor.getCount());
            m_itemsCategoryCode = new ArrayList<Integer>(cursor.getCount());
            for (int i = 0; i < cursor.getCount(); i++) {
                m_itemsCategoryNames.add(cursor.getString(3));
                m_itemsCategoryCode.add(Integer.parseInt(cursor.getString(2)));
                cursor.moveToNext();
            }
        }
        if (cursor != null) cursor.close();

        getContentResolver().delete(
                ItemDetailsEntry.CONTENT_URI,
                null,
                null
        );
        m_viewPager = (ViewPager) findViewById(R.id.items_pager);

        m_itemsTabPagerAdapter = new ItemsTabPagerAdapter(getSupportFragmentManager(), m_itemsCategoryCode);
        m_viewPager.setAdapter(m_itemsTabPagerAdapter);
        if (m_currentPage != 0)
            m_viewPager.setCurrentItem(m_currentPage);
        else
            m_viewPager.setCurrentItem(getFirstTab());


        setSupportActionBar(m_toolbar);
        getSupportActionBar().setTitle("Shop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        tabs.setViewPager(m_viewPager);
        m_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                tabs.setViewPager(m_viewPager);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.shopping_cart_sliding_layout);
        mLayout.setPadding(0, Utils.getStatusBarHeight(this), 0, 0);
        mLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Cursor shopping_cart_cursor = getContentResolver().query(
                        ShoppingCartEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);

                if (shopping_cart_cursor != null && shopping_cart_cursor.moveToFirst()) {
                    if (shopping_cart_cursor.getCount() > 0) {
                        m_shoppingCartItemsList = new ArrayList<ItemSnippet>(shopping_cart_cursor.getCount());
                        for (int i = 0; i < shopping_cart_cursor.getCount(); i++) {
                            int itemsId = i + 1;
                            String m_itemCode = shopping_cart_cursor.getString(1);
                            String m_itemName = shopping_cart_cursor.getString(2);
                            int m_itemImgFlag = shopping_cart_cursor.getInt(3);
                            String m_itemImgAddress = shopping_cart_cursor.getString(4);
                            String m_itemUnit = shopping_cart_cursor.getString(5);
                            double m_itemCost = shopping_cart_cursor.getDouble(6);
                            String m_itemShortDesc = shopping_cart_cursor.getString(7);  //Standard Weight
                            String m_itemDesc = shopping_cart_cursor.getString(8);
                            String m_itemStatus = shopping_cart_cursor.getString(9);  //Available and Not Available
                            String m_itemServiceType = m_stCode;
                            int itemSelected = shopping_cart_cursor.getInt(10);
                            int itemQuantitySelected = shopping_cart_cursor.getInt(11);
                            String m_itemServiceTypeCategory = shopping_cart_cursor.getString(13);
                            String m_itemServiceProvider = shopping_cart_cursor.getString(14);
                            double m_itemOfferPrice = shopping_cart_cursor.getDouble(15);
                            int m_itemInOffer = shopping_cart_cursor.getInt(16);
                            Log.d("m_itemOfferPrice1",m_itemOfferPrice+"");

                            String[] mProjection = {OrderDetailsEntry.COLUMN_SERV_PROV_NAME};
                            String whereClause = OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
                            String[] selectionArgs = new String[]{m_itemServiceProvider};
                            Cursor cursor1 = getContentResolver().query(
                                    OrderDetailsEntry.CONTENT_URI,
                                    mProjection,
                                    whereClause,
                                    selectionArgs,
                                    null);

                            if (cursor1.moveToFirst()) {
                                String sellerName = cursor1.getString(cursor1.getColumnIndex("serv_prov_name"));
                                ItemSnippet currenOrderObj = new ItemSnippet(itemsId, m_itemCode, m_itemName, m_itemImgFlag, m_itemImgAddress,
                                        m_itemUnit, m_itemCost, m_itemShortDesc, m_itemDesc, m_itemStatus, itemSelected, itemQuantitySelected,
                                        m_itemServiceType, m_itemServiceTypeCategory, m_itemServiceProvider, sellerName,m_itemOfferPrice,m_itemInOffer);
                                m_shoppingCartItemsList.add(currenOrderObj);
                            }
                            shopping_cart_cursor.moveToNext();
                        }
                    }
                }
                if (shopping_cart_cursor != null)
                    shopping_cart_cursor.close();

                Log.i(TAG, "onPanelSlide, offset " + slideOffset);

            }

            @Override
            public void onPanelExpanded(View panel) {

                m_shoppingCartAdapter = new ShoppingCartAdapter(ItemsActivity.this,ItemsActivity.this.getApplicationContext(), m_shoppingCartItemsList);
                m_shoppingCartListView.setAdapter((ListAdapter) m_shoppingCartAdapter);
                m_currentPage=m_viewPager.getCurrentItem();
                Log.d("m_currentPage",m_currentPage+"");
                Log.i(TAG, "onPanelExpanded");
            }

            @Override
            public void onPanelCollapsed(View panel) {
                onResume();
            }

            @Override
            public void onPanelAnchored(View panel) {

            }

            @Override
            public void onPanelHidden(View panel) {

            }
        });

        m_checkOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Tracker cart = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
                cart.send(new HitBuilders.EventBuilder()
                        .setCategory(Double.toString(totalCost))
                        .setAction(Integer.toString(numOfItemsInShoppingCart))
                        .build());

                startCheckOutActivity();
            }
        });

        setShoppingCartList();
    }


    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.ItemsActivity");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void startCheckOutActivity() {
        Cursor m_cursor = getContentResolver().query(
                ShoppingCartEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() > 0) {
                m_shoppingCartItemsCheckOutList = new ArrayList<ItemSnippet>(m_cursor.getCount());
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    int itemsId = i + 1;
                    String m_itemCode = m_cursor.getString(1);
                    String m_itemName = m_cursor.getString(2);
                    int m_itemImgFlag = m_cursor.getInt(3);
                    String m_itemImgAddress = m_cursor.getString(4);
                    String m_itemUnit = m_cursor.getString(5);
                    double m_itemCost = m_cursor.getDouble(6);
                    String m_itemShortDesc = m_cursor.getString(7);  //Standard Weight
                    String m_itemDesc = m_cursor.getString(8);
                    String m_itemStatus = m_cursor.getString(9);  //Available and Not Available
                    String m_itemServiceType = m_stCode;
                    int itemSelected = m_cursor.getInt(10);
                    int itemQuantitySelected = m_cursor.getInt(11);
                    String m_itemServiceTypeCategory = m_cursor.getString(13);
                    String m_itemServiceProvider = m_cursor.getString(14);
                    double m_itemOfferPrice = m_cursor.getDouble(15);
                    int m_itemInOffer = m_cursor.getInt(16);

                    String[] mProjection = {OrderDetailsEntry.COLUMN_SERV_PROV_NAME};
                    String whereClause = OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
                    String[] selectionArgs = new String[]{m_itemServiceProvider};
                    Cursor cursor2 = getContentResolver().query(
                            OrderDetailsEntry.CONTENT_URI,
                            mProjection,
                            whereClause,
                            selectionArgs,
                            null);

                    if (cursor2.moveToFirst()) {
                        String sellerName = cursor2.getString(cursor2.getColumnIndex("serv_prov_name"));

                        ItemSnippet currenOrderObj = new ItemSnippet(itemsId, m_itemCode, m_itemName, m_itemImgFlag, m_itemImgAddress,
                                m_itemUnit, m_itemCost, m_itemShortDesc, m_itemDesc, m_itemStatus, itemSelected, itemQuantitySelected,
                                m_itemServiceType, m_itemServiceTypeCategory, m_itemServiceProvider, sellerName,m_itemOfferPrice,m_itemInOffer);
                        m_shoppingCartItemsCheckOutList.add(currenOrderObj);
                    }
                    m_cursor.moveToNext();
                }
                Intent intent = new Intent(ItemsActivity.this, AddressActivity.class);
                intent.putExtra("ItemsDetails", m_shoppingCartItemsCheckOutList);
                intent.putExtra("sourceFlag", 1);

                Bundle b = new Bundle();
                b.putDouble("total_amount", totalCost);
                intent.putExtras(b);
                ItemsActivity.this.startActivity(intent);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        else {
            ItemsActivity.super.onBackPressed();
            overridePendingTransition(R.anim.anim_zoom_out_back, R.anim.anim_slideup_back);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        m_checkOutButton = (Button) findViewById(R.id.checkout_button);

        m_viewPager = (ViewPager) findViewById(R.id.items_pager);
        m_toolbar = (Toolbar) findViewById(R.id.items_toolbar);

        m_shoppingCartListView = (ListView) findViewById(R.id.shoping_cart_items_list_view);
        m_totalCostTextView = (TextView) findViewById(R.id.total_shopping_cost);
        m_totalItemsTextView = (TextView) findViewById(R.id.items_in_cart);

        SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        totalCost = Double.parseDouble(prefs.getString("totalCost", "0.0"));//m_variablesFromLastActivity[0];
        numOfItemsInShoppingCart = prefs.getInt("noOfItemsInShoppingCart", 0);

        setShoppingCartList();
        String whereClause = ServiceTypeCategoriesEntry.COLUMN_ST_CODE + " = ? ";
        String[] selectionArgs = new String[]{m_stCode};
        Cursor m_cursor = getContentResolver().query(
                ServiceTypeCategoriesEntry.CONTENT_URI,
                null,
                whereClause,
                selectionArgs,
                null);

        m_itemsCategoryNames = null;
        m_itemsCategoryCode = null;
        if (m_cursor != null && m_cursor.moveToFirst()) {
            m_itemsCategoryNames = new ArrayList<String>(m_cursor.getCount());
            m_itemsCategoryCode = new ArrayList<Integer>(m_cursor.getCount());
            for (int i = 0; i < m_cursor.getCount(); i++) {
                m_itemsCategoryNames.add(m_cursor.getString(3));
                m_itemsCategoryCode.add(Integer.parseInt(m_cursor.getString(2)));
                m_cursor.moveToNext();
            }
        }

        m_itemsTabPagerAdapter = new ItemsTabPagerAdapter(getSupportFragmentManager(), m_itemsCategoryCode);

        m_viewPager.setAdapter(m_itemsTabPagerAdapter);


        if (m_currentPage != 0)
            m_viewPager.setCurrentItem(m_currentPage);
        else
            m_viewPager.setCurrentItem(getFirstTab());



        setSupportActionBar(m_toolbar);
        getSupportActionBar().setTitle("Shop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        tabs.setViewPager(m_viewPager);
        m_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                tabs.setViewPager(m_viewPager);

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.shopping_cart_sliding_layout);
        mLayout.setPadding(0, Utils.getStatusBarHeight(this), 0, 0);
        mHelper.onResume(this);
    }

    public void setShoppingCartList() {

        m_totalCostTextView.setText(Double.toString(totalCost));
        m_totalItemsTextView.setText(Integer.toString(numOfItemsInShoppingCart));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_results, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView =
                (SearchView) menu.findItem(R.id.search_items).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length() > 2) {
                    loadData(s);
                }
                return true;
            }
        });
        return true;
    }

    private void loadData(final String searchText) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.searchFeeds(m_servProvCode, m_stCode, "1", searchText, "0", new Callback<ItemModel>() {

            @Override
            public void success(ItemModel itemResults, retrofit.client.Response response) {
                ArrayList<ItemSnippet> itemsList = new ArrayList<ItemSnippet>();
                itemsList = itemResults.items_service_providers;
                m_async5 = new StoreItemsInSqllite(searchText).execute(itemsList);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("RetrofitError", error.toString());
            }
        });
    }

    private class StoreItemsInSqllite extends AsyncTask<ArrayList<ItemSnippet>, Void, Void> {

        String searchText;

        public StoreItemsInSqllite(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected Void doInBackground(ArrayList<ItemSnippet>... params) {
            ArrayList<ItemSnippet> itemsList = params[0];
            storeItemsinSqliteForQuery(itemsList, searchText);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            loadQueryResultAgain();
        }
    }

    private void loadQueryResultAgain() {
        if (null != searchView) {
            View searchTextView = searchView.findViewById(R.id.search_src_text);
            if (null != searchTextView)
                searchTextView.performClick();
        }
    }

    public void storeItemsinSqliteForQuery(ArrayList<ItemSnippet> itemsList, String searchText) {
        getContentResolver().delete(
                SearchResultEntry.CONTENT_URI,
                null,
                null
        );

        Vector<ContentValues> cVVector = new Vector<ContentValues>(itemsList.size());

        for (int i = 0; i < itemsList.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(SearchResultEntry.COLUMN_ITEM_CODE, itemsList.get(i).m_itemCode);
            values.put(SearchResultEntry.COLUMN_ITEM_NAME, itemsList.get(i).m_itemName);
            values.put(SearchResultEntry.COLUMN_ITEM_IMG_FLAG, itemsList.get(i).m_itemImgFlag);
            values.put(SearchResultEntry.COLUMN_ITEM_IMG_ADDRESS, itemsList.get(i).m_itemImgAddress);
            values.put(SearchResultEntry.COLUMN_ITEM_UNIT, itemsList.get(i).m_itemUnit);
            values.put(SearchResultEntry.COLUMN_ITEM_COST, itemsList.get(i).m_itemCost);
            values.put(SearchResultEntry.COLUMN_ITEM_SHORT_DESC, itemsList.get(i).m_itemShortDesc);
            values.put(SearchResultEntry.COLUMN_ITEM_DESC, itemsList.get(i).m_itemDesc);
            values.put(SearchResultEntry.COLUMN_ITEM_STATUS, itemsList.get(i).m_itemStatus);
            values.put(SearchResultEntry.COLUMN_ITEM_SELECTED, 0);
            values.put(SearchResultEntry.COLUMN_ITEM_QUANTITY_SELECTED, 0);
            values.put(SearchResultEntry.COLUMN_ITEM_ST_CODE, itemsList.get(i).m_itemServiceType);
            values.put(SearchResultEntry.COLUMN_ITEM_SC_CODE, itemsList.get(i).m_itemServiceCategory);
            values.put(SearchResultEntry.COLUMN_ITEM_MRP, itemsList.get(i).m_itemMrp);
            values.put(SearchResultEntry.COLUMN_SEARCH_STRING, searchText);
            values.put(SearchResultEntry.COLUMN_ITEM_OFFER_PRICE, itemsList.get(i).m_itemOfferPrice);
            values.put(SearchResultEntry.COLUMN_ITEM_IN_OFFER, itemsList.get(i).m_itemInOffer);


            cVVector.add(values);
        }

        if (cVVector.size() > 0) {
            ContentValues[] cvArray = new ContentValues[cVVector.size()];
            cVVector.toArray(cvArray);
            getContentResolver().bulkInsert(SearchResultEntry.CONTENT_URI, cvArray);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String action = intent.getAction();
        String data = intent.getDataString();

        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        if (Intent.ACTION_VIEW.equals(action) && data != null) {
            Log.d("dataURL", data);
            try {

                query_pairs = splitQuery(data);
                if (query_pairs != null && !query_pairs.isEmpty()) {
                    String sp_code = query_pairs.get("sp_code");
                    String st_code = query_pairs.get("st_code");
                    String ServiceProviderDeliveryTime = query_pairs.get("sp_del_time");
                    String ServiceProviderMinimumOrder = query_pairs.get("sp_min_order");
                    String ServiceProviderName = query_pairs.get("sp_name");
                    String time_interval = query_pairs.get("time_interval");
                    String ServiceProviderOpenTime = query_pairs.get("sp_open_time");
                    String ServiceProviderCloseTime = query_pairs.get("sp_close_time");
                    setFirstTab(Integer.parseInt(query_pairs.get("first_sc_code")));


                    SharedPreferences prefs = getSharedPreferences(
                            "technologies.angular.lazylad", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("ServTypeCode", st_code);
                    editor.putString("ServiceProviderCode", sp_code);
                    editor.putString("ServiceProviderDeliveryTime", ServiceProviderDeliveryTime);
                    editor.putString("ServiceProviderMinimumOrder", ServiceProviderMinimumOrder);
                    editor.putString("ServiceProviderName", ServiceProviderName);
                    editor.putInt("time_interval", Integer.parseInt(time_interval));
                    editor.putInt("shop_open_time", Integer.parseInt(ServiceProviderOpenTime));
                    editor.putInt("shop_close_time", Integer.parseInt(ServiceProviderCloseTime));
                    editor.commit();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            //  mHelper.onNewIntent(this, intent);
        }
    }


    public static Map<String, String> splitQuery(String url) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = url.substring(url.indexOf("?") + 1);
        String[] pairs = query.split("/");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    private void setFirstTab(int i) {
        first_sc_code = i;
    }

    private int getFirstTab() {
        return first_sc_code;
    }


}





