package technologies.angular.lazylad;

/**
 * Created by Amud on 09/09/15.
 */

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;

import java.util.ArrayList;

import ru.noties.scrollable.CanScrollVerticallyDelegate;

public class MainActivityFragment extends Fragment implements CanScrollVerticallyDelegate {
    private String str;
    TextView txtview;
    private ServiceTypeAdapter m_servTypeAdapter;
    private GridView m_servTypeGridView;
    ImageView dotImage;
    ImageView dotImage2;
    int position;
    View mainView;
    private ArrayList<ServiceTypeSnippet> m_servTypeListDetails;

    public MainActivityFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        position = getArguments().getInt("position");
        m_servTypeListDetails = getArguments().getParcelableArrayList("servTypeListDetails");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mainscreen_fragment, container, false);

        m_servTypeAdapter = new ServiceTypeAdapter(getActivity(), m_servTypeListDetails);
        m_servTypeGridView = (GridView) v.findViewById(R.id.service_type_gridview);
        m_servTypeGridView.setAdapter((ListAdapter) m_servTypeAdapter);
        return v;
    }

    public CharSequence getTitle() {
        return "Main";
    }

    @Override
    public boolean canScrollVertically(int direction) {
        return m_servTypeGridView != null && m_servTypeGridView.canScrollVertically(direction);
    }
}
