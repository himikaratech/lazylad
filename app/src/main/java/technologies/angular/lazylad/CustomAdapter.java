package technologies.angular.lazylad;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.moe.pushlibrary.providers.MoEDataContract;
import com.moengage.addon.inbox.InboxManager;
import com.moengage.addon.inbox.InboxUtils;

/**
 * Created by sakshigupta on 15/12/15.
 */
public class CustomAdapter extends InboxManager.InboxAdapter<CustomHolder> {
    /**
         * Makes a new view to hold the data pointed to by cursor.
         *
         * @param context Interface to application's global information
         * @param cursor The cursor from which to get the data. The cursor is already moved to the
         * correct position.
         * @param parent The parent to which the new view is attached to
         * @return the new inflated view which will be used by the adapter
         */
        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent,
                            LayoutInflater layoutInflater) {
            return layoutInflater.inflate(R.layout.item_custom, parent, false);
        }

        /**
         * Bind an existing view to the data pointed to by cursor
         *
         * @param holder The ViewHolder which should be updated to represent the contents of the
         * item at the given position in the data set.
         * @param context Interface to application's global information
         * @param cursor The cursor from which to get the data. The cursor is already moved to the
         * correct position.
         * @return The binded View which is ready to be shown
         */
        @Override public void bindData(CustomHolder holder, Context context, Cursor cursor) {
            String details = cursor.getString(MoEDataContract.MessageEntity.COLUMN_INDEX_MSG_DETAILS);
            //validity long millis = cursor.getLong(MoEDataContract.MessageEntity.COLUMN_INDEX_GTIME)
            holder.timeStamp.setText(InboxUtils.getTimeStamp(details, "dd MMM"));
            holder.title.setText(InboxUtils.getTitle(details));
            holder.message.setText(InboxUtils.getMessage(details));
           }

        /**
         * Callback method to be invoked when an item in this AdapterView has been clicked.
         *
         * @param view The view within the AdapterView that was clicked (this will be a view provided
         * by the adapter)
         * @param context Application Context
         * @return true if Click is being overriden, false otherwise
         */
        @Override public boolean onItemClick(View view, Context context) {
            return false;
        }

        /**
         * Return the ViewHolder from this method which will be used to reduce Hierarchy lookup and also
         * be used to piggy back data required when view is clicked
         *
         * @param convertView The view which is used by the adapter
         * @return The ViewHolder which should be updated to represent the contents of the
         * item at the given position in the data set.
         */
        @Override public CustomHolder getViewHolder(View convertView) {
            CustomHolder holder = (CustomHolder) convertView.getTag();
            if( null == holder ){
                holder = new CustomHolder();
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.message = (TextView) convertView.findViewById(R.id.message);
                holder.timeStamp = (TextView) convertView.findViewById(R.id.date);
                convertView.setTag(holder);
            }
            return holder;
        }


        /**
         * Shows a Toast message
         *
         * @param message
         *            The message which needs to be shown
         * @param mContext
         *            An instance of the application Context
         */
        public static void showToast(String message, Context mContext) {
            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        }
}
