package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

/**
 * Created by Amud on 20/08/15.
 */
public class CityActivity extends ActionBarActivity {

    private ArrayList<CitySnippet> m_cityList;
    private ArrayList<AreaSnippet> m_areasList;

    private String m_areaCodeSelected;
    private String m_cityCodeSelected;
    private String m_city_image_addSelected;
    private int m_city_image_flagSelected;

    private String m_old_areaCodeSelected;
    private String m_old_cityCodeSelected;

    private String m_cityNameSelected;
    private String m_areaNameSelected;

    private Spinner m_cityListSpinner;
    private Spinner m_areaListSpinner;

    private ArrayList<String> m_cityNamesList;
    private ArrayList<String> m_areaImageAddress;
    private ArrayList<String> m_areaNamesList;
    private List m_servTypeListDetails2 = new ArrayList<String>();


    private SharedPreferences prefs_location;

    private Button m_startMainActivityButton;
    Boolean isInternetPresent = true;

    ConnectionDetector cd;
    //private ServiceTypeAdapter m_servTypeAdapter;
    private ArrayList<ServiceTypeSnippet> m_servTypeListDetailsFromServer;
    private GridView m_servTypeGridView;
    private ServiceTypeAdapter m_servTypeAdapter;
    public static int flag = 0;
    public static final String TAG = "MyTag";
    RecyclerView recyclerView;
    private Toolbar toolbar;
    FrameLayout overLayView;
    Bundle bundle;
    boolean loadedFlag;
    SharedPreferences prefs;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.city_layout);
        bundle = new Bundle();
        loadedFlag = false;
        prefs = getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Select City");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        recyclerView = (RecyclerView) findViewById(R.id.city_list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(CityActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        overLayView = (FrameLayout) findViewById(R.id.overLayView);

        prefs_location = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        m_old_areaCodeSelected = prefs_location.getString(PreferencesStore.AREA_SELECTED_CONSTANT, "Not_Available");
        m_old_cityCodeSelected = prefs_location.getString(PreferencesStore.CITY_SELECTED_CONSTANT, "Not Available");

        cd = new ConnectionDetector(getApplicationContext());
        setTracker();
        mHelper = new MoEHelper(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isInternetPresent) {
            if (!loadedFlag)
                loadCities();
        } else {
            Toast.makeText(CityActivity.this, "Sorry , Internet connection is not avaiable", Toast.LENGTH_SHORT).show();
        }
        mHelper.onResume(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_area_ncity_selection, menu);
        return true;
    }

    private void loadCities() {

        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getCitiesAPICall(new retrofit.Callback<APIResonseModel.GetCityResponseModel>() {

            @Override
            public void success(APIResonseModel.GetCityResponseModel getCityResponseModel, retrofit.client.Response response) {

                int success = getCityResponseModel.error;
                if (success == 1) {
                    loadedFlag = true;
                    m_cityList = new ArrayList<CitySnippet>();
                    m_cityList = getCityResponseModel.cities;
                    recyclerView.setAdapter(new CityAdapter(m_cityList, R.layout.city_list_layout, CityActivity.this, 0));
                    progressDialog.dismiss();

                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivityWithBanner.class);
        startActivity(intent);
        // overridePendingTransition(R.anim.anim_zoom_out_back,R.anim.anim_in);
        finish();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.ItemsActivity");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
