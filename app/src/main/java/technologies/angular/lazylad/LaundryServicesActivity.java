package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazylad.Snippets.LaundryServiceSnippet;
import technologies.angular.lazylad.Snippets.LaundryServicesRequestedSnippet;
import technologies.angular.lazylad.adaptors.LaundryServiceListAdaptor;
import technologies.angular.lazylad.common.*;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

public class LaundryServicesActivity extends AppCompatActivity implements LaundryServiceListAdaptor.OnServiceListItemListener {

    private static final String TAG = LaundryServicesActivity.class.getSimpleName();
    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @Bind(R.id.banner_image)
    ImageView mBannerImage;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.shopping_cart_image)
    ImageButton mShoppingCartImageButton;
    @Bind(R.id.laundry_services_recycler_view)
    RecyclerView mServicesRecyclerView;
    @Bind(R.id.minimum_order_textview)
    TextView mMinimumOrderTextView;
    @Bind(R.id.powered_by_textview)
    TextView mPoweredByTextView;

    public static final String SERVICE_TYPE_CODE_TAG = "SERVICE_TYPE_CODE_TAG";

    ArrayList<LaundryServiceSnippet> mServicesList;
    private SharedPreferences prefs;

    private ArrayList<LaundryServicesRequestedSnippet> mServicesRequestedList;
    private int mServiceTypeCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laundry_services);
        ButterKnife.bind(this);

        mServiceTypeCode = getIntent().getIntExtra(SERVICE_TYPE_CODE_TAG, 0);
        mShoppingCartImageButton.setVisibility(View.GONE);
        setupCollapsingToolbarLayout();

        prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        getLaundryServices();
    }

    private void setupCollapsingToolbarLayout() {
        if (mCollapsingToolbarLayout != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            mCollapsingToolbarLayout.setTitleEnabled(true);
            mCollapsingToolbarLayout.setTitle("Laundry");
            mCollapsingToolbarLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.backgroundcolor));
            ColorDrawable colorDrawable = new ColorDrawable();
            colorDrawable.setColor(ContextCompat.getColor(this, R.color.opaque));
            mCollapsingToolbarLayout.setContentScrim(colorDrawable);
        }
    }

    public void onClicked(View view) {
        Intent intent = null;

        PayloadBuilder builder = new PayloadBuilder();
        switch (view.getId()) {
            case R.id.checkout:
                if (mServicesRequestedList == null || mServicesRequestedList.isEmpty()) {
                    Toast.makeText(this, getString(R.string.choose_a_service), Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = prefs.edit();
                    Gson gson = new Gson();
                    String servicesRequestedJson = gson.toJson(mServicesRequestedList);
                    editor.putString(PreferencesStore.PREF_SERVICES_REQUESTED, servicesRequestedJson);
                    editor.putLong(PreferencesStore.PREF_PROCESSING_TIME, maxProcessingTime());
                    editor.apply();

                    // MoEngage Tracking
                    builder.putAttrString("Services Requested ", servicesRequestedJson);
                    MoEHelper.getInstance(this).trackEvent("Checkout Button Pressed", builder.build());

                    intent = new Intent(this, AddressActivity.class);
                    intent.putExtra("sourceFlag", Constants.LAUNDRY_ACTIVITY_CODE);
                    startActivity(intent);
                }

                break;
            case R.id.rate_list_button:
                if (Utils.isNetworkAvailable(this)) {
                    // MoEngage Tracking
                    builder.putAttrString("Rate List for user", Utils.getUserCode(this) + "");
                    MoEHelper.getInstance(this).trackEvent("Rate List Button Pressed", builder.build());

                    intent = new Intent(this, RateListActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    private void getLaundryServices() {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = Utils.providesRestAdapter(Constants.WEB_SERVICE_URL);
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getLaunderyServices(Utils.getUserCode(this), new Callback<APIResonseModel.LaunderServiceDetailsResponseModel>() {
            @Override
            public void success(APIResonseModel.LaunderServiceDetailsResponseModel launderServiceDetailsResponseModel, Response response) {
                boolean success = launderServiceDetailsResponseModel.success;
                if (success) {
                    mServicesList = launderServiceDetailsResponseModel.laundryServiceList;
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(PreferencesStore.PREF_SHOP_OPEN_TIME, mServicesList.get(0).open_time);
                    editor.putInt(PreferencesStore.PREF_SHOP_CLOSE_TIME, mServicesList.get(0).close_time);
                    editor.putInt(PreferencesStore.PREF_TIME_INTERVAL, mServicesList.get(0).interval);
                    editor.apply();
                    progressDialog.dismiss();
                    setUpUi();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(LaundryServicesActivity.this, getString(R.string.server_error_occured), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, error.getMessage());
                progressDialog.dismiss();
                Toast.makeText(LaundryServicesActivity.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpUi() {
        mServicesRequestedList = new ArrayList<>(mServicesList.size());

        setPoweredByLogo();
        LaundryServiceListAdaptor serviceListAdaptor = new LaundryServiceListAdaptor(this, mServicesList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mServicesRecyclerView.setLayoutManager(layoutManager);
        mServicesRecyclerView.setHasFixedSize(true);
        mServicesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mServicesRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        mServicesRecyclerView.setAdapter(serviceListAdaptor);

        //TODO - Change value if it comes from server
        // mMinimumOrderTextView.setText(String.format(getString(R.string.minimum_order), 200));
    }

    private long maxProcessingTime() {
        long maxProcessingTime = 0;
        for (LaundryServicesRequestedSnippet serviceItem : mServicesRequestedList) {
            if (maxProcessingTime < serviceItem.getGap_time()) {
                maxProcessingTime = serviceItem.getGap_time();
            }
        }
        return maxProcessingTime;
    }

    private LaundryServicesRequestedSnippet getServiceRequestByServiceCode(String serviceCode) {
        LaundryServicesRequestedSnippet serviceRequestedByCode = null;
        for (LaundryServicesRequestedSnippet service : mServicesRequestedList) {
            if (service.getService_code().equals(serviceCode)) {
                serviceRequestedByCode = service;
                break;
            }
        }
        return serviceRequestedByCode;
    }

    @Override
    public void setCheckedState(boolean isChecked, LaundryServiceSnippet service) {
        if (isChecked) {
            LaundryServicesRequestedSnippet servicesRequested = new LaundryServicesRequestedSnippet(service.st_code, service.service_code, service.gap_time);
            mServicesRequestedList.add(servicesRequested);
        } else {
            mServicesRequestedList.remove(getServiceRequestByServiceCode(service.service_code));
        }
    }

    private void setPoweredByLogo() {
        Drawable logo;
        if (mServiceTypeCode == 14) {
            logo = ResourcesCompat.getDrawable(getResources(), R.drawable.pml_logo, null);
            mPoweredByTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, logo, null);
        }
    }
}
