package technologies.angular.lazylad;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by Saurabh on 07/03/15.
 */
public class ItemsTabPagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<Integer> m_itemsCategoryCode;

    public ItemsTabPagerAdapter(FragmentManager fragmentManager, ArrayList<Integer> itemsCategoryCode) {
        super(fragmentManager);
        m_itemsCategoryCode = itemsCategoryCode;
    }

    @Override
    public Fragment getItem(int position) {

        if (null != m_itemsCategoryCode) {
            int sc_code = m_itemsCategoryCode.get(position);
            return ItemsCategoryFragment.newInstance(sc_code);
        }
        return null;
    }

    @Override
    public int getCount() {
        if (null != ItemsActivity.m_itemsCategoryNames)
            return ItemsActivity.m_itemsCategoryNames.size();
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return ItemsActivity.m_itemsCategoryNames.get(position);
    }
}