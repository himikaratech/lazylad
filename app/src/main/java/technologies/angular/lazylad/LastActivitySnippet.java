package technologies.angular.lazylad;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sakshigupta on 28/10/15.
 */
public class LastActivitySnippet implements Parcelable {

    public String sp_code;
    public String order_code;
    public double tot_amount;

    public LastActivitySnippet() {
    }

    public LastActivitySnippet(String servProvCode, String orderCode, double amountPayable, int delCharge) {
        sp_code = servProvCode;
        order_code = orderCode;
        tot_amount = amountPayable;
    }

    public LastActivitySnippet(Parcel in) {

        sp_code = in.readString();
        order_code = in.readString();
        tot_amount = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sp_code);
        dest.writeString(order_code);
        dest.writeDouble(tot_amount);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public LastActivitySnippet createFromParcel(Parcel in) {
            return new LastActivitySnippet(in);
        }

        public LastActivitySnippet[] newArray(int size) {
            return new LastActivitySnippet[size];
        }
    };
}
