package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

public class PaymentRedirectionActivity extends AppCompatActivity {

    private static final String TAG = PaymentRedirectionActivity.class.getSimpleName();

    private ProgressDialog mProgressDialog;

    public static final String TRANSACTION_ID_TAG = "TRANSACTION_ID_TAG";
    private String mTransId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_redirection);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setPadding(0, Utils.getStatusBarHeight(this), 0, 0);

        Intent intent = getIntent();
        mTransId = intent.getStringExtra(TRANSACTION_ID_TAG);

        mProgressDialog = new ProgressDialog(this, R.style.MyTheme);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        mProgressDialog.setMessage("Please wait...Do not press back button");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        getOrderDetails();


    }

    private void getOrderDetails(){
        RestAdapter restAdapter = Utils.providesRestAdapter("http://54.169.62.100");
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);

        APIRequestModel requestModel = new APIRequestModel();
        APIRequestModel.PaymentRequestModel paymentRequestModel = requestModel.new PaymentRequestModel();
        paymentRequestModel.trans_id = mTransId;

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getOrderDetailsResponse(paymentRequestModel, new Callback<APIResonseModel.SendOrderDetailsResponseModel>() {
            @Override
            public void success(APIResonseModel.SendOrderDetailsResponseModel sendOrderDetailsResponseModel, Response response) {
                boolean success = sendOrderDetailsResponseModel.success;
                ArrayList<LastActivitySnippet> postOrderArrayList = null;
                Log.i("success", success + "");
                if (success) {
                    double totalAmount = sendOrderDetailsResponseModel.total_amount;
                    Log.i(TAG, "Total amount " + totalAmount + "");
                    postOrderArrayList = sendOrderDetailsResponseModel.postOrderResponseModelArrayList;
                    Log.i(TAG, "Total list on success" + postOrderArrayList + "");
                    mProgressDialog.dismiss();
                    Intent intent = new Intent(PaymentRedirectionActivity.this, LastActivity.class);
                    intent.putParcelableArrayListExtra(LastActivity.FINAL_LIST_OF_ORDERS_TAG, postOrderArrayList);
                    intent.putExtra(LastActivity.TOTAL_AMOUNT_OF_ORDERS_TAG, totalAmount);
                    intent.putExtra(LastActivity.CALLING_ACTIVITY_TAG, 0);
                    startActivity(intent);
                    finish();
                }else{
                    mProgressDialog.dismiss();
                    Toast.makeText(PaymentRedirectionActivity.this, "Some server error occurred", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mProgressDialog.dismiss();
                Toast.makeText(PaymentRedirectionActivity.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }


}
