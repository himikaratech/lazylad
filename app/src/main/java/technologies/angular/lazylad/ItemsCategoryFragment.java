package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Vector;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.ItemDetailsEntry;
import technologies.angular.lazylad.LazyContract.SearchResultEntry;

/**
 * Created by Saurabh on 31/03/15.
 */
public class ItemsCategoryFragment extends Fragment {

    private String m_stCode;
    private String m_scCode;
    private String m_spCode;

    private ListView m_itemDetailsListView;
    private ArrayList<ItemSnippet> m_itemsList = null;
    private ItemsCategoryAdapter m_itemsDetailAdapter;

    private ProgressDialog progressDialog;

    private boolean doneLoading = true;
    private boolean shallLoad = true;

    double totalCost;
    int numOfItemsInShoppingCart;

    private int m_images_available;

    private AsyncTask m_async;

    public static ItemsCategoryFragment newInstance(int scCode) {
        ItemsCategoryFragment fragmentFirst = new ItemsCategoryFragment();
        Bundle args = new Bundle();
        args.putString("scCode", Integer.toString(scCode));
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.items_fragment, container, false);

        m_images_available = ((ItemsActivity) getActivity()).m_images_available;
        m_stCode = ItemsActivity.getM_stCode();
        m_spCode = ItemsActivity.getM_servProvCode();
        m_scCode = getArguments().getString("scCode");

        Log.i("Fragment", "Created " + m_scCode);

        SharedPreferences prefs = getActivity().getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        totalCost = Double.parseDouble(prefs.getString("totalCost", "0.0"));//m_variablesFromLastActivity[0];
        numOfItemsInShoppingCart = prefs.getInt("noOfItemsInShoppingCart", 0);

        m_itemDetailsListView = (ListView) rootView.findViewById(R.id.items_fg_details_list_view);

        m_itemsList = null;

        m_itemDetailsListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            int visibleThreshold = 1;
            int visibleItemCount;
            int totalItemCount;
            int firstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    if (shallLoad && doneLoading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItem + visibleThreshold)) {
                        doneLoading = false;
                        getItemsFromServerLimited();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.firstVisibleItem = firstVisibleItem;
                this.visibleItemCount = visibleItemCount;
                this.totalItemCount = totalItemCount;
            }
        });

        getItemsSnippetListReady();

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (m_async != null && m_async.getStatus() != AsyncTask.Status.FINISHED) {
            Log.i("thread", "interrupted");
            m_async.cancel(true);

        }
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private void getItemsSnippetListReady() {

        ArrayList<ItemSnippet> itemsList = new ArrayList<ItemSnippet>();
        String previous_item_code;
        if (null == m_itemsList || m_itemsList.isEmpty())
            previous_item_code = "0";
        else
            previous_item_code = m_itemsList.get(m_itemsList.size() - 1).m_itemCode;

        final String sSearchSelection =
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_ST_CODE + " = ? " + " and " +
                        SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_SC_CODE + " = ? " +
                        "and CAST( " + SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_CODE + " as INTEGER ) > ? ";

        Cursor m_cursor = getActivity().getContentResolver().query(
                SearchResultEntry.buildSearchResultItemsUri(1, m_spCode),
                null,
                sSearchSelection,
                new String[]{m_stCode, m_scCode, previous_item_code},
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() > 0) {

                for (int i = 0; i < m_cursor.getCount(); i++) {
                    int itemsId = i + 1;
                    String m_itemCode = m_cursor.getString(1);
                    String m_itemName = m_cursor.getString(2);
                    int m_itemImgFlag = m_cursor.getInt(3);
                    String m_itemImgAddress = m_cursor.getString(4);
                    String m_itemUnit = m_cursor.getString(5);
                    double m_itemCost = m_cursor.getDouble(6);
                    String m_itemShortDesc = m_cursor.getString(7);  //Standard Weight
                    String m_itemDesc = m_cursor.getString(8);
                    String m_itemStatus = m_cursor.getString(9);  //Available and Not Available

                    String m_itemServiceType = m_stCode;
                    String m_itemServiceTypeCategory = m_scCode;
                    String m_itemServiceProvider = m_spCode;

                    int itemSelected = m_cursor.getInt(10);
                    int itemQuantitySelected = m_cursor.getInt(11);
                    double itemMrp = m_cursor.getDouble(14);
                    double itemOfferCost = m_cursor.getDouble(15);
                    int m_itemInOffer = m_cursor.getInt(16);


                    ItemSnippet currenOrderObj = new ItemSnippet(itemsId, m_itemCode, m_itemName, m_itemImgFlag, m_itemImgAddress,
                            m_itemUnit, m_itemCost, m_itemShortDesc, m_itemDesc, m_itemStatus, itemSelected, itemQuantitySelected, m_itemServiceType, m_itemServiceTypeCategory, m_itemServiceProvider, itemMrp, itemOfferCost, m_itemInOffer);
                    itemsList.add(currenOrderObj);
                    m_cursor.moveToNext();
                }
            }
        }
        if (m_cursor != null) m_cursor.close();

        getItemsAdapterReady(itemsList);
    }

    private void getItemsAdapterReady(ArrayList<ItemSnippet> itemsList) {

        if (null != itemsList && !itemsList.isEmpty()) {
            if (null == m_itemsList) {
                m_itemsList = itemsList;
                m_itemsDetailAdapter = new ItemsCategoryAdapter(getActivity(), getActivity().getApplicationContext(), m_itemsList, m_images_available);
                m_itemDetailsListView.setAdapter((ListAdapter) m_itemsDetailAdapter);
            } else {
                m_itemsList.addAll(itemsList);
                m_itemsDetailAdapter.notifyDataSetChanged();
            }
        }

        //first time case, when database was empty
        if (shallLoad && doneLoading && (null == m_itemsList || m_itemsList.isEmpty())) {
            doneLoading = false;
            getItemsFromServerLimited();
        }
        //if this is the end of loading from server
        else if (!doneLoading) {
            doneLoading = true;
            if (null != progressDialog && progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    public void getItemsFromServerLimited() {

        progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        String previous_item_code;
        if (null == m_itemsList || m_itemsList.isEmpty())
            previous_item_code = "0";
        else
            previous_item_code = m_itemsList.get(m_itemsList.size() - 1).m_itemCode;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getItemsFromServerLimitedAPICall(m_spCode, m_stCode, m_scCode, previous_item_code, new retrofit.Callback<APIResonseModel.GetItemsFromServerLimitedResponseModel>() {

            @Override
            public void success(APIResonseModel.GetItemsFromServerLimitedResponseModel getItemsFromServerLimitedResponseModel, retrofit.client.Response response) {
                ArrayList<ItemSnippet> itemsList = new ArrayList<ItemSnippet>();

                itemsList = getItemsFromServerLimitedResponseModel.items_service_providers;
                if (null != itemsList && !itemsList.isEmpty()) {
                    m_async = new StoreItemsInSqlite(getActivity()).execute(itemsList);
                } else {
                    shallLoad = false;
                    progressDialog.dismiss();
                    doneLoading = true;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    private class StoreItemsInSqlite extends AsyncTask<ArrayList<ItemSnippet>, Void, Void> {
        Context myactivity;

        public StoreItemsInSqlite(Context myactivity) {
            this.myactivity = myactivity;
        }

        @Override
        protected Void doInBackground(ArrayList<ItemSnippet>... params) {
            ArrayList<ItemSnippet> itemsList = params[0];
            Vector<ContentValues> cVVector = new Vector<ContentValues>(itemsList.size());


            for (int i = 0; i < itemsList.size(); i++) {
                ContentValues itemDetailValues = new ContentValues();
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_CODE, itemsList.get(i).m_itemCode);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_NAME, itemsList.get(i).m_itemName);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_IMG_FLAG, itemsList.get(i).m_itemImgFlag);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_IMG_ADDRESS, itemsList.get(i).m_itemImgAddress);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_UNIT, itemsList.get(i).m_itemUnit);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_COST, itemsList.get(i).m_itemCost);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_SHORT_DESC, itemsList.get(i).m_itemShortDesc);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_DESC, itemsList.get(i).m_itemDesc);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_STATUS, itemsList.get(i).m_itemStatus);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_SELECTED, 0);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_QUANTITY_SELECTED, 0);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_ST_CODE, itemsList.get(i).m_itemServiceType);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_SC_CODE, itemsList.get(i).m_itemServiceCategory);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_MRP, itemsList.get(i).m_itemMrp);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_OFFER_PRICE, itemsList.get(i).m_itemOfferPrice);
                itemDetailValues.put(ItemDetailsEntry.COLUMN_ITEM_IN_OFFER, itemsList.get(i).m_itemInOffer);
                cVVector.add(itemDetailValues);
            }


            if (isCancelled()) {
                Log.i("cancelled", "yes");
            } else {
                if (cVVector.size() > 0) {
                    ContentValues[] cvArray = new ContentValues[cVVector.size()];
                    cVVector.toArray(cvArray);
                    getActivity().getContentResolver().bulkInsert(ItemDetailsEntry.CONTENT_URI, cvArray);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            getItemsSnippetListReady();
        }
    }
}