package technologies.angular.lazylad.common;

/**
 * Created by ashwinikumar on 07/04/16.
 */
public class Constants {

    public static final String WEB_SERVICE_URL = "http://54.169.62.100";

    public static final String TEST_WEB_SERVICE_URL = "http://54.179.150.162";

    public static final int GROCERIES_DELIVERY_SLOT_CODE = 1;
    public static final int LAUNDRY_PICKUP_SLOT_CODE = 2;
    public static final int LAUNDRY_DELIVERY_SLOT_CODE = 3;

    public static final int LAUNDRY_ACTIVITY_CODE = 4;
}
