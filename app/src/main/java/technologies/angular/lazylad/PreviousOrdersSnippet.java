package technologies.angular.lazylad;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh on 24/02/15.
 */
public class PreviousOrdersSnippet {

    public int m_id;
    @SerializedName("order_string")
    public String m_orderString;
    @SerializedName("order_date")
    public String m_orderDate;
    @SerializedName("status")
    public String m_orderStatus;
    @SerializedName("sp_name")
    public String m_orderServProvName;
    @SerializedName("to_be_paid")
    public Double to_be_paid;
    @SerializedName("paid")
    public Double paid;
    public String order_type;



    public PreviousOrdersSnippet() {
    }

    public PreviousOrdersSnippet(int id , String order_date , String order_status, String serv_prov_name  ) {
        m_id = id;
        m_orderDate = order_date;
        m_orderStatus = order_status;
        m_orderServProvName = serv_prov_name;
    }
}
