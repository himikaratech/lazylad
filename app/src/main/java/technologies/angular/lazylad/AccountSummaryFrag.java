package technologies.angular.lazylad;

/**
 * Created by Amud on 28/09/15.
 */

import android.app.ProgressDialog;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

public class AccountSummaryFrag extends Fragment {

    int m_userCode;
    String m_number;
    TextView wallet_amountTV;
    ImageView wallet_ImageView;
    EditText coupon_code_editText;
    Button applyCouponCode;
    private String couponString;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.account_summary_frag, container, false);
        wallet_amountTV = (TextView) v.findViewById(R.id.wallet_amount);
        wallet_ImageView = (ImageView) v.findViewById(R.id.wallet_image);
        coupon_code_editText = (EditText) v.findViewById(R.id.couponCode);
        applyCouponCode = (Button) v.findViewById(R.id.applyCouponButton);
        wallet_amountTV.setVisibility(View.GONE);
        wallet_ImageView.setVisibility(View.GONE);
        coupon_code_editText.setVisibility(View.GONE);
        applyCouponCode.setVisibility(View.GONE);

        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_userCode = m_cursor.getInt(1);
                    m_cursor.moveToNext();
                }
            }
        }

        getWalletDetails();

        applyCouponCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                couponString = coupon_code_editText.getText().toString();

                if (couponString.isEmpty() || couponString == null) {
                    Toast.makeText(getActivity(), "Please enter a valid coupon code", Toast.LENGTH_SHORT).show();
                }
                else {
                    APIRequestModel requestModel = new APIRequestModel();
                    final APIRequestModel.CouponPostModel postCouponCodeRequestModel = requestModel.new CouponPostModel();
                    postCouponCodeRequestModel.user_code = String.valueOf(m_userCode);
                    postCouponCodeRequestModel.coupon_code = couponString;

                    RestAdapter restAdapter = new RestAdapter.Builder()
                            .setEndpoint("http://54.169.62.100")
                            .build();

                    APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
                    final ProgressDialog pDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
                    pDialog.setCancelable(false);
                    pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                    pDialog.show();

                    apiSuggestionsService.postCouponCodeAPICall(postCouponCodeRequestModel.user_code, postCouponCodeRequestModel.coupon_code, new Callback<APIResonseModel.CouponPostResponseModel>() {

                        @Override
                        public void success(APIResonseModel.CouponPostResponseModel getWalletAmountResponseModel, Response response) {

                            boolean success = getWalletAmountResponseModel.success;
                            Log.i("success value", success+"");
                            if (success == true) {
                                Double balance = getWalletAmountResponseModel.wallet_balance;
                                wallet_amountTV.setText("₹ " + String.valueOf(balance));
                                coupon_code_editText.setText("");
                                Toast.makeText(getActivity(), "Rs. " + getWalletAmountResponseModel.amount_credited + " credited to your wallet", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                coupon_code_editText.setText("");
                                Toast.makeText(getActivity(), getWalletAmountResponseModel.message, Toast.LENGTH_SHORT).show();
                            }
                            if (pDialog.isShowing())
                                pDialog.dismiss();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(getActivity(), "Some server error. Try Again!!", Toast.LENGTH_SHORT).show();
                            Log.e("RetrofitError", error.toString());
                            if (pDialog.isShowing())
                                pDialog.dismiss();
                        }
                    });
                }
            }
        });
        return v;
    }

    private void getWalletDetails() {

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.GetWalletDetailsRequestModel getWalletDetailsRequestModel = requestModel.new GetWalletDetailsRequestModel();
        getWalletDetailsRequestModel.user_code = String.valueOf(m_userCode);
        getWalletDetailsRequestModel.owner_type = 0;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        apiSuggestionsService.getWalletDetailsAPICall(getWalletDetailsRequestModel, new Callback<APIResonseModel.GetWalletDetailsResponseModel>() {

            @Override
            public void success(APIResonseModel.GetWalletDetailsResponseModel getWalletDetailsResponseModel, Response response) {

                boolean success = getWalletDetailsResponseModel.success;
                if (success == true) {

                    Double balance = getWalletDetailsResponseModel.wallet.balance;
                    wallet_ImageView.setVisibility(View.VISIBLE);
                    wallet_amountTV.setVisibility(View.VISIBLE);
                    coupon_code_editText.setVisibility(View.VISIBLE);
                    applyCouponCode.setVisibility(View.VISIBLE);
                    wallet_amountTV.setText("₹ " + String.valueOf(balance));
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("WalletRetrofitError", error.toString());
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }
}