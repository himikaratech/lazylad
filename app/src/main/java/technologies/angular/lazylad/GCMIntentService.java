package technologies.angular.lazylad;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import static technologies.angular.lazylad.CommonUtilities.displayMessage;

public class GCMIntentService extends GCMBaseIntentService{

	private static final String TAG = "GCMIntentService";
	private static String SENDER_ID = "631661601999";

    private static final Integer MESSAGE_ORDER_TYPE = 1;
    private static final Integer MESSAGE_OFFER_TYPE = 2;

    public GCMIntentService() {
        super(SENDER_ID);
    }

        /**
         * Method called on Receiving a new message
         * */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        String order_message = intent.getExtras().getString("price");
        String offer_message = intent.getExtras().getString("offer");

        if(null != offer_message && null!=order_message){
            displayMessage(context, offer_message);
            // notifies user
            generateNotification(context, offer_message,MESSAGE_OFFER_TYPE);
        }

        else if(null!=order_message) {
            displayMessage(context, order_message);
            // notifies user
            generateNotification(context, order_message,MESSAGE_ORDER_TYPE);
        }

    }

    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message, Integer message_type) {
        int icon = R.drawable.logo;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        
        String title = context.getString(R.string.app_name);

        Intent notificationIntent = null;

        if(MESSAGE_ORDER_TYPE == message_type)
            notificationIntent = new Intent(context, PreviousOrders.class);
        else if(MESSAGE_OFFER_TYPE == message_type)
            notificationIntent = new Intent(context, NotificationCenter.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
//        notification.setLatestEventInfo(context, title, message, intent);
//        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//
//        // Play default notification sound
//        notification.defaults |= Notification.DEFAULT_SOUND;
//
//        //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
//
//        // Vibrate if vibrate is enabled
//        notification.defaults |= Notification.DEFAULT_VIBRATE;
//        notificationManager.notify(0, notification);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(icon)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setWhen(when)
                        .setDefaults(Notification.DEFAULT_ALL) // requires VIBRATE permission
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(message))
                        .setAutoCancel(true);
        builder.setContentIntent(intent);
        notificationManager.notify(0,builder.build());
    }

	@Override
	protected void onRegistered(Context context, String arg1) {
        Log.i(TAG, "Device registered: regId = "+arg1);
        //displayMessage(context, "Your device registred with GCM");
	}

	@Override
	protected void onUnregistered(Context context, String arg1) {
        Log.i(TAG, "Device unregistered");
        //displayMessage(context, arg1);
	}

}
