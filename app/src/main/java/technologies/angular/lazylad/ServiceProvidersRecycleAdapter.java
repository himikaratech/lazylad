package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by DELL on 23-Jul-2015.
 */
public class ServiceProvidersRecycleAdapter extends RecyclerView.Adapter<ServiceProvidersRecycleAdapter.ViewHolder> {

    private ArrayList<ServiceProvidersListSnippet> m_servProvDetails;
    private int itemLayout;
    private Activity m_activity;
    private int time_int;
    int viewFlag = 0;
    int m_servTypeFlag;

    public ServiceProvidersRecycleAdapter(ArrayList<ServiceProvidersListSnippet> items, int itemLayout, Context context,
                                          int viewFlag, int servTypeFlag) {
        m_activity = (Activity) context;
        this.m_servProvDetails = items;
        this.itemLayout = itemLayout;
        this.viewFlag = viewFlag;
        this.m_servTypeFlag = servTypeFlag;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v, viewFlag);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (viewFlag == 0) {
            if (m_servProvDetails != null) {
                final String servProvName = ((ServiceProvidersListSnippet) getItem(position)).m_servProvName;
                final String servProvDelTime = ((ServiceProvidersListSnippet) getItem(position)).m_servProvDelTime;
                final String servProvMinOrd = ((ServiceProvidersListSnippet) getItem(position)).m_servProvMinOrder;
                final String servProvCode = ((ServiceProvidersListSnippet) getItem(position)).m_servProvCode;
                final String stCode = ((ServiceProvidersListSnippet) getItem(position)).m_servType;
                final int shop_open_time = ((ServiceProvidersListSnippet) getItem(position)).m_shop_open_time;
                final int shop_close_time = ((ServiceProvidersListSnippet) getItem(position)).m_shop_close_time;
                final int images_available = ((ServiceProvidersListSnippet) getItem(position)).m_images_available;

                ServiceProvidersListSnippet.TaxPerSellerModel taxList = ((ServiceProvidersListSnippet)
                        getItem(position)).sellerTaxes;
                final String taxName1 = taxList.taxName1;
                final String taxName2 = taxList.taxName2;
                final String taxName3 = taxList.taxName3;
                final double taxCent1 = taxList.taxCent1;
                final double taxCent2 = taxList.taxCent2;
                final double taxCent3 = taxList.taxCent3;
                final int taxFlag1 = taxList.taxFlag1;
                final int taxFlag2 = taxList.taxFlag2;
                final int taxFlag3 = taxList.taxFlag3;

                ServiceProvidersListSnippet item = m_servProvDetails.get(position);
                holder.MinOrdAmt.setText("Free delivery above  ₹");
                holder.MinOrdAmt.append(servProvMinOrd);
                holder.servProvNameTextView.setText(servProvName);

                int shop_open_hour = shop_open_time / 60;
                int shop_open_minute = shop_open_time % 60;
                int shop_close_hour = shop_close_time / 60;
                int shop_close_minute = shop_close_time % 60;

                if (shop_open_hour > 12)
                    shop_open_hour = shop_open_hour - 12;
                if (shop_close_hour > 12)
                    shop_close_hour = shop_close_hour - 12;


                String del_interval = shop_open_hour + ":" + String.format("%02d", shop_open_minute) + " am - " + shop_close_hour + "." + String.format("%02d", shop_close_minute) + " pm";

                Calendar currentCal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                int hour = currentCal.get(Calendar.HOUR_OF_DAY);
                final int minute = currentCal.get(Calendar.MINUTE);

                int currentTime = (hour * 60) + minute;

                if (shop_open_time > currentTime || currentTime > shop_close_time)
                    holder.ServDelTime.setText("Closed");
                else
                    holder.ServDelTime.setText(servProvDelTime);

                holder.store_timimgTextView.setText(del_interval);

                if (m_servTypeFlag == 1) {
                    startCategoryListActivity(servProvCode, stCode, servProvDelTime, servProvMinOrd, servProvName,
                            shop_open_time, shop_close_time, images_available, taxName1, taxName2, taxName3, taxCent1,
                            taxCent2, taxCent3, taxFlag1, taxFlag2, taxFlag3);
                }
                holder.servProvRelativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startCategoryListActivity(servProvCode, stCode, servProvDelTime, servProvMinOrd, servProvName,
                                shop_open_time, shop_close_time, images_available, taxName1, taxName2, taxName3, taxCent1,
                                taxCent2, taxCent3, taxFlag1, taxFlag2, taxFlag3);
                    }
                });
            }
            if (viewFlag == 1) {
                final String servProvName = ((ServiceProvidersListSnippet) getItem(position)).m_servProvName;
                holder.servProvNameTextView.setText(servProvName);

            }
        }
    }

    @Override
    public int getItemCount() {
        return m_servProvDetails.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView servProvNameTextView;
        TextView ServDelTime;
        TextView MinOrdAmt;
        RelativeLayout servProvRelativeLayout;
        TextView store_timimgTextView;

        public ViewHolder(View itemView, int flag) {
            super(itemView);
            if (flag == 0) {
                servProvNameTextView = (TextView) itemView.findViewById(R.id.serv_prov_name);
                ServDelTime = (TextView) itemView.findViewById(R.id.max_del_time);
                MinOrdAmt = (TextView) itemView.findViewById(R.id.min_del_amt);
                store_timimgTextView = (TextView) itemView.findViewById(R.id.store_timimg);
                servProvRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.relative_layout_serv_prov_list);

            }
            if (flag == 1) {
                servProvNameTextView = (TextView) itemView.findViewById(R.id.serv_prov_name);
            }
        }
    }

    public Object getItem(int position) {
        if (m_servProvDetails != null) {
            return m_servProvDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_servProvDetails != null) {
            return m_servProvDetails.get(position).m_servProvId;
        }
        return 0;
    }

    private void startCategoryListActivity(String serv_prov_code, String st_code, String del_time, String min_order,
                                           String prov_name, int shop_open_time, int shop_close_time, int images_available,
                                           String tax_name_1, String tax_name_2, String tax_name_3, double tax_cent_1,
                                           double tax_cent_2, double tax_cent_3, int tax_flag_1, int tax_flag_2,
                                           int tax_flag_3) {
        int service_type = Integer.parseInt(st_code);
        String time_interval = del_time;
        if (del_time.contains(" ")) {
            time_interval = del_time.substring(0, del_time.indexOf(" "));
        }

        try{
            time_int = Integer.parseInt(time_interval);
        }
        catch(Exception e){
            time_int = 90;
        }


        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("Selected SPCode", serv_prov_code);
        builder.putAttrString("Selected STCode", st_code);
        builder.putAttrString("Activity Name", "ServiceProvidersList");
        MoEHelper.getInstance(m_activity).trackEvent("Sellers List", builder.build());


        SharedPreferences prefs = m_activity.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ServiceProviderCode", serv_prov_code);
        editor.putString("ServiceProviderDeliveryTime", del_time);
        editor.putString("ServiceProviderMinimumOrder", min_order);
        editor.putString("ServiceProviderName", prov_name);
        editor.putInt("time_interval", time_int);
        editor.putInt("shop_open_time", shop_open_time);
        editor.putInt("shop_close_time", shop_close_time);

        editor.putInt("images_available", images_available);

        editor.putString("taxName1", tax_name_1);
        editor.putString("taxName2", tax_name_2);
        editor.putString("taxName3", tax_name_3);
        editor.putString("taxCent1", String.valueOf(tax_cent_1));
        editor.putString("taxCent2", String.valueOf(tax_cent_2));
        editor.putString("taxCent3", String.valueOf(tax_cent_3));
        editor.putInt("taxFlag1", tax_flag_1);
        editor.putInt("taxFlag2", tax_flag_2);
        editor.putInt("taxFlag3", tax_flag_3);

        editor.commit();
        if (service_type == 2) {
            Intent intent = new Intent(m_activity, prescription.class);
            String[] details = new String[]{serv_prov_code, st_code};
            intent.putExtra("ItemsList", details);
            m_activity.startActivity(intent);
            m_activity.finish();
        } else {

            Intent intent = new Intent(m_activity, ItemsActivity.class);

            if (m_servTypeFlag == 1) {
                m_servTypeFlag = 0;
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                m_activity.startActivity(intent);
                m_activity.finish();
                m_activity.overridePendingTransition(0, 0);
            } else
                m_activity.startActivity(intent);
        }
    }
}
