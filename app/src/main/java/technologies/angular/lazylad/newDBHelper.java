package technologies.angular.lazylad;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import technologies.angular.lazylad.LazyContract.UserDetailsEntry;
import technologies.angular.lazylad.LazyContract.ServiceTypesEntry;
import technologies.angular.lazylad.LazyContract.ItemDetailsEntry;
import technologies.angular.lazylad.LazyContract.SearchResultEntry;
import technologies.angular.lazylad.LazyContract.ServiceTypeCategoriesEntry;
import technologies.angular.lazylad.LazyContract.ShoppingCartEntry;
import technologies.angular.lazylad.LazyContract.UserAddressDetailsEntry;
import technologies.angular.lazylad.LazyContract.OrderDetailsEntry;
import technologies.angular.lazylad.LazyContract.AddressSearchEntry;


/**
 * Created by Amud on 14/09/15.
 */
public class newDBHelper extends SQLiteOpenHelper {
    private static int DATABASE_VERSION = 11;
    public static final String DATABASE_NAME = "LazyLadData";


    public newDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        final String SQL_CREATE_USER_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + UserDetailsEntry.TABLE_NAME +
                        "(" +
                        UserDetailsEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        UserDetailsEntry.COLUMN_USER_CODE + " INTEGER, " +
                        UserDetailsEntry.COLUMN_VERIFIED_FLAG + " INT, " +
                        UserDetailsEntry.COLUMN_VERIFIED_NUMBER + " TEXT, " +
                        UserDetailsEntry.COLUMN_EMAIL_FLAG + " INT, " +
                        UserDetailsEntry.COLUMN_EMAIL_ID + " TEXT " +
                        " );";

        final String SQL_CREATE_PATH_SERVICE_TYPES =
                "CREATE TABLE IF NOT EXISTS " + ServiceTypesEntry.TABLE_NAME +
                        "(" +
                        ServiceTypesEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        ServiceTypesEntry.COLUMN_ST_CODE + " INTEGER, " +
                        ServiceTypesEntry.COLUMN_ST_NAME + " TEXT, " +
                        ServiceTypesEntry.COLUMN_ST_IMG_FLAG + " INTEGER, " +
                        ServiceTypesEntry.COLUMN_ST_IMG_ADD + " TEXT, " +
                        ServiceTypesEntry.COLUMN_ST_FLAG + " INTEGER DEFAULT 0, " +
                        ServiceTypesEntry.COLUMN_ST_IS_SERVICE + " INTEGER " +
                        " );";


        final String SQL_CREATE_PATH_USER_ADD_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + UserAddressDetailsEntry.TABLE_NAME +
                        "(" +
                        UserAddressDetailsEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        UserAddressDetailsEntry.COLUMN_USER_CODE + " INTEGER, " +
                        UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE + " INTEGER, " +
                        UserAddressDetailsEntry.COLUMN_USER_ADDRESS + " TEXT, " +
                        UserAddressDetailsEntry.COLUMN_USER_NAME + " TEXT, " +
                        UserAddressDetailsEntry.COLUMN_USER_EMAIL_ID + " TEXT, " +
                        UserAddressDetailsEntry.COLUMN_USER_PHONE_NUMBER + " TEXT," +
                        UserAddressDetailsEntry.COLUMN_USER_FLAT + " TEXT," +
                        UserAddressDetailsEntry.COLUMN_USER_SUBAREA + " TEXT ," +
                        UserAddressDetailsEntry.COLUMN_USER_AREA + " TEXT , " +
                        UserAddressDetailsEntry.COLUMN_LATITUDE + " DOUBLE , " +
                        UserAddressDetailsEntry.COLUMN_LONGITUDE + " DOUBLE " +
                        " );";


        final String SQL_CREATE_PATH_ITEM_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + ItemDetailsEntry.TABLE_NAME +
                        "(" +
                        ItemDetailsEntry._ID + "  INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        ItemDetailsEntry.COLUMN_ITEM_CODE + "  TEXT, " +
                        ItemDetailsEntry.COLUMN_ITEM_NAME + "  TEXT, " +
                        ItemDetailsEntry.COLUMN_ITEM_IMG_FLAG + "  INT, " +
                        ItemDetailsEntry.COLUMN_ITEM_IMG_ADDRESS + "  TEXT, " +
                        ItemDetailsEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                        ItemDetailsEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                        ItemDetailsEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                        ItemDetailsEntry.COLUMN_ITEM_DESC + " TEXT, " +
                        ItemDetailsEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                        ItemDetailsEntry.COLUMN_ITEM_SELECTED + " INTEGER, " +
                        ItemDetailsEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                        ItemDetailsEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                        ItemDetailsEntry.COLUMN_ITEM_SC_CODE + " TEXT, " +
                        ItemDetailsEntry.COLUMN_ITEM_MRP + " DOUBLE , " +
                        ItemDetailsEntry.COLUMN_ITEM_OFFER_PRICE + " DOUBLE ," +
                        ItemDetailsEntry.COLUMN_ITEM_IN_OFFER + " INTEGER " +
                        " );";


        final String SQL_CREATE_PATH_SHOPPING_CART =

                "CREATE TABLE IF NOT EXISTS " + ShoppingCartEntry.TABLE_NAME +
                        "(" +
                        ShoppingCartEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        ShoppingCartEntry.COLUMN_ITEM_CODE + " TEXT, " +
                        ShoppingCartEntry.COLUMN_ITEM_NAME + " TEXT, " +
                        ShoppingCartEntry.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                        ShoppingCartEntry.COLUMN_ITEM_IMG_ADDRESS + " TEXT, " +
                        ShoppingCartEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                        ShoppingCartEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                        ShoppingCartEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                        ShoppingCartEntry.COLUMN_ITEM_DESC + " TEXT, " +
                        ShoppingCartEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                        ShoppingCartEntry.COLUMN_ITEM_SELECTED + " INT, " +
                        ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                        ShoppingCartEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                        ShoppingCartEntry.COLUMN_ITEM_SC_CODE + " TEXT, " +
                        ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " TEXT ," +
                        ShoppingCartEntry.COLUMN_ITEM_OFFER_PRICE + " DOUBLE ," +
                        ShoppingCartEntry.COLUMN_ITEM_IN_OFFER + " INTEGER " +
                        " );";
        final String SQL_CREATE_PATH_SERVICE_TYPE_CATEGORY =

                "CREATE TABLE IF NOT EXISTS " + ServiceTypeCategoriesEntry.TABLE_NAME +
                        "(" +
                        ServiceTypeCategoriesEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        ServiceTypeCategoriesEntry.COLUMN_ST_CODE + " TEXT, " +
                        ServiceTypeCategoriesEntry.COLUMN_SC_CODE + " TEXT, " +
                        ServiceTypeCategoriesEntry.COLUMN_SC_NAME + " TEXT" +
                        " );";


        final String SQL_CREATE_PATH_SEARCH_RESULT =

                "CREATE TABLE IF NOT EXISTS " + SearchResultEntry.TABLE_NAME +
                        "(" +
                        SearchResultEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        SearchResultEntry.COLUMN_ITEM_CODE + " TEXT, " +
                        SearchResultEntry.COLUMN_ITEM_NAME + " TEXT, " +
                        SearchResultEntry.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                        SearchResultEntry.COLUMN_ITEM_IMG_ADDRESS + " TEXT, " +
                        SearchResultEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                        SearchResultEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                        SearchResultEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                        SearchResultEntry.COLUMN_ITEM_DESC + " TEXT, " +
                        SearchResultEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                        SearchResultEntry.COLUMN_ITEM_SELECTED + " INTEGER, " +
                        SearchResultEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                        SearchResultEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                        SearchResultEntry.COLUMN_ITEM_SC_CODE + " TEXT, " +
                        SearchResultEntry.COLUMN_ITEM_MRP + " DOUBLE, " +
                        SearchResultEntry.COLUMN_SEARCH_STRING + " TEXT ," +
                        SearchResultEntry.COLUMN_ITEM_OFFER_PRICE + " DOUBLE , " +
                        SearchResultEntry.COLUMN_ITEM_IN_OFFER + " INTEGER " +

                        " );";

        final String SQL_CREATE_ORDER_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + OrderDetailsEntry.TABLE_NAME +
                        "(" +
                        OrderDetailsEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " TEXT, " +
                        OrderDetailsEntry.COLUMN_DEL_TIME + " TEXT, " +
                        OrderDetailsEntry.COLUMN_MIN_ORDER + " INT, " +
                        OrderDetailsEntry.COLUMN_SERV_PROV_NAME + " TEXT, " +
                        OrderDetailsEntry.COLUMN_TIME_INTERVAL + " INT, " +
                        OrderDetailsEntry.COLUMN_SHOP_OPEN_TIME + " INT, " +
                        OrderDetailsEntry.COLUMN_SHOP_CLOSE_TIME + " INT, " +
                        OrderDetailsEntry.COLUMN_SHOP_TAX_NAME_1 + " TEXT, " +
                        OrderDetailsEntry.COLUMN_SHOP_TAX_CENT_1 + " DOUBLE, " +
                        OrderDetailsEntry.COLUMN_SHOP_TAX_FLAG_1 + " INT, " +
                        OrderDetailsEntry.COLUMN_SHOP_TAX_NAME_2 + " TEXT, " +
                        OrderDetailsEntry.COLUMN_SHOP_TAX_CENT_2 + " DOUBLE, " +
                        OrderDetailsEntry.COLUMN_SHOP_TAX_FLAG_2 + " INT, " +
                        OrderDetailsEntry.COLUMN_SHOP_TAX_NAME_3 + " TEXT, " +
                        OrderDetailsEntry.COLUMN_SHOP_TAX_CENT_3 + " DOUBLE, " +
                        OrderDetailsEntry.COLUMN_SHOP_TAX_FLAG_3 + " INT " +
                        " );";

        final String SQL_CREATE_ADDRESS_SEARCH =
                "CREATE TABLE IF NOT EXISTS " + AddressSearchEntry.TABLE_NAME +
                        "(" +
                        AddressSearchEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        AddressSearchEntry.COLUMN_FORMATTED_ADDRESS + " TEXT, " +
                        AddressSearchEntry.COLUMN_LATITUDE + " DOUBLE , " +
                        AddressSearchEntry.COLUMN_LONGITUDE + " DOUBLE , " +
                        AddressSearchEntry.COLUMN_CITY_CODE + " TEXT , " +
                        AddressSearchEntry.COLUMN_CITY_NAME + " TEXT , " +
                        AddressSearchEntry.COLUMN_AREA_NAME + " TEXT , " +
                        AddressSearchEntry.COLUMN_CITY_IMAGE_ADDRESS + " TEXT , " +
                        AddressSearchEntry.COLUMN_COUPON_IN_CITY + " INTEGER DEFAULT 0 " +

                        " );";


        final String SQL_CREATE_COUPONS_VIEWS_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + LazyContract.CouponsViewsDetails.TABLE_NAME +
                        "(" +
                        LazyContract.CouponsViewsDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        LazyContract.CouponsStatistics.COLUMN_COUPON_ID + " TEXT, " +
                        LazyContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT + " INTEGER DEFAULT 0, " +
                        LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " INTEGER DEFAULT 0 , " +
                        LazyContract.CouponsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                        " );";

        final String SQL_CREATE_COUPONS_CLICKS_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + LazyContract.CouponsClicksDetails.TABLE_NAME +
                        "(" +
                        LazyContract.CouponsClicksDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        LazyContract.CouponsStatistics.COLUMN_COUPON_ID + " TEXT, " +
                        LazyContract.CouponsClicksDetails.COLUMN_COUNT + " INTEGER DEFAULT 0, " +
                        LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " INTEGER DEFAULT 0 , " +
                        LazyContract.CouponsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                        " );";


        final String SQL_CREATE_ITEMS_VIEWS_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + LazyContract.ItemsViewsDetails.TABLE_NAME +
                        "(" +
                        LazyContract.ItemsViewsDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE + " TEXT, " +
                        LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE + " TEXT, " +
                        LazyContract.ItemsViewsDetails.COLUMN_VIEWS_COUNT + " INTEGER DEFAULT 0, " +
                        LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC + " INTEGER DEFAULT 0 , " +
                        LazyContract.ItemsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                        " );";

        final String SQL_CREATE_ITEMS_CLICKS_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + LazyContract.ItemsClicksDetails.TABLE_NAME +
                        "(" +
                        LazyContract.ItemsClicksDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE + " TEXT, " +
                        LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE + " TEXT, " +
                        LazyContract.ItemsClicksDetails.COLUMN_COUNT + " INTEGER DEFAULT 0, " +
                        LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC + " INTEGER DEFAULT 0 , " +
                        LazyContract.ItemsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                        " );";


        db.execSQL(SQL_CREATE_USER_DETAILS);
        db.execSQL(SQL_CREATE_PATH_SERVICE_TYPES);
        db.execSQL(SQL_CREATE_PATH_USER_ADD_DETAILS);
        db.execSQL(SQL_CREATE_PATH_ITEM_DETAILS);
        db.execSQL(SQL_CREATE_PATH_SHOPPING_CART);
        db.execSQL(SQL_CREATE_PATH_SERVICE_TYPE_CATEGORY);
        db.execSQL(SQL_CREATE_PATH_SEARCH_RESULT);
        db.execSQL(SQL_CREATE_ORDER_DETAILS);
        db.execSQL(SQL_CREATE_ADDRESS_SEARCH);
        db.execSQL(SQL_CREATE_COUPONS_VIEWS_DETAILS);
        db.execSQL(SQL_CREATE_COUPONS_CLICKS_DETAILS);

        db.execSQL(SQL_CREATE_ITEMS_VIEWS_DETAILS);
        db.execSQL(SQL_CREATE_ITEMS_CLICKS_DETAILS);

        insertointodb(db);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        if (2 > oldVersion && 2 <= newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + ServiceTypesEntry.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + ServiceTypeCategoriesEntry.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + ItemDetailsEntry.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + ShoppingCartEntry.TABLE_NAME);

            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + ServiceTypesEntry.TABLE_NAME +
                            "(" +
                            "id" + " INTEGER PRIMARY KEY, " +
                            ServiceTypesEntry.COLUMN_ST_CODE + " INTEGER, " +
                            ServiceTypesEntry.COLUMN_ST_NAME + " TEXT, " +
                            ServiceTypesEntry.COLUMN_ST_IMG_FLAG + " INTEGER, " +
                            ServiceTypesEntry.COLUMN_ST_IMG_ADD + " INTEGER" +
                            ");"
            );
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + ItemDetailsEntry.TABLE_NAME +
                            "(" +
                            "id" + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            ItemDetailsEntry.COLUMN_ITEM_CODE + " TEXT, " +
                            ItemDetailsEntry.COLUMN_ITEM_NAME + " TEXT, " +
                            ItemDetailsEntry.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                            ItemDetailsEntry.COLUMN_ITEM_IMG_ADDRESS + " TEXT, " +
                            ItemDetailsEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                            ItemDetailsEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                            ItemDetailsEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                            ItemDetailsEntry.COLUMN_ITEM_DESC + " TEXT, " +
                            ItemDetailsEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                            ItemDetailsEntry.COLUMN_ITEM_SELECTED + " INTEGER, " +
                            ItemDetailsEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                            ItemDetailsEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                            ItemDetailsEntry.COLUMN_ITEM_SC_CODE + " TEXT, " +
                            ItemDetailsEntry.COLUMN_ITEM_MRP + " DOUBLE" +
                            ");"
            );
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + ShoppingCartEntry.TABLE_NAME +
                            "(" +
                            "id" + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            ShoppingCartEntry.COLUMN_ITEM_CODE + " TEXT, " +
                            ShoppingCartEntry.COLUMN_ITEM_NAME + " TEXT, " +
                            ShoppingCartEntry.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                            ShoppingCartEntry.COLUMN_ITEM_IMG_ADDRESS + " TEXT, " +
                            ShoppingCartEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                            ShoppingCartEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                            ShoppingCartEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                            ShoppingCartEntry.COLUMN_ITEM_DESC + " TEXT, " +
                            ShoppingCartEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                            ShoppingCartEntry.COLUMN_ITEM_SELECTED + " INTEGER, " +
                            ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                            ShoppingCartEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                            ShoppingCartEntry.COLUMN_ITEM_SC_CODE + " TEXT" +
                            ");"
            );
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + ServiceTypeCategoriesEntry.TABLE_NAME +
                            "(" +
                            "id" + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            ServiceTypeCategoriesEntry.COLUMN_ST_CODE + " TEXT, " +
                            ServiceTypeCategoriesEntry.COLUMN_SC_CODE + " TEXT, " +
                            ServiceTypeCategoriesEntry.COLUMN_SC_NAME + " TEXT" +
                            ");"
            );

            insertointodb(db);
        }


        if (3 > oldVersion && 3 <= newVersion) {
            db.execSQL("ALTER TABLE  " + UserAddressDetailsEntry.TABLE_NAME +
                    " ADD COLUMN " + UserAddressDetailsEntry.COLUMN_USER_FLAT + " TEXT ;");
            db.execSQL("ALTER TABLE " + UserAddressDetailsEntry.TABLE_NAME +
                    " ADD COLUMN " + UserAddressDetailsEntry.COLUMN_USER_SUBAREA + " TEXT ;");
            db.execSQL("ALTER TABLE " + UserAddressDetailsEntry.TABLE_NAME +
                    " ADD COLUMN " + UserAddressDetailsEntry.COLUMN_USER_AREA +
                    " TEXT ;");
            db.execSQL("DROP TABLE IF EXISTS " + ServiceTypesEntry.TABLE_NAME);
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + ServiceTypesEntry.TABLE_NAME +
                            "(" +
                            "id" + " INTEGER PRIMARY KEY, " +
                            ServiceTypesEntry.COLUMN_ST_CODE + " INTEGER, " +
                            ServiceTypesEntry.COLUMN_ST_NAME + " TEXT, " +
                            ServiceTypesEntry.COLUMN_ST_IMG_FLAG + " INTEGER, " +
                            ServiceTypesEntry.COLUMN_ST_IMG_ADD + " TEXT " +
                            ");"
            );

            insertointodb(db);
        }

        if (4 > oldVersion && 4 <= newVersion)

        {

            db.execSQL(" DROP TABLE IF EXISTS " + ServiceTypeCategoriesEntry.TABLE_NAME);
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + ServiceTypeCategoriesEntry.TABLE_NAME +
                            "(" +
                            "id" + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            ServiceTypeCategoriesEntry.COLUMN_ST_CODE + " TEXT, " +
                            ServiceTypeCategoriesEntry.COLUMN_SC_CODE + " TEXT, " +
                            ServiceTypeCategoriesEntry.COLUMN_SC_NAME + " TEXT " +
                            ");"
            );

            db.execSQL("ALTER TABLE " + UserDetailsEntry.TABLE_NAME + "  ADD COLUMN " + UserDetailsEntry.COLUMN_VERIFIED_FLAG + "  INTEGER  ;");
            db.execSQL("ALTER TABLE " + UserDetailsEntry.TABLE_NAME + "  ADD COLUMN  " + UserDetailsEntry.COLUMN_VERIFIED_NUMBER + " TEXT ;");
            db.execSQL("ALTER TABLE " + UserDetailsEntry.TABLE_NAME + "  ADD COLUMN  " + UserDetailsEntry.COLUMN_EMAIL_FLAG + " INTEGER ;");
            db.execSQL("ALTER TABLE " + UserDetailsEntry.TABLE_NAME + "  ADD COLUMN  " + UserDetailsEntry.COLUMN_EMAIL_ID + " TEXT ;");

            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + SearchResultEntry.TABLE_NAME +
                            "(" +
                            "id" + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            SearchResultEntry.COLUMN_ITEM_CODE + " TEXT, " +
                            SearchResultEntry.COLUMN_ITEM_NAME + " TEXT, " +
                            SearchResultEntry.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                            SearchResultEntry.COLUMN_ITEM_IMG_ADDRESS + " TEXT, " +
                            SearchResultEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                            SearchResultEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                            SearchResultEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                            SearchResultEntry.COLUMN_ITEM_DESC + " TEXT, " +
                            SearchResultEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                            SearchResultEntry.COLUMN_ITEM_SELECTED + " INTEGER, " +
                            SearchResultEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                            SearchResultEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                            SearchResultEntry.COLUMN_ITEM_SC_CODE + " TEXT, " +
                            SearchResultEntry.COLUMN_ITEM_MRP + " DOUBLE, " +
                            SearchResultEntry.COLUMN_SEARCH_STRING + " TEXT " +
                            ");"
            );

        }

        if (5 > oldVersion && 5 <= newVersion) {
            db.execSQL("ALTER TABLE  " + ServiceTypesEntry.TABLE_NAME +
                    " ADD COLUMN " + ServiceTypesEntry.COLUMN_ST_FLAG + " INTEGER DEFAULT 0 ;");

            db.execSQL("DROP TABLE IF EXISTS " + ShoppingCartEntry.TABLE_NAME);
            db.execSQL("CREATE TABLE IF NOT EXISTS " + ShoppingCartEntry.TABLE_NAME +
                    "(" +
                    ShoppingCartEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                    ShoppingCartEntry.COLUMN_ITEM_CODE + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_NAME + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                    ShoppingCartEntry.COLUMN_ITEM_IMG_ADDRESS + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                    ShoppingCartEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_DESC + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_SELECTED + " INT, " +
                    ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                    ShoppingCartEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_SC_CODE + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " TEXT " +
                    " );");

            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + OrderDetailsEntry.TABLE_NAME +
                            "(" +
                            OrderDetailsEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " TEXT, " +
                            OrderDetailsEntry.COLUMN_DEL_TIME + " TEXT, " +
                            OrderDetailsEntry.COLUMN_MIN_ORDER + " INT, " +
                            OrderDetailsEntry.COLUMN_SERV_PROV_NAME + " TEXT, " +
                            OrderDetailsEntry.COLUMN_TIME_INTERVAL + " INT, " +
                            OrderDetailsEntry.COLUMN_SHOP_OPEN_TIME + " INT, " +
                            OrderDetailsEntry.COLUMN_SHOP_CLOSE_TIME + " INT, " +
                            OrderDetailsEntry.COLUMN_SHOP_TAX_NAME_1 + " TEXT, " +
                            OrderDetailsEntry.COLUMN_SHOP_TAX_CENT_1 + " DOUBLE, " +
                            OrderDetailsEntry.COLUMN_SHOP_TAX_FLAG_1 + " INT, " +
                            OrderDetailsEntry.COLUMN_SHOP_TAX_NAME_2 + " TEXT, " +
                            OrderDetailsEntry.COLUMN_SHOP_TAX_CENT_2 + " DOUBLE, " +
                            OrderDetailsEntry.COLUMN_SHOP_TAX_FLAG_2 + " INT, " +
                            OrderDetailsEntry.COLUMN_SHOP_TAX_NAME_3 + " TEXT, " +
                            OrderDetailsEntry.COLUMN_SHOP_TAX_CENT_3 + " DOUBLE, " +
                            OrderDetailsEntry.COLUMN_SHOP_TAX_FLAG_3 + " INT " +
                            " );"
            );
        }

        if (6 > oldVersion && 6 <= newVersion) {

            renameIdInAllTables(db);
        }

        if (7 > oldVersion && 7 <= newVersion) {
            db.execSQL("ALTER TABLE  " + UserAddressDetailsEntry.TABLE_NAME +
                    " ADD COLUMN " + UserAddressDetailsEntry.COLUMN_LATITUDE + " DOUBLE ;");

            db.execSQL("ALTER TABLE  " + UserAddressDetailsEntry.TABLE_NAME +
                    " ADD COLUMN " + UserAddressDetailsEntry.COLUMN_LONGITUDE + " DOUBLE ;");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + AddressSearchEntry.TABLE_NAME +
                    "(" +
                    AddressSearchEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                    AddressSearchEntry.COLUMN_FORMATTED_ADDRESS + " TEXT, " +
                    AddressSearchEntry.COLUMN_LATITUDE + " DOUBLE , " +
                    AddressSearchEntry.COLUMN_LONGITUDE + " DOUBLE , " +
                    AddressSearchEntry.COLUMN_CITY_CODE + " TEXT , " +
                    AddressSearchEntry.COLUMN_CITY_NAME + " TEXT , " +
                    AddressSearchEntry.COLUMN_AREA_NAME + " TEXT , " +
                    AddressSearchEntry.COLUMN_CITY_IMAGE_ADDRESS + " TEXT " +

                    " );");
        }

        if (8 > oldVersion && 8 <= newVersion) {
            db.execSQL("ALTER TABLE  " + AddressSearchEntry.TABLE_NAME +
                    " ADD COLUMN " + AddressSearchEntry.COLUMN_CITY_IMAGE_ADDRESS + " TEXT ;");
        }
        if (9 > oldVersion && 9 <= newVersion) {
            db.execSQL("delete from " + AddressSearchEntry.TABLE_NAME);
            db.execSQL("ALTER TABLE  " + AddressSearchEntry.TABLE_NAME +
                    " ADD COLUMN " + AddressSearchEntry.COLUMN_COUPON_IN_CITY + " INTEGER DEFAULT 0 ;");


            final String SQL_CREATE_COUPONS_VIEWS_DETAILS =
                    "CREATE TABLE IF NOT EXISTS " + LazyContract.CouponsViewsDetails.TABLE_NAME +
                            "(" +
                            LazyContract.CouponsViewsDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            LazyContract.CouponsStatistics.COLUMN_COUPON_ID + " TEXT, " +
                            LazyContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT + " INTEGER DEFAULT 0, " +
                            LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " INTEGER DEFAULT 0 , " +
                            LazyContract.CouponsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                            " );";

            final String SQL_CREATE_COUPONS_CLICKS_DETAILS =
                    "CREATE TABLE IF NOT EXISTS " + LazyContract.CouponsClicksDetails.TABLE_NAME +
                            "(" +
                            LazyContract.CouponsClicksDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            LazyContract.CouponsStatistics.COLUMN_COUPON_ID + " TEXT, " +
                            LazyContract.CouponsClicksDetails.COLUMN_COUNT + " INTEGER DEFAULT 0, " +
                            LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " INTEGER DEFAULT 0 , " +
                            LazyContract.CouponsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                            " );";
            db.execSQL(SQL_CREATE_COUPONS_VIEWS_DETAILS);
            db.execSQL(SQL_CREATE_COUPONS_CLICKS_DETAILS);
        }


        if (10 > oldVersion && 10 <= newVersion) {


            final String SQL_CREATE_ITEMS_VIEWS_DETAILS =
                    "CREATE TABLE IF NOT EXISTS " + LazyContract.ItemsViewsDetails.TABLE_NAME +
                            "(" +
                            LazyContract.ItemsViewsDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE + " TEXT, " +
                            LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE + " TEXT, " +
                            LazyContract.ItemsViewsDetails.COLUMN_VIEWS_COUNT + " INTEGER DEFAULT 0, " +
                            LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC + " INTEGER DEFAULT 0 , " +
                            LazyContract.ItemsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                            " );";

            final String SQL_CREATE_ITEMS_CLICKS_DETAILS =
                    "CREATE TABLE IF NOT EXISTS " + LazyContract.ItemsClicksDetails.TABLE_NAME +
                            "(" +
                            LazyContract.ItemsClicksDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE + " TEXT, " +
                            LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE + " TEXT, " +
                            LazyContract.ItemsClicksDetails.COLUMN_COUNT + " INTEGER DEFAULT 0, " +
                            LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC + " INTEGER DEFAULT 0 , " +
                            LazyContract.ItemsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                            " );";

            db.execSQL("ALTER TABLE  " + ItemDetailsEntry.TABLE_NAME +
                    " ADD COLUMN " + ItemDetailsEntry.COLUMN_ITEM_OFFER_PRICE + " INTEGER ;");


            db.execSQL("ALTER TABLE  " + SearchResultEntry.TABLE_NAME +
                    " ADD COLUMN " + SearchResultEntry.COLUMN_ITEM_OFFER_PRICE + " DOUBLE ;");

            db.execSQL("ALTER TABLE  " + ShoppingCartEntry.TABLE_NAME +
                    " ADD COLUMN " + ShoppingCartEntry.COLUMN_ITEM_OFFER_PRICE + " DOUBLE ;");

            db.execSQL("ALTER TABLE  " + ItemDetailsEntry.TABLE_NAME +
                    " ADD COLUMN " + ItemDetailsEntry.COLUMN_ITEM_IN_OFFER + " DOUBLE ;");

            db.execSQL("ALTER TABLE  " + SearchResultEntry.TABLE_NAME +
                    " ADD COLUMN " + SearchResultEntry.COLUMN_ITEM_IN_OFFER + " DOUBLE ;");

            db.execSQL("ALTER TABLE  " + ShoppingCartEntry.TABLE_NAME +
                    " ADD COLUMN " + ShoppingCartEntry.COLUMN_ITEM_IN_OFFER + " DOUBLE ;");


            db.execSQL(SQL_CREATE_ITEMS_VIEWS_DETAILS);
            db.execSQL(SQL_CREATE_ITEMS_CLICKS_DETAILS);
        }

        if (11 > oldVersion && 11 <= newVersion) {
            db.execSQL("ALTER TABLE  " + ServiceTypesEntry.TABLE_NAME +
                    " ADD COLUMN " + ServiceTypesEntry.COLUMN_ST_IS_SERVICE + " INTEGER ;");
        }

    }


    public void insertointodb(SQLiteDatabase db) {
        db.execSQL("INSERT INTO " + ServiceTypesEntry.TABLE_NAME + " VALUES(1, 1, 'Groceries & Provisions', 1, '', 0, 0);");
        db.execSQL("INSERT INTO " + ServiceTypesEntry.TABLE_NAME + " VALUES(3, 3, 'Bakery', 1, '', 0, 0);");
        db.execSQL("INSERT INTO " + ServiceTypesEntry.TABLE_NAME + " VALUES(4, 4, 'Stationery', 1, '', 0, 0);");
        db.execSQL("INSERT INTO " + ServiceTypesEntry.TABLE_NAME + " VALUES(5, 5, 'Meats & Seafood', 1, '', 0, 0);");
        db.execSQL("INSERT INTO " + ServiceTypesEntry.TABLE_NAME + " VALUES(6, 6, 'Fruits & Vegetables', 1, '', 0, 0);");
        db.execSQL("INSERT INTO " + ServiceTypesEntry.TABLE_NAME + " VALUES(7, 7, 'Flowers', 1, '', 0, 0);");
        db.execSQL("INSERT INTO " + ServiceTypesEntry.TABLE_NAME + " VALUES(8, 8, 'Laundry', 1, '', 0, 1);");
    }

    public void renameIdInAllTables(SQLiteDatabase db) {
        Cursor cursor;
        int idColumnIndex;

        cursor = db.rawQuery("SELECT * FROM " + UserDetailsEntry.TABLE_NAME, null); // grab cursor for all data
        idColumnIndex = cursor.getColumnIndex("id");  // see if the column is there
        if (idColumnIndex >= 0) {
            // missing_column not there - add it
            db.execSQL("ALTER TABLE " + UserDetailsEntry.TABLE_NAME + " RENAME TO tmp_table_name;");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + UserDetailsEntry.TABLE_NAME +
                    "(" +
                    UserDetailsEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                    UserDetailsEntry.COLUMN_USER_CODE + " INTEGER, " +
                    UserDetailsEntry.COLUMN_VERIFIED_FLAG + " INTEGER, " +
                    UserDetailsEntry.COLUMN_VERIFIED_NUMBER + " TEXT, " +
                    UserDetailsEntry.COLUMN_EMAIL_FLAG + " INTEGER, " +
                    UserDetailsEntry.COLUMN_EMAIL_ID + " TEXT " +
                    " );");
            db.execSQL("INSERT INTO " + UserDetailsEntry.TABLE_NAME +
                    "(" +
                    UserDetailsEntry._ID + " , " +
                    UserDetailsEntry.COLUMN_USER_CODE + " , " +
                    UserDetailsEntry.COLUMN_VERIFIED_FLAG + " , " +
                    UserDetailsEntry.COLUMN_VERIFIED_NUMBER + " , " +
                    UserDetailsEntry.COLUMN_EMAIL_FLAG + " , " +
                    UserDetailsEntry.COLUMN_EMAIL_ID + " " +
                    ") " +
                    "SELECT " +
                    "id" + " , " +
                    UserDetailsEntry.COLUMN_USER_CODE + " , " +
                    UserDetailsEntry.COLUMN_VERIFIED_FLAG + " , " +
                    UserDetailsEntry.COLUMN_VERIFIED_NUMBER + " , " +
                    UserDetailsEntry.COLUMN_EMAIL_FLAG + " , " +
                    UserDetailsEntry.COLUMN_EMAIL_ID + " " +
                    " FROM tmp_table_name;");
            db.execSQL("DROP TABLE tmp_table_name;");
        }

        cursor = db.rawQuery("SELECT * FROM " + ServiceTypesEntry.TABLE_NAME, null); // grab cursor for all data
        idColumnIndex = cursor.getColumnIndex("id");  // see if the column is there
        if (idColumnIndex >= 0) {
            // missing_column not there - add it
            db.execSQL("ALTER TABLE " + ServiceTypesEntry.TABLE_NAME + " RENAME TO tmp_table_name;");
            db.execSQL("CREATE TABLE " + ServiceTypesEntry.TABLE_NAME +
                    "(" +
                    ServiceTypesEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                    ServiceTypesEntry.COLUMN_ST_CODE + " INTEGER, " +
                    ServiceTypesEntry.COLUMN_ST_NAME + " TEXT, " +
                    ServiceTypesEntry.COLUMN_ST_IMG_FLAG + " INTEGER, " +
                    ServiceTypesEntry.COLUMN_ST_IMG_ADD + " TEXT, " +
                    ServiceTypesEntry.COLUMN_ST_FLAG + " INTEGER DEFAULT 0 " +
                    " );");
            // seller automation flag added in schema in version 5 here itself..
            // a kind of hack, everybody welcome to fix

            db.execSQL("INSERT INTO " + ServiceTypesEntry.TABLE_NAME +
                    "(" +
                    ServiceTypesEntry._ID + " , " +
                    ServiceTypesEntry.COLUMN_ST_CODE + " , " +
                    ServiceTypesEntry.COLUMN_ST_NAME + " , " +
                    ServiceTypesEntry.COLUMN_ST_IMG_FLAG + " , " +
                    ServiceTypesEntry.COLUMN_ST_IMG_ADD + " , " +
                    ServiceTypesEntry.COLUMN_ST_FLAG + " " +
                    ") " +
                    "SELECT " +
                    " id " + " , " +
                    ServiceTypesEntry.COLUMN_ST_CODE + " , " +
                    ServiceTypesEntry.COLUMN_ST_NAME + " , " +
                    ServiceTypesEntry.COLUMN_ST_IMG_FLAG + " , " +
                    ServiceTypesEntry.COLUMN_ST_IMG_ADD + " , " +
                    ServiceTypesEntry.COLUMN_ST_FLAG + "  " +
                    " FROM tmp_table_name;");
            db.execSQL("DROP TABLE tmp_table_name;");
        }

        cursor = db.rawQuery("SELECT * FROM " + UserAddressDetailsEntry.TABLE_NAME, null); // grab cursor for all data
        idColumnIndex = cursor.getColumnIndex("id");  // see if the column is there
        if (idColumnIndex >= 0) {
            // missing_column not there - add it
            db.execSQL("ALTER TABLE " + UserAddressDetailsEntry.TABLE_NAME + " RENAME TO tmp_table_name;");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + UserAddressDetailsEntry.TABLE_NAME +
                    "(" +
                    UserAddressDetailsEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                    UserAddressDetailsEntry.COLUMN_USER_CODE + " INTEGER, " +
                    UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE + " INTEGER, " +
                    UserAddressDetailsEntry.COLUMN_USER_ADDRESS + " TEXT, " +
                    UserAddressDetailsEntry.COLUMN_USER_NAME + " TEXT, " +
                    UserAddressDetailsEntry.COLUMN_USER_EMAIL_ID + " TEXT, " +
                    UserAddressDetailsEntry.COLUMN_USER_PHONE_NUMBER + " TEXT," +
                    UserAddressDetailsEntry.COLUMN_USER_FLAT + " TEXT," +
                    UserAddressDetailsEntry.COLUMN_USER_SUBAREA + " TEXT ," +
                    UserAddressDetailsEntry.COLUMN_USER_AREA + " TEXT " +
                    " );");
            db.execSQL("INSERT INTO " + UserAddressDetailsEntry.TABLE_NAME +
                    "(" +
                    UserAddressDetailsEntry._ID + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_CODE + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_ADDRESS + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_NAME + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_EMAIL_ID + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_PHONE_NUMBER + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_FLAT + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_SUBAREA + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_AREA + " " +
                    ") " +
                    "SELECT " +
                    " id " + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_CODE + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_ADDRESS + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_NAME + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_EMAIL_ID + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_PHONE_NUMBER + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_FLAT + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_SUBAREA + " , " +
                    UserAddressDetailsEntry.COLUMN_USER_AREA + " " +
                    " FROM tmp_table_name;");
            db.execSQL("DROP TABLE tmp_table_name;");
        }

        cursor = db.rawQuery("SELECT * FROM " + ItemDetailsEntry.TABLE_NAME, null); // grab cursor for all data
        idColumnIndex = cursor.getColumnIndex("id");  // see if the column is there
        if (idColumnIndex >= 0) {
            // missing_column not there - add it
            db.execSQL("ALTER TABLE " + ItemDetailsEntry.TABLE_NAME + " RENAME TO tmp_table_name;");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + ItemDetailsEntry.TABLE_NAME +
                    "(" +
                    ItemDetailsEntry._ID + "  INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                    ItemDetailsEntry.COLUMN_ITEM_CODE + "  TEXT, " +
                    ItemDetailsEntry.COLUMN_ITEM_NAME + "  TEXT, " +
                    ItemDetailsEntry.COLUMN_ITEM_IMG_FLAG + "  INTEGER, " +
                    ItemDetailsEntry.COLUMN_ITEM_IMG_ADDRESS + "  TEXT, " +
                    ItemDetailsEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                    ItemDetailsEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                    ItemDetailsEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                    ItemDetailsEntry.COLUMN_ITEM_DESC + " TEXT, " +
                    ItemDetailsEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                    ItemDetailsEntry.COLUMN_ITEM_SELECTED + " INTEGER, " +
                    ItemDetailsEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                    ItemDetailsEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                    ItemDetailsEntry.COLUMN_ITEM_SC_CODE + " TEXT, " +
                    ItemDetailsEntry.COLUMN_ITEM_MRP + " DOUBLE" +
                    " );");
            db.execSQL("INSERT INTO " + ItemDetailsEntry.TABLE_NAME +
                    "(" +
                    ItemDetailsEntry._ID + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_CODE + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_NAME + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_IMG_FLAG + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_IMG_ADDRESS + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_UNIT + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_COST + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_SHORT_DESC + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_DESC + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_STATUS + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_SELECTED + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_QUANTITY_SELECTED + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_ST_CODE + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_SC_CODE + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_MRP + " " +
                    ") " +
                    "SELECT " +
                    " id " + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_CODE + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_NAME + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_IMG_FLAG + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_IMG_ADDRESS + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_UNIT + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_COST + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_SHORT_DESC + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_DESC + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_STATUS + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_SELECTED + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_QUANTITY_SELECTED + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_ST_CODE + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_SC_CODE + " , " +
                    ItemDetailsEntry.COLUMN_ITEM_MRP + " " +
                    " FROM tmp_table_name;");
            db.execSQL("DROP TABLE tmp_table_name;");
        }

        cursor = db.rawQuery("SELECT * FROM " + ShoppingCartEntry.TABLE_NAME, null); // grab cursor for all data
        idColumnIndex = cursor.getColumnIndex("id");  // see if the column is there
        if (idColumnIndex >= 0) {
            // missing_column not there - add it
            db.execSQL("ALTER TABLE " + ShoppingCartEntry.TABLE_NAME + " RENAME TO tmp_table_name;");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + ShoppingCartEntry.TABLE_NAME +
                    "(" +
                    ShoppingCartEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                    ShoppingCartEntry.COLUMN_ITEM_CODE + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_NAME + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                    ShoppingCartEntry.COLUMN_ITEM_IMG_ADDRESS + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                    ShoppingCartEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_DESC + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_SELECTED + " INT, " +
                    ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                    ShoppingCartEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_SC_CODE + " TEXT, " +
                    ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " TEXT " +
                    " );");
            db.execSQL("INSERT INTO " + ShoppingCartEntry.TABLE_NAME +
                    "(" +
                    ShoppingCartEntry._ID + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_CODE + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_NAME + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_IMG_FLAG + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_IMG_ADDRESS + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_UNIT + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_COST + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_SHORT_DESC + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_DESC + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_STATUS + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_SELECTED + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_ST_CODE + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_SC_CODE + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " " +
                    ") " +
                    "SELECT " +
                    " id " + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_CODE + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_NAME + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_IMG_FLAG + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_IMG_ADDRESS + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_UNIT + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_COST + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_SHORT_DESC + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_DESC + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_STATUS + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_SELECTED + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_ST_CODE + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_SC_CODE + " , " +
                    ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " " +
                    " FROM tmp_table_name;");
            db.execSQL("DROP TABLE tmp_table_name;");
        }

        cursor = db.rawQuery("SELECT * FROM " + ServiceTypeCategoriesEntry.TABLE_NAME, null); // grab cursor for all data
        idColumnIndex = cursor.getColumnIndex("id");  // see if the column is there
        if (idColumnIndex >= 0) {
            // missing_column not there - add it
            db.execSQL("ALTER TABLE " + ServiceTypeCategoriesEntry.TABLE_NAME + " RENAME TO tmp_table_name;");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + ServiceTypeCategoriesEntry.TABLE_NAME +
                    "(" +
                    ServiceTypeCategoriesEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                    ServiceTypeCategoriesEntry.COLUMN_ST_CODE + " TEXT, " +
                    ServiceTypeCategoriesEntry.COLUMN_SC_CODE + " TEXT, " +
                    ServiceTypeCategoriesEntry.COLUMN_SC_NAME + " TEXT " +
                    " );");
            db.execSQL("INSERT INTO " + ServiceTypeCategoriesEntry.TABLE_NAME +
                    "(" +
                    ServiceTypeCategoriesEntry._ID + " , " +
                    ServiceTypeCategoriesEntry.COLUMN_ST_CODE + " , " +
                    ServiceTypeCategoriesEntry.COLUMN_SC_CODE + " , " +
                    ServiceTypeCategoriesEntry.COLUMN_SC_NAME + " " +
                    ") " +
                    "SELECT " +
                    " id " + " , " +
                    ServiceTypeCategoriesEntry.COLUMN_ST_CODE + " , " +
                    ServiceTypeCategoriesEntry.COLUMN_SC_CODE + " , " +
                    ServiceTypeCategoriesEntry.COLUMN_SC_NAME + " " +
                    " FROM tmp_table_name;");
            db.execSQL("DROP TABLE tmp_table_name;");
        }

        cursor = db.rawQuery("SELECT * FROM " + SearchResultEntry.TABLE_NAME, null); // grab cursor for all data
        idColumnIndex = cursor.getColumnIndex("id");  // see if the column is there
        if (idColumnIndex >= 0) {
            // missing_column not there - add it
            db.execSQL("ALTER TABLE " + SearchResultEntry.TABLE_NAME + " RENAME TO tmp_table_name;");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + SearchResultEntry.TABLE_NAME +
                    "(" +
                    SearchResultEntry._ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                    SearchResultEntry.COLUMN_ITEM_CODE + " TEXT, " +
                    SearchResultEntry.COLUMN_ITEM_NAME + " TEXT, " +
                    SearchResultEntry.COLUMN_ITEM_IMG_FLAG + " INTEGER, " +
                    SearchResultEntry.COLUMN_ITEM_IMG_ADDRESS + " TEXT, " +
                    SearchResultEntry.COLUMN_ITEM_UNIT + " TEXT, " +
                    SearchResultEntry.COLUMN_ITEM_COST + " DOUBLE, " +
                    SearchResultEntry.COLUMN_ITEM_SHORT_DESC + " TEXT, " +
                    SearchResultEntry.COLUMN_ITEM_DESC + " TEXT, " +
                    SearchResultEntry.COLUMN_ITEM_STATUS + " TEXT, " +
                    SearchResultEntry.COLUMN_ITEM_SELECTED + " INTEGER, " +
                    SearchResultEntry.COLUMN_ITEM_QUANTITY_SELECTED + " INTEGER, " +
                    SearchResultEntry.COLUMN_ITEM_ST_CODE + " TEXT, " +
                    SearchResultEntry.COLUMN_ITEM_SC_CODE + " TEXT, " +
                    SearchResultEntry.COLUMN_ITEM_MRP + " DOUBLE, " +
                    SearchResultEntry.COLUMN_SEARCH_STRING + " TEXT" +
                    " );");
            db.execSQL("INSERT INTO " + SearchResultEntry.TABLE_NAME +
                    "(" +
                    SearchResultEntry._ID + " , " +
                    SearchResultEntry.COLUMN_ITEM_CODE + " , " +
                    SearchResultEntry.COLUMN_ITEM_NAME + " , " +
                    SearchResultEntry.COLUMN_ITEM_IMG_FLAG + " , " +
                    SearchResultEntry.COLUMN_ITEM_IMG_ADDRESS + " , " +
                    SearchResultEntry.COLUMN_ITEM_UNIT + " , " +
                    SearchResultEntry.COLUMN_ITEM_COST + " , " +
                    SearchResultEntry.COLUMN_ITEM_SHORT_DESC + " , " +
                    SearchResultEntry.COLUMN_ITEM_DESC + " , " +
                    SearchResultEntry.COLUMN_ITEM_STATUS + " , " +
                    SearchResultEntry.COLUMN_ITEM_SELECTED + " , " +
                    SearchResultEntry.COLUMN_ITEM_QUANTITY_SELECTED + " , " +
                    SearchResultEntry.COLUMN_ITEM_ST_CODE + " , " +
                    SearchResultEntry.COLUMN_ITEM_SC_CODE + " , " +
                    SearchResultEntry.COLUMN_ITEM_MRP + " , " +
                    SearchResultEntry.COLUMN_SEARCH_STRING + " " +
                    ") " +
                    "SELECT " +
                    " id " + " , " +
                    SearchResultEntry.COLUMN_ITEM_CODE + " , " +
                    SearchResultEntry.COLUMN_ITEM_NAME + " , " +
                    SearchResultEntry.COLUMN_ITEM_IMG_FLAG + " , " +
                    SearchResultEntry.COLUMN_ITEM_IMG_ADDRESS + " , " +
                    SearchResultEntry.COLUMN_ITEM_UNIT + " , " +
                    SearchResultEntry.COLUMN_ITEM_COST + " , " +
                    SearchResultEntry.COLUMN_ITEM_SHORT_DESC + " , " +
                    SearchResultEntry.COLUMN_ITEM_DESC + " , " +
                    SearchResultEntry.COLUMN_ITEM_STATUS + " , " +
                    SearchResultEntry.COLUMN_ITEM_SELECTED + " , " +
                    SearchResultEntry.COLUMN_ITEM_QUANTITY_SELECTED + " , " +
                    SearchResultEntry.COLUMN_ITEM_ST_CODE + " , " +
                    SearchResultEntry.COLUMN_ITEM_SC_CODE + " , " +
                    SearchResultEntry.COLUMN_ITEM_MRP + " , " +
                    SearchResultEntry.COLUMN_SEARCH_STRING + " " +
                    " FROM tmp_table_name;");
            db.execSQL("DROP TABLE tmp_table_name;");
        }

    }
}
