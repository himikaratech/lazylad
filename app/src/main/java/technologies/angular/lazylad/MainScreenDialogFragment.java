package technologies.angular.lazylad;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

/**
 * Created by Amud on 09/09/15.
 */
public class MainScreenDialogFragment extends DialogFragment {
    ViewPager pager;
    OfferPagerAdapter adapter;
    int Numboftabs = 2;
    Context m_context;
    Toolbar toolbar;
    private ImageView dotImage;
    Runnable runnable;
    private Handler handler = new Handler();
    static int[] position = new int[1];
    int index;
    boolean stopScroll = false;
    ArrayList<offerSnippet> offer_details_list;

    public MainScreenDialogFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        offer_details_list = getArguments().getParcelableArrayList("offer_list");
        position[0] = offer_details_list.size();
        Numboftabs = offer_details_list.size();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Dialog d = getDialog();
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Drawable drawable = new ColorDrawable(Color.parseColor("#33FFFFFF"));
        d.getWindow().setBackgroundDrawable(drawable);
        final View rootView = inflater.inflate(R.layout.main_activity_fragment_dialog, container, false);

        LinearLayout linearLayout1 = (LinearLayout) rootView.findViewById(R.id.dotLinearLayout);
        LinearLayout.LayoutParams layoutParams;
        layoutParams = new LinearLayout.LayoutParams(12, 12);
        layoutParams.setMargins(10, 0, 0, 0);
        linearLayout1.removeAllViews();

        for (int x = 1; x <= Numboftabs; x++) {
            Log.e("Numboftabs1", Numboftabs + "");
            ImageView image = new ImageView(getActivity());
            image.setId(x);
            Log.e("image", image + "");
            linearLayout1.addView(image, layoutParams);
            dotImage = (ImageView) rootView.findViewById(x);
            dotImage.setImageResource(R.drawable.active);
        }

        dotImage = (ImageView) rootView.findViewById(1);
        dotImage.setImageResource(R.drawable.inactive);

        ImageView closeView = (ImageView) rootView.findViewById(R.id.close);

        closeView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Dialog d = getDialog();
                handler.removeCallbacks(runnable);
                d.dismiss();
                return false;
            }
        });

        View dismissview = (View) rootView.findViewById(R.id.DismissLinearLayout);
        dismissview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Dialog d = getDialog();
                handler.removeCallbacks(runnable);
                d.dismiss();
                return true;
            }
        });

        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar_main);

        // Creating The MainScreenPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) rootView.findViewById(R.id.pageID);

        pager.setAdapter(buildAdapter());

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                stopScroll = true;
                Log.e("logging2", position + "");
                position = position + 1;
                for (int i = 1; i <= Numboftabs; i++) {
                    dotImage = (ImageView) rootView.findViewById(i);
                    dotImage.setImageResource(R.drawable.active);
                }
                dotImage = (ImageView) rootView.findViewById(position);
                dotImage.setImageResource(R.drawable.inactive);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        timerFunction();

        return rootView;
    }

    public void timerFunction() {

        runnable = new Runnable() {
            @Override
            public void run() {
                Log.e("runnable", (position[0]) + "");
                pager.setCurrentItem(position[0], true);
                //  pager.setCurrentItem(position-1, true);
                position[0]++;
                position[0] = position[0] % Numboftabs;


                index = position[0] + 1;
                for (int i = 1; i <= Numboftabs; i++) {
                    dotImage = (ImageView) getView().findViewById(i);
                    dotImage.setImageResource(R.drawable.active);
                }
                dotImage = (ImageView) getView().findViewById(index);
                dotImage.setImageResource(R.drawable.inactive);
                if (stopScroll == false)
                    timerFunction();
            }
        };
        handler = new Handler();
        handler.postDelayed(runnable, 5000);
    }

    private PagerAdapter buildAdapter() {
        Log.e("buildAdapter", "buildAdapter");
        return (new OfferPagerAdapter(pager, getActivity(), getChildFragmentManager(), offer_details_list));
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onPause() {
        Log.e("DEBUG", "OnPause of loginFragment");
        super.onPause();
        //handler.removeCallbacks(runnable);
    }
}
