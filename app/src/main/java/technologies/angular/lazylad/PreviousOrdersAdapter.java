package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

/**
 * Created by Saurabh on 24/02/15.
 */
public class PreviousOrdersAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_LAZYLAD_SERVICE = 0;
    private static final int VIEW_TYPE_LAZYLAD_ORDER = 1;

    private class ViewHolder {
        TextView orderCodeTextView;
        TextView orderDateTextView;
        TextView orderAmountTextView;
        TextView tobePaidAmountTextView;
        TextView orderStatusTextView;
        TextView orderServProvNameTextView;
        LinearLayout header;
        ImageView received_imageview;
        ImageView confirmed_imageview;
        ImageView delivered_imageview;
        View confirmed_line;
        View delivered_line;
        Button cancel_order_button;
        Button rate_order;
        Button viewBill;
        Button view_details;
        TextView cancelledText;
    }

    private Activity m_activity;
    private ArrayList<PreviousOrdersSnippet> m_prevOrderSnippet;

    public PreviousOrdersAdapter(Activity activity, ArrayList<PreviousOrdersSnippet> prevOrderSnippet) {
        m_activity = activity;
        m_prevOrderSnippet = prevOrderSnippet;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_prevOrderSnippet != null) {
            count = m_prevOrderSnippet.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_prevOrderSnippet != null) {
            return m_prevOrderSnippet.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_prevOrderSnippet != null) {
            return position;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_prevOrderSnippet != null) {
            final int positionFinal = position;
            final ViewHolder holder;

            PreviousOrdersSnippet previousOrdersSnippet = m_prevOrderSnippet.get(position);
            final String orderCode = previousOrdersSnippet.m_orderString;
            String orderDate = previousOrdersSnippet.m_orderDate;
            String orderStatus = previousOrdersSnippet.m_orderStatus;
            //String orderStatus = "Delivered";
            String servProvName = previousOrdersSnippet.m_orderServProvName;

            final Double paid = previousOrdersSnippet.paid;
            final Double to_be_paid = previousOrdersSnippet.to_be_paid;
            final Double orderAmount = paid + to_be_paid;

            int viewType = getItemViewType(position);

            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (viewType == VIEW_TYPE_LAZYLAD_SERVICE) {
                    convertView = infalInflater.inflate(R.layout.previous_order_service_list_item, parent, false);
                } else {
                    convertView = infalInflater.inflate(R.layout.previous_orders_list, parent, false);
                }

                holder.orderCodeTextView = (TextView) convertView.findViewById(R.id.order_code);
                holder.orderDateTextView = (TextView) convertView.findViewById(R.id.order_date);
                holder.orderAmountTextView = (TextView) convertView.findViewById(R.id.order_amount);
                holder.orderStatusTextView = (TextView) convertView.findViewById(R.id.order_status);
                holder.orderServProvNameTextView = (TextView) convertView.findViewById(R.id.order_store);
                holder.header = (LinearLayout) convertView.findViewById(R.id.head_order);
                holder.received_imageview = (ImageView) convertView.findViewById(R.id.received_image);
                holder.confirmed_imageview = (ImageView) convertView.findViewById(R.id.confirmed_image);
                holder.delivered_imageview = (ImageView) convertView.findViewById(R.id.delivered_image);
                holder.confirmed_line = (View) convertView.findViewById(R.id.confirmed_line);
                holder.delivered_line = (View) convertView.findViewById(R.id.delivered_line);
                holder.cancel_order_button = (Button) convertView.findViewById(R.id.cancel_prev_order_button);
                holder.rate_order = (Button) convertView.findViewById(R.id.rate_order);
                holder.viewBill = (Button) convertView.findViewById(R.id.view_bill);
                holder.view_details = (Button) convertView.findViewById(R.id.view_details);
                holder.cancelledText = (TextView) convertView.findViewById(R.id.cancelled_text);
                holder.tobePaidAmountTextView = (TextView) convertView.findViewById(R.id.to_be_paid);


                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
                holder.received_imageview.setImageResource(R.drawable.inactive_state);
                holder.confirmed_imageview.setImageResource(R.drawable.inactive_state);
                holder.delivered_imageview.setImageResource(R.drawable.inactive_state);
                holder.confirmed_line.setBackgroundColor(m_activity.getResources().getColor(R.color.tick_blank));
                holder.delivered_line.setBackgroundColor(m_activity.getResources().getColor(R.color.tick_blank));
                holder.cancel_order_button.setVisibility(View.VISIBLE);
                holder.cancelledText.setVisibility(View.INVISIBLE);
                holder.viewBill.setVisibility(View.INVISIBLE);
                holder.rate_order.setVisibility(View.INVISIBLE);
                holder.view_details.setVisibility(View.VISIBLE);
            }

            if (paid.compareTo(0.0) == 0)
                holder.tobePaidAmountTextView.setVisibility(View.GONE);
            holder.orderCodeTextView.setText(orderCode.toString());
            holder.orderDateTextView.setText(orderDate.toString());
            holder.orderAmountTextView.setText("TOTAL : ₹ " + Double.toString(orderAmount));
            holder.tobePaidAmountTextView.setText("COD : ₹ " + Double.toString(to_be_paid));
            holder.orderStatusTextView.setText(orderStatus.toString());
            holder.orderServProvNameTextView.setText(servProvName);

            Log.i("order status", previousOrdersSnippet.m_orderStatus);


            if (orderStatus.equals("Cancelled")) {
                holder.cancel_order_button.setVisibility(View.GONE);
                holder.cancelledText.setVisibility(View.VISIBLE);
                holder.received_imageview.setImageResource(R.drawable.inactive_state);
                holder.view_details.setVisibility(View.GONE);

            }
            if (orderStatus.equals("Payment_Failed")) {
                holder.cancel_order_button.setVisibility(View.GONE);
                holder.cancelledText.setText("Payment Failed");
                holder.cancelledText.setVisibility(View.VISIBLE);
                holder.received_imageview.setImageResource(R.drawable.inactive_state);
                holder.view_details.setVisibility(View.GONE);

            }
            if (orderStatus.equals("Pending")) {
                holder.received_imageview.setImageResource(R.drawable.done_state);
            }
            if (orderStatus.equals("Confirmed")) {
                holder.viewBill.setVisibility(View.VISIBLE);
                holder.received_imageview.setImageResource(R.drawable.active_state);
                holder.confirmed_imageview.setImageResource(R.drawable.done_state);
                holder.confirmed_line.setBackgroundColor(m_activity.getResources().getColor(R.color.primaryColor));
                holder.cancel_order_button.setVisibility(View.INVISIBLE);
            }
            if (orderStatus.equals("Delivered")) {
                holder.viewBill.setVisibility(View.VISIBLE);
                holder.rate_order.setVisibility(View.VISIBLE);
                holder.received_imageview.setImageResource(R.drawable.active_state);
                holder.confirmed_imageview.setImageResource(R.drawable.active_state);
                holder.delivered_imageview.setImageResource(R.drawable.done_state);
                holder.confirmed_line.setBackgroundColor(m_activity.getResources().getColor(R.color.primaryColor));
                holder.delivered_line.setBackgroundColor(m_activity.getResources().getColor(R.color.primaryColor));
                holder.cancel_order_button.setVisibility(View.INVISIBLE);
            }


            holder.cancel_order_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelThisOrder(orderCode);
                    ((PreviousOrdersSnippet) getItem(positionFinal)).m_orderStatus = "Cancelled";
                    holder.received_imageview.setImageResource(R.drawable.inactive_state);
                    holder.cancel_order_button.setText("CANCELLED");
                    holder.cancel_order_button.setEnabled(false);
                    holder.cancel_order_button.setTextColor(m_activity.getResources().getColor(R.color.white));
                    holder.cancel_order_button.setBackgroundColor(m_activity.getResources().getColor(R.color.red));
                    holder.received_imageview.setImageResource(R.drawable.inactive_state);
                    holder.view_details.setVisibility(View.GONE);

                    PayloadBuilder builder = new PayloadBuilder();
                    builder.putAttrString("Order Code", orderCode);
                    builder.putAttrString("Activity Name", this.getClass().getSimpleName());
                    MoEHelper.getInstance(m_activity).trackEvent("Cancel Order", builder.build());

                }
            });


            holder.view_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(m_activity, PreviousOrderDetailsNew.class);
                    String data[] = {orderCode};
                    intent.putExtra("data_string_array", data);
                    m_activity.startActivity(intent);
                    PayloadBuilder builder = new PayloadBuilder();
                    builder.putAttrString("Order Code", orderCode);
                    builder.putAttrString("Activity Name", this.getClass().getSimpleName());
                    MoEHelper.getInstance(m_activity).trackEvent("View Details", builder.build());

                }
            });

            holder.viewBill.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(m_activity, BillActivity.class);
                    intent.putExtra("PREV_ORDER_CODE", orderCode);
                    m_activity.startActivity(intent);
                    PayloadBuilder builder = new PayloadBuilder();
                    builder.putAttrString("Order Code", orderCode);
                    builder.putAttrString("Activity Name", this.getClass().getSimpleName());
                    MoEHelper.getInstance(m_activity).trackEvent("View Bill", builder.build());

                }
            });

            holder.rate_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(m_activity, RatingActivity.class);
                    intent.putExtra("ORDER_CODE", orderCode);
                    m_activity.startActivity(intent);
                    PayloadBuilder builder = new PayloadBuilder();
                    builder.putAttrString("Order Code", orderCode);
                    builder.putAttrString("Activity Name", this.getClass().getSimpleName());
                    MoEHelper.getInstance(m_activity).trackEvent("Rate Order", builder.build());

                }
            });

        }
        return convertView;
    }

    private void cancelThisOrder(String order_code) {

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.CancelOrderRequestModel cancelOrderRequestModel = requestModel.new CancelOrderRequestModel();
        cancelOrderRequestModel.order_code = order_code;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        apiSuggestionsService.cancelOrderAPICall(cancelOrderRequestModel, new retrofit.Callback<APIResonseModel.CancelOrderResponseModel>() {

            @Override
            public void success(APIResonseModel.CancelOrderResponseModel cancelOrderResponseModel, retrofit.client.Response response) {

                int success = cancelOrderResponseModel.error;
                if (success == 1) {


                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }


            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");

            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = VIEW_TYPE_LAZYLAD_ORDER;
        PreviousOrdersSnippet previousOrdersSnippet = m_prevOrderSnippet.get(position);
        switch (previousOrdersSnippet.order_type) {
            case "Lazylad Service":
                viewType = VIEW_TYPE_LAZYLAD_SERVICE;
                break;
            case "Lazylad Order":
                viewType = VIEW_TYPE_LAZYLAD_ORDER;
                break;
        }
        return viewType;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }
}