package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

/**
 * Created by Amud on 07/10/15.
 */
public class PreviousOrderDetailsNew extends ActionBarActivity {

    TextView m_addressTextView;
    TextView m_userNameTextView;
    TextView m_userPhoneNumTextView;
    Toolbar toolbar;

    private String m_userCode;
    private static String m_orderCode;

    private ArrayList<PreviousOrdersDetailsSnippet> m_previousOrdersDetailsArraylist;
    private ArrayList<PreviousOrdersDetailsSnippet> m_previousConfirmOrdersDetailsArraylist;
    private PreviousOrdersUserDetailsSnippetNew m_previousOrdersUserDetailsSnippetNewArrayList;
    private PreviousOrdersDetailsAdapter m_prevOrdDetAdapter;
    private ListView m_previousOrdersDetailsListView;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.previous_order_details_new);
        Intent intent = getIntent();
        String data[] = intent.getStringArrayExtra("data_string_array");
        m_orderCode = data[0];
        m_previousOrdersDetailsArraylist = new ArrayList<PreviousOrdersDetailsSnippet>();
        m_previousConfirmOrdersDetailsArraylist = new ArrayList<PreviousOrdersDetailsSnippet>();

        m_previousOrdersDetailsListView = (ListView) findViewById(R.id.previous_orders_details_listview);
        m_addressTextView = (TextView) findViewById(R.id.user_address_in_list);
        m_userNameTextView = (TextView) findViewById(R.id.user_name);
        m_userPhoneNumTextView = (TextView) findViewById(R.id.user_phone_number);
        Cursor m_cursor = this.getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        toolbar = (Toolbar) findViewById(R.id.toolbar_card);
        if (toolbar != null)
            toolbar.inflateMenu(R.menu.card_toolbar);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_userCode = String.valueOf(m_cursor.getInt(1));
                    m_cursor.moveToNext();
                }
            }
        }
        loadOrderPaymentDetails();
        mHelper = new MoEHelper(this);
    }

    private void loadOrderPaymentDetails() {

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.LoadPaymentDetailsRequestModel loadPaymentDetailsRequestModel = requestModel.new LoadPaymentDetailsRequestModel();
        loadPaymentDetailsRequestModel.user_code = m_userCode;
        loadPaymentDetailsRequestModel.order_code = m_orderCode;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();
        apiSuggestionsService.loadPaymentDetailsAPICall(loadPaymentDetailsRequestModel, new Callback<APIResonseModel.LoadPaymentDetailsResponseModel>() {
            @Override
            public void success(APIResonseModel.LoadPaymentDetailsResponseModel getWalletDetailsResponseModel, Response response) {

                int success = getWalletDetailsResponseModel.error_code;
                if (success == 1) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    String user_name = getWalletDetailsResponseModel.order_delivery_details.user_name;
                    String user_address = getWalletDetailsResponseModel.order_delivery_details.user_address;
                    String user_phone_number = getWalletDetailsResponseModel.order_delivery_details.user_phone_number;
                    Double wallet_paid = getWalletDetailsResponseModel.order_payment_details.wallet_paid;
                    Double to_be_paid = getWalletDetailsResponseModel.order_payment_details.to_be_paid;
                    Double discount_amount = getWalletDetailsResponseModel.order_payment_details.discount_amount;
                    String coupon_code = getWalletDetailsResponseModel.order_payment_details.coupon_code;
                    Double paid = getWalletDetailsResponseModel.order_payment_details.paid;
                    m_previousOrdersUserDetailsSnippetNewArrayList = new PreviousOrdersUserDetailsSnippetNew(user_name, user_address, user_phone_number, to_be_paid, wallet_paid,discount_amount,coupon_code,paid);

                    loadPreviousOrderDetails();
                }
                else {
                    if(progressDialog.isShowing())
                    progressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("RetrofitError", error.toString());
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    private void loadPreviousOrderDetails() {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.loadPreviousOrderDetails2APICall(m_orderCode, m_userCode, new retrofit.Callback<APIResonseModel.LoadPreviousOrderDetails2ResponseModel>() {

            @Override
            public void success(APIResonseModel.LoadPreviousOrderDetails2ResponseModel loadPreviousOrderDetailsResponseModel, retrofit.client.Response response) {
                if (loadPreviousOrderDetailsResponseModel.error == 1) {
                    int numOfItemsInPrevOrder = loadPreviousOrderDetailsResponseModel.previous_orders_details.size();
                    m_previousOrdersDetailsArraylist = new ArrayList<PreviousOrdersDetailsSnippet>(numOfItemsInPrevOrder);

                    for (int i = 0; i < numOfItemsInPrevOrder; i++) {
                        int prevOrderItemId = i + 1;
                        String m_itemCode = loadPreviousOrderDetailsResponseModel.previous_orders_details.get(i).item_code;
                        String m_itemName = loadPreviousOrderDetailsResponseModel.previous_orders_details.get(i).item_name;
                        int m_itemImgFlag = loadPreviousOrderDetailsResponseModel.previous_orders_details.get(i).item_img_flag;
                        String m_itemImgAdd = loadPreviousOrderDetailsResponseModel.previous_orders_details.get(i).item_img_address;
                        String m_itemShortDesc = loadPreviousOrderDetailsResponseModel.previous_orders_details.get(i).item_short_desc;
                        int m_itemQuantity = loadPreviousOrderDetailsResponseModel.previous_orders_details.get(i).item_quantity;
                        double m_itemCost = loadPreviousOrderDetailsResponseModel.previous_orders_details.get(i).item_cost;
                        String m_itemDesc = loadPreviousOrderDetailsResponseModel.previous_orders_details.get(i).item_description;

                        PreviousOrdersDetailsSnippet prevOrdObj = new PreviousOrdersDetailsSnippet(prevOrderItemId, m_itemCode, m_itemName, m_itemImgFlag, m_itemImgAdd, m_itemShortDesc
                                , m_itemQuantity, m_itemCost, m_itemDesc, false);
                        m_previousOrdersDetailsArraylist.add(prevOrdObj);
                    }
                    loadPreviousConfirmOrderDetails();
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    private void loadPreviousConfirmOrderDetails() {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.loadPreviousOrderDetails1APICall(m_orderCode, m_userCode, new retrofit.Callback<APIResonseModel.LoadPreviousOrderDetails1ResponseModel>() {

            @Override
            public void success(APIResonseModel.LoadPreviousOrderDetails1ResponseModel loadPreviousOrderDetailsResponseModel, retrofit.client.Response response) {
                if (loadPreviousOrderDetailsResponseModel.error == 1) {
                    int numOfItemsInPrevOrder = loadPreviousOrderDetailsResponseModel.previous_orders_confirmed_details.size();
                    m_previousConfirmOrdersDetailsArraylist = new ArrayList<PreviousOrdersDetailsSnippet>(numOfItemsInPrevOrder);
                    for (int i = 0; i < loadPreviousOrderDetailsResponseModel.previous_orders_confirmed_details.size(); i++) {
                        String m_itemCode = loadPreviousOrderDetailsResponseModel.previous_orders_confirmed_details.get(i).item_code;
                        for (int j = 0; j < m_previousOrdersDetailsArraylist.size(); j++) {
                            if (m_previousOrdersDetailsArraylist.get(j).m_itemCode.equals(m_itemCode)) {
                                m_previousOrdersDetailsArraylist.get(j).m_b_confirmed = true;
                            }
                        }
                    }
                    m_prevOrdDetAdapter = new PreviousOrdersDetailsAdapter(PreviousOrderDetailsNew.this, m_previousOrdersDetailsArraylist, m_previousOrdersUserDetailsSnippetNewArrayList);
                    m_previousOrdersDetailsListView.setAdapter((ListAdapter) m_prevOrdDetAdapter);
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
