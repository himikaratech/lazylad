package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

public class BillActivity extends ActionBarActivity {

    private String m_custName;
    private String m_custAdd;
    private String m_orderNumber;
    private double m_totalCost;
    private double m_payableCost;
    private double m_deliveryCost;
    private double m_discountAmount;
    private double m_taxes;
    private int m_userCode;
    private String m_orderCode;

    private ArrayList<BillItemsSnippet> m_itemsDetails;
    private BillItemsAdapter m_billItemsAdapter;
    private ListView m_billItemsListView;
    private TextView m_custNameTextView;
    private TextView m_custAddTextView;
    private TextView m_orderNumberTextView;
    private TextView m_totalCostTextView;
    private TextView m_payableCostTextView;
    private TextView m_deliveryCostTextView;
    private TextView m_discountCostTextView;
    private TextView m_taxTextView;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bill);
        View card_top = (View) findViewById(R.id.card_top);

        Intent intent = getIntent();
        m_orderCode = intent.getStringExtra("PREV_ORDER_CODE");

        m_billItemsListView = (ListView) findViewById(R.id.cust_item_details_in_bill);
        m_custNameTextView = (TextView) findViewById(R.id.cust_name_in_bill);
        m_custAddTextView = (TextView) findViewById(R.id.cust_add_in_bill);
        m_orderNumberTextView = (TextView) findViewById(R.id.cust_order_num_in_bill);
        m_totalCostTextView = (TextView) findViewById(R.id.cust_total_cost_in_bill);
        m_payableCostTextView = (TextView) findViewById(R.id.cust_total_payable_in_bill);
        m_deliveryCostTextView = (TextView) findViewById(R.id.cust_del_charge_in_bill);
        m_discountCostTextView = (TextView) findViewById(R.id.cust_discount_in_bill);
        m_taxTextView = (TextView) findViewById(R.id.cust_total_tax_in_bill);
        getOrderBill();

        mHelper = new MoEHelper(this);
    }

    private void getOrderBill() {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getOrderBillAPICall(m_orderCode, new retrofit.Callback<APIResonseModel.GetOrderBillResponseModel>() {

            @Override
            public void success(APIResonseModel.GetOrderBillResponseModel getOrderBillResponseModel, retrofit.client.Response response) {
                {
                    m_custName = getOrderBillResponseModel.user_name;
                    m_custAdd = getOrderBillResponseModel.user_address;
                    m_orderNumber = getOrderBillResponseModel.order_code;
                    m_payableCost = getOrderBillResponseModel.order_tot_amount;
                    m_totalCost = getOrderBillResponseModel.order_total_cost;
                    m_deliveryCost = getOrderBillResponseModel.order_delivery_cost;
                    m_discountAmount = getOrderBillResponseModel.order_discount_cost;
                    m_taxes = getOrderBillResponseModel.taxes;

                    m_custNameTextView.setText(m_custName);
                    m_custAddTextView.setText(m_custAdd);
                    m_orderNumberTextView.setText("Order Number: " + m_orderNumber);
                    m_totalCostTextView.setText("₹" + Double.toString(m_totalCost));
                    m_payableCostTextView.setText("₹" + Double.toString(m_payableCost));
                    m_deliveryCostTextView.setText("₹" + Double.toString(m_deliveryCost));
                    m_discountCostTextView.setText("- ₹" + Double.toString(m_discountAmount));
                    m_taxTextView.setText("₹" + Double.toString(m_taxes));


                    m_itemsDetails = new ArrayList<BillItemsSnippet>();
                    m_itemsDetails = getOrderBillResponseModel.order_item_details;
                    m_billItemsAdapter = new BillItemsAdapter(BillActivity.this, m_itemsDetails);
                    m_billItemsListView.setAdapter((ListAdapter) m_billItemsAdapter);
                    progressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bill, menu);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
