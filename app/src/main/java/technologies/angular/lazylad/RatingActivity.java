package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.moe.pushlibrary.MoEHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

/**
 * Created by sakshigupta on 20/11/15.
 */
public class RatingActivity extends ActionBarActivity {

    private RatingBar ratingBar1, ratingBar2, ratingBar3, ratingBar4;
    private Button btnSubmit;
    private String suggestion;
    private String m_orderCode;
    private int m_userCode;

    private MoEHelper mHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating);

        Intent intent = getIntent();
        m_orderCode = intent.getStringExtra("ORDER_CODE");

        m_userCode = Utils.getUserCode(this);

        ratingBar1 = (RatingBar) findViewById(R.id.ratingBar1);
        ratingBar2 = (RatingBar) findViewById(R.id.ratingBar2);
        ratingBar3 = (RatingBar) findViewById(R.id.ratingBar3);
        ratingBar4 = (RatingBar) findViewById(R.id.ratingBar4);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        final EditText suggestionEditText = (EditText) findViewById(R.id.feedback_editView);

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                suggestion = suggestionEditText.getText().toString();
                Log.i("suggestion",suggestion);
                JSONObject rating1JsonObject = new JSONObject();
                try {
                    rating1JsonObject.put("score", ratingBar1.getRating());
                    rating1JsonObject.put("out_of", ratingBar1.getNumStars());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JSONObject rating2JsonObject = new JSONObject();

                try {
                    rating2JsonObject.put("score", ratingBar2.getRating());
                    rating2JsonObject.put("out_of", ratingBar2.getNumStars());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONObject rating3JsonObject = new JSONObject();

                try {
                    rating3JsonObject.put("score", ratingBar3.getRating());
                    rating3JsonObject.put("out_of", ratingBar3.getNumStars());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONObject rating4JsonObject = new JSONObject();

                try {
                    rating4JsonObject.put("score", ratingBar4.getRating());
                    rating4JsonObject.put("out_of", ratingBar4.getNumStars());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                  JSONObject starsJsonObject = new JSONObject();
                try {
                    starsJsonObject.put("Factor1", rating1JsonObject);
                    starsJsonObject.put("Factor2", rating2JsonObject);
                    starsJsonObject.put("Factor3", rating3JsonObject);
                    starsJsonObject.put("Factor4", rating4JsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();

                }

                APIRequestModel requestModel = new APIRequestModel();
                final APIRequestModel.RatingAndReviewRequestModel rateAndReviewRequestModel = requestModel.new RatingAndReviewRequestModel();
                rateAndReviewRequestModel.urid_type = "ORDER";
                rateAndReviewRequestModel.urid = m_orderCode;

                rateAndReviewRequestModel.user_code = Integer.toString(m_userCode);
                rateAndReviewRequestModel.owner_type = "0";

                rateAndReviewRequestModel.feedback = suggestion;
                rateAndReviewRequestModel.rating.Factor1.score = (int)ratingBar1.getRating();
                rateAndReviewRequestModel.rating.Factor2.score = (int)ratingBar2.getRating();
                rateAndReviewRequestModel.rating.Factor3.score = (int)ratingBar3.getRating();
                rateAndReviewRequestModel.rating.Factor4.score = (int)ratingBar4.getRating();
                rateAndReviewRequestModel.rating.Factor1.out_of = ratingBar1.getNumStars();
                rateAndReviewRequestModel.rating.Factor2.out_of = ratingBar2.getNumStars();
                rateAndReviewRequestModel.rating.Factor3.out_of = ratingBar3.getNumStars();
                rateAndReviewRequestModel.rating.Factor4.out_of = ratingBar4.getNumStars();

                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint("http://54.169.62.100")

                        .build();

                final ProgressDialog progressDialog = new ProgressDialog(RatingActivity.this, R.style.MyTheme);
                progressDialog.setCancelable(false);
                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                progressDialog.show();

                APISuggestionsService sendRatingObj = restAdapter.create(APISuggestionsService.class);
                sendRatingObj.ratingAndReviewAPICall(rateAndReviewRequestModel, new Callback<APIResonseModel.RatingAndReviewResponseModel>() {

                    @Override
                    public void success(APIResonseModel.RatingAndReviewResponseModel ratingAndReviewResponseModel, Response response) {

                        boolean success = ratingAndReviewResponseModel.success;

                        if (success == true) {
                            Toast.makeText(RatingActivity.this, "Thank you for your time.", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("RetroError", error.toString());
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            }
        });

        mHelper = new MoEHelper(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
