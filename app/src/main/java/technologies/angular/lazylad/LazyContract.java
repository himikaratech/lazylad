package technologies.angular.lazylad;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Amud on 14/09/15.
 */
public class LazyContract {


    public static final String CONTENT_AUTHORITY = "technologies.angular.lazylad.provider";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_RAW_QUERY = "raw_query";
    public static final String PATH_USER_DETAILS = "user_details";
    public static final String PATH_SERVICE_TYPES = "service_types";
    public static final String PATH_USER_ADD_DETAILS = "user_address_details";
    public static final String PATH_ITEM_DETAILS = "item_details";
    public static final String PATH_SHOPPING_CART = "shopping_cart";
    public static final String PATH_SERVICE_TYPE_CATEGORY = "service_type_categories";
    public static final String PATH_SEARCH_RESULT = "search_result";
    public static final String PATH_SEARCH_QUERY = "search_query";
    public static final String PATH_ORDER_DETAILS = "order_details";
    public static final String PATH_ADDRESS_SEARCH = "address_search";
    public static final String PATH_COUPONS_VIEWS = "coupons_views";
    public static final String PATH_COUPONS_CLICKS = "coupons_clicks";

    public static final String PATH_ITEMS_VIEWS = "items_views";
    public static final String PATH_ITEMS_CLICKS = "items_clicks";


    public static final Uri RAW_CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(PATH_RAW_QUERY).build();

    public static final class UserDetailsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER_DETAILS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_USER_DETAILS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_USER_DETAILS;
        public static final String TABLE_NAME = "user_details";

        public static final String COLUMN_USER_CODE = "user_code";
        public static final String COLUMN_VERIFIED_FLAG = "varified_flag";
        public static final String COLUMN_VERIFIED_NUMBER = "varified_number";
        public static final String COLUMN_EMAIL_FLAG = "email_flag";
        public static final String COLUMN_EMAIL_ID = "email_id";

        public static Uri buildUserDetailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ServiceTypesEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SERVICE_TYPES).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_SERVICE_TYPES;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_SERVICE_TYPES;

        // Table name
        public static final String TABLE_NAME = "service_types";

        public static final String COLUMN_ST_CODE = "st_code";
        public static final String COLUMN_ST_NAME = "st_name";
        public static final String COLUMN_ST_IMG_FLAG = "st_img_flag";
        public static final String COLUMN_ST_IMG_ADD = "st_img_add";
        public static final String COLUMN_ST_FLAG = "st_Flag";
        public static final String COLUMN_ST_IS_SERVICE = "st_is_service";

        public static Uri buildServiceTypesUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class UserAddressDetailsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER_ADD_DETAILS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_USER_ADD_DETAILS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_USER_ADD_DETAILS;

        // Table name
        public static final String TABLE_NAME = "user_address_details";

        public static final String COLUMN_USER_CODE = "user_code";
        public static final String COLUMN_USER_ADDRESS_CODE = "user_address_code";
        public static final String COLUMN_USER_ADDRESS = "user_address";
        public static final String COLUMN_USER_NAME = "user_name";
        public static final String COLUMN_USER_EMAIL_ID = "user_email_id";
        public static final String COLUMN_USER_PHONE_NUMBER = "user_phone_number";
        public static final String COLUMN_USER_FLAT = "user_flat";
        public static final String COLUMN_USER_SUBAREA = "user_subarea";
        public static final String COLUMN_USER_AREA = "user_area";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";

        public static Uri buildUserAddressDetailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ItemDetailsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ITEM_DETAILS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_ITEM_DETAILS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_ITEM_DETAILS;

        // Table name
        public static final String TABLE_NAME = "item_details";

        public static final String COLUMN_ITEM_CODE = "item_code";
        public static final String COLUMN_ITEM_NAME = "item_name";
        public static final String COLUMN_ITEM_IMG_FLAG = "item_img_flag";
        public static final String COLUMN_ITEM_IMG_ADDRESS = "item_img_address";
        public static final String COLUMN_ITEM_UNIT = "item_unit";
        public static final String COLUMN_ITEM_COST = "item_cost";
        public static final String COLUMN_ITEM_SHORT_DESC = "item_short_desc";
        public static final String COLUMN_ITEM_DESC = "item_desc";
        public static final String COLUMN_ITEM_STATUS = "item_status";
        public static final String COLUMN_ITEM_SELECTED = "item_selected";
        public static final String COLUMN_ITEM_QUANTITY_SELECTED = "item_quantity_selected";
        public static final String COLUMN_ITEM_ST_CODE = "item_st_code";
        public static final String COLUMN_ITEM_SC_CODE = "item_sc_code";
        public static final String COLUMN_ITEM_MRP = "item_mrp";
        public static final String COLUMN_ITEM_OFFER_PRICE = "item_offer_price";
        public static final String COLUMN_ITEM_IN_OFFER = "item_in_offer";

        public static Uri buildItemDetailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ShoppingCartEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SHOPPING_CART).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_SHOPPING_CART;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_SHOPPING_CART;

        // Table name
        public static final String TABLE_NAME = "shopping_cart";
        public static final String TABLE_ALIAS_NAME = "cart";

        // The location setting string is what will be sent to openweathermap
        // as the location query.
        public static final String COLUMN_ITEM_CODE = "item_code";
        public static final String COLUMN_ITEM_NAME = "item_name";
        public static final String COLUMN_ITEM_IMG_FLAG = "item_img_flag";
        public static final String COLUMN_ITEM_IMG_ADDRESS = "item_img_address";
        public static final String COLUMN_ITEM_UNIT = "item_unit";
        public static final String COLUMN_ITEM_COST = "item_cost";
        public static final String COLUMN_ITEM_SHORT_DESC = "item_short_desc";
        public static final String COLUMN_ITEM_DESC = "item_desc";
        public static final String COLUMN_ITEM_STATUS = "item_status";
        public static final String COLUMN_ITEM_SELECTED = "item_selected";
        public static final String COLUMN_ITEM_QUANTITY_SELECTED = "item_quantity_selected";
        public static final String COLUMN_ITEM_ST_CODE = "item_st_code";
        public static final String COLUMN_ITEM_SC_CODE = "item_sc_code";
        public static final String COLUMN_ITEM_SP_CODE = "item_sp_code";
        public static final String COLUMN_ITEM_OFFER_PRICE = "item_offer_price";
        public static final String COLUMN_ITEM_IN_OFFER = "item_in_offer";

        public static Uri buildShoppingCartUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ServiceTypeCategoriesEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SERVICE_TYPE_CATEGORY).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_SERVICE_TYPE_CATEGORY;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_SERVICE_TYPE_CATEGORY;

        // Table name
        public static final String TABLE_NAME = "service_type_categories";

        public static final String COLUMN_ST_CODE = "st_code";
        public static final String COLUMN_SC_CODE = "sc_code";
        public static final String COLUMN_SC_NAME = "sc_name";

        public static Uri buildServiceTypeCategoriesUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class SearchResultEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SEARCH_RESULT).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_SEARCH_RESULT;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_SEARCH_RESULT;

        // Table name
        public static final String TABLE_NAME = "search_result";
        public static final String TABLE_ALIAS_NAME = "item";

        public static final String COLUMN_ITEM_CODE = "item_code";
        public static final String COLUMN_ITEM_NAME = "item_name";
        public static final String COLUMN_ITEM_IMG_FLAG = "item_img_flag";
        public static final String COLUMN_ITEM_IMG_ADDRESS = "item_img_address";
        public static final String COLUMN_ITEM_UNIT = "item_unit";
        public static final String COLUMN_ITEM_COST = "item_cost";
        public static final String COLUMN_ITEM_SHORT_DESC = "item_short_desc";
        public static final String COLUMN_ITEM_DESC = "item_desc";
        public static final String COLUMN_ITEM_STATUS = "item_status";
        public static final String COLUMN_ITEM_SELECTED = "item_selected";
        public static final String COLUMN_ITEM_QUANTITY_SELECTED = "item_quantity_selected";
        public static final String COLUMN_ITEM_ST_CODE = "item_st_code";
        public static final String COLUMN_ITEM_SC_CODE = "item_sc_code";
        public static final String COLUMN_ITEM_MRP = "item_mrp";
        public static final String COLUMN_SEARCH_STRING = "search_string";
        public static final String COLUMN_ITEM_OFFER_PRICE = "item_offer_price";
        public static final String COLUMN_ITEM_IN_OFFER = "item_in_offer";

        public static final String WHERE_CURRENT_SPCODE = "sp_code";

        public static Uri buildSearchResultUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildSearchResultItemsUri(long id, String sp_code) {
            return ContentUris.withAppendedId(CONTENT_URI, id)
                    .buildUpon().appendQueryParameter(WHERE_CURRENT_SPCODE, sp_code).build();
        }

        public static String getJoinTable(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static final class OrderDetailsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ORDER_DETAILS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_ORDER_DETAILS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_ORDER_DETAILS;

        // Table name
        public static final String TABLE_NAME = "order_details";
        public static final String TABLE_ALIAS_NAME = "order";

        // The location setting string is what will be sent to openweathermap
        // as the location query.
        public static final String COLUMN_SERV_PROV_CODE = "serv_prov_code";
        public static final String COLUMN_DEL_TIME = "del_time";
        public static final String COLUMN_MIN_ORDER = "min_order";
        public static final String COLUMN_SERV_PROV_NAME = "serv_prov_name";
        public static final String COLUMN_TIME_INTERVAL = "time_interval";
        public static final String COLUMN_SHOP_OPEN_TIME = "open_time";
        public static final String COLUMN_SHOP_CLOSE_TIME = "close_time";
        public static final String COLUMN_SHOP_TAX_NAME_1 = "tax_name_1";
        public static final String COLUMN_SHOP_TAX_CENT_1 = "tax_cent_1";
        public static final String COLUMN_SHOP_TAX_FLAG_1 = "tax_flag_1";
        public static final String COLUMN_SHOP_TAX_NAME_2 = "tax_name_2";
        public static final String COLUMN_SHOP_TAX_CENT_2 = "tax_cent_2";
        public static final String  COLUMN_SHOP_TAX_FLAG_2 = "tax_flag_2";
        public static final String COLUMN_SHOP_TAX_NAME_3 = "tax_name_3";
        public static final String COLUMN_SHOP_TAX_CENT_3 = "tax_cent_3";
        public static final String COLUMN_SHOP_TAX_FLAG_3 = "tax_flag_3";

        public static Uri buildOrderDetailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }


    public static final class AddressSearchEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ADDRESS_SEARCH).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_ADDRESS_SEARCH;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_ADDRESS_SEARCH;
        public static final String TABLE_NAME = "address_search";

        public static final String COLUMN_FORMATTED_ADDRESS = "formatted_address";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
        public static  final  String COLUMN_CITY_CODE ="city_code";
        public static final String COLUMN_CITY_NAME= "city_name";
        public static final String COLUMN_AREA_NAME= "area_name";
        public static final String COLUMN_CITY_IMAGE_ADDRESS= "city_image_address";
        public static final String COLUMN_COUPON_IN_CITY= "coupon_in_city";


        public static Uri buildAddressSearchUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }



    public static final class CouponsStatistics implements BaseColumns {

        public static final String COLUMN_COUPON_ID = "coupon_id";
        public static final String COLUMN_COUPON_UP_SYNC = "coupon_up_sync";
        public static final String COLUMN_TIME_STAMP= "time_stamp";

    }

    public static final class CouponsViewsDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COUPONS_VIEWS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_COUPONS_VIEWS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_COUPONS_VIEWS;
        public static final String TABLE_NAME = "coupons_views";

        public static final String COLUMN_VIEWS_COUNT = "views_count";

        public static Uri buildCouponsViewsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class CouponsClicksDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COUPONS_CLICKS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_COUPONS_CLICKS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_COUPONS_CLICKS;
        public static final String TABLE_NAME = "coupons_clicks";

        public static final String COLUMN_COUNT = "views_count";

        public static Uri buildCouponsClicksUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }




    public static final class ItemsStatistics implements BaseColumns {

        public static final String COLUMN_ITEMS_CODE = "items_code";
        public static final String COLUMN_ITEMS_UP_SYNC = "items_up_sync";
        public static final String COLUMN_ITEMS_SP_CODE = "items_sp_code";
        public static final String COLUMN_TIME_STAMP= "time_stamp";

    }

    public static final class ItemsViewsDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ITEMS_VIEWS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_VIEWS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_VIEWS;
        public static final String TABLE_NAME = "items_views";

        public static final String COLUMN_VIEWS_COUNT = "views_count";

        public static Uri buildItemsViewsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ItemsClicksDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ITEMS_CLICKS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_CLICKS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_ITEMS_CLICKS;
        public static final String TABLE_NAME = "items_clicks";

        public static final String COLUMN_COUNT = "views_count";

        public static Uri buildItemsClicksUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

}
