package technologies.angular.lazylad;

/**
 * Created by Amud on 28/09/15.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;

/**
 * Created by user on 27-07-2015.
 */
public class AccountClass extends ActionBarActivity {

    Toolbar toolbar;
    ViewPager pager;
    AccountsPagerAdapter adapter;
    PagerSlidingTabStrip tabs;
    CharSequence Titles[] = {"Summary", "Account"};
    int Numboftabs = 2;

    String TITLES[] = {"Home", "Notifications", "Addresses", "Order History", "Share & Earn", "Lazy Wallet", "", "Rate Us", "Feedback", "Call Us", "About Us"};
    int ICONS[] = {R.drawable.home, R.drawable.notification, R.drawable.address, R.drawable.orders, R.drawable.share, R.drawable.wallet, R.drawable.ic_launcher, R.drawable.rate, R.drawable.feedback, R.drawable.call, R.drawable.aboutus};
    public static String mAddressOutput;
    //Similarly we Create a String Resource for the
    // and user_email in the header view
    //And we also create a int resource for profile picture in the header view

    String Name = "Add number";
    //int PROFILE = R.drawable.pro;
    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;

    private SharedPreferences prefs;
    private SharedPreferences prefs_location;
    private String areaName;
    private String cityName;
    private String city_image_add;// Declaring DrawerLayout
    private View frame;

    public ActionBarDrawerToggle mDrawerToggle;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_layout);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lazy Wallet");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);

        prefs_location = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        areaName = prefs_location.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, "Not_Available");
        cityName = prefs_location.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, "Not Available");

        mAddressOutput = areaName + "," + " " + cityName;

        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView); // Assigning the RecyclerView Object to the xml View
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayViewWidth_ = size.x;
        frame = (View) findViewById(R.id.content_frame);

        mRecyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, AccountClass.this, 6);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer object Assigned to the view
        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.width = 9 * (displayViewWidth_ / 10);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutParams(params);
        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.opendrawer, R.string.closedrawer) {


            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (mRecyclerView.getWidth() * slideOffset);
                frame.setTranslationX(moveFactor);
            }
        };
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State
        adapter = new AccountsPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);
        pager = (ViewPager) findViewById(R.id.pager);

        setTracker();
        mHelper = new MoEHelper(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.Wallet");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        Drawer.closeDrawer(Gravity.LEFT);
        mHelper.onStop(this);
    }

    @Override
    public void onResume() {
        Drawer.closeDrawer(Gravity.LEFT);
        super.onResume();
        SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        Bundle bundle = new Bundle();
        Utils.verify_num(this, bundle, 3, false, NumberParameters.WALLET_SKIP_CONSTANT, NumberParameters.WALLET_VERIFIED_CONSTANT);
        boolean prefVerified = prefs.getBoolean(NumberParameters.WALLET_VERIFIED_CONSTANT, false);
        if (prefVerified) {
            Drawer.closeDrawer(Gravity.LEFT);
            pager.setAdapter(adapter);
            tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
            tabs.setShouldExpand(true);

            // Setting the ViewPager For the SlidingTabsLayout
            tabs.setViewPager(pager);
        }
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}