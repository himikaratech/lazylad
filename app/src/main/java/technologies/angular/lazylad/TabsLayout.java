package technologies.angular.lazylad;

import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import technologies.angular.lazylad.R;

/**
 * Created by Amud on 31/01/16.
 */

public class TabsLayout extends HorizontalScrollView {

    private ViewGroup mContainer;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private LayoutInflater mInflater;

    private final Rect mRect;
    {
        mRect = new Rect();
    }

    public TabsLayout(Context context) {
        super(context);
        init(context, null);
    }

    public TabsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TabsLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attributeSet) {

        mInflater = LayoutInflater.from(context);

        mContainer = new LinearLayout(context);
        ((LinearLayout) mContainer).setOrientation(LinearLayout.HORIZONTAL);
        mContainer.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mContainer.setLayoutTransition(new LayoutTransition());
        mContainer.setHorizontalScrollBarEnabled(false);

        addView(mContainer);


    }

    public void setViewPager(ViewPager pager) {
        if (mPagerAdapter != null) {
            mContainer.removeAllViews();
        }

        mPager = pager;
        mPagerAdapter = pager.getAdapter();
        populateViews();
        setItemSelected(0);

    }

    private void populateViews() {
        final int count = mPagerAdapter != null ? mPagerAdapter.getCount() : 0;
        if (count < 0) {
            return;
        }

        View view;
        TextView view1;
        TextView view2;

        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        for (int i = 0; i < count; i++) {
            view = createTabView();
            /*LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            view.setLayoutParams(layoutParams);
            TextView name=(TextView)view.findViewById(R.id.coupon_name);
            name.setGravity(Gravity.CENTER);
            name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            //name.setText(mPagerAdapter.getPageTitle(i));
            name.setText("Ashirvad aata - 10kg & 5kg");


            TextView short_desc=(TextView)view.findViewById(R.id.coupon_short_desc);
            short_desc.setGravity(Gravity.CENTER);
            short_desc.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            //name.setText(mPagerAdapter.getPageTitle(i));
            short_desc.setText("25%OFF");

*/
            final int position = i;
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPager.setCurrentItem(position);
                }
            });


            mContainer.addView(view, i);
        }
    }

    private View createTabView() {
        return (View) mInflater.inflate(R.layout.view_tab_item, mContainer, false);
    }

    public void setItemSelected(int position) {
        final int count = mContainer.getChildCount();
        for (int i = 0; i < count; i++) {
            mContainer.getChildAt(i).setSelected(i == position);
        }
    }

    public void changeTab(int position)
    {
        Log.d("page", "selected");
        setItemSelected(position);
        mContainer.getChildAt(position).getHitRect(mRect);
        post(new Runnable() {
            @Override
            public void run() {
                smoothScrollTo(mRect.left, 0);
            }
        });
    }
}

