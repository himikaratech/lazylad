package technologies.angular.lazylad;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh on 08/04/15.
 */
public class BillItemsSnippet {

    @SerializedName("item_code")
    public String m_itemCode;
    @SerializedName("item_name")
    public String m_itemName;
    @SerializedName("item_cost")
    public double m_itemCost;
    @SerializedName("item_short_desc")
    public String m_itemShortDesc;   //Standard Weight
    @SerializedName("item_desc")
    public String m_itemDesc;
    @SerializedName("item_quantity")
    public int m_itemQuantity;

    public BillItemsSnippet() {

    }

    public BillItemsSnippet(String itemCode, String itemName, double itemCost, String itemShortDesc, String itemDesc,
                            int itemQuantity) {

        m_itemCode = itemCode;
        m_itemName = itemName;
        m_itemCost = itemCost;
        m_itemShortDesc = itemShortDesc;
        m_itemDesc = itemDesc;
        m_itemQuantity = itemQuantity;
    }
}
