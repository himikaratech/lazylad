package technologies.angular.lazylad;


/**
 * Created by Amud on 09/09/15.
 */

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazylad.common.Constants;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.UserAddressDetailsEntry;
import technologies.angular.lazylad.LazyContract.UserDetailsEntry;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";

    static ArrayList<AddressSnippet> mDataset;
    ArrayList<AddressSnippet> initalDataset;

    FragmentManager fragmentManager;
    private int m_userAddressId;
    String action;
    int sourceFlag;
    Context m_context;
    int changing_user_code = 0;
    private SharedPreferences prefs_location;
    Double latitude;
    Double longitude;
    String areaName;
    String cityName;


    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView m_addressTextView;
        TextView m_userNameTextView;
        TextView m_userPhoneNumTextView;
        TextView m_serialNumber;
        View line;
        TextView selectView;
        View v;
        Toolbar toolbar;


        public DataObjectHolder(View itemView) {
            super(itemView);

            m_addressTextView = (TextView) itemView.findViewById(R.id.user_address_in_list);
            m_userNameTextView = (TextView) itemView.findViewById(R.id.user_name);
            m_userPhoneNumTextView = (TextView) itemView.findViewById(R.id.user_phone_number);
            m_serialNumber = (TextView) itemView.findViewById(R.id.serial);
            line = (View) itemView.findViewById(R.id.line);
            selectView = (TextView) itemView.findViewById(R.id.selectView);
            v = (View) itemView.findViewById(R.id.submit);

            toolbar = (Toolbar) itemView.findViewById(R.id.toolbar_card);
            if (toolbar != null)
                toolbar.inflateMenu(R.menu.card_toolbar);
            Cursor m_cursor = m_context.getContentResolver().query(
                    UserDetailsEntry.CONTENT_URI,
                    null,
                    null,
                    null,
                    null);

            if (m_cursor != null && m_cursor.moveToFirst()) {
                if (m_cursor.getCount() != 0) {
                    changing_user_code = m_cursor.getInt(1);

                    Cursor m_cursorAddress = m_context.getContentResolver().query(
                            UserAddressDetailsEntry.CONTENT_URI,
                            null,
                            null,
                            null,
                            UserAddressDetailsEntry._ID + " DESC LIMIT 1");
                    if (m_cursorAddress != null && m_cursorAddress.moveToFirst()) {
                        if (m_cursorAddress.getCount() != 0) {
                            m_userAddressId = m_cursorAddress.getInt(0);
                        } else {
                            m_userAddressId = 0;

                            //TODO error handling first time user created, address is also created.
                        }
                    }
                } else {
                    m_userAddressId = 0;
                }
            } else {
                m_userAddressId = 0;
                //TODO error handling
            }

            if (m_cursor != null && m_cursor.moveToFirst()) {
                if (m_cursor.getCount() != 0) {
                    changing_user_code = m_cursor.getInt(1);
                } else {
                    changing_user_code = 0;
                }
                Log.i("user_code", Integer.toString(changing_user_code));
            } else {
                changing_user_code = 0;
            }
            Log.i("user_code", Integer.toString(changing_user_code));
        }
    }

    public AddressAdapter(Context context, ArrayList<AddressSnippet> m_addressList, FragmentManager fragmentManager, int sourceFlag) {
        mDataset = m_addressList;
        initalDataset = m_addressList;
        this.sourceFlag = sourceFlag;
        this.fragmentManager = fragmentManager;
        this.m_context = context;
        prefs_location = context.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        latitude = Double.longBitsToDouble(prefs_location.getLong(PreferencesStore.LATITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LATITUDE_SELECTED_CONSTANT));
        longitude = Double.longBitsToDouble(prefs_location.getLong(PreferencesStore.LONGITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LONGITUDE_SELECTED_CONSTANT));
        areaName = prefs_location.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, PreferencesStore.DEFAULT_AREA_SELECTED_CONSTANT);
        cityName = prefs_location.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT);

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_card_layout, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

        if (!(mDataset.isEmpty())) {

            if (sourceFlag == 0) {
                holder.line.setVisibility(View.GONE);
                holder.selectView.setVisibility(View.GONE);
            } else {
                holder.v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int index = holder.getAdapterPosition();
                        int addressCode = mDataset.get(index).m_addressCode;
                        final int m_address_userCode = mDataset.get(index).m_userCode;

                        PayloadBuilder builder = new PayloadBuilder();
                        builder.putAttrString("City Selected", cityName);
                        builder.putAttrString("Area Selected", areaName);
                        builder.putAttrDouble("Latitude", latitude);
                        builder.putAttrDouble("Longitude", longitude);
                        MoEHelper.getInstance(m_context).trackEvent("Address", builder.build());

                        geoTagAddress(Integer.toString(addressCode), Integer.toString(m_address_userCode), Integer.toString(changing_user_code));
                        startConfirmationActivity(Integer.toString(addressCode), Integer.toString(m_address_userCode), Integer.toString(changing_user_code));
                    }
                });
            }

            int id = mDataset.get(position).m_addressId;
            String m_address = mDataset.get(position).m_addressText;
            String m_userName = mDataset.get(position).m_userName;
            String m_userPhoneNum = mDataset.get(position).m_userPhone;
            holder.m_serialNumber.setText("S.No. - " + Integer.toString(id));
            holder.m_addressTextView.setText(m_address);
            holder.m_userNameTextView.setText(m_userName);
            holder.m_userPhoneNumTextView.setText(m_userPhoneNum);

            holder.toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                int index;
                int addressCode;
                int user_code;

                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    switch (menuItem.getItemId()) {
                        case R.id.menu_edit:
                            index = holder.getAdapterPosition();
                            addressCode = mDataset.get(index).m_addressCode;
                            user_code = mDataset.get(index).m_userCode;
                            editAddress(index, addressCode, user_code);
                            return true;
                        case R.id.menu_delete:
                            index = holder.getAdapterPosition();
                            addressCode = mDataset.get(index).m_addressCode;
                            user_code = mDataset.get(index).m_userCode;
                            deleteAddress(addressCode, index, user_code, addressCode);
                            return true;
                        default:
                            break;
                    }
                    return false;
                }
            });
        }
    }

    private void geoTagAddress(final String addressCode, final String m_address_userCode, final String m_userCode) {
        String whereClause = UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(addressCode)};
        Cursor m_cursor = m_context.getContentResolver().query(
                UserAddressDetailsEntry.CONTENT_URI,
                null,
                whereClause,
                selectionArgs,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {

            Double geoTag = m_cursor.getDouble(10);
            Log.d("geoTag",geoTag.toString());
             if (geoTag == 0) {
                 Log.d("not Tag", geoTag.toString());

            int user_code = m_cursor.getInt(1);
            String customer_name = m_cursor.getString(4);
            String customer_email = m_cursor.getString(5);
            String customer_number = m_cursor.getString(6);
            String customer_flat = m_cursor.getString(7);
            String customer_subarea = m_cursor.getString(8);
            String customer_area = m_cursor.getString(9);
            String customer_city = m_cursor.getString(3);


            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://54.169.62.100")
                    .build();

            APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
            APIRequestModel requestModel = new APIRequestModel();
            final APIRequestModel.PostNewAddForUserRequestModel postNewAddForUserRequestModel = requestModel.new PostNewAddForUserRequestModel();
            postNewAddForUserRequestModel.user_code = String.valueOf(user_code);
            postNewAddForUserRequestModel.customer_name = customer_name;
            postNewAddForUserRequestModel.customer_number = customer_number;
            postNewAddForUserRequestModel.customer_email = customer_email;
            postNewAddForUserRequestModel.customer_flat = customer_flat;
            postNewAddForUserRequestModel.customer_subarea = customer_subarea;
            postNewAddForUserRequestModel.customer_area = customer_area;
            postNewAddForUserRequestModel.customer_city = customer_city;
            postNewAddForUserRequestModel.address_code = addressCode;
            postNewAddForUserRequestModel.changing_user_code = Integer.toString(changing_user_code);
            postNewAddForUserRequestModel.address_lat = latitude;
            postNewAddForUserRequestModel.address_long = longitude;

            String url = "editUserAddressDetailedSynced";

            apiSuggestionsService.postNewAddForUserAPICall(url, postNewAddForUserRequestModel, new Callback<APIResonseModel.PostNewAddForUserResponseModel>() {

                @Override
                public void success(APIResonseModel.PostNewAddForUserResponseModel postNewAddForUserResponseModel, retrofit.client.Response response) {
                    Log.d("address", "geo tagged");
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("retro", error + "");
                }
            });

            }
            else
                Log.d("geoTag","not zero");
        } else
            Log.d("dbQuery", "fails");

    }

    public void deleteItem(int index, int indexofCode) {

        int position = getIndex(indexofCode);
        mDataset.remove(position);
        notifyItemRemoved(index);
    }

    private void startConfirmationActivity(String addressCode, String m_address_userCode, String m_userCode) {
        Intent intent;
        if(sourceFlag == Constants.LAUNDRY_ACTIVITY_CODE){
            intent = new Intent(m_context, LaundryConfirmationActivity.class);
        }else{
            intent = new Intent(m_context, ConfirmationMessageActivity.class);

        }
        String m_dataSend[] = {addressCode, m_address_userCode, m_userCode};
        intent.putExtra("data_send_to_confirm_activity", m_dataSend);
        m_context.startActivity(intent);

    }

    public int getIndex(int indexofCode) {
        for (int i = 0; i < mDataset.size(); i++) {
            int code = mDataset.get(i).m_addressCode;
            if (code == indexofCode) {
                return i;
            }
        }
        return -1;
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private void deleteAddress(final int addressCode, final int position, final int m_userCode, final int indexofAddressCode) {

        final ProgressDialog progressDialog = new ProgressDialog(m_context, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.DeleteAddressRequestModel deleteAddressRequestModel = requestModel.new DeleteAddressRequestModel();
        deleteAddressRequestModel.user_code = Integer.toString(m_userCode);
        deleteAddressRequestModel.changing_user_code = Integer.toString(changing_user_code);
        deleteAddressRequestModel.address_code = Integer.toString(addressCode);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        apiSuggestionsService.deleteAddressAPICall(deleteAddressRequestModel, new retrofit.Callback<APIResonseModel.DeleteAddressResponseModel>() {

            @Override
            public void success(APIResonseModel.DeleteAddressResponseModel deleteAddressResponseModel, retrofit.client.Response response) {

                int success = deleteAddressResponseModel.error;
                if (success == 1) {

                    Toast.makeText(m_context, "Deleted", Toast.LENGTH_LONG).show();

                    m_context.getContentResolver().delete(
                            UserAddressDetailsEntry.CONTENT_URI,
                            UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE + " =? ",
                            new String[]{String.valueOf(addressCode)});
                    deleteItem(position, addressCode);

                    Cursor m_cursor = m_context.getContentResolver().query(
                            UserAddressDetailsEntry.CONTENT_URI,
                            null,
                            null,
                            null,
                            null);

                    LinearLayout address = ((AddressActivity) m_context).addressFragment;
                    if (m_cursor == null || m_cursor.getCount() == 0) {
                        address.setVisibility(View.GONE);
                    }

                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    final AddressActivityFragment addressActivityFragment = new AddressActivityFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("flag", 0);
                    addressActivityFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.new_fragment_container, addressActivityFragment, "HELLO");
                    fragmentTransaction.commit();
                } else {
                    Toast.makeText(m_context, "Unable to delete, Please try again later", Toast.LENGTH_LONG).show();
                }
                progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Log.e("retro", error + "");

            }
        });
    }

    private void editAddress(int position, int addressCode, int user_code) {

        String user_address = "";
        String user_name = "";
        String user_phone_number = "";
        String user_flat = "";
        String user_subarea = "";
        String user_area = "";
        String user_city = "";
        String whereClause = UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(addressCode)};
        Cursor m_cursor = m_context.getContentResolver().query(
                UserAddressDetailsEntry.CONTENT_URI,
                null,
                whereClause,
                selectionArgs,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            m_userAddressId = m_cursor.getInt(0);
            user_city = m_cursor.getString(3);
            user_name = m_cursor.getString(4);
            user_phone_number = m_cursor.getString(6);
            user_flat = m_cursor.getString(7);
            user_subarea = m_cursor.getString(8);
            user_area = m_cursor.getString(9);
            user_address = user_flat + " " + user_subarea + " " + user_area + " " + user_city;

        }

        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putInt("m_userAddressId", m_userAddressId);
        bundle.putInt("changing_user_code", changing_user_code);
        bundle.putInt("user_code", user_code);
        bundle.putInt("addressCode", addressCode);
        bundle.putString("user_address", user_city);
        bundle.putString("user_name", user_name);
        bundle.putString("user_phone_number", user_phone_number);
        bundle.putString("user_flat", user_flat);
        bundle.putString("user_subarea", user_subarea);
        bundle.putString("user_area", user_area);


        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        final AddressActivityFragment addressActivityFragment = new AddressActivityFragment();
        bundle.putInt("flag", 1);
        addressActivityFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.new_fragment_container, addressActivityFragment, "HELLO");
        fragmentTransaction.commit();
    }
}