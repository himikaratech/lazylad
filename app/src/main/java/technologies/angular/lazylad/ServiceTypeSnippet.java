package technologies.angular.lazylad;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Saurabh on 18/02/15.
 */
public class ServiceTypeSnippet implements Parcelable {

    int m_id;
    int m_servTypeCode;
    int m_servTypeFlag;
    String m_servTypeName;
    int m_servTypeImgFlag;
    String m_servTypeImgAdd;
    int m_isService;

    public ServiceTypeSnippet() {
    }

    public ServiceTypeSnippet(Parcel in) {
        this.m_servTypeName = in.readString();
        this.m_servTypeImgAdd = in.readString();
        this.m_id = in.readInt();
        this.m_servTypeCode = in.readInt();
        this.m_servTypeImgFlag = in.readInt();
        this.m_servTypeFlag = in.readInt();
        this.m_isService = in.readInt();
    }

    public ServiceTypeSnippet(int id, int st_code, String st_name, int st_img_flag, String st_img_add, int servTypeFlag, int isService) {
        m_id = id;
        m_servTypeCode = st_code;
        m_servTypeName = st_name;
        m_servTypeImgFlag = st_img_flag;
        m_servTypeImgAdd = st_img_add;
        m_servTypeFlag = servTypeFlag;
        m_isService = isService;
    }

    public int getm_id() {
        return m_id;
    }

    public int getm_servTypeCode() {
        return m_servTypeCode;
    }

    public String getm_servTypeName() {
        return m_servTypeName;
    }

    public int getm_servTypeImgFlag() {
        return m_servTypeImgFlag;
    }

    public String getm_servTypeImgAdd() {
        return m_servTypeImgAdd;
    }

    public int getm_servTypeFlag() {
        return m_servTypeFlag;
    }

    public int getM_isService(){
        return m_isService;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.m_servTypeName);
        dest.writeString(this.m_servTypeImgAdd);
        dest.writeInt(this.m_id);
        dest.writeInt(this.m_servTypeCode);
        dest.writeInt(this.m_servTypeImgFlag);
        dest.writeInt(this.m_servTypeFlag);
        dest.writeInt(this.m_isService);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ServiceTypeSnippet createFromParcel(Parcel in) {
            return new ServiceTypeSnippet(in);
        }

        public ServiceTypeSnippet[] newArray(int size) {
            return new ServiceTypeSnippet[size];
        }
    };
}
