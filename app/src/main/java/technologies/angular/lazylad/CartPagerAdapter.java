package technologies.angular.lazylad;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by sakshigupta on 18/10/15.
 */
public class CartPagerAdapter extends FragmentStatePagerAdapter {
    // This will Store the Titles of the Tabs which are Going to be passed when CartPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the CartPagerAdapter is created

    Context m_context;
    private ArrayList<ItemSnippet> m_shoppingCartItemsList;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public CartPagerAdapter(FragmentManager fm, int mNumbOfTabsumb, Context context, ArrayList<ItemSnippet> shoppingCartItemsList) {
        super(fm);
        this.NumbOfTabs = mNumbOfTabsumb;
        this.m_context = context;
        m_shoppingCartItemsList = shoppingCartItemsList;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if (!m_shoppingCartItemsList.isEmpty()) {
            CartPagerFragment cartPagerFragmentObj = new CartPagerFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("shoppingCartItemsList", m_shoppingCartItemsList);
            bundle.putInt("position", position);
            cartPagerFragmentObj.setArguments(bundle);

            return cartPagerFragmentObj;
        } else
            return null;
    }
    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
