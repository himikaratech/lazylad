package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

public class PreviousOrders extends ActionBarActivity {

    Toolbar toolbar;
    private int m_userCode;
    private SessionManager m_sessionManager;

    private ListView m_previousOrdersListView;
    private ArrayList<PreviousOrdersSnippet> m_previousOrdersArraylist;
    private PreviousOrdersAdapter m_prevOrdAdapter;
    private Boolean dataLoading = false;
    private Boolean shallLoad = true;
    private String mAddressOutput;
    private SharedPreferences prefs_location;
    private String areaName;
    private String cityName;

    String TITLES[] = {"Home", "Notifications", "Addresses", "Order History", "Share & Earn", "Lazy Wallet", "", "Rate Us", "Feedback", "Call Us", "About Us"};
    int ICONS[] = {R.drawable.home, R.drawable.notification, R.drawable.address, R.drawable.orders, R.drawable.share, R.drawable.wallet, R.drawable.ic_launcher, R.drawable.rate, R.drawable.feedback, R.drawable.call, R.drawable.aboutus};
    //Similarly we Create a String Resource for the name and email in the header view
    //And we also create a int resource for profile picture in the header view

    String Name = "Current Location:";
    int PROFILE = R.drawable.pro;
    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout
    private View frame;
    ActionBarDrawerToggle mDrawerToggle;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.previous_orders);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setPadding(0, Utils.getStatusBarHeight(this), 0, 0);

        m_previousOrdersListView = (ListView) findViewById(R.id.previous_orders_list_view);
        prefs_location = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        areaName = prefs_location.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, "Not_Available");
        cityName = prefs_location.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, "Not Available");

        mAddressOutput = areaName + "," + " " + cityName;


        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView); // Assigning the RecyclerView Object to the xml View

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayViewWidth_ = size.x;

        frame = (View) findViewById(R.id.content_frame);

        mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, PreviousOrders.this, 4);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.width = 9 * (displayViewWidth_ / 10);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutParams(params); // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.opendrawer, R.string.closedrawer) {
            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (mRecyclerView.getWidth() * slideOffset);
                frame.setTranslationX(moveFactor);
            }
        };
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State

        Cursor m_cursor = getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() == 1) {
                m_userCode = m_cursor.getInt(1);
            } else {
                m_userCode = 0;
                //TODO Some UI Element asking to add address
            }
        } else {
            m_userCode = 0;
            //TODO Error Handling
        }

        m_previousOrdersListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            int visibleThreshold = 1;
            int visibleItemCount;
            int totalItemCount;
            int firstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    if (shallLoad && !dataLoading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItem + visibleThreshold)) {
                        dataLoading = true;
                        getPreviousOrderHistory(Integer.toString(m_userCode));
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.firstVisibleItem = firstVisibleItem;
                this.visibleItemCount = visibleItemCount;
                this.totalItemCount = totalItemCount;
            }
        });

        getPreviousOrderHistory(Integer.toString(m_userCode));

        setTracker();

        mHelper = new MoEHelper(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void getPreviousOrderHistory(String userCode) {
        int page_number;
        if (m_previousOrdersArraylist == null)
            page_number = 1;
        else {
            int curCount = m_previousOrdersArraylist.size();
            if (curCount % 10 == 0) {
                // Since the count is starting from 1
                page_number = curCount / 10 + 1;
            } else {
                return;
            }
        }

        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getPreviousOrderHistoryAPICall(userCode, page_number, new retrofit.Callback<APIResonseModel.GetPreviousOrderResponseModel>() {

            @Override
            public void success(APIResonseModel.GetPreviousOrderResponseModel getPreviousOrderResponseModel, retrofit.client.Response response) {
                ArrayList<PreviousOrdersSnippet> m_responseList = new ArrayList<PreviousOrdersSnippet>();
                m_responseList = getPreviousOrderResponseModel.previous_orders_details;
                if (!m_responseList.isEmpty()) {
                    if (m_previousOrdersArraylist == null) {
                        m_previousOrdersArraylist = m_responseList;
                        m_prevOrdAdapter = new PreviousOrdersAdapter(PreviousOrders.this, m_previousOrdersArraylist);
                        m_previousOrdersListView.setAdapter((ListAdapter) m_prevOrdAdapter);
                    } else {
                        m_previousOrdersArraylist.addAll(m_responseList);
                        m_prevOrdAdapter.notifyDataSetChanged();
                    }
                } else
                    shallLoad = false;
                dataLoading = false;
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.PreviousOrders");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onStop() {
        super.onStop();
        Drawer.closeDrawer(Gravity.LEFT);
        mHelper.onStop(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
        mHelper.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
        mHelper.onResume(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}