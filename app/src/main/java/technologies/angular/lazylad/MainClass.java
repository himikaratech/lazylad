package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Amud on 08/02/16.
 */
public class MainClass extends Activity {
    private SharedPreferences prefs_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs_location = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        int coupon_in_city = prefs_location.getInt(PreferencesStore.COUPON_IN_CITY_CONSTANT, PreferencesStore.DEFAULT_COUPON_IN_CITY_CONSTANT);
        Log.d("coupon_in_city",coupon_in_city+"");
        if (coupon_in_city == 1)
            startActivity(new Intent(getApplicationContext(), (MainActivityWithBanner.class)));
        else
            startActivity(new Intent(getApplicationContext(), (MainActivity.class)));
        MainClass.this.finish();
    }
}
