package technologies.angular.lazylad;

import android.widget.TextView;

import com.moengage.addon.inbox.InboxManager;

/**
 * Created by sakshigupta on 15/12/15.
 */
public class CustomHolder extends InboxManager.ViewHolder {

    public TextView title;
    public TextView message;
    public TextView timeStamp;
}
