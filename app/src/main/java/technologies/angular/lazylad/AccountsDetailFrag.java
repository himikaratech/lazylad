package technologies.angular.lazylad;

/**
 * Created by Amud on 28/09/15.
 */

import android.app.ProgressDialog;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

public class AccountsDetailFrag extends Fragment {

    int m_userCode;
    String m_number;
    ArrayList<AccountTransactionsDetailSnippet> m_walletList;
    RecyclerView recyclerView;
    AccountTransDetailAdap accountTransDetailAdap;
    TextView m_noTransactions;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.account_detail_frag, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.transation_list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        m_noTransactions = (TextView) v.findViewById(R.id.noTransactions);
        m_noTransactions.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        getAllTransactions();
        return v;
    }

    private void getAllTransactions() {
        Cursor m_cursor = getActivity().getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_userCode = m_cursor.getInt(1);
                    m_number = m_cursor.getString(3);
                    m_cursor.moveToNext();
                }
            }
        }

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.GetAllTransactionsRequestModel getAllTransactionsRequestModel = requestModel.new GetAllTransactionsRequestModel();
        getAllTransactionsRequestModel.user_code = String.valueOf(m_userCode);
        getAllTransactionsRequestModel.owner_type = 0;
        getAllTransactionsRequestModel.previous_tid = 0;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        Log.e("detailObject", getAllTransactionsRequestModel.toString());

        apiSuggestionsService.getAllTransactionsAPICall(getAllTransactionsRequestModel.user_code
                , getAllTransactionsRequestModel.owner_type,
                new Callback<APIResonseModel.GetAllTransactionsResponseModel>() {


                    @Override
                    public void success(APIResonseModel.GetAllTransactionsResponseModel getAllTransactionsResponseModel, Response response) {
                        Boolean success = getAllTransactionsResponseModel.success;
                        Log.e("Detailsuccess", success + "");
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (success == true) {
                            m_walletList = new ArrayList<AccountTransactionsDetailSnippet>();
                            m_walletList = getAllTransactionsResponseModel.transactions;
                            if (!m_walletList.isEmpty()) {
                                accountTransDetailAdap = new AccountTransDetailAdap(getActivity(), m_walletList);
                                recyclerView.setVisibility(View.VISIBLE);
                                recyclerView.setAdapter(accountTransDetailAdap);
                            } else {
                                m_noTransactions.setVisibility(View.VISIBLE);
                            }

                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("GetAllTransationRetro", error.toString());
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }
}