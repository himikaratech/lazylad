package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Saurabh on 11/02/15.
 */
public class ServiceProvidersListAdapter extends BaseAdapter {
    private ArrayList<ServiceProvidersListSnippet> m_servProvDetails;
    private Activity m_activity;
    private int time_int;
    private int servOpenTime;
    private int servCloseTime;

    public ServiceProvidersListAdapter(Activity activity, ArrayList<ServiceProvidersListSnippet> servProvDetails) {
        m_activity = activity;
        m_servProvDetails = servProvDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_servProvDetails != null) {
            count = m_servProvDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_servProvDetails != null) {
            return m_servProvDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_servProvDetails != null) {
            return m_servProvDetails.get(position).m_servProvId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_servProvDetails != null) {
            final String servProvName = ((ServiceProvidersListSnippet) getItem(position)).m_servProvName;
            final String servProvDelTime = ((ServiceProvidersListSnippet) getItem(position)).m_servProvDelTime;
            final String servProvMinOrd = ((ServiceProvidersListSnippet) getItem(position)).m_servProvMinOrder;
            final String servProvCode = ((ServiceProvidersListSnippet) getItem(position)).m_servProvCode;
            final String stCode = ((ServiceProvidersListSnippet) getItem(position)).m_servType;
            final int servOpenTime = ((ServiceProvidersListSnippet) getItem(position)).m_shop_open_time;
            final int servCloseTime = ((ServiceProvidersListSnippet) getItem(position)).m_shop_close_time;

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.service_provider_display_list_view, (ViewGroup) null);
            }
            TextView servProvNameTextView = (TextView) convertView.findViewById(R.id.serv_prov_name);
            TextView ServDelTime = (TextView) convertView.findViewById(R.id.max_del_time);
            TextView MinOrdAmt = (TextView) convertView.findViewById(R.id.min_del_amt);
            MinOrdAmt.setText("Free delivery above ₹");
            MinOrdAmt.append(servProvMinOrd);
            ServDelTime.setText(servProvDelTime);
            servProvNameTextView.setText(servProvName);
            RelativeLayout servProvRelativeLayout = (RelativeLayout) convertView.findViewById(R.id.relative_layout_serv_prov_list);
            servProvRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startCategoryListActivity(servProvCode, stCode, servProvDelTime, servProvMinOrd, servProvName, servOpenTime, servCloseTime);
                }
            });

        }
        return convertView;
    }

    private void startCategoryListActivity(String serv_prov_code, String st_code, String del_time, String min_order, String prov_name, int open_time, int close_time) {


        int service_type = Integer.parseInt(st_code);
        String time_interval = null;
        if (del_time.contains(" ")) {
            time_interval = del_time.substring(0, del_time.indexOf(" "));
        }
        time_int = Integer.parseInt(time_interval);

        SharedPreferences prefs = m_activity.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ServiceProviderCode", serv_prov_code);
        editor.putString("ServiceProviderDeliveryTime", del_time);
        editor.putString("ServiceProviderMinimumOrder", min_order);
        editor.putString("ServiceProviderName", prov_name);
        editor.putInt("time_interval", time_int);
        editor.putInt("ServiceProviderOpenTime", open_time);
        editor.putInt("ServiceProviderCloseTime", close_time);
        editor.commit();

        if (service_type == 2) {
            Intent intent = new Intent(m_activity, prescription.class);
            String[] details = new String[]{serv_prov_code, st_code};
            intent.putExtra("ItemsList", details);
            m_activity.startActivity(intent);
            m_activity.finish();
        } else {

            Intent intent = new Intent(m_activity, ItemsActivity.class);
            m_activity.startActivity(intent);
        }
    }
}
