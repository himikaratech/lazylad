package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.UserDetailsEntry;

/**
 * Created by Amud on 20/08/15.
 */
public class AreaActivity extends ActionBarActivity {

    private ArrayList<AreaSnippet> m_areasList;

    private String m_areaCodeSelected;
    private String m_cityCodeSelected;
    private String m_old_areaCodeSelected;
    private String m_old_cityCodeSelected;

    private String m_areaNameSelected;
    private Spinner m_cityListSpinner;
    private Spinner m_areaListSpinner;

    private ArrayList<String> m_cityNamesList;
    private ArrayList<String> m_areaImageAddress;
    private ArrayList<String> m_areaNamesList;
    private List m_servTypeListDetails2 = new ArrayList<String>();

    private SharedPreferences prefs_location;

    private Button m_startMainActivityButton;
    Boolean isInternetPresent = true;
    private static ContentResolver m_conRes2 = null;
    ConnectionDetector cd;
    int comp_flag = 0;
    private ArrayList<ServiceTypeSnippet> m_servTypeListDetailsFromServer;
    private GridView m_servTypeGridView;
    private ServiceTypeAdapter m_servTypeAdapter;
    public static int flag = 0;
    public static final String TAG = "MyTag";
    RecyclerView recyclerView;
    private Toolbar toolbar;
    String m_cityCode;
    String m_cityNameSelected;
    String m_city_image_addSelected;
    int m_city_image_flagSelected;

    TextView cityNameTextView;
    ImageView cityImageView;
    CollapsingToolbarLayout collapsingToolbarLayout;
    int vibrant;

    private int m_userCode;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.area_layout);

        setupToolbar();

        recyclerView = (RecyclerView) findViewById(R.id.area_list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(AreaActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        cityImageView = (ImageView) findViewById(R.id.city_image);

        prefs_location = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        m_old_areaCodeSelected = prefs_location.getString(PreferencesStore.AREA_SELECTED_CONSTANT, "Not_Available");
        m_old_cityCodeSelected = prefs_location.getString(PreferencesStore.CITY_SELECTED_CONSTANT, "Not Available");

        Intent intent = getIntent();
        m_cityCode = intent.getStringExtra("cityCode");
        m_cityNameSelected = intent.getStringExtra("cityNameSelected");
        m_city_image_addSelected = intent.getStringExtra("city_image_addSelected");
        m_city_image_flagSelected = intent.getIntExtra("city_image_flagSelected", 0);

        cityImageView.setVisibility(View.INVISIBLE);

        cd = new ConnectionDetector(getApplicationContext());
        setTracker();
        mHelper = new MoEHelper(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Picasso.with(this)
                .load(m_city_image_addSelected)
                .placeholder(R.drawable.cityimage)
                .fit().centerCrop()
                .error(R.drawable.cityimage)
                .into(cityImageView, new Callback() {
                    @Override
                    public void onSuccess() {

                        vibrant = R.color.primaryColor;
                        setupCollapsingToolbarLayout();
                    }

                    @Override
                    public void onError() {

                    }
                });

        if (isInternetPresent) {
            loadAreas(m_cityCode);

        } else {
            Toast.makeText(AreaActivity.this, "Sorry , Internet connection is not avaiable", Toast.LENGTH_SHORT).show();
        }
        mHelper.onResume(this);
    }


    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null)
            setSupportActionBar(toolbar);

        // Show menu icon
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayShowTitleEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    private void setupCollapsingToolbarLayout() {

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        if (collapsingToolbarLayout != null) {
            collapsingToolbarLayout.setTitle(m_cityNameSelected);
            collapsingToolbarLayout.setBackgroundColor(vibrant);
            ColorDrawable colorDrawable = new ColorDrawable();
            colorDrawable.setColor(vibrant);
            collapsingToolbarLayout.setContentScrim(colorDrawable);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_area_ncity_selection, menu);
        return true;
    }

    private void startMainActivity() {
        Intent intent = new Intent(AreaActivity.this, AreaActivity.class);
        startActivity(intent);
        finish();
    }

    private void loadAreas(final String cityCode) {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getAreasAPICall(cityCode, new retrofit.Callback<APIResonseModel.GetAreaResponseModel>() {

            @Override
            public void success(APIResonseModel.GetAreaResponseModel getAreaResponseModel, retrofit.client.Response response) {

                m_areasList = new ArrayList<AreaSnippet>();
                m_areasList = getAreaResponseModel.areas;

                Cursor m_cursor = getContentResolver().query(
                        UserDetailsEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);
                if (m_cursor != null && m_cursor.moveToFirst()) {
                    if (m_cursor.getCount() != 0) {
                        m_userCode = m_cursor.getInt(1);
                    } else {
                        m_userCode = 0;
                    }
                } else {
                    m_userCode = 0;
                    //TODO error handling
                }

                recyclerView.setAdapter(new AreaAdapter(m_areasList, R.layout.area_list_layout, AreaActivity.this, 0,
                        m_old_cityCodeSelected, m_old_areaCodeSelected, m_cityCode, m_cityNameSelected,
                        m_city_image_flagSelected, m_city_image_addSelected, m_userCode));
                cityImageView.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivityWithBanner.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.ItemsActivity");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
