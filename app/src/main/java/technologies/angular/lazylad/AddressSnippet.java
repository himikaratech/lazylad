package technologies.angular.lazylad;

import java.util.ArrayList;

/**
 * Created by Saurabh on 21/02/15.
 */
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Saurabh on 21/02/15.
 */
public class AddressSnippet {

    public int error;

    @SerializedName("user_addresses")
    public ArrayList<AddressSnippet> user_addresses;

    @SerializedName("user_code")
    public int m_userCode;
    @SerializedName("user_address_code")
    public int m_addressCode;
    @SerializedName("user_flat")
    public String m_addressFlat;
    @SerializedName("user_subarea")
    public String m_userSubarea;
    @SerializedName("user_area")
    public String m_userArea;
    @SerializedName("user_city")
    public String m_userCity;
    @SerializedName("user_name")
    public String m_userName;
    @SerializedName("user_email_id")
    public String m_userEmail;
    @SerializedName("user_phone_number")
    public String m_userPhone;
    @SerializedName("address_lat")
    public Double m_lat;
    @SerializedName("address_long")
    public Double m_longi;

    public int m_addressId;
    public String m_addressText;


    public AddressSnippet() {

    }

    public AddressSnippet(int addressId, int userCode, int addressCode, String addressText, String userName, String userEmail, String userPhone,Double lat,Double longi) {
        m_addressId = addressId;
        m_userCode = userCode;
        m_addressCode = addressCode;
        m_addressText = addressText;
        m_userName = userName;
        m_userEmail = userEmail;
        m_userPhone = userPhone;
        m_lat=lat;
        m_longi=longi;
    }
}
