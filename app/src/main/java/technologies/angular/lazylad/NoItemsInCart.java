package technologies.angular.lazylad;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by sakshigupta on 23/10/15.
 */
public class NoItemsInCart extends ActionBarActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.no_items_in_cart);
    }
}
