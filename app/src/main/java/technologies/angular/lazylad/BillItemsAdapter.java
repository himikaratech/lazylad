package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Saurabh on 08/04/15.
 */
public class BillItemsAdapter extends BaseAdapter {

    private Activity m_activity;
    private ArrayList<BillItemsSnippet> m_billItemsList;

    public BillItemsAdapter(Activity activity, ArrayList<BillItemsSnippet> billItemslist) {
        m_activity = activity;
        m_billItemsList = billItemslist;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_billItemsList != null) {
            count = m_billItemsList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_billItemsList != null) {
            return m_billItemsList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_billItemsList != null) {
            return position;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_billItemsList != null) {

            String itemCode = m_billItemsList.get(position).m_itemCode;
            String itemName = m_billItemsList.get(position).m_itemName;
            String itemShortDesc = m_billItemsList.get(position).m_itemShortDesc;
            String itemDesc = m_billItemsList.get(position).m_itemDesc;
            double itemCost = m_billItemsList.get(position).m_itemCost;
            int itemQuantity = m_billItemsList.get(position).m_itemQuantity;
            int itemTotalCost;

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.bill_items_list_view, (ViewGroup) null);
            }

            TextView m_itemNameBillTextView = (TextView) convertView.findViewById(R.id.item_name_in_bill_textview);
            TextView m_itemCostBillTextView = (TextView) convertView.findViewById(R.id.item_cost_in_bill_textview);
            TextView m_itemDescBillTextView = (TextView) convertView.findViewById(R.id.item_desc_in_bill_textview);
            TextView m_itemQuantityBillTextView = (TextView) convertView.findViewById(R.id.item_quantity_in_bill_textview);
            TextView m_itemTotalCostBillTextView = (TextView) convertView.findViewById(R.id.item_tatal_cost_in_bill_textview);

            int itemCostInt = (int) itemCost;
            itemTotalCost = itemQuantity * itemCostInt;

            m_itemNameBillTextView.setText(itemName);
            m_itemCostBillTextView.setText("₹" + Double.toString(itemCost));
            m_itemQuantityBillTextView.setText(Integer.toString(itemQuantity));
            m_itemDescBillTextView.setText(itemDesc);
            m_itemTotalCostBillTextView.setText("₹" + Integer.toString(itemTotalCost));
        }
        return convertView;
    }
}
