package technologies.angular.lazylad;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import technologies.angular.lazylad.common.Constants;

/**
 * Created by ashwinikumar on 28/03/16.
 */
public class DeliverySlot {
    private long shopOpeningTime;
    private long shopClosingTime;
    private long timeInterval;
    private long processingTime;

    public DeliverySlot(long shopOpeningTime, long shopClosingTime, long timeInterval, long processingTime) {
        this.shopOpeningTime = shopOpeningTime;
        this.shopClosingTime = shopClosingTime;
        this.timeInterval = timeInterval;
        this.processingTime = processingTime;
    }

    public long getShopOpeningTime() {
        return shopOpeningTime;
    }

    public void setShopOpeningTime(long shopOpeningTime) {
        this.shopOpeningTime = shopOpeningTime;
    }

    public long getShopClosingTime() {
        return shopClosingTime;
    }

    public void setShopClosingTime(long shopClosingTime) {
        this.shopClosingTime = shopClosingTime;
    }

    public long getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(long timeInterval) {
        this.timeInterval = timeInterval;
    }

    public ArrayList<String> getTimeSlotsForToday(long currentTime) {

        Calendar dayStartCal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        dayStartCal.set(Calendar.HOUR_OF_DAY, 0);
        dayStartCal.set(Calendar.MINUTE, 0);
        dayStartCal.set(Calendar.SECOND, 0);
        dayStartCal.set(Calendar.MILLISECOND, 0);

        ArrayList<String> timeSlotList = null;
        if (currentTime >= shopOpeningTime && currentTime <= shopClosingTime) {
            timeSlotList = getTimeSlots(currentTime, shopClosingTime);
        } else if (currentTime > dayStartCal.getTimeInMillis() && currentTime < shopOpeningTime) {
            timeSlotList = getTimeSlots(shopOpeningTime, shopClosingTime);
        }
        return timeSlotList;
    }

    public ArrayList<String> getTimeSlotsForTomorrow() {

        return getTimeSlots(shopOpeningTime, shopClosingTime);
    }

    private ArrayList<String> getTimeSlots(long startTime, long endTime) {
        ArrayList<String> timeSlotList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.US);

        String slot = "";

        while (startTime < endTime) {
            String slotStartTime = dateFormat.format(startTime);
            String slotEndTime = dateFormat.format(startTime + timeInterval);
            slot = slotStartTime + " - " + slotEndTime;
            timeSlotList.add(slot);
            startTime += timeInterval;
        }

        return timeSlotList;
    }

    public String getTodayDate(int serviceCode) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);

        if (serviceCode == Constants.LAUNDRY_DELIVERY_SLOT_CODE) {
            calendar.add(Calendar.DAY_OF_MONTH, (int) processingTime);
        }

        return dateFormat.format(calendar.getTime());
    }

    public String getTomorrowDate(int serviceCode) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        if (serviceCode == Constants.LAUNDRY_DELIVERY_SLOT_CODE) {
            calendar.add(Calendar.DAY_OF_MONTH, (int) processingTime + 1);
        } else {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        return dateFormat.format(calendar.getTime());
    }


}
