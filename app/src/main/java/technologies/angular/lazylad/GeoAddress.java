package technologies.angular.lazylad;

/**
 * Created by Amud on 23/11/15.
 */
public class GeoAddress {

    public String m_formattedAddress;
    public  Double m_latitude;
    public Double m_longitude;
    public  String m_cityCode;
    public String m_cityName;
    public   String m_areaName;
    public String m_cityImageAddress;
    public int m_couponInCity;


    public GeoAddress(String formatted_address, Double latitude , Double longitude,String city_code, String city_name , String area_name,String cityImageAddress,int couponInCity )
    {
        m_formattedAddress=formatted_address;
        m_latitude=latitude;
        m_longitude=longitude;
        m_cityCode=city_code;
        m_areaName=area_name;
        m_cityName=city_name;
        m_cityImageAddress=cityImageAddress;
        m_couponInCity=couponInCity;
    }

    public GeoAddress(String formatted_address)
    {
        m_formattedAddress=formatted_address;
    }
}

