package technologies.angular.lazylad;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh on 20/04/15.
 */
public class CitySnippet {

    @SerializedName("city_id")
    public int m_cityId;
    @SerializedName("city_code")
    public String m_cityCode;
    @SerializedName("city_name")
    public String m_cityName;
    @SerializedName("city_image_add")
    public String m_city_image_add;
    @SerializedName("city_image_flag")
    public int m_city_image_flag;

    @SerializedName("city_alias_name")
    public String m_city_alias_name;

    @SerializedName("coupon_exists_in_city")
    public int m_coupon_in_city;

    @SerializedName("show_city_flag")
    public int m_showCityFlag;


    public CitySnippet() {
    }

    public CitySnippet(int id, String cityCode, String cityName, String city_image_add, int city_image_flag,String city_alias_name,int coupon_in_city,int show_city_flag) {
        m_cityId = id;
        m_cityCode = cityCode;
        m_cityName = cityName;
        m_city_image_add = city_image_add;
        m_city_image_flag = city_image_flag;
        m_city_alias_name=city_alias_name;
        m_coupon_in_city=coupon_in_city;
        m_showCityFlag=show_city_flag;
    }
}
