package technologies.angular.lazylad;

/**
 * Created by Amud on 01/10/15.
 */

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;


public class ReferralClass extends ActionBarActivity {
    TextView referralSMSEditBox;
    TextView referralFacebookEditBox;
    TextView referralWhatsappEditBox;
    TextView referralTwitterEditBox;
    TextView referralTextView;
    int m_userCode;
    String referral_code;
    private Toolbar m_toolbar;
    private String mAddressOutput;

    String TITLES[] = {"Home", "Notifications", "Addresses", "Order History", "Share & Earn", "Lazy Wallet", "", "Rate Us", "Feedback", "Call Us", "About Us"};
    int ICONS[] = {R.drawable.home, R.drawable.notification, R.drawable.address, R.drawable.orders, R.drawable.share, R.drawable.wallet, R.drawable.ic_launcher, R.drawable.rate, R.drawable.feedback, R.drawable.call, R.drawable.aboutus};
    String Name = "Current Location:";
    int PROFILE = R.drawable.pro;
    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;
    private View frame;
    private SharedPreferences prefs_location;
    private String areaName;
    private String cityName;
    View shareLAyout;
    View referralLayout;
    private boolean m_b_loaded;
    View scrollReferral;
    String refrerralString;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("referralActivity", "launched");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.referra_layout);
        m_b_loaded = false;

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        m_toolbar = (Toolbar) findViewById(R.id.toolbar);
        referralLayout = findViewById(R.id.referralLayout);
        shareLAyout = findViewById(R.id.shareLayout);
        scrollReferral = findViewById(R.id.scrollReferral);
        scrollReferral.setVisibility(View.GONE);
        m_toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        setSupportActionBar(m_toolbar);
        getSupportActionBar().setTitle("Refer and Earn");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prefs_location = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        areaName = prefs_location.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, "Not_Available");
        cityName = prefs_location.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, "Not Available");

        mAddressOutput = areaName + "," + " " + cityName;


        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView); // Assigning the RecyclerView Object to the xml View
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayViewWidth = size.x;
        frame = (View) findViewById(R.id.content_frame);

        mRecyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, ReferralClass.this, 5);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer object Assigned to the view
        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.width = 9 * (displayViewWidth / 10);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutParams(params);
        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, m_toolbar, R.string.opendrawer, R.string.closedrawer) {


            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (mRecyclerView.getWidth() * slideOffset);
                frame.setTranslationX(moveFactor);
            }
        };
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State


        referralSMSEditBox = (TextView) findViewById(R.id.referralSMS);
        referralFacebookEditBox = (TextView) findViewById(R.id.referralFacebook);
        referralWhatsappEditBox = (TextView) findViewById(R.id.referralWhatsapp);
        referralTwitterEditBox = (TextView) findViewById(R.id.referralTwitter);
        referralTextView = (TextView) findViewById(R.id.referralTextView);
        referral_code = new String();

        setTracker();
        mHelper = new MoEHelper(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("m_b_loaded", m_b_loaded + "");
        SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        Bundle bundle = new Bundle();
        Utils.verify_num(this, bundle, 3, false, NumberParameters.REFERRAL_SKIP_CONSTANT, NumberParameters.REFERRAL_VERIFIED_CONSTANT);
        boolean prefVerified = prefs.getBoolean(NumberParameters.REFERRAL_VERIFIED_CONSTANT, false);
        if (prefVerified) {
            Drawer.closeDrawer(Gravity.LEFT);
            if (m_b_loaded == false)
                getReferralCode();
        }
        mHelper.onResume(this);
    }

    private void getReferralCode() {
        Cursor m_cursor = this.getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_userCode = m_cursor.getInt(1);
                    m_cursor.moveToNext();
                }
            }
        }

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.GetReferralCodeForOwnerRequestModel getReferralCodeForOwnerRequestModel = requestModel.new GetReferralCodeForOwnerRequestModel();
        getReferralCodeForOwnerRequestModel.user_code = String.valueOf(m_userCode);
        getReferralCodeForOwnerRequestModel.owner_type = 0;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();
        apiSuggestionsService.getReferralCodeForOwnerAPICall(getReferralCodeForOwnerRequestModel, new Callback<APIResonseModel.GetReferralCodeForOwnerResponseModel>() {

            @Override
            public void success(APIResonseModel.GetReferralCodeForOwnerResponseModel getReferralCodeForOwnerResponseModel, Response response) {

                int error_code = getReferralCodeForOwnerResponseModel.error_code;
                if (error_code == 45) {
                    ContentValues values = new ContentValues();
                    values.put(LazyContract.UserDetailsEntry.COLUMN_EMAIL_FLAG, 0);
                    values.put(LazyContract.UserDetailsEntry.COLUMN_EMAIL_ID, "");
                    getContentResolver().update(LazyContract.UserDetailsEntry.CONTENT_URI, values, LazyContract.UserDetailsEntry.COLUMN_USER_CODE + "=?", new String[]{String.valueOf(m_userCode)});

                    Bundle bundle = new Bundle();
                    Utils.verify_email(ReferralClass.this, bundle, false, NumberParameters.REFERRAL_SKIP_CONSTANT, NumberParameters.REFERRAL_VERIFIED_CONSTANT);
                } else {

                    boolean success = getReferralCodeForOwnerResponseModel.success;
                    if (success == true) {
                        m_b_loaded = true;
                        scrollReferral.setVisibility(View.VISIBLE);
                        referral_code = getReferralCodeForOwnerResponseModel.referral_code;
                        refrerralString = "Free your evenings and weekends from daily needs shopping with LazyLad. Use referral code - " + Html.fromHtml("<b>" + referral_code + "</b>") + " and get free credits in your wallet." + "\n" + "Download it from  :" + "\n" +
                                "bit.ly/shoplazy";

                        referralTextView.setText(referral_code);
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("RetroError", error.toString());
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    public void SMS(View v) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, refrerralString);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void EMAIL(View v) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "", null));
        intent.putExtra(Intent.EXTRA_SUBJECT, "refer friend");
        intent.putExtra(Intent.EXTRA_TEXT, refrerralString);
        this.startActivity(Intent.createChooser(intent, "Choose an Email client :"));
    }

    public void WHATSAPP(View v) {
        PackageManager pm = getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = refrerralString;

            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public void FACEBOOK(View v) {
        final CallbackManager callbackManager;
        ShareDialog shareDialog;
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("Great App for daily needs")
                    .setContentDescription(refrerralString)
                    .setContentUrl((Uri.parse("https://www.facebook.com/lazyladapp")))
                    .build();
            shareDialog.show(linkContent);
        }
        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(ReferralClass.this,
                        "Successfully posted ", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(ReferralClass.this,
                        "Not successfully  posted ", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(ReferralClass.this,
                        "Please try again after sometime", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void TWITTER(View v) {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.putExtra(Intent.EXTRA_TEXT, refrerralString);
        tweetIntent.setType("text/plain");

        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (resolved) {
            startActivity(tweetIntent);
        } else {
            Intent i = new Intent();
            i.putExtra(Intent.EXTRA_TEXT, refrerralString);
            i.setAction(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://twitter.com/intent/tweet?text=message&via=profileName"));
            startActivity(i);
            Toast.makeText(this, "Twitter app isn't found", Toast.LENGTH_LONG).show();
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.Referral");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
