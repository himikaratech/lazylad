package technologies.angular.lazylad.Snippets;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Amud on 03/02/16.
 */

public class TabSnippet implements Parcelable {

    public  String m_couponName;
    public  String m_couponShortDesc;
    public  String m_couponImageAddress;


    public TabSnippet()
    {

    }

    public TabSnippet(Parcel in)
    {
        this.m_couponName=in.readString();
        this.m_couponShortDesc=in.readString();
        this.m_couponImageAddress=in.readString();
    }
    public TabSnippet(String coupon_name,String coupon_short_desc,
                               String coupon_image_address)
    {
        m_couponName=coupon_name;
        m_couponShortDesc=coupon_short_desc;
        m_couponImageAddress=coupon_image_address;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.m_couponName);
        dest.writeString(this.m_couponShortDesc);
        dest.writeString(this.m_couponImageAddress);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CouponBannerSnippet createFromParcel(Parcel in) {
            return new CouponBannerSnippet(in);
        }

        public CouponBannerSnippet[] newArray(int size) {
            return new CouponBannerSnippet[size];
        }
    };
}

