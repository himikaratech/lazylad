package technologies.angular.lazylad.Snippets;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ashwinikumar on 07/04/16.
 */
public class LaundryCategoryItemSnippet implements Parcelable {

    public String item_name;
    public double item_cost;
    public String item_img;
    public int group;
    public String unit;
    public String item_tax;
    public String item_desc;
    public int itemQuantitySelected;

    protected LaundryCategoryItemSnippet(Parcel in) {
        item_name = in.readString();
        item_cost = in.readDouble();
        item_img = in.readString();
        group = in.readInt();
        unit = in.readString();
        item_tax = in.readString();
        item_desc = in.readString();
        itemQuantitySelected = in.readInt();
    }

    public static final Creator<LaundryCategoryItemSnippet> CREATOR = new Creator<LaundryCategoryItemSnippet>() {
        @Override
        public LaundryCategoryItemSnippet createFromParcel(Parcel in) {
            return new LaundryCategoryItemSnippet(in);
        }

        @Override
        public LaundryCategoryItemSnippet[] newArray(int size) {
            return new LaundryCategoryItemSnippet[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(item_name);
        parcel.writeDouble(item_cost);
        parcel.writeString(item_img);
        parcel.writeInt(group);
        parcel.writeString(unit);
        parcel.writeString(item_tax);
        parcel.writeString(item_desc);
        parcel.writeInt(itemQuantitySelected);
    }
}
