package technologies.angular.lazylad.Snippets;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ashwinikumar on 08/04/16.
 */
public class LaundryServicesRequestedSnippet implements Parcelable {

    String st_code;
    String service_code;
    long pickup_time;
    long delivery_time;
    long gap_time;

    public LaundryServicesRequestedSnippet(String st_code, String service_code, long pickup_time, long delivery_time, long gap_time) {
        this.st_code = st_code;
        this.service_code = service_code;
        this.pickup_time = pickup_time;
        this.delivery_time = delivery_time;
        this.gap_time = gap_time;
    }

    protected LaundryServicesRequestedSnippet(Parcel in) {
        st_code = in.readString();
        service_code = in.readString();
        pickup_time = in.readLong();
        delivery_time = in.readLong();
        gap_time = in.readLong();
    }

    public static final Creator<LaundryServicesRequestedSnippet> CREATOR = new Creator<LaundryServicesRequestedSnippet>() {
        @Override
        public LaundryServicesRequestedSnippet createFromParcel(Parcel in) {
            return new LaundryServicesRequestedSnippet(in);
        }

        @Override
        public LaundryServicesRequestedSnippet[] newArray(int size) {
            return new LaundryServicesRequestedSnippet[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(st_code);
        parcel.writeString(service_code);
        parcel.writeLong(pickup_time);
        parcel.writeLong(delivery_time);
        parcel.writeLong(gap_time);
    }

    public LaundryServicesRequestedSnippet(String st_code, String service_code, long gap_time) {
        this.st_code = st_code;
        this.service_code = service_code;
        this.gap_time = gap_time;
    }

    public String getSt_code() {
        return st_code;
    }

    public String getService_code() {
        return service_code;
    }

    public long getPickup_time() {
        return pickup_time;
    }

    public long getDelivery_time() {
        return delivery_time;
    }

    public void setSt_code(String st_code) {
        this.st_code = st_code;
    }

    public void setService_code(String service_code) {
        this.service_code = service_code;
    }

    public void setPickup_time(long pickup_time) {
        this.pickup_time = pickup_time;
    }

    public void setDelivery_time(long delivery_time) {
        this.delivery_time = delivery_time;
    }

    public void setGap_time(long gap_time) {
        this.gap_time = gap_time;
    }

    public long getGap_time() {
        return gap_time;
    }

}
