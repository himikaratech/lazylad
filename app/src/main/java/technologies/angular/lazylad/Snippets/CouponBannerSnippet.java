package technologies.angular.lazylad.Snippets;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Amud on 30/01/16.
 */
public class CouponBannerSnippet implements Parcelable {

    public String coupon_id;
    public int coupon_img_flag;
    public String coupon_img_add;
    public String coupon_short_desc;
    public String coupon_desc;
    public String redirect_link;
    public String redirect_type;


    public CouponBannerSnippet() {


    }

    public CouponBannerSnippet(Parcel in) {

        this.coupon_id = in.readString();
        this.coupon_img_flag = in.readInt();
        this.coupon_img_add = in.readString();
        this.coupon_short_desc = in.readString();
        this.coupon_desc = in.readString();
        this.redirect_link = in.readString();
        this.redirect_type = in.readString();

    }

    public CouponBannerSnippet(String coupon_id, int coupon_img_flag, String coupon_img_add,
                               String coupon_short_desc, String coupon_desc, String redirect_link,
                               String redirect_type) {


        this.coupon_id = coupon_id;
        this.coupon_img_flag = coupon_img_flag;
        this.coupon_img_add = coupon_img_add;
        this.coupon_short_desc = coupon_short_desc;
        this.coupon_desc = coupon_desc;
        this.redirect_link = redirect_link;
        this.redirect_type = redirect_type;


    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {


        dest.writeString(this.coupon_id);
        dest.writeInt(this.coupon_img_flag);
        dest.writeString(this.coupon_img_add);
        dest.writeString(this.coupon_short_desc);
        dest.writeString(this.coupon_desc);
        dest.writeString(this.redirect_link);
        dest.writeString(this.redirect_type);


    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CouponBannerSnippet createFromParcel(Parcel in) {
            return new CouponBannerSnippet(in);
        }

        public CouponBannerSnippet[] newArray(int size) {
            return new CouponBannerSnippet[size];
        }
    };
}
