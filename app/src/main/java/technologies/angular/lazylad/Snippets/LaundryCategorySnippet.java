package technologies.angular.lazylad.Snippets;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ashwinikumar on 07/04/16.
 */
public class LaundryCategorySnippet implements Parcelable {
    public String category_name;
    public String category_code;
    public String category_desc;
    public ArrayList<LaundryCategoryItemSnippet> items;

    protected LaundryCategorySnippet(Parcel in) {
        category_name = in.readString();
        category_code = in.readString();
        category_desc = in.readString();
        items = in.createTypedArrayList(LaundryCategoryItemSnippet.CREATOR);
    }

    public static final Creator<LaundryCategorySnippet> CREATOR = new Creator<LaundryCategorySnippet>() {
        @Override
        public LaundryCategorySnippet createFromParcel(Parcel in) {
            return new LaundryCategorySnippet(in);
        }

        @Override
        public LaundryCategorySnippet[] newArray(int size) {
            return new LaundryCategorySnippet[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(category_name);
        parcel.writeString(category_code);
        parcel.writeString(category_desc);
        parcel.writeTypedList(items);
    }
}
