package technologies.angular.lazylad.Snippets;

/**
 * Created by ashwinikumar on 07/04/16.
 */
public class LaundryServiceSnippet {
    public String st_code;
    public String st_name;
    public String service_code;
    public String service_name;
    public String score;
    public String desc;
    public int open_time;
    public int close_time;
    public int gap_time;
    public int interval;
    public String service_img;

}