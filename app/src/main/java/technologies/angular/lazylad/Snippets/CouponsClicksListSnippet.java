package technologies.angular.lazylad.Snippets;

/**
 * Created by Amud on 31/01/16.
*/
public class CouponsClicksListSnippet {

    public String coupon_id;
    public int counters;
    public String user_code;

    public CouponsClicksListSnippet(String coupon_id, int clicks_count,String user_code) {
        this.coupon_id = coupon_id;
        counters = clicks_count;
        this.user_code=user_code;
    }

}

