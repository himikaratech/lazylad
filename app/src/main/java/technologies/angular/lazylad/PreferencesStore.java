package technologies.angular.lazylad;

import android.content.Context;
import android.content.SharedPreferences;

import com.truecaller.android.sdk.TrueProfile;

/**
 * Created by Saurabh on 20/04/15.
 */
public class PreferencesStore {

    public static String CITY_SELECTED_CONSTANT = "CITY_SELECTED";
    public static String AREA_SELECTED_CONSTANT = "AREA_SELECTED";
    public static String CITY_NAME_SELECTED_CONSTANT = "CITY_NAME_SELECTED";
    public static String AREA_NAME_SELECTED_CONSTANT = "AREA_NAME_SELECTED";
    public static String CITY_IMAGE_ADD_FLAF_CONSTANT = "CITY_IMAGE_FLAF";
    public static String CITY_IMAGE_ADD_CONSTANT = "CITY_IMAGE";
    public static String VARIFIED_FLAG_PREF_CONSTANT = "VARIFIED_FLAG_PREF";
    public static String VARIFIED_EMAIL_FLAG_PREF_CONSTANT = "VARIFIED_EMAIL_FLAG_PREF";
    public static String APP_START_FLAG_CONSTANT = "APP_START_FLAG";
    public static String SHALL_NUMBER_VERIFY_CONSTANT = "SHALL_NUMBER_VERIFY";




    public static String REFERRAL_JUST_ADDED_CONSTANT = "REFERRAL_JUST_ADDED";
    public static String REFERRAL_CODE_CONSTANT = "REFERRAL_CODE";


    public static String NUMBER_ADDED_CONSTANT = "NUMBER_ADDED";

    public static String NUMBER_JUST_ADDED_CONSTANT = "NUMBER_JUST_ADDED";

    //Lazylad Contact Keys
    public static String CALL_US_PHONE_NUMBER_CONSTANT = "LAZYLAD_CONTACT";



    public static String CITY_CODE_SELECTED_CONSTANT = "CITY_CODE_SELECTED";
    public static String DEFAULT_CITY_CODE_SELECTED_CONSTANT= "Not Available";

    public static  String LATITUDE_SELECTED_CONSTANT = "LATITUDE_SELECTED" ;
    public static  String LONGITUDE_SELECTED_CONSTANT = "LONGITUDE_SELECTED" ;


    public static  Long DEFAULT_LATITUDE_SELECTED_CONSTANT = 0l ;
    public static  Long DEFAULT_LONGITUDE_SELECTED_CONSTANT = 0l ;

    public static String DEFAULT_CITY_SELECTED_CONSTANT= "City";
    public static String DEFAULT_AREA_SELECTED_CONSTANT= "Area";
    public static String DEFAULT_ADDRESS_SELECTED_CONSTANT= "Address";


    public static String CURRENT_APP_VERSION_CONSTANT = "CURRENT_APP_VERSION_CONSTANT";
    public static String SHALL_VERSION_ALLOW_CONSTANT = "SHALL_VERSION_ALLOW";
    public static int DEFAULT_SHALL_VERSION_ALLOW_CONSTANT= 0;

    public static String COUPON_IN_CITY_CONSTANT="coupon_in_city";
    public static int DEFAULT_COUPON_IN_CITY_CONSTANT=0;

    public static final String PREF_USER_FIRST_NAME = "PREF_USER_FIRST_NAME";
    public static final String PREF_USER_LAST_NAME = "PREF_USER_LAST_NAME";
    public static  final String PREF_USER_EMAIL_ID = "PREF_USER_EMAIL_ID";
    public static final String PREF_TRUECALLER_SIGN_IN  = "PREF_TRUECALLER_SIGN_IN";

    public static final String PREF_SHOP_OPEN_TIME = "PREF_SHOP_OPEN_TIME";
    public static final String PREF_SHOP_CLOSE_TIME = "PREF_SHOP_CLOSE_TIME";
    public static final String PREF_TIME_INTERVAL = "PREF_TIME_INTERVAL";
    public static final String PREF_PROCESSING_TIME = "PREF_PROCESSING_TIME";

    public static final String PREF_SERVICES_REQUESTED = "PREF_SERVICES_REQUESTED";

    public static void storeUserCredentials(Context context, TrueProfile trueProfile){
        SharedPreferences preferences = context.getSharedPreferences("technologies.angular.lazylad", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_USER_FIRST_NAME, trueProfile.firstName);
        editor.putString(PREF_USER_LAST_NAME, trueProfile.lastName);
        editor.putString(PREF_USER_EMAIL_ID, trueProfile.email);
        editor.putBoolean(PREF_TRUECALLER_SIGN_IN, true);
        editor.apply();
    }
}
