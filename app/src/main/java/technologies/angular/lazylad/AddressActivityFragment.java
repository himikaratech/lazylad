package technologies.angular.lazylad;

/**
 * Created by Amud on 09/09/15.
 */

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.UserDetailsEntry;
import technologies.angular.lazylad.LazyContract.UserAddressDetailsEntry;

public class AddressActivityFragment extends Fragment {
    private EditText m_customerNameEditText;
    private EditText m_customerNumberEditText;
    private EditText m_customerEmailEditText;
    private EditText m_customerFlatNumEditText;
    private EditText m_customerSubAreaEditText;
    private EditText m_customerAreaEditText;
    private EditText m_customerCityEditText;
    private Button m_addNewAddress;
    private String m_customerName = "";
    private String m_customerNumber = "";
    private String m_customerEmail;
    private String m_customerFlat = "";
    private String m_customerSubArea = "";
    private String m_customerArea = "";
    private String m_customerCity = "";
    private String m_customerAddress = "";
    int m_customerFlag;
    int m_addressCode;
    int m_userCode;
    private SharedPreferences prefs_location;
    private Bundle bundle = new Bundle();
    int flag;
    private ArrayList<AddressSnippet> m_addressList;
    private int m_userAddressId;
    private int position;
    private String userAddress = "";
    int addressCode;
    int changing_user_code = 0;
    Double latitude;
    Double longitude;


    public AddressActivityFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundle = getArguments();
        flag = getArguments().getInt("flag");
        prefs_location = getActivity().getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        latitude = Double.longBitsToDouble(prefs_location.getLong(PreferencesStore.LATITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LATITUDE_SELECTED_CONSTANT));
        longitude = Double.longBitsToDouble(prefs_location.getLong(PreferencesStore.LONGITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LONGITUDE_SELECTED_CONSTANT));


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.addressactivityfragmentlayout, null);
        m_customerArea = prefs_location.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, "Not_Available");
        m_customerCity = prefs_location.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, "Not Available");
        addressCode = bundle.getInt("addressCode", 0);

        Cursor m_cursor = getActivity().getContentResolver().query(
                UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                m_userCode = m_cursor.getInt(1);
                Cursor m_cursorAddress = getActivity().getContentResolver().query(
                        UserAddressDetailsEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        UserAddressDetailsEntry._ID + " DESC LIMIT 1 ");

                if (m_cursorAddress != null && m_cursorAddress.moveToFirst()) {
                    if (m_cursorAddress.getCount() != 0) {
                        m_userAddressId = m_cursorAddress.getInt(0);
                    } else {
                        m_userAddressId = 0;

                        //TODO error handling first time user created, address is also created.
                    }
                }
            } else {
                m_userCode = 0;
                m_userAddressId = 0;
            }
        } else {
            m_userCode = 0;
            m_userAddressId = 0;
            //TODO error handling
        }

        if (flag == 1) {
            changing_user_code = bundle.getInt("changing_user_code", 0);
            m_userCode = bundle.getInt("user_code", 0);
        }


        m_addNewAddress = (Button) v.findViewById(R.id.add_address_button);
        if (flag == 1) {
            m_addNewAddress.setText("Update Address");
        } else {
            m_addNewAddress.setText("Add Address");
        }
        m_customerNameEditText = (EditText) v.findViewById(R.id.customer_name);
        m_customerNumberEditText = (EditText) v.findViewById(R.id.customer_phone_number);
        m_customerFlatNumEditText = (EditText) v.findViewById(R.id.customer_flat_num);
        m_customerSubAreaEditText = (EditText) v.findViewById(R.id.customer_sub_area);
        m_customerAreaEditText = (EditText) v.findViewById(R.id.customer_area);
        m_customerCityEditText = (EditText) v.findViewById(R.id.customer_city);

        m_customerNameEditText.setOnFocusChangeListener(myClickLIstener);
        m_customerNumberEditText.setOnFocusChangeListener(myClickLIstener);
        m_customerFlatNumEditText.setOnFocusChangeListener(myClickLIstener);
        m_customerSubAreaEditText.setOnFocusChangeListener(myClickLIstener);
        m_customerAreaEditText.setOnFocusChangeListener(myClickLIstener);
        m_customerCityEditText.setOnFocusChangeListener(myClickLIstener);


        m_customerAreaEditText.setText(m_customerArea);
        m_customerCityEditText.setText(m_customerCity);
        if (flag == 1) {
            position = bundle.getInt("position");
            m_customerFlag = bundle.getInt("flag");
            m_addressCode = bundle.getInt("addressCode");
            m_userAddressId = bundle.getInt("m_userAddressId");
            m_customerName = bundle.getString("user_name");
            m_customerNumber = bundle.getString("user_phone_number");
            m_customerFlat = bundle.getString("user_flat");
            m_customerSubArea = bundle.getString("user_subarea");
            m_customerArea = bundle.getString("user_area");


            userAddress = bundle.getString("user_address");
            m_customerNameEditText.setText(m_customerName);
            m_customerNumberEditText.setText(m_customerNumber);
            m_customerFlatNumEditText.setText(m_customerFlat);
            m_customerSubAreaEditText.setText(m_customerSubArea);
        }

        m_addNewAddress = (Button) v.findViewById(R.id.add_address_button);
        m_addNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag != 0) {
                    Log.d("flag working", "yes");
                }

                m_customerName = m_customerNameEditText.getText().toString();
                m_customerNumber = m_customerNumberEditText.getText().toString();
                m_customerEmail = "";//m_customerEmailEditText.getText().toString();

                m_customerFlat = m_customerFlatNumEditText.getText().toString();
                m_customerSubArea = m_customerSubAreaEditText.getText().toString();


                if (m_customerName.isEmpty() || m_customerName == null) {
                    showToast("Enter Name");
                } else if (m_customerNumber.isEmpty() || m_customerNumber == null) {
                    showToast("Enter Phone Number");
                } else if ((m_customerFlat.isEmpty() || m_customerFlat == null)) {
                    showToast("Enter Flat Number/House Number");
                } else if ((m_customerArea.isEmpty() || m_customerArea == null || m_customerCity.isEmpty() || m_customerCity == null)) {
                    showToast("Select your area and city");
                } else {
                    View frame;
                    View linear_layout_address_View;

                    frame = (View) getActivity().findViewById(R.id.new_fragment_container);
                    int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
                    linear_layout_address_View = (View) getActivity().findViewById(R.id.overLayView);
                    ViewGroup.LayoutParams params = linear_layout_address_View.getLayoutParams();
                    params.height = height;
                    linear_layout_address_View.setLayoutParams(params);
                    linear_layout_address_View.setBackgroundColor(000000);
                    ObjectAnimator mover = ObjectAnimator.ofFloat(frame, "translationY", 0f);
                    mover.setDuration(300);
                    mover.start();
                    ((AddressActivity) getActivity()).animated_flag = 0;
                    View view = getActivity().getCurrentFocus();

                    if(view != null) {
                        view.clearFocus();
                    }
                    postNewAddForUser();
                }
            }
        });

        return v;
    }

    View.OnFocusChangeListener myClickLIstener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {

                Cursor m_cursor = getActivity().getContentResolver().query(
                        UserAddressDetailsEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);

                if (m_cursor != null && m_cursor.getCount() == 0) {
                    return;
                }

                Log.e("focus", b + "");
                View frame;
                View linear_layout_address_View;
                View scrollView__layout = (View) getView().findViewById(R.id.scrollViewLayout);
                frame = (View) getActivity().findViewById(R.id.new_fragment_container);
                int fragment_height = (getActivity().findViewById(R.id.topLayout)).getHeight();
                Log.e("height", fragment_height + "");
                ViewGroup.LayoutParams params2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, fragment_height);
                scrollView__layout.setLayoutParams(params2);

                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());
                linear_layout_address_View = (View) getActivity().findViewById(R.id.overLayView);
                ViewGroup.LayoutParams params = linear_layout_address_View.getLayoutParams();
                params.height = height;
                linear_layout_address_View.setBackgroundColor(getResources().getColor(R.color.opaque));
                ObjectAnimator mover = ObjectAnimator.ofFloat(frame, "translationY", -250f);
                ((AddressActivity) getActivity()).animated_flag = 1;
                mover.setDuration(300);
                mover.start();
                frame.setLayoutParams(params2);
            }
        }
    };

    private void postNewAddForUser() {


        final ProgressDialog progressDialog = new ProgressDialog(getView().getContext(), R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.PostNewAddForUserRequestModel postNewAddForUserRequestModel = requestModel.new PostNewAddForUserRequestModel();
        postNewAddForUserRequestModel.user_code = Integer.toString(m_userCode);
        postNewAddForUserRequestModel.customer_name = m_customerName;
        postNewAddForUserRequestModel.customer_number = m_customerNumber;
        postNewAddForUserRequestModel.customer_email = m_customerEmail;
        postNewAddForUserRequestModel.customer_flat = m_customerFlat;
        postNewAddForUserRequestModel.customer_subarea = m_customerSubArea;
        postNewAddForUserRequestModel.customer_area = m_customerArea;
        postNewAddForUserRequestModel.customer_city = m_customerCity;
        postNewAddForUserRequestModel.address_lat=latitude;
        postNewAddForUserRequestModel.address_long=longitude;


        String url = new String();

        if (flag != 1) {
            url = "addNewAddressDetailed";
        }


        if (flag == 1) {
            url = "editUserAddressDetailedSynced";
            postNewAddForUserRequestModel.address_code = Integer.toString(addressCode);
            postNewAddForUserRequestModel.changing_user_code = Integer.toString(changing_user_code);
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        apiSuggestionsService.postNewAddForUserAPICall(url, postNewAddForUserRequestModel, new retrofit.Callback<APIResonseModel.PostNewAddForUserResponseModel>() {

            @Override
            public void success(APIResonseModel.PostNewAddForUserResponseModel postNewAddForUserRequestModel, retrofit.client.Response response) {

                int success = postNewAddForUserRequestModel.error;
                if (success == 1) {
                    if (flag != 1) {

                        int num = postNewAddForUserRequestModel.user_details.size();

                        for (int i = 0; i < num; i++) {
                            int user_code = postNewAddForUserRequestModel.user_details.get(i).user_code;
                            int user_address_code = postNewAddForUserRequestModel.user_details.get(i).user_address_code;
                            String userAddress = "";
                            if (m_customerFlat != null && m_customerFlat != "")
                                userAddress += m_customerFlat + ", ";
                            if (m_customerSubArea != null && m_customerSubArea != "")
                                userAddress += m_customerSubArea + ", ";
                            if (m_customerArea != null && m_customerArea != "")
                                userAddress += m_customerArea + ", ";

                            userAddress += m_customerCity;

                            ContentValues addAddressValues = new ContentValues();
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_CODE, user_code);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE, user_address_code);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_ADDRESS, m_customerCity);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_NAME, m_customerName);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_EMAIL_ID, m_customerEmail);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_PHONE_NUMBER, m_customerNumber);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_FLAT, m_customerFlat);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_SUBAREA, m_customerSubArea);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_AREA, m_customerArea);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_LATITUDE, latitude);
                            addAddressValues.put(UserAddressDetailsEntry.COLUMN_LONGITUDE, longitude);



                            if (m_userCode == 0) {
                                ContentValues newUserValues = new ContentValues();
                                newUserValues.put(UserDetailsEntry._ID, 1);
                                newUserValues.put(UserDetailsEntry.COLUMN_USER_CODE, user_code);
                                newUserValues.put(UserDetailsEntry.COLUMN_VERIFIED_FLAG, 0);
                                newUserValues.put(UserDetailsEntry.COLUMN_VERIFIED_NUMBER, "");
                                newUserValues.put(UserDetailsEntry.COLUMN_EMAIL_FLAG, 0);
                                newUserValues.put(UserDetailsEntry.COLUMN_EMAIL_ID, "");

                                addAddressValues.put(UserAddressDetailsEntry._ID, 1);

                                Uri insertUri1 = getActivity().getContentResolver().insert(UserDetailsEntry.CONTENT_URI, newUserValues);
                                Uri insertUri2 = getActivity().getContentResolver().insert(UserAddressDetailsEntry.CONTENT_URI, addAddressValues);

                            } else {
                                addAddressValues.put(UserAddressDetailsEntry._ID, m_userAddressId + 1);
                                Uri insertUri = getActivity().getContentResolver().insert(UserAddressDetailsEntry.CONTENT_URI, addAddressValues);
                            }
                            AddressSnippet m_addSnippet = new AddressSnippet(m_userAddressId + 1, m_userCode, user_address_code, userAddress, m_customerName, m_customerEmail, m_customerNumber,latitude,longitude);
                            ((AddressActivity) getActivity()).addItem(m_addSnippet);

                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                            final AddressActivityFragment addressActivityFragment = new AddressActivityFragment();
                            bundle.putInt("flag", 0);
                            addressActivityFragment.setArguments(bundle);
                            fragmentTransaction.replace(R.id.new_fragment_container, addressActivityFragment, "HELLO");
                            fragmentTransaction.commit();
                        }

                        Cursor m_cursor = getActivity().getContentResolver().query(
                                UserAddressDetailsEntry.CONTENT_URI,
                                null,
                                null,
                                null,
                                null);

                        LinearLayout address = ((AddressActivity) getActivity()).addressFragment;
                        if (m_cursor == null || m_cursor.getCount() == 0) {
                            address.setVisibility(View.GONE);
                        } else
                            address.setVisibility(View.VISIBLE);

                        progressDialog.dismiss();
                    }
                    if (flag == 1) {

                        ContentValues updateAddressValues = new ContentValues();
                        updateAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_ADDRESS, m_customerCity);
                        updateAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_NAME, m_customerName);
                        updateAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_PHONE_NUMBER, m_customerNumber);
                        updateAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_FLAT, m_customerFlat);
                        updateAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_SUBAREA, m_customerSubArea);
                        updateAddressValues.put(UserAddressDetailsEntry.COLUMN_USER_AREA, m_customerArea);
                        updateAddressValues.put(UserAddressDetailsEntry.COLUMN_LATITUDE,latitude);
                        updateAddressValues.put(UserAddressDetailsEntry.COLUMN_LONGITUDE,longitude);

                        String whereClause = UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE + " =? and " + UserAddressDetailsEntry.COLUMN_USER_CODE + " =? ";
                        String[] selectionArgs = new String[]{String.valueOf(addressCode), String.valueOf(m_userCode)};

                        getActivity().getContentResolver().update(UserAddressDetailsEntry.CONTENT_URI, updateAddressValues, whereClause, selectionArgs);
                        progressDialog.dismiss();
                        ((AddressActivity) getActivity()).editItem(m_customerName, m_customerNumber, m_customerFlat, m_customerSubArea, m_customerCity, m_customerArea, m_addressCode);
                        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                        final AddressActivityFragment addressActivityFragment = new AddressActivityFragment();
                        bundle.putInt("flag", 0);
                        addressActivityFragment.setArguments(bundle);
                        fragmentTransaction.replace(R.id.new_fragment_container, addressActivityFragment, "HELLO");
                        fragmentTransaction.commit();
                    }

                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });

    }

    protected void showToast(String text) {
        Toast.makeText(getView().getContext(), text, Toast.LENGTH_SHORT).show();
    }
}