package technologies.angular.lazylad;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Paresh on 22-06-2015.
 */
public class NotificationCenterSnippet {

    @SerializedName("offer_code")
    public int m_offerCode;
    @SerializedName("offer_desc")
    public String m_offerDesc;
    @SerializedName("offer_launch_time")
    public Long m_launchTime;
    @SerializedName("offer_validity")
    public Long m_offerValidity;
    @SerializedName("offer_img_flag")
    public int m_offerImgFlag;
    @SerializedName("offer_img_add")
    public String m_offerImgAdd;

    public NotificationCenterSnippet(int m_offerCode, String m_offerDesc, Long m_launchTime,
                                     Long m_offerValidity, int m_offerImgFlag, String m_offerImgAdd) {
        this.m_offerCode = m_offerCode;
        this.m_offerDesc = m_offerDesc;
        this.m_launchTime = m_launchTime;
        this.m_offerValidity = m_offerValidity;
        this.m_offerImgFlag = m_offerImgFlag;
        this.m_offerImgAdd = m_offerImgAdd;
    }
}
