package technologies.angular.lazylad;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class LastActivity extends ActionBarActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.totalAmount)
    TextView totalAmountTextView;
    @Bind(R.id.call)
    ImageView callImageView;
    @Bind(R.id.cust_item_details_at_final)
    ListView m_allItemsListView;
    @Bind(R.id.main_activity_button)
    Button m_mainActivityButton;

    @Bind(R.id.service_order_layout)
    LinearLayout serviceLayout;
    @Bind(R.id.service_pickup_time_textview)
    TextView pickupTimeTextView;
    @Bind(R.id.service_delivery_time_textview)
    TextView deliveryTimeTextView;
    @Bind(R.id.service_order_code_textview)
    TextView orderCodeTextView;

    LastActivityAdapter m_lastActivityAdapter;

    public static final String TOTAL_AMOUNT_OF_ORDERS_TAG = "totalAmountOfAllOrders";
    public static final String FINAL_LIST_OF_ORDERS_TAG = "finalListOfOrders";

    public static final String CALLING_ACTIVITY_TAG = "CALLING_ACTIVITY_TAG";
    public static final String SERVICE_ORDER_CODE = "SERVICE_ORDER_CODE";
    public static final String SERVICE_PICKUP_TIME = "SERVICE_PICKUP_TIME";
    public static final String SERVICE_DELIVERY_TIME = "SERVICE_DELIVERY_TIME";

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.last);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Order Confirmation");
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);

        Intent intent = getIntent();
        int callingActivityCode = intent.getIntExtra(CALLING_ACTIVITY_TAG, 0);
        if (callingActivityCode == 1) {
            String pickupTime = intent.getStringExtra(SERVICE_PICKUP_TIME);
            String deliveryTime = intent.getStringExtra(SERVICE_DELIVERY_TIME);
            String orderCode = intent.getStringExtra(SERVICE_ORDER_CODE);
            m_allItemsListView.setVisibility(View.GONE);
            totalAmountTextView.setVisibility(View.GONE);
            serviceLayout.setVisibility(View.VISIBLE);
            pickupTimeTextView.setText(pickupTime);
            deliveryTimeTextView.setText(deliveryTime);
            orderCodeTextView.setText(orderCode);
        } else {
            serviceLayout.setVisibility(View.GONE);
            m_allItemsListView.setVisibility(View.VISIBLE);
            totalAmountTextView.setVisibility(View.VISIBLE);
            double displayTotalAmount = intent.getDoubleExtra(TOTAL_AMOUNT_OF_ORDERS_TAG, 0);
            totalAmountTextView.setText("TOTAL AMOUNT : " + displayTotalAmount);

            ArrayList<LastActivitySnippet> totalOrderList = intent.getParcelableArrayListExtra(FINAL_LIST_OF_ORDERS_TAG);
            int numberOfItems = totalOrderList.size();
            m_lastActivityAdapter = new LastActivityAdapter(LastActivity.this, totalOrderList);
            m_allItemsListView.setAdapter((ListAdapter) m_lastActivityAdapter);
            Utils.setListViewHeightBasedOnItems(m_allItemsListView);
        }

        callImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences prefs = getSharedPreferences(
                        "technologies.angular.lazylad", Context.MODE_PRIVATE);

                String phoneNumberForCall = prefs.getString(PreferencesStore.CALL_US_PHONE_NUMBER_CONSTANT, "Not Available");
                try {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:+91-" + phoneNumberForCall));
                    startActivity(callIntent);
                } catch (ActivityNotFoundException e) {
                    Log.e("helloandroid dialing", "Call failed", e);
                }
            }
        });

        m_mainActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShoppingCartUtils.destroyCart(v.getContext());

                Intent myIntent = new Intent(getApplicationContext(), MainClass.class);
                myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myIntent);
                LastActivity.this.finish();

            }
        });

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


        setTracker();
        mHelper = new MoEHelper(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
        mHelper.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
        mHelper.onResume(this);
    }

    public void rate(View v) {
        Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
        }
    }

    public void share(View v) {
        Intent i = new Intent(android.content.Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Try this app. I just ordered my daily needs from it and its awesome. Use coupon code SHOPLAZY and get ₹50 off on your order.");
        i.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/" +
                "details?id=technologies.angular.lazylad");
        startActivity(Intent.createChooser(i, "Share via"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        ShoppingCartUtils.destroyCart(this);

        Intent intent = new Intent(this, MainClass.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_last, menu);
        return true;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.LastActivity");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void animate(final ImageView imageView, final int images[], final int imageIndex, final boolean forever) {

        //imageView <-- The View which displays the images
        //images[] <-- Holds R references to the images to display
        //imageIndex <-- index of the first image to show in images[]
        //forever <-- If equals true then after the last image it starts all over again with the first image resulting in an infinite loop. You have been warned.

        int fadeInDuration = 1000; // Configure time values here
        int timeBetween = 1000;
        int fadeOutDuration = 1000;

        imageView.setVisibility(View.INVISIBLE);    //Visible or invisible by default - this will apply when the animation ends
        imageView.setImageResource(images[imageIndex]);

        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
        fadeIn.setDuration(fadeInDuration);

        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
        fadeOut.setStartOffset(fadeInDuration + timeBetween);
        fadeOut.setDuration(fadeOutDuration);

        AnimationSet animation = new AnimationSet(false); // change to false
        animation.addAnimation(fadeIn);
        animation.addAnimation(fadeOut);
        animation.setRepeatCount(1);
        imageView.setAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (images.length - 1 > imageIndex) {
                    animate(imageView, images, imageIndex + 1, forever); //Calls itself until it gets to the end of the array
                } else {
                    if (forever == true) {
                        animate(imageView, images, 0, forever);  //Calls itself to start the animation all over again in a loop if forever = true
                    }
                }
            }

            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub
            }

            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
