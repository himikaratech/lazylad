package technologies.angular.lazylad;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.moe.pushlibrary.utils.MoEHelperConstants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

/**
 * Created by Amud on 09/09/15.
 */
public class EmailVerify extends Activity {

    private EditText mEmailEditText;
    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mReferralCodeEditText;
    private Button mSubmitBtn;

    private String mChangedEmailId;
    private String mChangedFirstName;
    private String mChangedLastName;

    private String mOldEmailId;
    private String mOldFirstName;
    private String mOldLastName;
    private boolean mIsTrueCallerSignIn = false;
    private int mSituation;

    static Context mContext;
    private int m_userCode = 0;
    private int email_flag = 0;
    SharedPreferences prefs;
    String prefVerifiedString;
    String prefSkippedString;
    Boolean skippable;

    private Pattern pattern;
    private Matcher matcher;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public EmailVerify() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    private MoEHelper mHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        Intent intent = getIntent();
        prefVerifiedString = intent.getStringExtra(NumberParameters.PREF_VERIFIED_CONSTANT);
        prefSkippedString = intent.getStringExtra(NumberParameters.PREF_STRING_CONTANT);
        skippable = intent.getBooleanExtra(NumberParameters.SKIPPABLE_CONSTANT, false);

        prefs = EmailVerify.this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        mChangedFirstName = prefs.getString(PreferencesStore.PREF_USER_FIRST_NAME, "");
        mChangedLastName = prefs.getString(PreferencesStore.PREF_USER_LAST_NAME, "");
        mChangedEmailId = prefs.getString(PreferencesStore.PREF_USER_EMAIL_ID, "");
        mIsTrueCallerSignIn = prefs.getBoolean(PreferencesStore.PREF_TRUECALLER_SIGN_IN, false);

        mEmailEditText = (EditText) findViewById(R.id.user_email_edit_text);
        mFirstNameEditText = (EditText) findViewById(R.id.userr_fist_name_edit_text);
        mLastNameEditText = (EditText) findViewById(R.id.user_last_name_edit_text);
        mReferralCodeEditText = (EditText) findViewById(R.id.user_referral_edit_text);
        mSubmitBtn = (Button) findViewById(R.id.submitButton);

        Cursor m_cursor = getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                m_userCode = m_cursor.getInt(1);
                email_flag = m_cursor.getInt(2);
            }
        } else {
            m_userCode = 0;
            email_flag = 0;
        }

        if (!mIsTrueCallerSignIn) {
            getUserProfileDetails(m_userCode);
        } else {
            mEmailEditText.setText(mChangedEmailId);
            mFirstNameEditText.setText(mChangedFirstName);
            mLastNameEditText.setText(mChangedLastName);
        }

        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mChangedEmailId = mEmailEditText.getText().toString().trim();
                mChangedFirstName = mFirstNameEditText.getText().toString().trim();
                mChangedLastName = mLastNameEditText.getText().toString().trim();

                if (mChangedFirstName.isEmpty() || mChangedFirstName == null) {
                    Toast.makeText(EmailVerify.this, "Please enter Fist Name", Toast.LENGTH_LONG).show();
                } else if (validate(mChangedEmailId)) {
                    sendEmailToServer();
                } else
                    Toast.makeText(EmailVerify.this, "Please enter a valid Email ID", Toast.LENGTH_LONG).show();
            }
        });
        setTracker();
        mHelper = new MoEHelper(this);
    }

    private void getUserProfileDetails(int userCode) {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.GetUserProfileRequestModel getUserProfileRequestModel = requestModel.new GetUserProfileRequestModel();
        getUserProfileRequestModel.user_code = Integer.toString(userCode);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getUserProfileAPICall(getUserProfileRequestModel, new retrofit.Callback<APIResonseModel.GetUserProfileResponseModel>() {

            @Override
            public void success(APIResonseModel.GetUserProfileResponseModel sendEmailToServerResponseModel, retrofit.client.Response response) {
                int success = sendEmailToServerResponseModel.error;
                if (success == 1) {
                    mOldEmailId = sendEmailToServerResponseModel.user_profile_details.user_email;
                    mOldFirstName = sendEmailToServerResponseModel.user_profile_details.user_first_name;
                    mOldLastName = sendEmailToServerResponseModel.user_profile_details.user_last_name;

                    mEmailEditText.setText(mOldEmailId);
                    mFirstNameEditText.setText(mOldFirstName);
                    mLastNameEditText.setText(mOldLastName);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                Toast.makeText(EmailVerify.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void sendEmailToServer() {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.LoudUserProfileRequestModel sendEmailToServerRequestModel = requestModel.new LoudUserProfileRequestModel();

        sendEmailToServerRequestModel.user_code = Integer.toString(m_userCode);

        if (!mChangedEmailId.equals(mOldEmailId))
            sendEmailToServerRequestModel.user_email = mChangedEmailId;
        if (!mChangedFirstName.equals(mOldFirstName))
            sendEmailToServerRequestModel.user_first_name = mChangedFirstName;
        if (!mChangedLastName.equals(mOldLastName))
            sendEmailToServerRequestModel.user_last_name = mChangedLastName;

        String referralCode = mReferralCodeEditText.getText().toString();
        sendEmailToServerRequestModel.referralCode = !referralCode.isEmpty()? referralCode: null;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.loadUserProfileAPICall(sendEmailToServerRequestModel, new retrofit.Callback<APIResonseModel.LoadUserProfileResponseModel>() {

            @Override
            public void success(APIResonseModel.LoadUserProfileResponseModel sendEmailToServerResponseModel, retrofit.client.Response response) {
                int success = sendEmailToServerResponseModel.error;
                if (success == 1) {
                    ContentValues values = new ContentValues();
                    values.put(LazyContract.UserDetailsEntry.COLUMN_EMAIL_FLAG, 1);
                    values.put(LazyContract.UserDetailsEntry.COLUMN_EMAIL_ID, mEmailEditText.getText().toString());
                    getContentResolver().update(LazyContract.UserDetailsEntry.CONTENT_URI, values, LazyContract.UserDetailsEntry.COLUMN_USER_CODE + "=?", new String[]{String.valueOf(m_userCode)});

                    SharedPreferences.Editor editor = prefs.edit();
                    if (skippable == true) {
                        editor.putBoolean(prefSkippedString, true);
                        editor.putBoolean(prefVerifiedString, true);
                    }
                    editor.putInt(PreferencesStore.VARIFIED_EMAIL_FLAG_PREF_CONSTANT, 1);
                    editor.putBoolean(prefVerifiedString, true);
                    editor.commit();
                    progressDialog.dismiss();

                    MoEHelper.getInstance(EmailVerify.this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_EMAIL, mChangedEmailId);
                    MoEHelper.getInstance(EmailVerify.this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_FIRST_NAME, mChangedFirstName);
                    MoEHelper.getInstance(EmailVerify.this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_LAST_NAME, mChangedLastName);

                    PayloadBuilder builder = new PayloadBuilder();
                    builder.putAttrString("User Email Id", mChangedEmailId);
                    builder.putAttrString("User First Name", mChangedFirstName);
                    builder.putAttrString("User Last Name", mChangedLastName);
                    MoEHelper.getInstance(EmailVerify.this).trackEvent("User Profile", builder.build());

                    Bundle bundle = new Bundle();
                    bundle = getIntent().getExtras();
                    // Finish the main activity to avoid two instances
                    ((Activity) mContext).finish();
                    Intent in = new Intent(EmailVerify.this, mContext.getClass());
                    in.putExtras(bundle);

                    startActivity(in);
                    EmailVerify.this.finish();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                Toast.makeText(EmailVerify.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public boolean validate(final String hex) {
        matcher = pattern.matcher(hex);
        return matcher.matches();
    }

    public static void setContext(Context Context) {
        mContext = Context;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ((Activity) mContext).finish();
        EmailVerify.this.finish();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.ProfileScreen");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}


