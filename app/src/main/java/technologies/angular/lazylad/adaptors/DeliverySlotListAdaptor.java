package technologies.angular.lazylad.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;

import java.util.ArrayList;

import technologies.angular.lazylad.R;

/**
 * Created by ashwinikumar on 29/03/16.
 */
public class DeliverySlotListAdaptor extends RecyclerView.Adapter<DeliverySlotListAdaptor.DeliveryListViewHolder> {
    private ArrayList<String> mSlotList = null;
    private Context mContext = null;
    private int mSelectedPosition = -1;
    private InSlotListCallBack mCallBack;
    private boolean mIsListChanged = false;

    public DeliverySlotListAdaptor(ArrayList<String> slotList, Context context, InSlotListCallBack callBack) {
        mSlotList = slotList;
        mContext = context;
        mCallBack = callBack;

    }

    @Override
    public DeliveryListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.delivery_slot_list_item, parent, false);

        DeliveryListViewHolder viewHolder = new DeliveryListViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DeliveryListViewHolder holder, final int position) {
        String slot = mSlotList.get(position);
        holder.mTimeSlotRadioBtn.setText(slot);
        if (!mIsListChanged) {
            holder.mTimeSlotRadioBtn.setChecked(position == mSelectedPosition);
        } else {
            holder.mTimeSlotRadioBtn.setChecked(false);
        }
        holder.mTimeSlotRadioBtn.setTag(position);
        holder.mTimeSlotRadioBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallBack.onClicked(position);
                mSelectedPosition = (int) v.getTag();
                mIsListChanged = false;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSlotList.size();
    }

    public static class DeliveryListViewHolder extends RecyclerView.ViewHolder {

        private RadioButton mTimeSlotRadioBtn;

        public DeliveryListViewHolder(View itemView) {
            super(itemView);

            mTimeSlotRadioBtn = (RadioButton) itemView.findViewById(R.id.time_slot_radio_btn);

        }
    }

    public void updateSlotList(ArrayList<String> slotList) {
        mSlotList = slotList;
        mIsListChanged = true;
    }

    public interface InSlotListCallBack {
        void onClicked(int position);
    }
}
