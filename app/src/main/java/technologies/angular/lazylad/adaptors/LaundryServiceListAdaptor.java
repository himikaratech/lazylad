package technologies.angular.lazylad.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import technologies.angular.lazylad.R;
import technologies.angular.lazylad.Snippets.LaundryServiceSnippet;

/**
 * Created by ashwinikumar on 15/04/16.
 */
public class LaundryServiceListAdaptor extends RecyclerView.Adapter<LaundryServiceListAdaptor.LaundryServiceViewHolder> {

    private ArrayList<LaundryServiceSnippet> mServicesList;
    private Context mContext;
    private OnServiceListItemListener mListener;

    public LaundryServiceListAdaptor(Context context, ArrayList<LaundryServiceSnippet> serviceList, OnServiceListItemListener listener) {
        mContext = context;
        mServicesList = serviceList;
        mListener = listener;
    }

    @Override
    public LaundryServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.laundry_service_list_item, parent, false);

        LaundryServiceViewHolder holder = new LaundryServiceViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LaundryServiceViewHolder holder, int position) {

        final LaundryServiceSnippet service = mServicesList.get(position);
        holder.serviceCheckBox.setText(service.service_name);
        holder.serviceGapTimeTextView.setText(service.gap_time/60 + " hrs");
        holder.serviceCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                mListener.setCheckedState(isChecked, service);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mServicesList.size();
    }

    public static class LaundryServiceViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.service_checkbox)
        CheckBox serviceCheckBox;
        @Bind(R.id.service_processing_time_textview)
        TextView serviceGapTimeTextView;

        public LaundryServiceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnServiceListItemListener{
        void setCheckedState(boolean isChecked, LaundryServiceSnippet service);
    }
}
