package technologies.angular.lazylad.adaptors;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import technologies.angular.lazylad.Snippets.LaundryCategoryItemSnippet;
import technologies.angular.lazylad.Snippets.LaundryCategorySnippet;
import technologies.angular.lazylad.fragments.LaundryCategoryFragment;

/**
 * Created by ashwinikumar on 07/04/16.
 */
public class LaundryRateListPagerAdaptor extends FragmentStatePagerAdapter {

    private ArrayList<ArrayList<LaundryCategoryItemSnippet>>mLaundryCategorySnippetList;
    private ArrayList<String>mCategoryTypeList;

    public LaundryRateListPagerAdaptor(FragmentManager fm, ArrayList<ArrayList<LaundryCategoryItemSnippet>>categoriesList, ArrayList<String>categoryTypeList) {
        super(fm);
        mLaundryCategorySnippetList = categoriesList;
        mCategoryTypeList = categoryTypeList;

    }

    @Override
    public Fragment getItem(int position) {
        LaundryCategoryFragment fragment = LaundryCategoryFragment.newInstance(mLaundryCategorySnippetList.get(position));
        return fragment;
    }

    @Override
    public int getCount() {
        return mLaundryCategorySnippetList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mCategoryTypeList.get(position);
    }
}
