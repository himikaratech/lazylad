package technologies.angular.lazylad.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import technologies.angular.lazylad.R;
import technologies.angular.lazylad.Snippets.LaundryCategoryItemSnippet;

/**
 * Created by ashwinikumar on 07/04/16.
 */
public class LaundryItemListAdaptor extends RecyclerView.Adapter<LaundryItemListAdaptor.LaundryItemHolder> {
    private Context mContext;
    private ArrayList<LaundryCategoryItemSnippet> mItemList;

    private OnItemBtnClickedListener mOnItemBtnClickedListener;

    public LaundryItemListAdaptor(Context context, ArrayList<LaundryCategoryItemSnippet> itemList, OnItemBtnClickedListener clickedListener) {
        mContext = context;
        mItemList = itemList;
        mOnItemBtnClickedListener = clickedListener;
    }

    @Override
    public LaundryItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.laundry_category_list_item, parent, false);

        LaundryItemHolder viewHolder = new LaundryItemHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final LaundryItemHolder holder, final int position) {

        final LaundryCategoryItemSnippet categoryItem = mItemList.get(position);
        String itemImageAdd = categoryItem.item_img;

        if (itemImageAdd == null || itemImageAdd.equals("")) itemImageAdd = "\"\"";

        Picasso.with(mContext)
                .load(itemImageAdd)
                .fit().centerInside()
                .placeholder(R.drawable.loading)
                .error(R.drawable.no_image)
                .into(holder.mItemImageView);
        holder.mItemNameTextView.setText(categoryItem.item_name);
        holder.mItemDescTextView.setText(categoryItem.unit);
        holder.mItemPriceTextView.setText("₹ " + categoryItem.item_cost + "");
        holder.mItemCategoryTextView.setText(categoryItem.item_desc);
        holder.mItemQuantity.setText(categoryItem.itemQuantitySelected + "");

        // MoEngage Tracking
        final PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("Item Name", categoryItem.item_name);

        holder.mItemPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // MoEngage Tracking
                MoEHelper.getInstance(mContext).trackEvent("Item plus pressed", builder.build());

                categoryItem.itemQuantitySelected++;
                holder.mItemQuantity.setText(categoryItem.itemQuantitySelected + "");
                mOnItemBtnClickedListener.onPlusBtnClicked(categoryItem.item_cost + "", categoryItem.item_tax);

            }
        });

        holder.mItemMinusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int itemQuantity = categoryItem.itemQuantitySelected;
                if (itemQuantity > 0) {
                    // MoEngage Tracking
                    MoEHelper.getInstance(mContext).trackEvent("Item minus pressed" + categoryItem.item_name, builder.build());

                    categoryItem.itemQuantitySelected--;
                    holder.mItemQuantity.setText(categoryItem.itemQuantitySelected + "");
                    mOnItemBtnClickedListener.onMinusBtnClicked(categoryItem.item_cost + "", categoryItem.item_tax);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    public static class LaundryItemHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.item_image)
        ImageView mItemImageView;
        @Bind(R.id.item_name)
        TextView mItemNameTextView;
        @Bind(R.id.item_description)
        TextView mItemDescTextView;
        @Bind(R.id.item_price)
        TextView mItemPriceTextView;
        @Bind(R.id.item_mrp)
        TextView mItemMrpTextView;
        @Bind(R.id.item_offer_price_layout)
        LinearLayout mOfferPriceLayout;
        @Bind(R.id.item_offer_price)
        TextView mOfferPriceTextView;
        @Bind(R.id.item_plus_button)
        Button mItemPlusButton;
        @Bind(R.id.item_minus_button)
        Button mItemMinusButton;
        @Bind(R.id.item_quantity_selected)
        TextView mItemQuantity;
        @Bind(R.id.item_category)
        TextView mItemCategoryTextView;

        public LaundryItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemBtnClickedListener {
        void onPlusBtnClicked(String price, String tax);

        void onMinusBtnClicked(String price, String tax);
    }
}
