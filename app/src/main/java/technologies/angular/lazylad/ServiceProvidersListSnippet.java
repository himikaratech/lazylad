package technologies.angular.lazylad;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Saurabh on 11/02/15.
 */
public class ServiceProvidersListSnippet {
    public int m_servProvId;
    @SerializedName("sp_code")
    public String m_servProvCode;
    @SerializedName("sp_name")
    public String m_servProvName;
    @SerializedName("sp_number")
    public String m_servProvNumber;
    @SerializedName("sp_del_time")
    public String m_servProvDelTime;
    @SerializedName("sp_min_order")
    public String m_servProvMinOrder;
    @SerializedName("sp_type")
    public String m_servType;
    @SerializedName("shop_open_time")
    public int m_shop_open_time;
    @SerializedName("shop_close_time")
    public int m_shop_close_time;
    @SerializedName("images_available")
    public int m_images_available;

    @SerializedName("seller_taxes")
    public TaxPerSellerModel sellerTaxes;

    public class TaxPerSellerModel {
        @SerializedName("tax_display_1")
        public String taxName1;
        @SerializedName("tax_number_1")
        public double taxCent1;
        @SerializedName("tax_flag_1")
        public int taxFlag1;
        @SerializedName("tax_display_2")
        public String taxName2;
        @SerializedName("tax_number_2")
        public double taxCent2;
        @SerializedName("tax_flag_2")
        public int taxFlag2;
        @SerializedName("tax_display_3")
        public String taxName3;
        @SerializedName("tax_number_3")
        public double taxCent3;
        @SerializedName("tax_flag_3")
        public int taxFlag3;
    }

    public ServiceProvidersListSnippet() {

    }

    public ServiceProvidersListSnippet(int id, String servProvCode, String servProvName, String servProvNumber, String servProvDelTime, String servProvMinOrder, String servType, int shop_open_time, int shop_close_time, int images_available) {
        m_servProvId = id;
        m_servProvCode = servProvCode;
        m_servProvName = servProvName;
        m_servProvNumber = servProvNumber;
        m_servProvDelTime = servProvDelTime;
        m_servProvMinOrder = servProvMinOrder;
        m_servType = servType;
        m_shop_open_time = shop_open_time;
        m_shop_close_time = shop_close_time;
        m_images_available = images_available;
    }
}
