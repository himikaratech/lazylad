package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Vector;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.SearchResultEntry;

public class SearchResultsActivity extends ActionBarActivity {
    static String TAG = "technologies.angular.lazylad.SearchResultsActivity";

    private ListView m_searchResultListView;
    private ArrayList<ItemSnippet> m_searchItemsDetailsArrayList;
    private SearchResultsAdapter m_searchResultsAdapter;

    private static String m_servProvCode;
    private static String m_stCode;
    Toolbar toolbar;

    private AsyncTask m_async5;

    private String m_query = null;
    private String m_itemCode;

    private boolean shallLoad = true;
    private boolean doneLoading = true;

    ProgressDialog m_progressDialog;
    SearchView searchView;
    public int m_images_available;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_results);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Search Results");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        m_servProvCode = prefs.getString("ServiceProviderCode", "Not Available");
        m_stCode = prefs.getString("ServTypeCode", "Not Available");
        m_images_available = prefs.getInt("images_available", 1);
        m_searchResultListView = (ListView) findViewById(R.id.search_result_list_view);

        Intent searchIntent = getIntent();
        handleIntent(searchIntent);

        if (Intent.ACTION_SEARCH.equals(searchIntent.getAction())) {
            m_searchResultListView.setOnScrollListener(new LL_ScrollListener());
        }
        setTracker();
    }

    private void getMoreSearchedItemsFromServerLimited() {

        m_progressDialog = new ProgressDialog(this, R.style.MyTheme);
        m_progressDialog.setCancelable(false);
        m_progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        m_progressDialog.show();

        String previous_item_code;
        if (null == m_searchItemsDetailsArrayList || m_searchItemsDetailsArrayList.isEmpty())
            previous_item_code = "0";
        else
            previous_item_code = m_searchItemsDetailsArrayList.get(m_searchItemsDetailsArrayList.size() - 1).m_itemCode;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.searchFeeds(m_servProvCode, m_stCode, "1", m_query, previous_item_code, new Callback<ItemModel>() {

            @Override
            public void success(ItemModel searchResponse, retrofit.client.Response response) {
                ArrayList<ItemSnippet> itemsList = new ArrayList<ItemSnippet>();
                itemsList = searchResponse.items_service_providers;
                if (null != itemsList && !itemsList.isEmpty()) {
                    m_async5 = new StoreItemsInSqliteandLoad(m_query).execute(itemsList);
                } else {
                    shallLoad = false;
                    doneLoading = true;
                    m_progressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("", error.toString());
                m_progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(TAG);

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private class LL_ScrollListener implements AbsListView.OnScrollListener {

        int visibleThreshold = 1;
        int visibleItemCount;
        int totalItemCount;
        int firstVisibleItem;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE) {
                if (shallLoad && doneLoading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    doneLoading = false;
                    getMoreSearchedItemsFromServerLimited();
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            this.firstVisibleItem = firstVisibleItem;
            this.visibleItemCount = visibleItemCount;
            this.totalItemCount = totalItemCount;
            if (this.totalItemCount == 0)
                onScrollStateChanged(view, SCROLL_STATE_IDLE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        m_searchResultListView.setOnScrollListener(null);
        m_searchItemsDetailsArrayList = null;
        handleIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            m_searchResultListView.setOnScrollListener(new LL_ScrollListener());
        }
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            m_query = intent.getStringExtra(SearchManager.QUERY);

            Tracker search = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
            search.send(new HitBuilders.EventBuilder()
                    .setCategory("Search")
                    .setAction(m_query)
                    .build());

            Cursor cursor = loadNewestItemsFromSqliteForQuery(m_query);
            processCursorData(cursor);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Handle a suggestions click (because the suggestions all use ACTION_VIEW)
            m_itemCode = intent.getDataString();
            final String sSearchSelection =
                    LazyContract.SearchResultEntry.TABLE_ALIAS_NAME + "." + LazyContract.ItemDetailsEntry.COLUMN_ITEM_CODE + " = ? ";
            Cursor cursor = getContentResolver().query(
                    LazyContract.SearchResultEntry.buildSearchResultItemsUri(2, m_servProvCode),
                    null,
                    sSearchSelection,
                    new String[]{m_itemCode},
                    String.valueOf(1));

            processCursorData(cursor);
        }
    }

    private void deleteItemsForOtherQueries() {
        String whereClause;
        String[] whereArg;

        if (m_query != null) {
            whereClause = LazyContract.SearchResultEntry.COLUMN_SEARCH_STRING + " NOT LIKE ? ";
            whereArg = new String[]{m_query};
        } else {
            whereClause = null;
            whereArg = null;
        }
        getContentResolver().delete(
                SearchResultEntry.CONTENT_URI,
                whereClause,
                whereArg
        );
    }

    private void processCursorData(Cursor cursor) {
        ArrayList<ItemSnippet> searchItemsDetailsArrayList = new ArrayList<ItemSnippet>();

        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    String itemCode = cursor.getString(1);
                    String itemName = cursor.getString(2);
                    int item_img_flag = cursor.getInt(3);
                    String item_img_address = cursor.getString(4);
                    String item_unit = cursor.getString(5);
                    double item_cost = cursor.getDouble(6);
                    String item_short_desc = cursor.getString(7);
                    String item_desc = cursor.getString(8);
                    String item_status = cursor.getString(9);
                    int selected = cursor.getInt(10);
                    int quantityselected = cursor.getInt(11);
                    String itemServiceType = cursor.getString(12);
                    String itemServiceTypeCategory = cursor.getString(13);
                    double itemMrp = cursor.getDouble(14);
                    double itemOfferCost = cursor.getDouble(15);
                    int m_itemInOffer = cursor.getInt(16);

                    ItemSnippet m_itemSnipObj = new ItemSnippet(i + 1, itemCode, itemName, item_img_flag,
                            item_img_address, item_unit, item_cost, item_short_desc, item_desc, item_status,
                            selected, quantityselected, itemServiceType, itemServiceTypeCategory, m_servProvCode, itemMrp,itemOfferCost,m_itemInOffer);
                    searchItemsDetailsArrayList.add(m_itemSnipObj);
                    cursor.moveToNext();
                }
            }
        }
        if (cursor != null) cursor.close();

        if (null == m_searchItemsDetailsArrayList) {
            m_searchItemsDetailsArrayList = searchItemsDetailsArrayList;
            m_searchResultsAdapter = new SearchResultsAdapter(SearchResultsActivity.this,SearchResultsActivity.this.getApplicationContext(), m_searchItemsDetailsArrayList, m_images_available,m_servProvCode);
            m_searchResultListView.setAdapter((ListAdapter) m_searchResultsAdapter);
        } else {
            m_searchItemsDetailsArrayList.addAll(searchItemsDetailsArrayList);
            m_searchResultsAdapter.notifyDataSetChanged();
        }

        if (!doneLoading)
            doneLoading = true;

        if (null != m_progressDialog && m_progressDialog.isShowing())
            m_progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_results, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView =
                (SearchView) menu.findItem(R.id.search_items).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchString) {
                if (m_query == null || (!(searchString.toLowerCase()).equals(m_query.toLowerCase())
                        && searchString.length() > 2)) {
                    loadData(searchString);
                }
                return true;
            }
        });

        return true;
    }

    public void continueShopping(View v) {
        onBackPressed();
    }

    private void loadData(final String searchText) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.searchFeeds(m_servProvCode, m_stCode, "1", searchText, "0", new Callback<ItemModel>() {

            @Override
            public void success(ItemModel itemsResponse, retrofit.client.Response response) {
                ArrayList<ItemSnippet> itemsList = new ArrayList<ItemSnippet>();
                itemsList = itemsResponse.items_service_providers;
                m_async5 = new StoreItemsInSqlite(searchText).execute(itemsList);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("", error.toString());
            }
        });
    }

    private class StoreItemsInSqlite extends AsyncTask<ArrayList<ItemSnippet>, Void, Void> {

        String searchText;

        public StoreItemsInSqlite(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected Void doInBackground(ArrayList<ItemSnippet>... params) {
            ArrayList<ItemSnippet> itemsList = params[0];

            deleteItemsForOtherQueries();
            storeItemsinSqliteForQuery(itemsList, searchText);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            loadQueryResultAgain();
        }
    }

    private void loadQueryResultAgain() {
        if (null != searchView) {
            View searchTextView = searchView.findViewById(R.id.search_src_text);
            if (null != searchTextView)
                searchTextView.performClick();
        }
    }

    private class StoreItemsInSqliteandLoad extends AsyncTask<ArrayList<ItemSnippet>, Void, Cursor> {

        String searchText;

        public StoreItemsInSqliteandLoad(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected Cursor doInBackground(ArrayList<ItemSnippet>... params) {
            ArrayList<ItemSnippet> itemsList = params[0];
            storeItemsinSqliteForQuery(itemsList, searchText);
            Cursor cursor = loadNewestItemsFromSqliteForQuery(searchText);
            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            processCursorData(cursor);
        }
    }

    public void storeItemsinSqliteForQuery(ArrayList<ItemSnippet> itemsList, String searchText) {

        Vector<ContentValues> cVVector = new Vector<ContentValues>(itemsList.size());
        for (int i = 0; i < itemsList.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(SearchResultEntry.COLUMN_ITEM_CODE, itemsList.get(i).m_itemCode);
            values.put(SearchResultEntry.COLUMN_ITEM_NAME, itemsList.get(i).m_itemName);
            values.put(SearchResultEntry.COLUMN_ITEM_IMG_FLAG, itemsList.get(i).m_itemImgFlag);
            values.put(SearchResultEntry.COLUMN_ITEM_IMG_ADDRESS, itemsList.get(i).m_itemImgAddress);
            values.put(SearchResultEntry.COLUMN_ITEM_UNIT, itemsList.get(i).m_itemUnit);
            values.put(SearchResultEntry.COLUMN_ITEM_COST, itemsList.get(i).m_itemCost);
            values.put(SearchResultEntry.COLUMN_ITEM_SHORT_DESC, itemsList.get(i).m_itemShortDesc);
            values.put(SearchResultEntry.COLUMN_ITEM_DESC, itemsList.get(i).m_itemDesc);
            values.put(SearchResultEntry.COLUMN_ITEM_STATUS, itemsList.get(i).m_itemStatus);
            values.put(SearchResultEntry.COLUMN_ITEM_SELECTED, 0);
            values.put(SearchResultEntry.COLUMN_ITEM_QUANTITY_SELECTED, 0);
            values.put(SearchResultEntry.COLUMN_ITEM_ST_CODE, itemsList.get(i).m_itemServiceType);
            values.put(SearchResultEntry.COLUMN_ITEM_SC_CODE, itemsList.get(i).m_itemServiceCategory);
            values.put(SearchResultEntry.COLUMN_ITEM_MRP, itemsList.get(i).m_itemMrp);
            values.put(SearchResultEntry.COLUMN_SEARCH_STRING, searchText);
            values.put(SearchResultEntry.COLUMN_ITEM_OFFER_PRICE, itemsList.get(i).m_itemOfferPrice);
            values.put(SearchResultEntry.COLUMN_ITEM_IN_OFFER, itemsList.get(i).m_itemInOffer);

            cVVector.add(values);
        }

        if (cVVector.size() > 0) {
            ContentValues[] cvArray = new ContentValues[cVVector.size()];
            cVVector.toArray(cvArray);
            getContentResolver().bulkInsert(SearchResultEntry.CONTENT_URI, cvArray);
        }
    }

    Cursor loadNewestItemsFromSqliteForQuery(String searchText) {
        String previous_item_code;
        if (null == m_searchItemsDetailsArrayList || m_searchItemsDetailsArrayList.isEmpty())
            previous_item_code = "0";
        else
            previous_item_code = m_searchItemsDetailsArrayList.get(m_searchItemsDetailsArrayList.size() - 1).m_itemCode;

        final String sSearchSelection =
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_SEARCH_STRING + " LIKE ? COLLATE NOCASE " +
                        "and CAST( " + SearchResultEntry.TABLE_ALIAS_NAME + "." + LazyContract.SearchResultEntry.COLUMN_ITEM_CODE + " AS INTEGER ) > ? ";

        Cursor cursor = getContentResolver().query(
                SearchResultEntry.buildSearchResultItemsUri(2, m_servProvCode),
                null,
                sSearchSelection,
                new String[]{searchText, previous_item_code},
                null);

        return cursor;
    }

    @Override
    public void onBackPressed() {
        SearchResultsActivity.this.finish();
        super.onBackPressed();
    }
}