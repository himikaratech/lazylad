package technologies.angular.lazylad;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import technologies.angular.lazylad.Snippets.CouponBannerSnippet;

/**
 * Created by Amud on 25/01/16.
 */

public class MainScreenPagerBannerAdapter extends FragmentStatePagerAdapter {
    // This will Store the Titles of the Tabs which are Going to be passed when CartPagerAdapter is created
    // Store the number of tabs, this will also be passed when the CartPagerAdapter is created

    Context m_context;
    private ArrayList<CouponBannerSnippet> m_bannerImageList;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public MainScreenPagerBannerAdapter(FragmentManager fm, Context context, ArrayList<CouponBannerSnippet> bannerImageList) {
        super(fm);
        this.m_context = context;
        m_bannerImageList = bannerImageList;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if (!m_bannerImageList.isEmpty()) {
            OfferBannerFragment offerBannerFragment = new OfferBannerFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("m_bannerImageList", m_bannerImageList);
            bundle.putInt("position", position);
            offerBannerFragment.setArguments(bundle);

            return offerBannerFragment;
        } else
            return null;
    }


    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return m_bannerImageList.size();
    }



    @Override
    public CharSequence getPageTitle(int position) {
        return m_bannerImageList.get(position).coupon_id;
    }
}
