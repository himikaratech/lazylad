package technologies.angular.lazylad;

/**
 * Created by Paresh on 11-03-2015.
 */
public class Config {
    // File upload url (replace the ip with your server address)
    public static final String FILE_UPLOAD_URL = "http://54.169.62.100/AndroidFileUpload/fileUpload.php";
    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";
}