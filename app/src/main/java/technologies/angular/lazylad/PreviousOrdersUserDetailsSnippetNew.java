package technologies.angular.lazylad;

/**
 * Created by Amud on 08/10/15.
 */
public class PreviousOrdersUserDetailsSnippetNew {

    public String m_user_name;
    public String m_user_address;
    public String m_user_phone_number;
    public Double m_cod_amount;
    public Double m_wallet_amount;
    public String m_coupon_code;
    public Double m_discount_amount;
    public Double m_paid;

    public PreviousOrdersUserDetailsSnippetNew() {
    }

    public PreviousOrdersUserDetailsSnippetNew(String user_name, String user_address, String user_phone_number, Double cod_amount, Double wallet_amount,Double discount_amount,String coupon_code,Double paid ) {
        this.m_user_name = user_name;
        this.m_user_address = user_address;
        this.m_user_phone_number = user_phone_number;
        this.m_cod_amount = cod_amount;
        this.m_wallet_amount = wallet_amount;
        this.m_coupon_code=coupon_code;
        this.m_discount_amount=discount_amount;
        this.m_paid=paid;

    }
}
