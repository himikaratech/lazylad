package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Amud on 24/08/15.
 */
public class DeliveryTimeAdapter extends RecyclerView.Adapter<DeliveryTimeAdapter.ViewHolder> {

    private ArrayList<String> m_del_time_list;
    private Activity m_activity;
    protected static int selected = 0;
    static OnItemClickListener mItemClickListener;

    public DeliveryTimeAdapter(Context context, ArrayList<String> items) {
        m_activity = (Activity) context;
        this.m_del_time_list = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivery_time_list_layout, parent, false);
        return new ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (!m_del_time_list.isEmpty()) {
            final String delTime = m_del_time_list.get(position);
            holder.del_timeTextView.setText(delTime);
            holder.del_timeTextView.setTextColor(Color.parseColor("#b3b3b3"));
            holder.del_timeTextView.setBackgroundColor(Color.parseColor("#ffffff"));
            ((ConfirmationMessageActivity) m_activity).scrollViewLayout.fullScroll(ScrollView.FOCUS_DOWN);

            if (selected == 0 && position == 0) {
                holder.del_timeTextView.setTextColor(Color.parseColor("#333333"));
                holder.del_timeTextView.setBackgroundColor(m_activity.getResources().getColor(R.color.backgroundcolor));
            }
        }
    }

    @Override
    public int getItemCount() {
        return m_del_time_list.size();
    }

    public void onItemClick(View view, int position) {
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView del_timeTextView;

        public ViewHolder(View itemView, int flag) {
            super(itemView);
            del_timeTextView = (TextView) itemView.findViewById(R.id.del_time);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public Object getItem(int position) {
        if (m_del_time_list != null) {
            return m_del_time_list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_del_time_list != null) {
            return position;
        }
        return 0;
    }
}
