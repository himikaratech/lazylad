package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Saurabh on 12/02/15.
 */
public class ShoppingCartConfirmationPageAdapter extends BaseAdapter {
    private ArrayList<ItemSnippet> m_itemsDetails;
    private Activity m_activity;

    public ShoppingCartConfirmationPageAdapter(Activity activity, ArrayList<ItemSnippet> itemsDetails) {
        m_activity = activity;
        m_itemsDetails = itemsDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_itemsDetails != null) {
            count = m_itemsDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_itemsDetails != null) {
            return m_itemsDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_itemsDetails != null) {
            return m_itemsDetails.get(position).m_itemId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_itemsDetails != null) {
            final String itemName = ((ItemSnippet) getItem(position)).m_itemName;
            final double itemCost = ((ItemSnippet) getItem(position)).m_itemCost;
            int itemQuantitySelected = ((ItemSnippet) getItem(position)).m_itemQuantitySelected;
            String itemImageAdd = ((ItemSnippet) getItem(position)).m_itemImgAddress;
            final String itemDesc = ((ItemSnippet) getItem(position)).m_itemDesc;

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.shoping_cart_on_confirmation_page_list_view, (ViewGroup) null);
            }
            TextView itemNameTextView = (TextView) convertView.findViewById(R.id.shopping_cart_item_name);
            final TextView itemQuantitySelectedTextView = (TextView) convertView.findViewById(R.id.shopping_cart_item_quantity_selected);
            TextView price = (TextView) convertView.findViewById(R.id.shopping_cart_item_price);
            TextView Description = (TextView) convertView.findViewById(R.id.shopping_cart_item_description);

            Description.setText(itemDesc);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.shopping_cart_item_image);

            if (itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
            Picasso.with(parent.getContext())
                    .load(itemImageAdd)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.no_image)
                    .into(imageView);

            price.setText("₹ " + itemCost);
            itemNameTextView.setText(itemName);
            itemQuantitySelectedTextView.setText(Integer.toString(itemQuantitySelected));
        }
        return convertView;
    }
}
