package technologies.angular.lazylad;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Amud on 20/08/15.
 */
public class offerSnippet implements Parcelable {

    @SerializedName("offer_img_add")
    public String offer_img_add;

    public offerSnippet(Parcel in) {
        this.offer_img_add = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.offer_img_add);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public offerSnippet createFromParcel(Parcel in) {
            return new offerSnippet(in);
        }

        public offerSnippet[] newArray(int size) {
            return new offerSnippet[size];
        }
    };
}