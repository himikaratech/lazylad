package technologies.angular.lazylad;

/**
 * Created by Amud on 09/09/15.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.mobile_verify_sdk.AsyncResponse;
import technologies.angular.mobile_verify_sdk.CheckMobiService;
import technologies.angular.mobile_verify_sdk.ErrorCode;
import technologies.angular.mobile_verify_sdk.ValidationType;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.UserDetailsEntry;

public class NumberVerify extends ActionBarActivity {

    private WeakReference<ProgressDialog> loadingDialog;

    private EditText pinEditText;
    private TextView chargeLabel;
    private TextView startingText;
    private Button resendCode;

    private Button rcli;
    private int m_userCode = 0;
    private String callId;
    private String dialingNumber;
    private String validationKey;
    private boolean pinStep = false;
    private String mobileNumber;
    private String pin_hash;
    private String key;
    private int validationTypeFlag = 0;

    static Context mContext;
    static Intent sendingIntent;
    boolean loaderflag = false;
    private AsyncTask m_async5;
    int pinsubmitted = 0;
    Boolean isInternetPresent = false;
    Boolean pausedFlag;
    String prefSkippedString;
    String prefVerifiedString;
    Boolean skippable;

    private String callerid_hash;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();
    public static String smsCode = "";
    public static int smsFlag = 0;
    private int smsCounter = 20;
    private RelativeLayout smsScreen;

    CircleProgressBar circleProgressBar;
    private TextView textViewProgress;
    Thread thread = null;
    Thread thread2 = null;
    int progressStatus = 0;
    Runnable runnable;
    SharedPreferences prefs;
    int lockVariable = 0;
    int resendFlag = 0;
    TextWatcher mTextEditorWatcher1;

    private MoEHelper mHelper;

    final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CallReceiver.MSG_CALL_START)) {
                if (!intent.getBooleanExtra("incoming", false)) {
                    if (NumberVerify.this.dialingNumber == null || NumberVerify.this.validationKey == null || NumberVerify.this.callId != null)
                        return;

                    NumberVerify.this.callId = intent.getStringExtra("number");
                } else {
                    String number = intent.getStringExtra("number");
                    //on some devices the number is in local format for local number.
                    boolean matching = false;
                    try {
                        String hash = AeSimpleSHA1.SHA1(number.substring(number.length() - 3));

                        if (hash.equals(callerid_hash))
                            matching = true;
                    } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Incoming call received: " + number + " hash: " + callerid_hash + " matching: " + matching);
                    Log.e("prblm", "4");
                    if (matching) {
                        StopReverseCliTimer();
                        HangupCall();
                        //check validation

                        String pinNumber = number.substring(number.length() - 4);

                        CheckMobiService.getInstance().VerifyPin(NumberVerify.this.validationKey, pinNumber, new AsyncResponse() {
                            @Override
                            public void OnRequestCompleted(int httpStatus, Map<String, Object> result, String error) {

                                if (lockVariable == 0) {
                                    lockVariable = 1;
                                    if (validationTypeFlag == 1 || pinsubmitted == 1)
                                        ShowLoadingMessage(false, 0);

                                    if (httpStatus == CheckMobiService.STATUS_SUCCESS && result != null) {
                                        Boolean validated = (Boolean) result.get("validated");

                                        if (!validated) {
                                            Utils.ShowMessageBox(new AlertDialog.Builder(NumberVerify.this), "Error", "Validation failed!");
                                            return;
                                        }
                                        sendNoToserver(mobileNumber);
                                    } else {
                                        HandleValidationServiceError(httpStatus, result, error);
                                    }
                                }
                            }
                        });
                    }
                }
            }
            if (intent.getAction().equals(CallReceiver.MSG_CALL_END)) {
                if (!intent.getBooleanExtra("incoming", false)) {
                    NumberVerify pThis = NumberVerify.this;

                    if (pThis.dialingNumber == null || pThis.validationKey == null || pThis.callId == null)
                        return;

                    if (pThis.callId.compareTo(intent.getStringExtra("number")) != 0)
                        return;

                    DismissKeyboard();
                    if (validationTypeFlag == 1 || pinsubmitted == 1)
                        ShowLoadingMessage(true, 1);

                    CheckMobiService.getInstance().CheckValidationStatus(pThis.validationKey, new AsyncResponse() {
                        @Override
                        public void OnRequestCompleted(int httpStatus, Map<String, Object> result, String error) {

                            if (validationTypeFlag == 1 || pinsubmitted == 1)
                                ShowLoadingMessage(false, 0);

                            if (httpStatus == CheckMobiService.STATUS_SUCCESS && result != null) {
                                Boolean validated = (Boolean) result.get("validated");

                                if (!validated) {
                                    Utils.ShowMessageBox(new AlertDialog.Builder(NumberVerify.this), "Error", "Number not validated ! Check your phone number!");
                                    return;
                                }

                                sendNoToserver(mobileNumber);
                            } else {
                                HandleValidationServiceError(httpStatus, result, error);
                            }
                        }
                    });
                }
            }
        }
    };

    private void StopReverseCliTimer() {

        if (timer != null) {
            timer.cancel();
            timer = null;
            timerTask = null;
        }
    }

    private void HangupCall() {
        try {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            Object telephonyService = m.invoke(tm);
            c = Class.forName(telephonyService.getClass().getName());
            m = c.getDeclaredMethod("endCall");
            m.setAccessible(true);
            m.invoke(telephonyService);
        } catch (Exception ex) {
            System.out.println("Failed to hangup the call...:" + ex.getMessage());
        }
    }

    public static Context getContext() {
        return mContext;
    }

    public static void setContext(Context Context) {
        mContext = Context;
    }

    public static Intent getsendingIntent() {
        return sendingIntent;
    }

    public static void setsendingIntent(Intent intent) {
        sendingIntent = intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.verify_layout);
        pausedFlag = false;
        key = new String();
        prefs = NumberVerify.this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        prefSkippedString = getIntent().getStringExtra(NumberParameters.PREF_STRING_CONTANT);
        prefVerifiedString = getIntent().getStringExtra(NumberParameters.PREF_VERIFIED_CONSTANT);
        skippable = getIntent().getBooleanExtra(NumberParameters.SKIPPABLE_CONSTANT, false);

        this.pinEditText = (EditText) findViewById(R.id.pinTextEdit);
        this.chargeLabel = (TextView) findViewById(R.id.enterCode);
        this.startingText = (TextView) findViewById(R.id.textView);
        this.rcli = (Button) findViewById(R.id.rcli);

        this.textViewProgress = (TextView) findViewById(R.id.textView4);
        this.resendCode = (Button) findViewById(R.id.resetBtt);
        circleProgressBar = (CircleProgressBar) findViewById(R.id.custom_progressBar);

        pinEditText.setVisibility(View.GONE);
        chargeLabel.setVisibility(View.GONE);
        resendCode.setVisibility(View.GONE);
        rcli.setVisibility(View.GONE);
        circleProgressBar.setVisibility(View.GONE);
        textViewProgress.setVisibility(View.GONE);

        registerReceiver(receiver, new IntentFilter(CallReceiver.MSG_CALL_START));
        registerReceiver(receiver, new IntentFilter(CallReceiver.MSG_CALL_END));

        loaderflag = false;
        pinsubmitted = 0;

        Utils.TrustInvalidSslCertificates();
        CheckMobiService.getInstance().SetSecretKey("AAAAAAAA-AAAA-AAAA-AAAA-111111111111");

        thread2 = new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 1000) {
                    progressStatus += 1;
                    if (loaderflag == true)
                        break;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            circleProgressBar.setProgress(progressStatus);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Log.e("logging thread", "thread2 ended");
                if (key.isEmpty()) {
                    handler.post(new Runnable() {
                        public void run() {
                            checkFlag(key);

                        }
                    });
                }
            }
        });

        int number_added_flag = prefs.getInt(PreferencesStore.NUMBER_JUST_ADDED_CONSTANT, 0);
        Log.e("number_added_flag", number_added_flag + "");

        if (number_added_flag == 1) {
            int verified_flag = 0;
            mobileNumber = prefs.getString(PreferencesStore.NUMBER_ADDED_CONSTANT, "");
            startingText.setText("Waiting to automatically detect a SMS code");

            ShowProgressBar(true);
            Cursor m_cursor = getContentResolver().query(
                    LazyContract.UserDetailsEntry.CONTENT_URI,
                    null,
                    null,
                    null,
                    null);
            if (m_cursor != null && m_cursor.moveToFirst()) {
                if (m_cursor.getCount() != 0) {
                    m_userCode = m_cursor.getInt(1);
                    verified_flag = m_cursor.getInt(2);
                }
            } else {
                m_userCode = 0;
                verified_flag = 0;

            }
            if (verified_flag == 0 && resendFlag == 0) {

                OnClickValidate();
            }
        }

        resendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                smsFlag = 0;
                pinStep = false;
                resendFlag = 1;
                OnClickValidate();
                ShowLoadingMessage(true, 5);
            }
        });

        setTracker();
        mHelper = new MoEHelper(this);
    }

    private void reverseCli(String key, String hash) {
        this.validationKey = key;
        this.callerid_hash = hash;
        lockVariable = 0;

        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        StopReverseCliTimer();
                        if (validationTypeFlag == 1 || pinsubmitted == 1)
                            ShowLoadingMessage(false, 0);
                        Utils.ShowMessageBox(new AlertDialog.Builder(NumberVerify.this), "Error", "Validation failed!");
                        onBackPressed();
                    }
                });
            }
        };
        timer.schedule(timerTask, 15000);
    }

    public void sendNoToserver(final String number) {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        if (pausedFlag == false) {
            progressDialog.show();
        }
        int referral_code_added_flag = prefs.getInt(PreferencesStore.REFERRAL_JUST_ADDED_CONSTANT, 0);
        String referral_code = prefs.getString(PreferencesStore.REFERRAL_CODE_CONSTANT, "NULL");

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.SendNumberToserverRequestModel sendNumberToserverRequestModel = requestModel.new SendNumberToserverRequestModel();
        sendNumberToserverRequestModel.user_code = Integer.toString(m_userCode);
        sendNumberToserverRequestModel.phone_number = number;
        sendNumberToserverRequestModel.verification_source = "check_mobi";

//        if (referral_code_added_flag == 1)
//            sendNumberToserverRequestModel.referral_code = referral_code;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.sendNumberToserverAPICall(sendNumberToserverRequestModel, new retrofit.Callback<APIResonseModel.SendNumberToserverResponseModel>() {

            @Override
            public void success(APIResonseModel.SendNumberToserverResponseModel sendNumberToserverResponseModel, retrofit.client.Response response) {

                int success = sendNumberToserverResponseModel.error;
                if (success == 1) {
                    prefs = NumberVerify.this.getSharedPreferences(
                            "technologies.angular.lazylad", Context.MODE_PRIVATE);
                    ContentValues phoneNumberValues = new ContentValues();
                    phoneNumberValues.put(UserDetailsEntry.COLUMN_VERIFIED_FLAG, 1);
                    phoneNumberValues.put(UserDetailsEntry.COLUMN_VERIFIED_NUMBER, mobileNumber);

                    getContentResolver().update(UserDetailsEntry.CONTENT_URI, phoneNumberValues, UserDetailsEntry.COLUMN_USER_CODE + "=?", new String[]{String.valueOf(m_userCode)});

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(PreferencesStore.VARIFIED_FLAG_PREF_CONSTANT, 1);
                    editor.apply();
                    smsCode = "";
                    smsFlag = 0;
                    Toast.makeText(NumberVerify.this, "Verification Completed.", Toast.LENGTH_SHORT).show();
                    Bundle bundle = getIntent().getExtras();

                    if (pausedFlag == false)
                        Utils.verify_email(mContext, bundle, skippable, prefSkippedString, prefVerifiedString);
                    NumberVerify.this.finish();
                } else {
                    Log.e("server error", "unable to send request to server");
                }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                smsCode = "";
                smsFlag = 0;
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    protected void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private ValidationType GetCurrentValidationType() {
        if (validationTypeFlag == 1)
            return ValidationType.REVERSE_CLI;
        else
            return ValidationType.SMS;
    }

    private void checkFlag(final String key) {

        if (!key.isEmpty()) {
            smsCounter--;
            if (smsFlag == 1) {
                PerformPinValidation(key, 1);
                smsFlag = 0;
            } else if (smsCounter > 0) {
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        checkFlag(key);
                    }
                };
                handler = new Handler();
                handler.postDelayed(runnable, 800);
            }
            if (smsCounter <= 0) {
                loaderflag = true;
                //  thread2.interrupt();
                ShowProgressBar(false);
                if (validationTypeFlag == 0) {
                    PerformPinValidation(key, 0);
                }
            }
        } else
            PerformPinValidation(key, 0);
    }

    private void OnClickValidate() {
        if (!Utils.isNetworkAvailable(this)) {
            Toast.makeText(this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!this.pinStep) {
            if (mobileNumber.length() == 0) {
                Utils.ShowMessageBox(new AlertDialog.Builder(this), "Invalid number", "Please provide a valid number");
                return;
            }

            DismissKeyboard();
            if ((validationTypeFlag == 1 || pinsubmitted == 1) && resendFlag == 0)
                ShowLoadingMessage(true, 2);

            CheckMobiService.getInstance().RequestValidation(GetCurrentValidationType(), ("+91" + mobileNumber), new AsyncResponse() {
                @Override
                public void OnRequestCompleted(int httpStatus, Map<String, Object> result, String error) {
                    System.out.println("Status= " + httpStatus + " result= " + (result != null ? result.toString() : "null") + " error=" + error);

                    if (httpStatus == CheckMobiService.STATUS_SUCCESS && result != null) {
                        key = String.valueOf(result.get("id"));
                        String type = String.valueOf(result.get("type"));
                        pin_hash = String.valueOf(result.get("pin_hash"));

                        @SuppressWarnings("unchecked")
                        Map<String, Object> validation_info = (Map<String, Object>) result.get("validation_info");

                        if (validationTypeFlag == 0 && resendFlag == 0) {
                            checkFlag(key);
                        } else if (validationTypeFlag == 0 && resendFlag == 1) {
                            PerformPinValidation(key, 0);
                        } else {
                            reverseCli(key, pin_hash);
                        }
                    } else {
                        if (validationTypeFlag == 1 || pinsubmitted == 1)
                            ShowLoadingMessage(false, 0);
                        HandleValidationServiceError(httpStatus, result, error);
                    }
                }
            });
        } else {
            CheckMobiService.getInstance().VerifyPin(this.validationKey, smsCode, new AsyncResponse() {
                @Override
                public void OnRequestCompleted(int httpStatus, Map<String, Object> result, String error) {
                    if (lockVariable == 0) {
                        lockVariable = 1;
                        if (validationTypeFlag == 1 || pinsubmitted == 1)
                            ShowLoadingMessage(false, 0);

                        if (httpStatus == CheckMobiService.STATUS_SUCCESS && result != null) {
                            Boolean validated = (Boolean) result.get("validated");

                            if (!validated) {
                                Utils.ShowMessageBox(new AlertDialog.Builder(NumberVerify.this), "Error", "Invalid PIN!");
                                lockVariable = 0;
                                pinEditText.setText("");
                                return;
                            }
                            sendNoToserver(mobileNumber);
                        } else {
                            HandleValidationServiceError(httpStatus, result, error);
                        }
                    }
                }
            });
        }
    }

    private void PerformPinValidation(String key, int flag) {
        final int[] Status = {0};
        this.pinStep = true;

        if (validationTypeFlag == 1 || pinsubmitted == 1 || resendFlag == 1)
            ShowLoadingMessage(false, 0);

        this.validationKey = key;

        Log.i("CheckCode", smsCode);
        if (flag == 1) {
            OnClickValidate();
        } else {
            if (resendFlag == 0) {
                startingText.setVisibility(View.GONE);
                pinEditText.setVisibility(View.VISIBLE);
                chargeLabel.setVisibility(View.VISIBLE);

                circleProgressBar.setVisibility(View.GONE);
                textViewProgress.setVisibility(View.VISIBLE);
                resendCode.setVisibility(View.VISIBLE);
                rcli.setVisibility(View.VISIBLE);

                rcli.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pinStep = false;
                        validationTypeFlag = 1;
                        ShowLoadingMessage(true, 5);
                        OnClickValidate();
                    }
                });

            }

            mTextEditorWatcher1 = new TextWatcher() {
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    int value = s.length();
                    if (value == 6) {

                        smsCode = s.toString();
                        loaderflag = true;
                        pinsubmitted = 1;
                        ShowLoadingMessage(true, 5);
                        OnClickValidate();
                    }
                }

                public void afterTextChanged(Editable s) {
                }
            };

            if (resendFlag == 0)
                pinEditText.addTextChangedListener(mTextEditorWatcher1);
        }
    }

    private void HandleValidationServiceError(int httpStatus, Map<String, Object> body, String error) {
        System.out.println("HandleValidationServiceError status= " + httpStatus + " body= " + (body != null ? body.toString() : "null") + " error=" + error);

        if (body != null) {
            Number err = (Number) body.get("code");
            String error_message;

            switch (ErrorCode.get(err.intValue())) {
                case ErrorCodeInvalidPhoneNumber:
                    error_message = "Invalid phone number. Please provide the number in E164 format.";
                    break;

                default:
                    error_message = "Service unavailable. Please try later.";
            }

            Utils.ShowMessageBox(new AlertDialog.Builder(this), "Error", error_message);
        } else
            Utils.ShowMessageBox(new AlertDialog.Builder(this), "Error", "Service unavailable. Please try later.");
    }

    private void ShowProgressBar(boolean show) {
        if (show) {
            circleProgressBar.setMax(1000);
            thread2.start();
            circleProgressBar.setVisibility(View.VISIBLE);
        } else {

            thread2.interrupt();
            circleProgressBar.setVisibility(View.INVISIBLE);

            startingText.setText("Waiting to detect a SMS code sent to");
            pinEditText.setVisibility(View.VISIBLE);
            chargeLabel.setVisibility(View.VISIBLE);
        }
    }

    private void DismissKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(pinEditText.getWindowToken(), 0);
    }

    @Override
    public void onRestart() {
        super.onRestart();
        pausedFlag = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        pausedFlag = true;
        mHelper.onPause(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(runnable);
        loaderflag = true;
        smsCode = "";
        smsFlag = 0;
        pinsubmitted = 1;
        ((Activity) mContext).finish();
        this.finish();
    }

    private void ShowLoadingMessage(boolean show, int i) {
        if (show) {
            if (this.loadingDialog == null)
                this.loadingDialog = new WeakReference<>(ProgressDialog.show(NumberVerify.this, "", "Please wait... ", true));
        } else {
            if (this.loadingDialog != null) {
                this.loadingDialog.get().dismiss();
                this.loadingDialog = null;
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.NumberVerify");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }


    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}