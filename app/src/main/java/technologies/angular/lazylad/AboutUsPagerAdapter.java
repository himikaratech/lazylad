package technologies.angular.lazylad;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Amud on 09/09/15.
 */

public class AboutUsPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Integer> m_aboutCode;
    Context m_context;

    public AboutUsPagerAdapter(FragmentManager fragmentManager, ArrayList<Integer> aboutCode, Context context) {
        super(fragmentManager);
        m_aboutCode = aboutCode;
        m_context = context;
    }

    @Override
    public Fragment getItem(int position) {
        int m_aboutFragmentCode = ((AboutUs) m_context).m_aboutCode.get(position);
        Bundle bundle = new Bundle();
        bundle.putInt("m_aboutFragmentCode", m_aboutFragmentCode);
        AboutUsFragment aboutUsFragment = new AboutUsFragment();
        aboutUsFragment.setArguments(bundle);
        return aboutUsFragment;
    }

    @Override
    public int getCount() {
        if (null != ((AboutUs) m_context).m_aboutNames)
            return ((AboutUs) m_context).m_aboutNames.size();
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ((AboutUs) m_context).m_aboutNames.get(position);
    }
}