package technologies.angular.lazylad;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.squareup.picasso.Picasso;

import technologies.angular.lazylad.LazyContract.ShoppingCartEntry;
import technologies.angular.lazylad.LazyContract.OrderDetailsEntry;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by Saurabh on 11/02/15.
 */
public class ItemsCategoryAdapter extends BaseAdapter {
    public ArrayList<ItemSnippet> m_itemsDetailsAdapterList;
    private Context m_activity;
    private Context m_appContext;

    String sp_code;
    String sp_name;
    String del_time;
    String min_order;
    int time_interval;
    int open_time;
    int close_time;

    int m_images_available;
    final private int TYPE_WITH_IMAGE = 0;
    final private int TYPE_WITHOUT_IMAGE = 1;

    String taxName1;
    String taxName2;
    String taxName3;
    String taxCentString1;
    String taxCentString2;
    String taxCentString3;
    double taxCent1 = 0.0;
    double taxCent2 = 0.0;
    double taxCent3 = 0.0;
    int taxFlag1;
    int taxFlag2;
    int taxFlag3;


    private class ViewHolder {

        TextView itemNameTextView ;
        Button itemPlusButton ;
        Button itemMinusButton ;
         TextView itemQuantitySelectedTextView ;
        TextView price ;
        TextView Description ;
        TextView Mrp;
        TextView OfferPrice ;
        View OfferPriceLayout;
        ImageView imageView ;



    }

    public ItemsCategoryAdapter(Context activity,Context app_context, ArrayList<ItemSnippet> itemsDetails, int images_available) {
        m_activity = activity;
        m_appContext=app_context;
        m_itemsDetailsAdapterList = itemsDetails;
        m_images_available = images_available;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_itemsDetailsAdapterList != null) {
            count = m_itemsDetailsAdapterList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_itemsDetailsAdapterList != null) {
            return m_itemsDetailsAdapterList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_itemsDetailsAdapterList != null) {
            return m_itemsDetailsAdapterList.get(position).m_itemId;
        }
        return 0;
    }

    public void updateDataList(ArrayList<ItemSnippet> itemsDetailsAdapterList) {
        m_itemsDetailsAdapterList.clear();
        m_itemsDetailsAdapterList = itemsDetailsAdapterList;
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        if (m_itemsDetailsAdapterList != null) {

            final ViewHolder holder;


            final String itemCode = ((ItemSnippet) getItem(position)).m_itemCode;
            final String itemName = ((ItemSnippet) getItem(position)).m_itemName;
            final double itemCost = ((ItemSnippet) getItem(position)).m_itemCost;
            int itemQuantitySelected = ((ItemSnippet) getItem(position)).m_itemQuantitySelected;
            String itemImageAdd = ((ItemSnippet) getItem(position)).m_itemImgAddress;
            final String itemDesc = ((ItemSnippet) getItem(position)).m_itemDesc;
            final int itemImgFlag = ((ItemSnippet) getItem(position)).m_itemImgFlag;
            final String itemUnit = ((ItemSnippet) getItem(position)).m_itemUnit;
            final String itemShortDesc = ((ItemSnippet) getItem(position)).m_itemShortDesc;
            final String itemStatus = ((ItemSnippet) getItem(position)).m_itemStatus;
            final String itemStCode = ((ItemSnippet) getItem(position)).m_itemServiceType;
            final String itemScCode = ((ItemSnippet) getItem(position)).m_itemServiceCategory;
            final String itemSpCode = ((ItemSnippet) getItem(position)).m_itemServiceProvider;
            final double itemMrp = ((ItemSnippet) getItem(position)).m_itemMrp;
            final double itemOfferPrice = ((ItemSnippet) getItem(position)).m_itemOfferPrice;
            final int itemInOffer = ((ItemSnippet) getItem(position)).m_itemInOffer;




            final int itemPosition = position;


            if (convertView == null) {
                new StoreViewsInSqlite(m_appContext).execute(itemCode,itemSpCode);
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (type == TYPE_WITH_IMAGE) {
                    convertView = infalInflater.inflate(R.layout.items_display_list_view, (ViewGroup) null);
                    holder.imageView = (ImageView) convertView.findViewById(R.id.item_image);
                }
                else {
                    convertView = infalInflater.inflate(R.layout.item_display_list_no_image, (ViewGroup) null);
                }
                holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name_textview);
                holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name);
                holder.itemPlusButton = (Button) convertView.findViewById(R.id.item_plus_button);
                holder.itemMinusButton = (Button) convertView.findViewById(R.id.item_minus_button);
                holder.itemQuantitySelectedTextView = (TextView) convertView.findViewById(R.id.item_quantity_selected);
                holder.price = (TextView) convertView.findViewById(R.id.item_price);
                holder.Description = (TextView) convertView.findViewById(R.id.item_description);
                holder.Mrp = (TextView) convertView.findViewById(R.id.item_mrp);
                holder.OfferPrice = (TextView) convertView.findViewById(R.id.item_offer_price);
                holder.OfferPriceLayout=convertView.findViewById(R.id.item_offer_price_layout);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
                holder.OfferPriceLayout.setVisibility(View.GONE);
                holder.Mrp.setPaintFlags(holder.Mrp.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                holder.price.setPaintFlags(holder.price.getPaintFlags() &(~ Paint.STRIKE_THRU_TEXT_FLAG));
            }





            holder.Mrp.setVisibility(View.INVISIBLE);
            //OfferPriceLayout.setVisibility(View.INVISIBLE);

            if (((ItemSnippet) getItem(itemPosition)).m_itemCost < ((ItemSnippet) getItem(itemPosition)).m_itemMrp) {
                holder.Mrp.setVisibility(View.VISIBLE);
                holder.Mrp.setText("₹ " + itemMrp);
                holder.Mrp.setPaintFlags(holder.Mrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }


            if (((ItemSnippet) getItem(itemPosition)).m_itemOfferPrice < ((ItemSnippet) getItem(itemPosition)).m_itemCost) {
                holder.OfferPriceLayout.setVisibility(View.VISIBLE);
                holder.OfferPrice.setText("₹ " + itemOfferPrice);
                holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            }

            holder.Description.setText(itemDesc);

            if (type == TYPE_WITH_IMAGE) {


                if (itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
                Picasso.with(parent.getContext())
                        .load(itemImageAdd)
                        .fit().centerInside()
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.no_image)
                        .into(holder.imageView);
            }

            holder.price.setText("₹ " + itemCost);

            holder.itemNameTextView.setText(itemName);
            holder.itemQuantitySelectedTextView.setText(Integer.toString(itemQuantitySelected));

            holder.itemPlusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ItemSnippet) getItem(itemPosition)).m_itemSelected = 1;

                    int itemQuan = Integer.parseInt(holder.itemQuantitySelectedTextView.getText().toString());
                    itemQuan = itemQuan + 1;
                    holder.itemQuantitySelectedTextView.setText(Integer.toString(itemQuan));
                    ((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected = itemQuan;

                    SharedPreferences cartprefs = m_activity.getSharedPreferences(
                            "technologies.angular.lazylad", Context.MODE_PRIVATE);

                    double m_totalCost = Double.parseDouble(cartprefs.getString("totalCost", "0.0"));
                    ;
                    int m_numOfItemsInShoppingCart = cartprefs.getInt("noOfItemsInShoppingCart", 0);


                    m_totalCost = Math.round(m_totalCost + (itemOfferPrice));
                    m_numOfItemsInShoppingCart = m_numOfItemsInShoppingCart + 1;

                    SharedPreferences.Editor editor = cartprefs.edit();
                    editor.putString("totalCost", String.valueOf(m_totalCost));
                    editor.putInt("noOfItemsInShoppingCart", m_numOfItemsInShoppingCart);
                    editor.commit();

                    SharedPreferences prefs = m_activity.getSharedPreferences(
                            "technologies.angular.lazylad", Context.MODE_PRIVATE);

                    sp_code = prefs.getString("ServiceProviderCode", "Not Available");
                    sp_name = prefs.getString("ServiceProviderName", "Not Available");
                    del_time = prefs.getString("ServiceProviderDeliveryTime", "Not Available");
                    min_order = prefs.getString("ServiceProviderMinimumOrder", "Not Available");
                    open_time = prefs.getInt("shop_open_time", 0);
                    close_time = prefs.getInt("shop_close_time", 0);
                    time_interval = prefs.getInt("time_interval", 0);

                    taxName1 = prefs.getString("taxName1", "Not Available");
                    taxName2 = prefs.getString("taxName2", "Not Available");
                    taxName3 = prefs.getString("taxName3", "Not Available");
                    taxCentString1 = prefs.getString("taxCent1", "0.0");
                    taxCent1 = Double.parseDouble(taxCentString1);
                    taxCentString2 = prefs.getString("taxCent2", "0.0");
                    taxCent2 = Double.parseDouble(taxCentString2);
                    taxCentString3 = prefs.getString("taxCent3", "0.0");
                    taxCent3 = Double.parseDouble(taxCentString3);
                    taxFlag1 = prefs.getInt("taxFlag1", 0);
                    taxFlag2 = prefs.getInt("taxFlag2", 0);
                    taxFlag3 = prefs.getInt("taxFlag3", 0);


                    TextView cost = ((ItemsActivity) m_activity).m_totalCostTextView;
                    cost.setText(Double.toString(m_totalCost));
                    TextView items = ((ItemsActivity) m_activity).m_totalItemsTextView;
                    items.setText(Integer.toString(m_numOfItemsInShoppingCart));

                    if (((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected == 1) {

                        ContentValues cartValues = new ContentValues();
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_CODE, itemCode);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_NAME, itemName);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_IMG_FLAG, itemImgFlag);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_IMG_ADDRESS, ((ItemSnippet) getItem(itemPosition)).m_itemImgAddress);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_UNIT, itemUnit);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_COST, itemCost);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_SHORT_DESC, itemShortDesc);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_DESC, itemDesc);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_STATUS, itemStatus);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_SELECTED, ((ItemSnippet) getItem(itemPosition)).m_itemSelected);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED, ((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_ST_CODE, itemStCode);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_SC_CODE, itemScCode);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_SP_CODE, itemSpCode);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_OFFER_PRICE, itemOfferPrice);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_IN_OFFER, itemInOffer);

                        Uri insertUri1 = m_activity.getContentResolver().insert(ShoppingCartEntry.CONTENT_URI, cartValues);

                        String whereClause = OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
                        String[] selectionArgs = new String[]{sp_code};

                        Cursor cursor = m_activity.getContentResolver().query(
                                OrderDetailsEntry.CONTENT_URI,
                                null,
                                whereClause,
                                selectionArgs,
                                null);

                        if (cursor == null || cursor.getCount() == 0) {

                            ContentValues orderDetailValues = new ContentValues();

                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SERV_PROV_CODE, sp_code);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_DEL_TIME, del_time);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_MIN_ORDER, min_order);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SERV_PROV_NAME, sp_name);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_TIME_INTERVAL, time_interval);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_CLOSE_TIME, close_time);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_OPEN_TIME, open_time);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_TAX_NAME_1, taxName1);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_TAX_CENT_1, taxCent1);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_TAX_FLAG_1, taxFlag1);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_TAX_NAME_2, taxName2);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_TAX_CENT_2, taxCent2);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_TAX_FLAG_2, taxFlag2);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_TAX_NAME_3, taxName3);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_TAX_CENT_3, taxCent3);
                            orderDetailValues.put(OrderDetailsEntry.COLUMN_SHOP_TAX_FLAG_3, taxFlag3);
                            Uri insertUriOrderDetails = m_activity.getContentResolver().insert(OrderDetailsEntry.CONTENT_URI, orderDetailValues);
                        }
                    } else {

                        ContentValues cartUpdatedValues = new ContentValues();
                        cartUpdatedValues.put(ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED, itemQuan);

                        String whereClause = ShoppingCartEntry.COLUMN_ITEM_CODE + " = ? " + " and " +
                                ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = ? ";
                        String[] selectionArgs = new String[]{itemCode, itemSpCode};
                        m_activity.getContentResolver().update(ShoppingCartEntry.CONTENT_URI, cartUpdatedValues, whereClause, selectionArgs);
                    }

                    PayloadBuilder builder = new PayloadBuilder();
                    builder.putAttrString("StCode", itemStCode);
                    builder.putAttrString("SpCode", itemSpCode);
                    builder.putAttrString("ScCode", itemScCode);
                    builder.putAttrString("ItemCode", itemCode);
                    builder.putAttrString("ItemName", itemName);
                    builder.putAttrInt("ItemQuantitySelected", itemQuan);
                    MoEHelper.getInstance(m_activity).trackEvent("Added Item", builder.build());

                    new StoreClicksInSqlite(m_appContext).execute(itemCode,sp_code);

                }
            });

            holder.itemMinusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(holder.itemQuantitySelectedTextView.getText().toString()) != 0) {
                        int sel;
                        if (Integer.parseInt(holder.itemQuantitySelectedTextView.getText().toString()) == 1) {
                            ((ItemSnippet) getItem(itemPosition)).m_itemSelected = 0;
                        }

                        int itemQuan = Integer.parseInt(holder.itemQuantitySelectedTextView.getText().toString());
                        itemQuan = itemQuan - 1;
                        holder.itemQuantitySelectedTextView.setText(Integer.toString(itemQuan));
                        ((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected = itemQuan;

                        if (((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected == 0) {

                            String whereClause = ShoppingCartEntry.COLUMN_ITEM_CODE + " = ? " + " and " +
                                    ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = ? ";
                            String[] selectionArgs = new String[]{itemCode, itemSpCode};
                            m_activity.getContentResolver().delete(
                                    ShoppingCartEntry.CONTENT_URI,
                                    whereClause,
                                    selectionArgs
                            );

                            String whereCondition = ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = ? ";
                            String[] selectionArgument = new String[]{itemSpCode};
                            Cursor cursor2 = m_activity.getContentResolver().query(
                                    ShoppingCartEntry.CONTENT_URI,
                                    null,
                                    whereCondition,
                                    selectionArgument,
                                    null);

                            if (cursor2 == null || cursor2.getCount() == 0) {
                                String whereCond = OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
                                String[] selArg = new String[]{itemSpCode};
                                m_activity.getContentResolver().delete(
                                        OrderDetailsEntry.CONTENT_URI,
                                        whereCond,
                                        selArg
                                );
                            }
                        } else {
                            ContentValues itemUpdateValue = new ContentValues();
                            itemUpdateValue.put(ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED, itemQuan);

                            String where = ShoppingCartEntry.COLUMN_ITEM_CODE + " = ? " + " and " +
                                    ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = ? ";
                            String[] Args = new String[]{itemCode, itemSpCode};
                            m_activity.getContentResolver().update(ShoppingCartEntry.CONTENT_URI, itemUpdateValue, where, Args);

                        }

                        PayloadBuilder builder = new PayloadBuilder();
                        builder.putAttrString("StCode", itemStCode);
                        builder.putAttrString("SpCode", itemSpCode);
                        builder.putAttrString("ScCode", itemScCode);
                        builder.putAttrString("ItemCode", itemCode);
                        builder.putAttrString("ItemName", itemName);
                        builder.putAttrInt("ItemQuantitySelected", itemQuan);
                        builder.putAttrString("Activity Name", this.getClass().getSimpleName());
                        MoEHelper.getInstance(m_activity).trackEvent("Deleted Item", builder.build());

                        SharedPreferences cartprefs = m_activity.getSharedPreferences(
                                "technologies.angular.lazylad", Context.MODE_PRIVATE);

                        double m_totalCost = Double.parseDouble(cartprefs.getString("totalCost", "0.0"));
                        ;
                        int m_numOfItemsInShoppingCart = cartprefs.getInt("noOfItemsInShoppingCart", 0);

                        m_totalCost = Math.round(m_totalCost - (itemOfferPrice));
                        m_numOfItemsInShoppingCart = m_numOfItemsInShoppingCart - 1;

                        SharedPreferences.Editor editor = cartprefs.edit();
                        editor.putString("totalCost", String.valueOf(m_totalCost));
                        editor.putInt("noOfItemsInShoppingCart", m_numOfItemsInShoppingCart);
                        editor.commit();

                        TextView cost = ((ItemsActivity) m_activity).m_totalCostTextView;
                        cost.setText(Double.toString(m_totalCost));
                        TextView items = ((ItemsActivity) m_activity).m_totalItemsTextView;
                        items.setText(Integer.toString(m_numOfItemsInShoppingCart));
                    }
                }
            });




        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (m_images_available == 0)
            return TYPE_WITHOUT_IMAGE;
        else
            return TYPE_WITH_IMAGE;
    }


    private class StoreViewsInSqlite extends AsyncTask<String, Void, Void> {
        Context myactivity;

        public StoreViewsInSqlite(Context myactivity) {
            this.myactivity = myactivity;
        }

        @Override
        protected Void doInBackground(String... param) {

            int views_count = 0;
            String[] viewsSelectionValues = new String[]{LazyContract.ItemsViewsDetails.COLUMN_VIEWS_COUNT};
            String viewsWhereClause = LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE + " = ? and " +
                    LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE + " = ? and " +
                    LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC + " = ? ";
            String[] viewsSelectionArgs = new String[]{param[0],param[1], String.valueOf(0)};


            Cursor m_cursor = myactivity.getContentResolver().query(
                    LazyContract.ItemsViewsDetails.CONTENT_URI,
                    viewsSelectionValues,
                    viewsWhereClause,
                    viewsSelectionArgs,
                    null);

            if (m_cursor != null && m_cursor.moveToFirst()) {
                views_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.ItemsViewsDetails.COLUMN_VIEWS_COUNT));
            }
            views_count = views_count + 1;
            ContentValues viewsValues = new ContentValues();
            viewsValues.put(LazyContract.ItemsViewsDetails.COLUMN_VIEWS_COUNT, views_count);
            int views_update = myactivity.getContentResolver().update(LazyContract.ItemsViewsDetails.CONTENT_URI, viewsValues, viewsWhereClause, viewsSelectionArgs);
            if (views_update < 1) {
                viewsValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE, param[0]);
                viewsValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE, param[1]);
                viewsValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC, 0);
                myactivity.getContentResolver().insert(LazyContract.ItemsViewsDetails.CONTENT_URI, viewsValues);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
        }
    }


    private class StoreClicksInSqlite extends AsyncTask<String, Void, Void> {
        Context myactivity;

        public StoreClicksInSqlite(Context myactivity) {
            this.myactivity = myactivity;
        }

        @Override
        protected Void doInBackground(String... param) {

            int clicks_count = 0;
            String[] clicksSelectionValues = new String[]{LazyContract.ItemsClicksDetails.COLUMN_COUNT};
            String clicksWhereClause = LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE + " = ? and " +
                    LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE + " = ? and " +
                    LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC + " = ? ";
            String[] clicksSelectionArgs = new String[]{param[0],param[1], String.valueOf(0)};


            Cursor m_cursor = myactivity.getContentResolver().query(
                    LazyContract.ItemsClicksDetails.CONTENT_URI,
                    clicksSelectionValues,
                    clicksWhereClause,
                    clicksSelectionArgs,
                    null);

            if (m_cursor != null && m_cursor.moveToFirst()) {
                clicks_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.ItemsClicksDetails.COLUMN_COUNT));
            }
            clicks_count = clicks_count + 1;
            ContentValues clicksValues = new ContentValues();
            clicksValues.put(LazyContract.ItemsClicksDetails.COLUMN_COUNT, clicks_count);
            int clicks_update = myactivity.getContentResolver().update(LazyContract.ItemsClicksDetails.CONTENT_URI, clicksValues, clicksWhereClause, clicksSelectionArgs);
            if (clicks_update < 1) {
                clicksValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE, param[0]);
                clicksValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE, param[1]);
                clicksValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC, 0);
                myactivity.getContentResolver().insert(LazyContract.ItemsClicksDetails.CONTENT_URI, clicksValues);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }


}
