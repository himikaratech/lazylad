package technologies.angular.lazylad;

/**
 * Created by Saurabh on 11/03/15.
 */
public class PreviousOrdersDetailsSnippet {
    public int m_itemId;
    public String m_itemCode;
    public String m_itemName;
    public int m_itemImgFlag;
    public String m_itemImgAdd;
    public String m_itemShortDesc; //Weight like 20 gm wali dabi
    public int m_itemQuantity;
    public double m_itemCost;   //total cost i.e. cost*quantity
    public String m_itemDesc;
    public Boolean m_b_confirmed;

    public PreviousOrdersDetailsSnippet() {
    }

    public PreviousOrdersDetailsSnippet(int itemId, String itemCode, String itemName, int itemImgFlag, String itemImgAdd,
                                        String itemShortDesc, int itemQuantity, double itemCost, String itemDesc, Boolean b_confirmed) {
        m_itemId = itemId;
        m_itemCode = itemCode;
        m_itemName = itemName;
        m_itemImgFlag = itemImgFlag;
        m_itemImgAdd = itemImgAdd;
        m_itemShortDesc = itemShortDesc;
        m_itemQuantity = itemQuantity;
        m_itemCost = itemCost;
        m_itemDesc = itemDesc;
        m_b_confirmed = b_confirmed;
    }
}