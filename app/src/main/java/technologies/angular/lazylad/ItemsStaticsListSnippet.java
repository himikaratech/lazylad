package technologies.angular.lazylad;

/**
 * Created by Amud on 25/02/16.
 */
public class ItemsStaticsListSnippet {


    public String item_code;
    public int counters;
    public String user_code;
    public String sp_code;

    public ItemsStaticsListSnippet(String item_code, int clicks_count, String user_code,String sp_code) {
        this.item_code = item_code;
        counters = clicks_count;
        this.user_code=user_code;
        this.sp_code=sp_code;
    }
}
