package technologies.angular.lazylad;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Saurabh on 11/02/15.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static int m_oldDBVersion = 3;
    //private static int m_currentDBVersion=4;
    private static int m_currentDBVersion = 4;
    private static String m_DB_NAME = "LazyLadData";
    private SQLiteDatabase m_db;
    private static DBHelper dbinstance;
    private static int[] version;

    private static final String USER_DETAILS = "user_details";
    private static final String SERVICE_TYPES = "service_types";
    private static final String USER_ADD_DETAILS = "user_address_details";
    private static final String ITEM_DETAILS = "item_details";
    private static final String SHOPPING_CART = "shopping_cart";
    private static final String SERVICE_TYPE_CATEGORY = "service_type_categories";
    private static final String SEARCH_RESULT = "search_result";
    private static final String ORDER_DETAILS = "order_details";


    static {
        DBHelper.dbinstance = null;
        DBHelper.version = null;
    }

    public DBHelper(Context context) {
        super(context, m_DB_NAME, (SQLiteDatabase.CursorFactory) (null), DBHelper.m_currentDBVersion);
        //Log.i("In DBHelper", "Class");
    }

    public DBHelper(Context context, String name, CursorFactory factory,
                    int version) {
        super(context, name, factory, DBHelper.m_currentDBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + USER_DETAILS +
                        "(" +
                        "id INTEGER PRIMARY KEY, user_code int , " +
                        "varified_flag int , " +
                        "varified_number text , " +
                        "email_flag int , " +
                        "email_id text " +
                        ");"
        );
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + SERVICE_TYPES +
                        "(" +
                        "id INTEGER PRIMARY KEY, " +
                        "st_code int, " +
                        "st_name text, " +
                        "st_img_flag int, " +
                        "st_img_add text, " +
                        "st_Flag int DEFAULT 0 " +
                        ");"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS " + USER_ADD_DETAILS +
                        "(" +
                        "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        "user_code int, user_address_code int, " +
                        "user_address text, user_name text, " +
                        "user_email_id text, " +
                        "user_phone_number text," +
                        "user_flat text," +
                        "user_subarea text ," +
                        "user_area text );"
        );
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + ITEM_DETAILS +
                        "(" +
                        "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        "item_code text, " +
                        "item_name text, " +
                        "item_img_flag int, " +
                        "item_img_address text, " +
                        "item_unit text, " +
                        "item_cost double, " +
                        "item_short_desc text, " +
                        "item_desc text, " +
                        "item_status text, " +
                        "item_selected int, " +
                        "item_quantity_selected int, " +
                        "item_st_code text, " +
                        "item_sc_code text, " +
                        "item_mrp double" +
                        ");"
        );
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + SHOPPING_CART +
                        "(" +
                        "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        "item_code text, " +
                        "item_name text, " +
                        "item_img_flag int, " +
                        "item_img_address text, " +
                        "item_unit text, " +
                        "item_cost double, " +
                        "item_short_desc text, " +
                        "item_desc text, " +
                        "item_status text, " +
                        "item_selected int, " +
                        "item_quantity_selected int, " +
                        "item_st_code text, " +
                        "item_sc_code text, " +
                        "item_sp_code text " +
                        ");"
        );
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + SERVICE_TYPE_CATEGORY +
                        "(" +
                        "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        "st_code text, " +
                        "sc_code text, " +
                        "sc_name text" +
                        ");"
        );
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + SEARCH_RESULT +
                        "(" +
                        "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        "item_code text, " +
                        "item_name text, " +
                        "item_img_flag int, " +
                        "item_img_address text, " +
                        "item_unit text, " +
                        "item_cost double, " +
                        "item_short_desc text, " +
                        "item_desc text, " +
                        "item_status text, " +
                        "item_selected int, " +
                        "item_quantity_selected int, " +
                        "item_st_code text, " +
                        "item_sc_code text, " +
                        "item_mrp double, " +
                        "search_string text" +
                        ");"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + ORDER_DETAILS +
                        "(" +
                        "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                        "serv_prov_code text, " +
                        "del_time text, " +
                        "min_order int, " +
                        "serv_prov_name text, " +
                        "time_interval int, " +
                        "open_time int, " +
                        "close_time int, " +
                        "tax_name_1 text, " +
                        "tax_cent_1 double, " +
                        "tax_flag_1 int, " +
                        "tax_name_2 text, " +
                        "tax_cent_2 double, " +
                        "tax_flag_2 int, " +
                        "tax_name_3 text, " +
                        "tax_cent_3 double, " +
                        "tax_flag_3 int " +
                        ");"
        );

        insertointodb(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (2 > oldVersion && 2 <= newVersion) {
            db.execSQL("drop table if exists service_types");
            db.execSQL("drop table if exists service_type_categories");
            db.execSQL("drop table if exists item_details");
            db.execSQL("drop table if exists shopping_cart");

            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + SERVICE_TYPES +
                            "(" +
                            "id INTEGER PRIMARY KEY, " +
                            "st_code int, " +
                            "st_name text, " +
                            "st_img_flag int, " +
                            "st_img_add int " +
                            ");"
            );
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + ITEM_DETAILS +
                            "(" +
                            "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            "item_code text, " +
                            "item_name text, " +
                            "item_img_flag int, " +
                            "item_img_address text, " +
                            "item_unit text, " +
                            "item_cost double, " +
                            "item_short_desc text, " +
                            "item_desc text, " +
                            "item_status text, " +
                            "item_selected int, " +
                            "item_quantity_selected int, " +
                            "item_st_code text, " +
                            "item_sc_code text, " +
                            "item_mrp double" +
                            ");"
            );
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + SHOPPING_CART +
                            "(" +
                            "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            "item_code text, " +
                            "item_name text, " +
                            "item_img_flag int, " +
                            "item_img_address text, " +
                            "item_unit text, " +
                            "item_cost double, " +
                            "item_short_desc text, " +
                            "item_desc text, " +
                            "item_status text, " +
                            "item_selected int, " +
                            "item_quantity_selected int, " +
                            "item_st_code text, " +
                            "item_sc_code text" +
                            ");"
            );
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + SERVICE_TYPE_CATEGORY +
                            "(" +
                            "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            "st_code text, " +
                            "sc_code text, " +
                            "sc_name text" +
                            ");"
            );

            insertointodb(db);

        }

        if (3 > oldVersion && 3 <= newVersion) {
            db.execSQL("ALTER TABLE user_address_details ADD COLUMN user_flat text ;");
            db.execSQL("ALTER TABLE user_address_details ADD COLUMN user_subarea text ;");
            db.execSQL("ALTER TABLE user_address_details ADD COLUMN user_area text ;");

            db.execSQL("drop table if exists service_types");
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + SERVICE_TYPES +
                            "(" +
                            "id INTEGER PRIMARY KEY, " +
                            "st_code int, " +
                            "st_name text, " +
                            "st_img_flag int, " +
                            "st_img_add text" +
                            ");"
            );

            insertointodb(db);
        }

        if (4 > oldVersion && 4 <= newVersion)

        {

            db.execSQL("DROP TABLE IF EXISTS " + SERVICE_TYPE_CATEGORY);
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + SERVICE_TYPE_CATEGORY +
                            "(" +
                            "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            "st_code text, " +
                            "sc_code text, " +
                            "sc_name text" +
                            ");"
            );

            db.execSQL("ALTER TABLE user_details ADD COLUMN  varified_flag int  ;");
            db.execSQL("ALTER TABLE user_details ADD COLUMN  varified_number text ;");
            db.execSQL("ALTER TABLE user_details ADD COLUMN  email_flag int ;");
            db.execSQL("ALTER TABLE user_details ADD COLUMN  email_id text ;");

            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + SEARCH_RESULT +
                            "(" +
                            "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            "item_code text, " +
                            "item_name text, " +
                            "item_img_flag int, " +
                            "item_img_address text, " +
                            "item_unit text, " +
                            "item_cost double, " +
                            "item_short_desc text, " +
                            "item_desc text, " +
                            "item_status text, " +
                            "item_selected int, " +
                            "item_quantity_selected int, " +
                            "item_st_code text, " +
                            "item_sc_code text, " +
                            "item_mrp double, " +
                            "search_string text" +
                            ");"
            );


        }

        if (5 > oldVersion && 5 <= newVersion)

        {

            db.execSQL("DROP TABLE IF EXISTS " + SHOPPING_CART);
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + SHOPPING_CART +
                            "(" +
                            "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            "item_code text, " +
                            "item_name text, " +
                            "item_img_flag int, " +
                            "item_img_address text, " +
                            "item_unit text, " +
                            "item_cost double, " +
                            "item_short_desc text, " +
                            "item_desc text, " +
                            "item_status text, " +
                            "item_selected int, " +
                            "item_quantity_selected int, " +
                            "item_st_code text, " +
                            "item_sc_code text, " +
                            "item_sp_code text " +
                            ");"
            );

            db.execSQL("DROP TABLE IF EXISTS " + ORDER_DETAILS);
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + ORDER_DETAILS +
                            "(" +
                            "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            "serv_prov_code text, " +
                            "del_time text, " +
                            "min_order int, " +
                            "serv_prov_name text, " +
                            "time_interval int, " +
                            "open_time int, " +
                            "close_time int, " +
                            "tax_name_1 text, " +
                            "tax_cent_1 double, " +
                            "tax_flag_1 int, " +
                            "tax_name_2 text, " +
                            "tax_cent_2 double, " +
                            "tax_flag_2 int, " +
                            "tax_name_3 text, " +
                            "tax_cent_3 double, " +
                            "tax_flag_3 int " +
                            ");"
            );
        }

    }

    public void insertointodb(SQLiteDatabase db) {
        db.execSQL("insert into service_types values(1, 1, 'Groceries & Provisions', 1, " + R.drawable.alpha_bottom + ", 0 );");
        db.execSQL("insert into service_types values(3, 3, 'Bakery', 1, " + R.drawable.alpha_bottom + ", 0 );");
        db.execSQL("insert into service_types values(4, 4, 'Stationery', 1, " + R.drawable.alpha_bottom + ", 0 );");
        db.execSQL("insert into service_types values(5, 5, 'Meats & Seafood', 1, " + R.drawable.alpha_bottom + ", 0 );");
        db.execSQL("insert into service_types values(6, 6, 'Fruits & Vegetables', 1, " + R.drawable.alpha_bottom + ", 0 );");
        db.execSQL("insert into service_types values(7, 7, 'Flowers', 1, " + R.drawable.alpha_bottom + ", 0 );");
    }
}
