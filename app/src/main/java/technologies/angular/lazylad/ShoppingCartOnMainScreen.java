package technologies.angular.lazylad;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;

import technologies.angular.lazylad.LazyContract.ShoppingCartEntry;
import technologies.angular.lazylad.LazyContract.OrderDetailsEntry;

import java.util.ArrayList;

/**
 * Created by sakshigupta on 23/10/15.
 */
public class ShoppingCartOnMainScreen extends ActionBarActivity {

    static final String TAG = "ShoppingCartOnMainScreen";
    TextView m_totalCostTextView;
    TextView m_totalItemsTextView;
    private Button m_checkOutButton;

    double totalCost;
    int numOfItemsInShoppingCart;

    private ListView m_shoppingCartListView;
    private ShoppingCartOnMainScreenAdapter m_shoppingCartAdapter;
    private ArrayList<ItemSnippet> m_shoppingCartItemsList;
    private ArrayList<ItemSnippet> m_shoppingCartItemsCheckOutList;

    private static String m_servProvCode;
    private static String m_stCode;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_on_main_screen);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        View topLayout = findViewById(R.id.shopping_cart_linear_layout);
        topLayout.setPadding(0, Utils.getStatusBarHeight(this), 0, 0);

        SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        totalCost = Double.parseDouble(prefs.getString("totalCost", "0.0"));//m_variablesFromLastActivity[0];
        numOfItemsInShoppingCart = prefs.getInt("noOfItemsInShoppingCart", 0);
        m_servProvCode = prefs.getString("ServiceProviderCode", "Not Available");
        m_stCode = prefs.getString("ServTypeCode", "Not Available");

        m_shoppingCartItemsList = new ArrayList<ItemSnippet>();
        m_shoppingCartItemsCheckOutList = new ArrayList<ItemSnippet>();

        m_checkOutButton = (Button) findViewById(R.id.checkout_button);
        m_shoppingCartListView = (ListView) findViewById(R.id.shoping_cart_items_list_view);
        m_totalCostTextView = (TextView) findViewById(R.id.total_shopping_cost);
        m_totalItemsTextView = (TextView) findViewById(R.id.items_in_cart);

        Cursor m_cursor = getContentResolver().query(
                ShoppingCartEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor == null) {
            Intent intent = new Intent(ShoppingCartOnMainScreen.this, NoItemsInCart.class);
            startActivity(intent);
        }

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() > 0) {
                m_shoppingCartItemsList = new ArrayList<ItemSnippet>(m_cursor.getCount());
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    int itemsId = i + 1;
                    String m_itemCode = m_cursor.getString(1);
                    String m_itemName = m_cursor.getString(2);
                    int m_itemImgFlag = m_cursor.getInt(3);
                    String m_itemImgAddress = m_cursor.getString(4);
                    String m_itemUnit = m_cursor.getString(5);
                    double m_itemCost = m_cursor.getDouble(6);
                    String m_itemShortDesc = m_cursor.getString(7);  //Standard Weight
                    String m_itemDesc = m_cursor.getString(8);
                    String m_itemStatus = m_cursor.getString(9);  //Available and Not Available
                    String m_itemServiceType = m_stCode;
                    int itemSelected = m_cursor.getInt(10);
                    int itemQuantitySelected = m_cursor.getInt(11);
                    String m_itemServiceTypeCategory = m_cursor.getString(13);
                    String m_itemServiceProvider = m_cursor.getString(14);
                    double m_itemOfferPrice = m_cursor.getDouble(15);
                    int m_itemInOffer = m_cursor.getInt(16);

                    String[] mProjection = {OrderDetailsEntry.COLUMN_SERV_PROV_NAME};
                    String whereClause = OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
                    String[] selectionArgs = new String[]{m_itemServiceProvider};
                    Cursor cursor1 = getContentResolver().query(
                            OrderDetailsEntry.CONTENT_URI,
                            mProjection,
                            whereClause,
                            selectionArgs,
                            null);

                    if (cursor1.moveToFirst()) {
                        String sellerName = cursor1.getString(cursor1.getColumnIndex("serv_prov_name"));
                        ItemSnippet currenOrderObj = new ItemSnippet(itemsId, m_itemCode, m_itemName, m_itemImgFlag, m_itemImgAddress,
                                m_itemUnit, m_itemCost, m_itemShortDesc, m_itemDesc, m_itemStatus, itemSelected, itemQuantitySelected,
                                m_itemServiceType, m_itemServiceTypeCategory, m_itemServiceProvider, sellerName,m_itemOfferPrice,m_itemInOffer);
                        m_shoppingCartItemsList.add(currenOrderObj);
                    }
                    m_cursor.moveToNext();
                }
            }
        }

        m_shoppingCartAdapter = new ShoppingCartOnMainScreenAdapter(ShoppingCartOnMainScreen.this,ShoppingCartOnMainScreen.this.getApplicationContext(), m_shoppingCartItemsList);
        m_shoppingCartListView.setAdapter((ListAdapter) m_shoppingCartAdapter);

        m_checkOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Tracker cart = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
                cart.send(new HitBuilders.EventBuilder()
                        .setCategory(Double.toString(totalCost))
                        .setAction(Integer.toString(numOfItemsInShoppingCart))
                        .build());

                startCheckOutActivity();
            }
        });

        setShoppingCartList();

        setTracker();
        mHelper = new MoEHelper(this);
    }

    private void startCheckOutActivity() {

        Cursor m_cursor = getContentResolver().query(
                ShoppingCartEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() > 0) {
                m_shoppingCartItemsCheckOutList = new ArrayList<ItemSnippet>(m_cursor.getCount());
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    int itemsId = i + 1;
                    String m_itemCode = m_cursor.getString(1);
                    String m_itemName = m_cursor.getString(2);
                    int m_itemImgFlag = m_cursor.getInt(3);
                    String m_itemImgAddress = m_cursor.getString(4);
                    String m_itemUnit = m_cursor.getString(5);
                    double m_itemCost = m_cursor.getDouble(6);
                    String m_itemShortDesc = m_cursor.getString(7);  //Standard Weight
                    String m_itemDesc = m_cursor.getString(8);
                    String m_itemStatus = m_cursor.getString(9);  //Available and Not Available
                    String m_itemServiceType = m_stCode;
                    int itemSelected = m_cursor.getInt(10);
                    int itemQuantitySelected = m_cursor.getInt(11);
                    String m_itemServiceTypeCategory = m_cursor.getString(13);
                    String m_itemServiceProvider = m_cursor.getString(14);
                    double m_itemOfferPrice = m_cursor.getDouble(15);
                    int m_itemInOffer = m_cursor.getInt(16);

                    String[] mProjection = {OrderDetailsEntry.COLUMN_SERV_PROV_NAME};
                    String whereClause = OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
                    String[] selectionArgs = new String[]{m_itemServiceProvider};
                    Cursor cursor2 = getContentResolver().query(
                            OrderDetailsEntry.CONTENT_URI,
                            mProjection,
                            whereClause,
                            selectionArgs,
                            null);

                    if (cursor2.moveToFirst()) {
                        String sellerName = cursor2.getString(cursor2.getColumnIndex("serv_prov_name"));

                        ItemSnippet currenOrderObj = new ItemSnippet(itemsId, m_itemCode, m_itemName, m_itemImgFlag, m_itemImgAddress,
                                m_itemUnit, m_itemCost, m_itemShortDesc, m_itemDesc, m_itemStatus, itemSelected, itemQuantitySelected,
                                m_itemServiceType, m_itemServiceTypeCategory, m_itemServiceProvider, sellerName,m_itemOfferPrice,m_itemInOffer);
                        m_shoppingCartItemsCheckOutList.add(currenOrderObj);
                    }
                    m_cursor.moveToNext();
                }
                Intent intent = new Intent(ShoppingCartOnMainScreen.this, AddressActivity.class);
                intent.putExtra("ItemsDetails", m_shoppingCartItemsCheckOutList);
                intent.putExtra("sourceFlag", 1);
                String retailerDetails[] = {m_servProvCode, m_stCode};
                intent.putExtra("retailer_details", retailerDetails);
                intent.putExtra("total_amount", Double.toString(totalCost));
                ShoppingCartOnMainScreen.this.startActivity(intent);
            }
        }
    }

    public void setShoppingCartList() {
        m_totalCostTextView.setText(Double.toString(totalCost));
        m_totalItemsTextView.setText(Integer.toString(numOfItemsInShoppingCart));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(this.getClass().getCanonicalName());

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
