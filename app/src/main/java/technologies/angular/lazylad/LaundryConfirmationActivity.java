package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.squareup.okhttp.internal.Util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazylad.Snippets.LaundryServicesRequestedSnippet;
import technologies.angular.lazylad.common.Constants;
import technologies.angular.lazylad.fragments.DeliverySlotFragment;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

public class LaundryConfirmationActivity extends AppCompatActivity implements DeliverySlotFragment.InDeliverySlotCallback {

    private static final String TAG = LaundryConfirmationActivity.class.getSimpleName();

    @Bind(R.id.pickup_checkbox)
    CheckBox mPickupTimeCheckBox;
    @Bind(R.id.delivery_checkbox)
    CheckBox mDeliveryTimeCheckBox;
    @Bind(R.id.pickup_time_textview)
    TextView mPickupTimeTextView;
    @Bind(R.id.delivery_time_textview)
    TextView mDeliveryTimeTextView;
    @Bind(R.id.coupon_code_edittext)
    EditText mCouponCodeEditText;
    @Bind(R.id.apply_code_button)
    Button mSubmitButton;
    @Bind(R.id.confirm_order_button)
    Button mConfirmOrderButton;
    @Bind(R.id.discount_applied_textview)
    TextView mDiscountAppliedTextView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    private boolean mIsPickupTimeSet = false;
    private boolean mIsDeliveryTimeSet = false;

    private String[] mAddressInfo;

    private SharedPreferences prefs;
    private String mCouponCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laundry_confirmation);
        ButterKnife.bind(this);
        toolbar.setTitle("Schedule");
        toolbar.setPadding(0, Utils.getStatusBarHeight(this), 0, 0);
        setSupportActionBar(toolbar);

        prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        final int shopOpenTime = prefs.getInt(PreferencesStore.PREF_SHOP_OPEN_TIME, 0);
        final int shopCloseTime = prefs.getInt(PreferencesStore.PREF_SHOP_CLOSE_TIME, 0);
        final int timeInterval = prefs.getInt(PreferencesStore.PREF_TIME_INTERVAL, 0);
        final long processingTime = prefs.getLong(PreferencesStore.PREF_PROCESSING_TIME, 0);

        Intent intent = getIntent();
        mAddressInfo = intent.getStringArrayExtra("data_send_to_confirm_activity");

        final Calendar shopClosingCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        shopClosingCalendar.set(Calendar.MINUTE, shopCloseTime % 60);
        shopClosingCalendar.set(Calendar.MILLISECOND, 0);
        shopClosingCalendar.set(Calendar.HOUR_OF_DAY, shopCloseTime / 60);

        final Calendar shopOpeningCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        shopOpeningCalendar.set(Calendar.MINUTE, shopOpenTime % 60);
        shopOpeningCalendar.set(Calendar.MILLISECOND, 0);
        shopOpeningCalendar.set(Calendar.HOUR_OF_DAY, shopOpenTime / 60);

        final Calendar deliveryShopOpeningCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        deliveryShopOpeningCalendar.set(Calendar.MINUTE, (int) (shopOpenTime + processingTime) % 60);
        deliveryShopOpeningCalendar.set(Calendar.MILLISECOND, 0);
        deliveryShopOpeningCalendar.set(Calendar.HOUR_OF_DAY, (int) (shopOpenTime + processingTime) / 60);

        mPickupTimeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    DeliverySlotFragment fragment = DeliverySlotFragment.newInstance(shopOpeningCalendar.getTimeInMillis(), shopClosingCalendar.getTimeInMillis(), timeInterval * 60000, Constants.LAUNDRY_PICKUP_SLOT_CODE, 0, 0);
                    fragment.show(getFragmentManager(), "DELIVERY_SLOT_FRAGMENT_TAG");
                } else {
                    mIsPickupTimeSet = false;
                }
            }
        });

        mDeliveryTimeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    if (mIsPickupTimeSet) {
                        DeliverySlotFragment fragment = DeliverySlotFragment.newInstance(deliveryShopOpeningCalendar.getTimeInMillis(), shopClosingCalendar.getTimeInMillis(), timeInterval * 60000, Constants.LAUNDRY_DELIVERY_SLOT_CODE, Utils.getEpochTimeByTimeSlot(mPickupTimeTextView.getText().toString()), processingTime / (60 * 24));
                        fragment.show(getFragmentManager(), "DELIVERY_SLOT_FRAGMENT_TAG");
                    } else {
                        Toast.makeText(LaundryConfirmationActivity.this, getString(R.string.select_pickup_time), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mIsDeliveryTimeSet = false;
                }
            }
        });

    }

    @Override
    public void onSetClicked(String timeSlot, int serviceCode) {
        PayloadBuilder builder = new PayloadBuilder();
        switch (serviceCode) {
            case Constants.LAUNDRY_PICKUP_SLOT_CODE:
                if (!timeSlot.isEmpty()) {
                    // MoEngage Tracking
                    builder.putAttrString("PickUp Time", timeSlot);
                    MoEHelper.getInstance(this).trackEvent("PickUp Time Selected", builder.build());

                    mPickupTimeTextView.setText(timeSlot);
                    mIsPickupTimeSet = true;
                }

                break;
            case Constants.LAUNDRY_DELIVERY_SLOT_CODE:
                if (!timeSlot.isEmpty()) {
                    // MoEngage Tracking
                    builder.putAttrString("Delivery Time", timeSlot);
                    MoEHelper.getInstance(this).trackEvent("Delivery Time Selected", builder.build());

                    mDeliveryTimeTextView.setText(timeSlot);
                    mIsDeliveryTimeSet = true;
                }
                break;
        }

    }

    public void onClicked(View view) {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        RestAdapter restAdapter = Utils.providesRestAdapter(Constants.WEB_SERVICE_URL);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        final PayloadBuilder builder = new PayloadBuilder();

        switch (view.getId()) {
            case R.id.apply_code_button:
                APIRequestModel.ServiceCouponRequestModel couponRequestModel = requestModel.new ServiceCouponRequestModel();
                couponRequestModel.coupon_code = mCouponCodeEditText.getText().toString();
                couponRequestModel.user_code = mAddressInfo[2];
                apiSuggestionsService.getCouponVailidity(couponRequestModel, new Callback<APIResonseModel.LaundryServiceCouponResponseModel>() {
                    @Override
                    public void success(APIResonseModel.LaundryServiceCouponResponseModel laundryServiceCouponResponseModel, Response response) {
                        boolean success = laundryServiceCouponResponseModel.success;
                        if (success) {
                            progressDialog.dismiss();
                            String discountAmount = laundryServiceCouponResponseModel.discount_amount;
                            String discount_type = laundryServiceCouponResponseModel.coupon_type;
                            double max_discount = laundryServiceCouponResponseModel.max_discount;
                            mDiscountAppliedTextView.setVisibility(View.VISIBLE);
                            mCouponCode = mCouponCodeEditText.getText().toString();
                            if (discount_type.equalsIgnoreCase("Percentage")) {
                                discountAmount += "% ( maximum discount ₹ " + max_discount + " )";
                            } else {
                                discountAmount = "₹" + discountAmount;
                            }
                            // MoEngage Tracking
                            builder.putAttrString("Coupon Code", mCouponCode);
                            builder.putAttrString("User Code", mAddressInfo[2]);
                            MoEHelper.getInstance(LaundryConfirmationActivity.this).trackEvent("Apply Code Button Pressed", builder.build());

                            Utils.hideKeyboard(LaundryConfirmationActivity.this);
                            mDiscountAppliedTextView.setText(String.format(getString(R.string.discount_given_through_coupon), discountAmount));
                        } else {
                            mCouponCode = "";
                            mDiscountAppliedTextView.setVisibility(View.GONE);
                            progressDialog.dismiss();
                            Toast.makeText(LaundryConfirmationActivity.this, getString(R.string.invalid_coupon), Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(TAG, error.toString());
                        progressDialog.dismiss();
                        mCouponCode = "";
                        mDiscountAppliedTextView.setVisibility(View.GONE);
                        Toast.makeText(LaundryConfirmationActivity.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case R.id.confirm_order_button:

                if (!mIsPickupTimeSet) {
                    Toast.makeText(this, getString(R.string.select_pickup_time), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    return;
                }
                if (!mIsDeliveryTimeSet) {
                    Toast.makeText(this, getString(R.string.select_delivery_time), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    return;
                }

                final String servicesRequested = prefs.getString(PreferencesStore.PREF_SERVICES_REQUESTED, "");
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<LaundryServicesRequestedSnippet>>() {
                }.getType();
                ArrayList<LaundryServicesRequestedSnippet> serviceRequestedList = gson.fromJson(servicesRequested, type);
                for (LaundryServicesRequestedSnippet serviceRequested : serviceRequestedList) {
                    serviceRequested.setPickup_time(Utils.getEpochTimeByTimeSlot(mPickupTimeTextView.getText().toString()));
                    serviceRequested.setDelivery_time(Utils.getEpochTimeByTimeSlot(mDeliveryTimeTextView.getText().toString()));
                }

                APIRequestModel.LaundryServicesRequestModel serviceRequestModel = requestModel.new LaundryServicesRequestModel();
                serviceRequestModel.address_code = mAddressInfo[0];
                serviceRequestModel.address_user_code = mAddressInfo[1];
                serviceRequestModel.user_code = mAddressInfo[2];
                serviceRequestModel.services_requested = serviceRequestedList;
                serviceRequestModel.coupon_code = mCouponCode;

                String whereClause = LazyContract.UserDetailsEntry.COLUMN_USER_CODE + " = ? ";
                String[] selectionArgs = new String[]{mAddressInfo[2]};
                Cursor cursor = getContentResolver().query(
                        LazyContract.UserDetailsEntry.CONTENT_URI,
                        null,
                        whereClause,
                        selectionArgs,
                        null);

                if (cursor != null && cursor.moveToFirst()) {
                    if (cursor.getCount() != 0) {
                        serviceRequestModel.phone = cursor.getString(3);
                        cursor.close();
                    }
                }

                apiSuggestionsService.sendRequestedServices(serviceRequestModel, new Callback<APIResonseModel.LaundryRequestedServicesResponseModel>() {
                    @Override
                    public void success(APIResonseModel.LaundryRequestedServicesResponseModel laundryRequestedServicesResponseModel, Response response) {
                        boolean success = laundryRequestedServicesResponseModel.success;
                        if (success) {
                            String pickUptime = laundryRequestedServicesResponseModel.pickup_time;
                            String deliveryTime = laundryRequestedServicesResponseModel.delivery_time;
                            String orderCode = laundryRequestedServicesResponseModel.order_code;
                            progressDialog.dismiss();

                            //MoEngage Tracking
                            PayloadBuilder builder = new PayloadBuilder();
                            builder.putAttrString("PickUp Time", pickUptime);
                            builder.putAttrString("Delivery Time", deliveryTime);
                            builder.putAttrString("Order Code", orderCode);
                            builder.putAttrString("Services Requested List", servicesRequested);
                            MoEHelper.getInstance(LaundryConfirmationActivity.this).trackEvent("Confirm Button Pressed", builder.build());

                            Intent intent = new Intent(LaundryConfirmationActivity.this, LastActivity.class);
                            intent.putExtra(LastActivity.CALLING_ACTIVITY_TAG, 1);
                            intent.putExtra(LastActivity.SERVICE_ORDER_CODE, orderCode);
                            intent.putExtra(LastActivity.SERVICE_PICKUP_TIME, mPickupTimeTextView.getText().toString());
                            intent.putExtra(LastActivity.SERVICE_DELIVERY_TIME, mDeliveryTimeTextView.getText().toString());
                            startActivity(intent);

                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(LaundryConfirmationActivity.this, getString(R.string.server_error_occured), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(TAG, error.toString());
                        progressDialog.dismiss();
                        Toast.makeText(LaundryConfirmationActivity.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    }
                });
                break;

        }
    }
}
