package technologies.angular.lazylad;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Saurabh on 11/02/15.
 */
public class ItemSnippet implements Serializable, Parcelable {

    private static final long serialVersionUID = 1L;

    public int m_itemId;
    @SerializedName("item_code")
    public String m_itemCode;
    @SerializedName("item_name")
    public String m_itemName;
    @SerializedName("item_img_flag")
    public int m_itemImgFlag;
    @SerializedName("item_img_address")
    public String m_itemImgAddress;
    @SerializedName("item_unit")
    public String m_itemUnit;
    @SerializedName("item_cost")
    public double m_itemCost;
    @SerializedName("item_short_desc")
    public String m_itemShortDesc;   //Standard Weight
    @SerializedName("item_desc")
    public String m_itemDesc;
    @SerializedName("item_status")
    public String m_itemStatus;   //Available and Not Available
    @SerializedName("item_selected")
    public int m_itemSelected;
    @SerializedName("item_quantity_selected")
    public int m_itemQuantitySelected;
    @SerializedName("item_service_type")
    public String m_itemServiceType;
    @SerializedName("item_service_category")
    public String m_itemServiceCategory;
    @SerializedName("item_service_provider")
    public String m_itemServiceProvider;
    @SerializedName("item_mrp")
    public double m_itemMrp;
    @SerializedName("item_offer_price")
    public double m_itemOfferPrice;

    @SerializedName("item_in_offer")
    public int m_itemInOffer;

    public String seller_Name;


    public ItemSnippet() {

    }

   /* public ItemSnippet(int id, String itemCode, String itemName, int itemImgFlag,
                       String itemImgAdd, String itemUnit, double itemCost, String itemShortDesc, String itemDesc,
                       String itemStatus, int itemSelected, int itemQuantitySelected) {
        m_itemId = id;
        m_itemCode = itemCode;
        m_itemName = itemName;
        m_itemImgFlag = itemImgFlag;
        m_itemImgAddress = itemImgAdd;
        m_itemUnit = itemUnit;
        m_itemCost = itemCost;
        m_itemShortDesc = itemShortDesc;
        m_itemDesc = itemDesc;
        m_itemStatus = itemStatus;
        m_itemSelected = itemSelected;
        m_itemQuantitySelected = itemQuantitySelected;
    }*/

    public ItemSnippet(int id, String itemCode, String itemName, int itemImgFlag,
                       String itemImgAdd, String itemUnit, double itemCost, String itemShortDesc, String itemDesc,
                       String itemStatus, int itemSelected, int itemQuantitySelected, String itemServiceType, String itemServiceCategory, String itemServiceProvider, String sellerName,double itemOfferPrice,int itemInOffer) {
        m_itemId = id;
        m_itemCode = itemCode;
        m_itemName = itemName;
        m_itemImgFlag = itemImgFlag;
        m_itemImgAddress = itemImgAdd;
        m_itemUnit = itemUnit;
        m_itemCost = itemCost;
        m_itemShortDesc = itemShortDesc;
        m_itemDesc = itemDesc;
        m_itemStatus = itemStatus;
        m_itemSelected = itemSelected;
        m_itemQuantitySelected = itemQuantitySelected;
        m_itemServiceType = itemServiceType;
        m_itemServiceCategory = itemServiceCategory;
        m_itemServiceProvider = itemServiceProvider;
        seller_Name = sellerName;
        m_itemOfferPrice=itemOfferPrice;
        m_itemInOffer=itemInOffer;
    }

    public ItemSnippet(int id, String itemCode, String itemName, int itemImgFlag,
                       String itemImgAdd, String itemUnit, double itemCost, String itemShortDesc, String itemDesc,
                       String itemStatus, int itemSelected, int itemQuantitySelected, String itemServiceType, String itemServiceCategory, String sellerName,double itemOfferPrice,int itemInOffer) {
        m_itemId = id;
        m_itemCode = itemCode;
        m_itemName = itemName;
        m_itemImgFlag = itemImgFlag;
        m_itemImgAddress = itemImgAdd;
        m_itemUnit = itemUnit;
        m_itemCost = itemCost;
        m_itemShortDesc = itemShortDesc;
        m_itemDesc = itemDesc;
        m_itemStatus = itemStatus;
        m_itemSelected = itemSelected;
        m_itemQuantitySelected = itemQuantitySelected;
        m_itemServiceType = itemServiceType;
        m_itemServiceCategory = itemServiceCategory;
        seller_Name = sellerName;
        this.m_itemOfferPrice=itemOfferPrice;
        m_itemInOffer=itemInOffer;
    }

    public ItemSnippet(int id, String itemCode, String itemName, int itemImgFlag,
                       String itemImgAdd, String itemUnit, double itemCost, String itemShortDesc, String itemDesc,
                       String itemStatus, int itemSelected, int itemQuantitySelected, String itemServiceType, String itemServiceCategory, String itemServiceProvider, double itemMrp,double itemOfferPrice,int itemInOffer) {
        m_itemId = id;
        m_itemCode = itemCode;
        m_itemName = itemName;
        m_itemImgFlag = itemImgFlag;
        m_itemImgAddress = itemImgAdd;
        m_itemUnit = itemUnit;
        m_itemCost = itemCost;
        m_itemShortDesc = itemShortDesc;
        m_itemDesc = itemDesc;
        m_itemStatus = itemStatus;
        m_itemSelected = itemSelected;
        m_itemQuantitySelected = itemQuantitySelected;
        m_itemServiceType = itemServiceType;
        m_itemServiceCategory = itemServiceCategory;
        m_itemServiceProvider = itemServiceProvider;
        m_itemMrp = itemMrp;
        m_itemOfferPrice =itemOfferPrice;
        m_itemInOffer=itemInOffer;

    }


    public ItemSnippet(Parcel in) {
        m_itemId = in.readInt();
        m_itemCode = in.readString();
        m_itemName = in.readString();
        m_itemImgFlag = in.readInt();
        m_itemImgAddress = in.readString();
        m_itemUnit = in.readString();
        m_itemCost = in.readDouble();
        m_itemShortDesc = in.readString();
        m_itemDesc = in.readString();
        m_itemStatus = in.readString();
        m_itemSelected = in.readInt();
        m_itemQuantitySelected = in.readInt();
        m_itemServiceType = in.readString();
        m_itemServiceCategory = in.readString();
        m_itemMrp = in.readDouble();
        m_itemOfferPrice = in.readDouble();
        m_itemInOffer= in.readInt();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(this.m_itemId);

        dest.writeString(m_itemCode);
        dest.writeString(m_itemName);
        dest.writeInt(m_itemImgFlag);
        dest.writeString(m_itemImgAddress);
        dest.writeString(m_itemUnit);
        dest.writeDouble(m_itemCost);
        dest.writeString(m_itemShortDesc);
        dest.writeString(m_itemDesc);
        dest.writeString(m_itemStatus);
        dest.writeInt(m_itemSelected);
        dest.writeInt(m_itemQuantitySelected);
        dest.writeString(m_itemServiceType);
        dest.writeString(m_itemServiceCategory);
        dest.writeDouble(m_itemMrp);
        dest.writeDouble(m_itemOfferPrice);
        dest.writeInt(m_itemInOffer);

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ItemSnippet createFromParcel(Parcel in) {
            return new ItemSnippet(in);
        }

        public ItemSnippet[] newArray(int size) {
            return new ItemSnippet[size];
        }
    };
}