package technologies.angular.lazylad;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.moe.pushlibrary.utils.MoEHelperConstants;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueButton;
import com.truecaller.android.sdk.TrueClient;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueProfile;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

/**
 * Created by Amud on 30/09/15.
 */
public class NumberClass extends Activity implements ITrueCallback {

    private static final String TAG = NumberClass.class.getSimpleName();
    TextView phoneNumberEditText;
    ImageView submit;
    private String mobileNumber;
    private TextView skipEditText;
    static Context mContext;
    Bundle bundle;
    SharedPreferences prefs;
    Boolean skippable;
    Boolean skipped;
    String prefSkippedString;
    String prefVerifiedString;

    private TrueButton mTrueBtn;
    private TrueClient mTrueClient;

    private MoEHelper mHelper;

    private static final int TRUE_ERROR_TYPE_INTERNAL = 0;
    private static final int TRUE_ERROR_TYPE_NETWORK = 1;
    private static final int TRUE_ERROR_TYPE_USER_DENIED = 2;
    private static final int TRUE_ERROR_TYPE_UNAUTHORIZED_PARTNER = 3;
    private static final int TRUE_ERROR_TYPE_UNAUTHORIZED_USER = 4;

    private int mUserCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.number_layout);
        prefs = NumberClass.this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        bundle = new Bundle();
        bundle = getIntent().getExtras();
        skippable = getIntent().getBooleanExtra(NumberParameters.SKIPPABLE_CONSTANT, false);

        prefSkippedString = getIntent().getStringExtra(NumberParameters.PREF_STRING_CONTANT);
        prefVerifiedString = getIntent().getStringExtra(NumberParameters.PREF_VERIFIED_CONSTANT);

        phoneNumberEditText = (EditText) findViewById(R.id.phoneNrTextEdit);
        submit = (ImageView) findViewById(R.id.submit);
        skipEditText = (TextView) findViewById(R.id.skipTextView);
        skipEditText.setVisibility(View.GONE);
        skipped = false;

        mUserCode = Utils.getUserCode(this);

        submit.setEnabled(false);

        if (skippable == true)
            skipEditText.setVisibility(View.VISIBLE);


        final TextWatcher mTextEditorWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int value = s.length();
                submit.setEnabled(false);
                submit.setImageDrawable(getResources().getDrawable(R.drawable.blanktick));
                if (value == 10) {
                    submit.setImageDrawable(getResources().getDrawable(R.drawable.done_state));
                    submit.setEnabled(true);
                    mobileNumber = s.toString();
                }
            }

            public void afterTextChanged(Editable s) {
            }
        };

        phoneNumberEditText.addTextChangedListener(mTextEditorWatcher);

        skipEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean(prefSkippedString, true);
                editor.putBoolean(prefVerifiedString, true);
                editor.commit();
                skipped = true;
                onBackPressed();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isNetworkAvailable(NumberClass.this)) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(PreferencesStore.NUMBER_JUST_ADDED_CONSTANT, 1);
                    editor.putString(PreferencesStore.NUMBER_ADDED_CONSTANT, mobileNumber);
                    editor.commit();

                    MoEHelper.getInstance(NumberClass.this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_MOBILE, mobileNumber);

                    PayloadBuilder builder = new PayloadBuilder();
                    builder.putAttrString("PhoneNumber", mobileNumber);
                    builder.putAttrString("Activity Name", this.getClass().getCanonicalName());
                    MoEHelper.getInstance(NumberClass.this).trackEvent("Phone Number Verification", builder.build());

                    Intent intent = new Intent(mContext, NumberVerify.class);
                    NumberVerify.setContext(mContext);
                    intent.putExtra(NumberParameters.PREF_VERIFIED_CONSTANT, prefVerifiedString);
                    intent.putExtra(NumberParameters.SKIPPABLE_CONSTANT, skippable);
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                    NumberClass.this.finish();
                } else {
                    Toast.makeText(NumberClass.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                }
            }


        });

        mTrueBtn = (TrueButton) findViewById(R.id.com_truecaller_android_sdk_truebutton);

        boolean isTrueBtnUsable = mTrueBtn.isUsable();
        if (isTrueBtnUsable) {

            mTrueClient = new TrueClient(this, this);
            mTrueBtn.setTrueClient(mTrueClient);
        } else {

            mTrueBtn.setVisibility(View.GONE);
        }
        mHelper = new MoEHelper(this);
    }


    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (null != mTrueClient && mTrueClient.onActivityResult(requestCode, resultCode, data)) {

            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public static Context getContext() {
        return mContext;
    }

    public static void setContext(Context Context) {
        mContext = Context;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (skippable == false || skipped == false)
            ((Activity) mContext).finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }

    @Override
    public void onSuccesProfileShared(@NonNull TrueProfile trueProfile) {
        MoEHelper.getInstance(NumberClass.this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_MOBILE, trueProfile.phoneNumber);

        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("PhoneNumber", trueProfile.phoneNumber);
        builder.putAttrString("Activity Name", this.getClass().getCanonicalName());
        MoEHelper.getInstance(NumberClass.this).trackEvent("TrueCaller Phone Number Verification", builder.build());

        PreferencesStore.storeUserCredentials(this, trueProfile);

        sendUserNumberToServer(trueProfile);


    }

    private void sendUserNumberToServer(final TrueProfile trueProfile) {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.SendNumberToserverRequestModel sendNumberToserverRequestModel = requestModel.new SendNumberToserverRequestModel();
        sendNumberToserverRequestModel.user_code = Integer.toString(mUserCode);
        String contact = trueProfile.phoneNumber;
        if (contact.length() == 10) {
            mobileNumber = contact;
        } else if (contact.length() > 10) {
            mobileNumber = contact.substring(contact.length() - 10);
        } else {
            Toast.makeText(this, "Mobile Number can not be less than 10 characters", Toast.LENGTH_LONG).show();
            throw new IllegalArgumentException("Mobile Number is less than 10 characters!");
        }
        sendNumberToserverRequestModel.phone_number = mobileNumber;
        sendNumberToserverRequestModel.verification_source = "true_caller";


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.sendNumberToserverAPICall(sendNumberToserverRequestModel, new Callback<APIResonseModel.SendNumberToserverResponseModel>() {
            @Override
            public void success(APIResonseModel.SendNumberToserverResponseModel sendNumberToserverResponseModel, Response response) {
                int status = sendNumberToserverResponseModel.error;

                if (status == 1) {
                    Log.d(TAG, "API sendNumberToserverAPICall success");
                    prefs = NumberClass.this.getSharedPreferences(
                            "technologies.angular.lazylad", Context.MODE_PRIVATE);
                    ContentValues phoneNumberValues = new ContentValues();
                    phoneNumberValues.put(LazyContract.UserDetailsEntry.COLUMN_VERIFIED_FLAG, 1);
                    phoneNumberValues.put(LazyContract.UserDetailsEntry.COLUMN_VERIFIED_NUMBER, mobileNumber);

                    getContentResolver().update(LazyContract.UserDetailsEntry.CONTENT_URI, phoneNumberValues, LazyContract.UserDetailsEntry.COLUMN_USER_CODE + "=?", new String[]{String.valueOf(mUserCode)});

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(PreferencesStore.VARIFIED_FLAG_PREF_CONSTANT, 1);
                    editor.apply();

                    Bundle bundle = getIntent().getExtras();

                    progressDialog.dismiss();
                    Utils.verify_email(mContext, bundle, skippable, prefSkippedString, prefVerifiedString);
                    finish();

                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Toast.makeText(NumberClass.this, "Some server error occurred", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onFailureProfileShared(@NonNull TrueError trueError) {

        String msg = "";
        switch (trueError.getErrorType()) {
            case TRUE_ERROR_TYPE_INTERNAL:
                msg = "Truecaller internal error,not recoverable";
                break;
            case TRUE_ERROR_TYPE_NETWORK:
                msg = "No current active internet connection on the device";
                break;
            case TRUE_ERROR_TYPE_USER_DENIED:
                msg = "User denied sharing his/her profile details with the partner";
                break;
            case TRUE_ERROR_TYPE_UNAUTHORIZED_USER:
                msg = "User does not have a Truecaller account";
                break;
            case TRUE_ERROR_TYPE_UNAUTHORIZED_PARTNER:
                msg = "We cannot authenticate the partner(things that can be wrong: partnerKey, certificate or package)";
                break;
            default:
                break;
        }

        Toast.makeText(this, "Failed sharing - Reason: " + msg, Toast.LENGTH_LONG).show();


    }
}
