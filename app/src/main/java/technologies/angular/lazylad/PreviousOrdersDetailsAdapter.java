package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Saurabh on 30/01/15.
 */
public class PreviousOrdersDetailsAdapter extends BaseAdapter {

    private ArrayList<PreviousOrdersDetailsSnippet> m_prevOrderDetails;
    private PreviousOrdersUserDetailsSnippetNew m_prevOrderUserDetails;
    final private int TYPE_HEADER = 0;
    final private int TYPE_DETAIL = 1;
    private Activity m_activity;

    private class ViewHolder {
        TextView itemNameTextView;
        TextView itemQuantityTextView;
        TextView itemCostTextView;
        TextView itemDescTextView;
        ImageView imageView;
        TextView confirmTextview;
        ImageView confirmed_imageImageView;
        TextView user_name_tv;
        TextView user_address_tv;
        TextView user_phone_number_tv;
        TextView cod_amount_tv;
        TextView wallet_amount_tv;
        TextView discount_amount_tv;
        TextView paid_amount_tv;
    }

    public PreviousOrdersDetailsAdapter(Activity activity, ArrayList<PreviousOrdersDetailsSnippet> currentOrderDetails, PreviousOrdersUserDetailsSnippetNew prevOrderUserDetails) {
        m_activity = activity;
        m_prevOrderDetails = currentOrderDetails;
        m_prevOrderUserDetails = prevOrderUserDetails;
    }

    @Override
    public int getCount() {
        int count = 1;
        if (m_prevOrderDetails != null) {
            count = m_prevOrderDetails.size() + 1;
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (position != 0) {
            if (m_prevOrderDetails != null) {
                return m_prevOrderDetails.get(position - 1);
            }
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (position != 0) {
            if (m_prevOrderDetails != null) {
                return m_prevOrderDetails.get(position - 1).m_itemId;
            }
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);
        if (type == TYPE_HEADER) {
            if (m_prevOrderUserDetails != null) {

                final ViewHolder holder;
                String user_name = m_prevOrderUserDetails.m_user_name;
                String user_address = m_prevOrderUserDetails.m_user_address;
                String user_phone_number = m_prevOrderUserDetails.m_user_phone_number;
                Double wallet_paid = m_prevOrderUserDetails.m_wallet_amount;
                Double to_be_paid = m_prevOrderUserDetails.m_cod_amount;
                Double discount_amount = m_prevOrderUserDetails.m_discount_amount;
                String coupon_code = m_prevOrderUserDetails.m_coupon_code;
                Double paid=m_prevOrderUserDetails.m_paid;




                final int pos = position;
                if (convertView == null) {
                    holder = new ViewHolder();
                    LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = infalInflater.inflate(R.layout.previous_order_user_list_item_new, (ViewGroup) null);

                    holder.user_name_tv = (TextView) convertView.findViewById(R.id.user_name);
                    holder.user_address_tv = (TextView) convertView.findViewById(R.id.user_address_in_list);
                    holder.user_phone_number_tv = (TextView) convertView.findViewById(R.id.user_phone_number);
                    holder.cod_amount_tv = (TextView) convertView.findViewById(R.id.cod_amount);
                    holder.wallet_amount_tv = (TextView) convertView.findViewById(R.id.wallet_amount);
                    holder.discount_amount_tv=(TextView) convertView.findViewById(R.id.discount_amount);
                    holder.paid_amount_tv=(TextView) convertView.findViewById(R.id.paid_amount);



                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }

                holder.user_name_tv.setText(user_name);
                holder.user_address_tv.setText(user_address);
                holder.user_phone_number_tv.setText(user_phone_number);
                holder.wallet_amount_tv.setText(String.valueOf(wallet_paid));
                holder.cod_amount_tv.setText(String.valueOf(to_be_paid));
                holder.discount_amount_tv.setText(String.valueOf(discount_amount));
                holder.paid_amount_tv.setText(String.valueOf(paid));
            }

        } else if (type == TYPE_DETAIL) {
            if (m_prevOrderDetails != null) {
                final ViewHolder holder;

                String itemCode = ((PreviousOrdersDetailsSnippet) getItem(position)).m_itemCode;
                String itemName = ((PreviousOrdersDetailsSnippet) getItem(position)).m_itemName;
                String itemShortDesc = ((PreviousOrdersDetailsSnippet) getItem(position)).m_itemShortDesc;
                int itemQuantity = ((PreviousOrdersDetailsSnippet) getItem(position)).m_itemQuantity;
                double itemCost = ((PreviousOrdersDetailsSnippet) getItem(position)).m_itemCost;
                String itemDesc = ((PreviousOrdersDetailsSnippet) getItem(position)).m_itemDesc;
                String itemImageAdd = ((PreviousOrdersDetailsSnippet) getItem(position)).m_itemImgAdd;
                Boolean b_confirm = ((PreviousOrdersDetailsSnippet) getItem(position)).m_b_confirmed;

                final int pos = position;

                if (convertView == null) {
                    holder = new ViewHolder();
                    LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = infalInflater.inflate(R.layout.previous_order_detail_list_item_new, (ViewGroup) null);
                    holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name_textview);
                    holder.itemQuantityTextView = (TextView) convertView.findViewById(R.id.item_quantity_textview);
                    holder.itemCostTextView = (TextView) convertView.findViewById(R.id.item_cost_textview);
                    holder.itemDescTextView = (TextView) convertView.findViewById(R.id.item_desc_textview);
                    holder.imageView = (ImageView) convertView.findViewById(R.id.current_item_image);
                    holder.confirmTextview = (TextView) convertView.findViewById(R.id.confirm_textview);
                    holder.confirmed_imageImageView = (ImageView) convertView.findViewById(R.id.confirmed_image);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }

                holder.itemNameTextView.setText(itemName);
                holder.itemQuantityTextView.setText(Integer.toString(itemQuantity));
                holder.itemCostTextView.setText(Double.toString(itemCost));
                holder.itemDescTextView.setText(itemDesc);

                if (b_confirm == true) {
                    holder.confirmTextview.setText("Confirmed");
                    holder.confirmed_imageImageView.setImageDrawable(m_activity.getResources().getDrawable(R.drawable.done_state));
                } else {
                    holder.confirmTextview.setText("Not confirmed");
                    holder.confirmed_imageImageView.setImageDrawable(m_activity.getResources().getDrawable(R.drawable.inactive_state));
                }

                Picasso.with(parent.getContext())
                        .load(itemImageAdd)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.no_image)
                        .into(holder.imageView);
            }
        }
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;
        else
            return TYPE_DETAIL;
    }
}