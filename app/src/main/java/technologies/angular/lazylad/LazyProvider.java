package technologies.angular.lazylad;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import technologies.angular.lazylad.LazyContract.ItemDetailsEntry;
import technologies.angular.lazylad.LazyContract.SearchResultEntry;
import technologies.angular.lazylad.LazyContract.ShoppingCartEntry;

/**
 * Created by Amud on 14/09/15.
 */
public class LazyProvider extends ContentProvider {

    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private newDBHelper newDBHelper;

    private static final int USER_DETAILS = 100;
    private static final int SERVICE_TYPES = 101;
    private static final int USER_ADD_DETAILS = 102;
    private static final int ITEM_DETAILS = 103;
    private static final int SHOPPING_CART = 104;
    private static final int SERVICE_TYPE_CATEGORY = 105;
    private static final int SEARCH_RESULT = 106;
    private static final int SEARCH_QUERY = 107;
    private static final int ORDER_DETAILS = 108;
    private static final int RAW_QUERY = 109;
    private static final int ADDRESS_SEARCH = 110;
    private static final int COUPONS_CLICKS = 111;
    private static final int COUPONS_VIEWS = 112;

    private static final int ITEMS_CLICKS = 113;
    private static final int ITEMS_VIEWS = 114;

    private Cursor getSearchResultCursor(Uri uri, String searchSelection, String[] selectionArgs, String limit) {

        String[] projection = new String[]{SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry._ID + " AS " + SearchResultEntry._ID,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_CODE + " AS " + SearchResultEntry.COLUMN_ITEM_CODE,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_NAME + " AS " + SearchResultEntry.COLUMN_ITEM_NAME,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_IMG_FLAG + " AS " + SearchResultEntry.COLUMN_ITEM_IMG_FLAG,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_IMG_ADDRESS + " AS " + SearchResultEntry.COLUMN_ITEM_IMG_ADDRESS,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_UNIT + " AS " + SearchResultEntry.COLUMN_ITEM_UNIT,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_COST + " AS " + SearchResultEntry.COLUMN_ITEM_COST,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_SHORT_DESC + " AS " + SearchResultEntry.COLUMN_ITEM_SHORT_DESC,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_DESC + " AS " + SearchResultEntry.COLUMN_ITEM_DESC,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_STATUS + " AS  " + SearchResultEntry.COLUMN_ITEM_STATUS,
                "IFNULL(" + ShoppingCartEntry.TABLE_ALIAS_NAME + "." + ShoppingCartEntry.COLUMN_ITEM_SELECTED + ",0) as  " + ShoppingCartEntry.COLUMN_ITEM_SELECTED,
                "IFNULL(" + ShoppingCartEntry.TABLE_ALIAS_NAME + "." + ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED + ",0) AS " + ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_ST_CODE + " AS " + SearchResultEntry.COLUMN_ITEM_ST_CODE,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_SC_CODE + " AS " + SearchResultEntry.COLUMN_ITEM_SC_CODE,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_MRP + " AS " + SearchResultEntry.COLUMN_ITEM_MRP,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_OFFER_PRICE + " AS " + SearchResultEntry.COLUMN_ITEM_OFFER_PRICE,
                SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_IN_OFFER + " AS " + SearchResultEntry.COLUMN_ITEM_IN_OFFER};


        String joinTableFromUri = LazyContract.SearchResultEntry.getJoinTable(uri);
        String sp_code = uri.getQueryParameter(SearchResultEntry.WHERE_CURRENT_SPCODE);

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        if (joinTableFromUri.equals(String.valueOf(1))) {
            queryBuilder.setTables(
                    ItemDetailsEntry.TABLE_NAME + "  AS " + SearchResultEntry.TABLE_ALIAS_NAME + " " +
                            " LEFT OUTER JOIN " +
                            ShoppingCartEntry.TABLE_NAME + " AS " + ShoppingCartEntry.TABLE_ALIAS_NAME + " " +
                            " ON " +
                            " ( " +
                            " " + SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_CODE + " = " + ShoppingCartEntry.TABLE_ALIAS_NAME + "." + ShoppingCartEntry.COLUMN_ITEM_CODE + " " +
                            " and " + ShoppingCartEntry.TABLE_ALIAS_NAME + "." + ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = " + sp_code + " " +
                            " ) ");

            return queryBuilder.query(newDBHelper.getReadableDatabase(),
                    projection,
                    searchSelection,
                    selectionArgs,
                    null,
                    null,
                    null,
                    limit
            );
        } else if ((joinTableFromUri.equals(String.valueOf(2)))) {
            queryBuilder.setTables(
                    SearchResultEntry.TABLE_NAME + "  AS " + SearchResultEntry.TABLE_ALIAS_NAME + " " +
                            " LEFT OUTER JOIN " +
                            ShoppingCartEntry.TABLE_NAME + " AS " + ShoppingCartEntry.TABLE_ALIAS_NAME + " " +
                            " ON " +
                            " ( " +
                            " " + SearchResultEntry.TABLE_ALIAS_NAME + "." + SearchResultEntry.COLUMN_ITEM_CODE + " = " + ShoppingCartEntry.TABLE_ALIAS_NAME + "." + ShoppingCartEntry.COLUMN_ITEM_CODE + " " +
                            " and " + ShoppingCartEntry.TABLE_ALIAS_NAME + "." + ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = " + sp_code + " " +
                            " ) ");

            return queryBuilder.query(newDBHelper.getReadableDatabase(),
                    projection,
                    searchSelection,
                    selectionArgs,
                    null,
                    null,
                    null,
                    limit
            );
        } else
            return null;

    }

    private static UriMatcher buildUriMatcher() {
        // I know what you're thinking.  Why create a UriMatcher when you can use regular
        // expressions instead?  Because you're not crazy, that's why.

        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = LazyContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, LazyContract.PATH_RAW_QUERY, RAW_QUERY);
        matcher.addURI(authority, LazyContract.PATH_USER_DETAILS, USER_DETAILS);
        matcher.addURI(authority, LazyContract.PATH_SERVICE_TYPES, SERVICE_TYPES);
        matcher.addURI(authority, LazyContract.PATH_USER_ADD_DETAILS, USER_ADD_DETAILS);
        matcher.addURI(authority, LazyContract.PATH_ITEM_DETAILS, ITEM_DETAILS);
        matcher.addURI(authority, LazyContract.PATH_SHOPPING_CART, SHOPPING_CART);
        matcher.addURI(authority, LazyContract.PATH_SERVICE_TYPE_CATEGORY, SERVICE_TYPE_CATEGORY);
        matcher.addURI(authority, LazyContract.PATH_SEARCH_RESULT, SEARCH_RESULT);
        matcher.addURI(authority, LazyContract.PATH_SEARCH_RESULT + "/*", SEARCH_QUERY);
        matcher.addURI(authority, LazyContract.PATH_SEARCH_RESULT + "/#", SEARCH_QUERY);
        matcher.addURI(authority, LazyContract.PATH_ORDER_DETAILS, ORDER_DETAILS);
        matcher.addURI(authority, LazyContract.PATH_ADDRESS_SEARCH, ADDRESS_SEARCH);
        matcher.addURI(authority, LazyContract.PATH_COUPONS_CLICKS, COUPONS_CLICKS);
        matcher.addURI(authority, LazyContract.PATH_COUPONS_VIEWS, COUPONS_VIEWS);
        matcher.addURI(authority, LazyContract.PATH_ITEMS_CLICKS, ITEMS_CLICKS);
        matcher.addURI(authority, LazyContract.PATH_ITEMS_VIEWS, ITEMS_VIEWS);


        return matcher;
    }

    @Override
    public boolean onCreate() {

        newDBHelper = new newDBHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        int match = sUriMatcher.match(uri);
        switch (match) {

            case RAW_QUERY: {
                String sqlQuery = selection;
                retCursor = newDBHelper.getReadableDatabase().rawQuery(sqlQuery, selectionArgs);
                break;
            }
            case SEARCH_QUERY: {
                retCursor = getSearchResultCursor(uri, selection, selectionArgs, sortOrder);
                break;
            }
            case USER_DETAILS: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.UserDetailsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SERVICE_TYPES: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.ServiceTypesEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case USER_ADD_DETAILS: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.UserAddressDetailsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case ITEM_DETAILS: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.ItemDetailsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SHOPPING_CART: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.ShoppingCartEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SERVICE_TYPE_CATEGORY: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.ServiceTypeCategoriesEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SEARCH_RESULT: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.SearchResultEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case ORDER_DETAILS: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.OrderDetailsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case ADDRESS_SEARCH: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.AddressSearchEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case COUPONS_CLICKS: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.CouponsClicksDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case COUPONS_VIEWS: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.CouponsViewsDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case ITEMS_CLICKS: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.ItemsClicksDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case ITEMS_VIEWS: {
                retCursor = newDBHelper.getReadableDatabase().query(
                        LazyContract.ItemsViewsDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (RAW_QUERY != match)
            getContext().getContentResolver().notifyChange(uri, null);

        return retCursor;
    }

    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case USER_DETAILS:
                return LazyContract.UserDetailsEntry.CONTENT_TYPE;
            case SERVICE_TYPES:
                return LazyContract.ServiceTypesEntry.CONTENT_TYPE;
            case USER_ADD_DETAILS:
                return LazyContract.UserAddressDetailsEntry.CONTENT_TYPE;
            case ITEM_DETAILS:
                return LazyContract.ItemDetailsEntry.CONTENT_TYPE;
            case SHOPPING_CART:
                return LazyContract.ShoppingCartEntry.CONTENT_TYPE;
            case SERVICE_TYPE_CATEGORY:
                return LazyContract.ServiceTypeCategoriesEntry.CONTENT_TYPE;
            case SEARCH_RESULT:
                return LazyContract.SearchResultEntry.CONTENT_TYPE;
            case ORDER_DETAILS:
                return LazyContract.OrderDetailsEntry.CONTENT_TYPE;
            case ADDRESS_SEARCH:
                return LazyContract.AddressSearchEntry.CONTENT_TYPE;
            case COUPONS_VIEWS:
                return LazyContract.CouponsViewsDetails.CONTENT_TYPE;
            case COUPONS_CLICKS:
                return LazyContract.CouponsClicksDetails.CONTENT_TYPE;
            case ITEMS_CLICKS:
                return LazyContract.ItemsClicksDetails.CONTENT_TYPE;
            case ITEMS_VIEWS:
                return LazyContract.ItemsViewsDetails.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        Log.e("inserting", "working");
        final SQLiteDatabase db = newDBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case USER_DETAILS: {
                long _id = db.insert(LazyContract.UserDetailsEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.UserDetailsEntry.buildUserDetailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SERVICE_TYPES: {
                long _id = db.insert(LazyContract.ServiceTypesEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.ServiceTypesEntry.buildServiceTypesUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case USER_ADD_DETAILS: {
                Log.e("address", "address");
                long _id = db.insert(LazyContract.UserAddressDetailsEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.UserAddressDetailsEntry.buildUserAddressDetailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case ITEM_DETAILS: {
                long _id = db.insert(LazyContract.ItemDetailsEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.ItemDetailsEntry.buildItemDetailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SHOPPING_CART: {
                long _id = db.insert(LazyContract.ShoppingCartEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.ShoppingCartEntry.buildShoppingCartUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SERVICE_TYPE_CATEGORY: {
                long _id = db.insert(LazyContract.ServiceTypeCategoriesEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.ServiceTypeCategoriesEntry.buildServiceTypeCategoriesUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SEARCH_RESULT: {
                long _id = db.insert(LazyContract.SearchResultEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.SearchResultEntry.buildSearchResultUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case ORDER_DETAILS: {
                long _id = db.insert(LazyContract.OrderDetailsEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.OrderDetailsEntry.buildOrderDetailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case ADDRESS_SEARCH: {
                long _id = db.insert(LazyContract.AddressSearchEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.AddressSearchEntry.buildAddressSearchUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case COUPONS_CLICKS: {
                long _id = db.insert(LazyContract.CouponsClicksDetails.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.CouponsClicksDetails.buildCouponsClicksUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case COUPONS_VIEWS: {
                long _id = db.insert(LazyContract.CouponsViewsDetails.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.CouponsViewsDetails.buildCouponsViewsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case ITEMS_VIEWS: {
                long _id = db.insert(LazyContract.ItemsViewsDetails.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.ItemsViewsDetails.buildItemsViewsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }

            case ITEMS_CLICKS: {
                long _id = db.insert(LazyContract.ItemsClicksDetails.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyContract.ItemsClicksDetails.buildItemsClicksUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        final SQLiteDatabase db = newDBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        switch (match) {
            case USER_DETAILS:
                rowsDeleted = db.delete(
                        LazyContract.UserDetailsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SERVICE_TYPES:
                rowsDeleted = db.delete(
                        LazyContract.ServiceTypesEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case USER_ADD_DETAILS:
                rowsDeleted = db.delete(
                        LazyContract.UserAddressDetailsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case ITEM_DETAILS:
                rowsDeleted = db.delete(
                        LazyContract.ItemDetailsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SHOPPING_CART:
                rowsDeleted = db.delete(
                        LazyContract.ShoppingCartEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SERVICE_TYPE_CATEGORY:
                rowsDeleted = db.delete(
                        LazyContract.ServiceTypeCategoriesEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SEARCH_RESULT:
                rowsDeleted = db.delete(
                        LazyContract.SearchResultEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case ORDER_DETAILS:
                rowsDeleted = db.delete(
                        LazyContract.OrderDetailsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case ADDRESS_SEARCH:
                rowsDeleted = db.delete(
                        LazyContract.AddressSearchEntry.TABLE_NAME, selection, selectionArgs);
            case COUPONS_VIEWS:
                rowsDeleted = db.delete(
                        LazyContract.CouponsViewsDetails.TABLE_NAME, selection, selectionArgs);
            case COUPONS_CLICKS:
                rowsDeleted = db.delete(
                        LazyContract.CouponsClicksDetails.TABLE_NAME, selection, selectionArgs);
            case ITEMS_CLICKS:
                rowsDeleted = db.delete(
                        LazyContract.ItemsClicksDetails.TABLE_NAME, selection, selectionArgs);
            case ITEMS_VIEWS:
                rowsDeleted = db.delete(
                        LazyContract.ItemsViewsDetails.TABLE_NAME, selection, selectionArgs);


                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (selection == null || rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        final SQLiteDatabase db = newDBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case USER_DETAILS:
                rowsUpdated = db.update(LazyContract.UserDetailsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case SERVICE_TYPES:

                rowsUpdated = db.update(LazyContract.ServiceTypesEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case USER_ADD_DETAILS:

                rowsUpdated = db.update(LazyContract.UserAddressDetailsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case ITEM_DETAILS:

                rowsUpdated = db.update(LazyContract.ItemDetailsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case SHOPPING_CART:

                rowsUpdated = db.update(LazyContract.ShoppingCartEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case SERVICE_TYPE_CATEGORY:

                rowsUpdated = db.update(LazyContract.ServiceTypeCategoriesEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case SEARCH_RESULT:

                rowsUpdated = db.update(LazyContract.SearchResultEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case ORDER_DETAILS:

                rowsUpdated = db.update(LazyContract.OrderDetailsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case ADDRESS_SEARCH:

                rowsUpdated = db.update(LazyContract.AddressSearchEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case COUPONS_CLICKS:

                rowsUpdated = db.update(LazyContract.CouponsClicksDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case COUPONS_VIEWS:
                rowsUpdated = db.update(LazyContract.CouponsViewsDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case ITEMS_VIEWS:
                rowsUpdated = db.update(LazyContract.ItemsViewsDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case ITEMS_CLICKS:
                rowsUpdated = db.update(LazyContract.ItemsClicksDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = newDBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int returnCount = 0;
        switch (match) {
            case ITEM_DETAILS:
                db.beginTransaction();

                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(LazyContract.ItemDetailsEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;

            case SEARCH_RESULT:
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(LazyContract.SearchResultEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;

            case SERVICE_TYPE_CATEGORY:
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(LazyContract.ServiceTypeCategoriesEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;

            case COUPONS_VIEWS:
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(LazyContract.CouponsViewsDetails.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;

            case COUPONS_CLICKS:
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(LazyContract.CouponsClicksDetails.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }
}
