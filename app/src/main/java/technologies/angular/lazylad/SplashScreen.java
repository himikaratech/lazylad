package technologies.angular.lazylad;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.utils.MoEHelperConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.backgroundsync.SyncAdapter;
import technologies.angular.lazylad.common.Constants;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.UserDetailsEntry;
import technologies.angular.lazylad.LazyContract.ServiceTypesEntry;


public class SplashScreen extends Activity {

    public static final String ACCOUNT = "LazyLad";
    public static final String ACCOUNT_TYPE = "AngularTechnologiesLazyLad.com";

    protected static final String TAG = "banner-activity";

    private int m_userCode;
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    private static String SENDER_ID = "631661601999";
    private GoogleCloudMessaging gcm;

    private ArrayList<ServiceTypeSnippet> m_servTypeListDetailsFromServer;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash_screen);

        FacebookSdk.sdkInitialize(getApplicationContext());

        SyncAdapter.syncImmediately(this);

        mHelper = new MoEHelper(this);


        final SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        if ((prefs.getBoolean("moengageFirst", false))) {
            mHelper.setExistingUser(true);
        } else {
            mHelper.setExistingUser(false);
            prefs.edit().putBoolean("moengageFirst", true).commit();
        }

        Cursor m_cursor = getContentResolver().query(
                UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_userCode = m_cursor.getInt(1);
                    Log.d("user_code", String.valueOf(m_userCode));
                    m_cursor.moveToNext();
                }
            } else {
                m_userCode = 0;
                //TODO Some UI Element asking to add address
            }
        } else {
            m_userCode = 0;
            //TODO Error Handling
        }

        if (m_userCode != 0)
            MoEHelper.getInstance(this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_UNIQUE_ID, m_userCode);


        Context appContext = getApplicationContext();
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            String regid = getRegistrationId(appContext);
            if (regid.isEmpty() || doesStoredVersionChanged(appContext)) {
                registerInBackground();
            }
        }

        if (doesStoredVersionChanged(appContext)) {
            updateStoredAppVersion(appContext);
        }

        updateServiceType();

        getConfig();

        int current_version = prefs.getInt(PreferencesStore.CURRENT_APP_VERSION_CONSTANT, 0);
        if (current_version > getAppVersion(getApplicationContext()))
            recommmedAppUpdate();

        setTracker();
        startMainActivity();
    }

    private void recommmedAppUpdate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("There is newer version of this application available, click OK to upgrade now?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    //if the user agrees to upgrade
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=technologies.angular.lazylad")));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=technologies.angular.lazylad")));
                        }
                        SplashScreen.this.finish();

                    }
                })
                .setNegativeButton("Remind Later", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        //show the alert message
        builder.create().show();
    }

    private void updateServiceType() {

        SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        String cityCodeSelected = prefs.getString(PreferencesStore.CITY_CODE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_CODE_SELECTED_CONSTANT);
        final Double latitude = Double.longBitsToDouble(prefs.getLong(PreferencesStore.LATITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LATITUDE_SELECTED_CONSTANT));
        final Double longitude = Double.longBitsToDouble(prefs.getLong(PreferencesStore.LONGITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LONGITUDE_SELECTED_CONSTANT));

        if ((cityCodeSelected.equals(PreferencesStore.DEFAULT_CITY_CODE_SELECTED_CONSTANT)))
            return;


        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.ProvidersRequestModel getCityCodeRequestModel = requestModel.new ProvidersRequestModel();
        getCityCodeRequestModel.user_code = String.valueOf(m_userCode);
        getCityCodeRequestModel.city_code = cityCodeSelected;
        getCityCodeRequestModel.lat = String.valueOf(latitude);
        getCityCodeRequestModel.longi = String.valueOf(longitude);

        RestAdapter restAdapter = Utils.providesRestAdapter(Constants.WEB_SERVICE_URL);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.updateServiceTypeAPICall(getCityCodeRequestModel, new retrofit.Callback<APIResonseModel.ProvidersResponseModel>() {

            @Override
            public void success(APIResonseModel.ProvidersResponseModel providersResponseModel, retrofit.client.Response response) {
                Log.d("splashScreen", "ServiceType");

                int size = providersResponseModel.service_types.size();

                m_servTypeListDetailsFromServer = new ArrayList<ServiceTypeSnippet>(size);
                for (int j = 0; j < size; j++) {
                    int id = providersResponseModel.service_types.get(j).id;
                    int st_code = providersResponseModel.service_types.get(j).st_code;
                    String st_name = providersResponseModel.service_types.get(j).st_name;
                    int img_flag = providersResponseModel.service_types.get(j).st_img_flag;
                    String img_add = providersResponseModel.service_types.get(j).st_img_add;
                    int servTypeFlag = providersResponseModel.service_types.get(j).seller_automated;
                    int isService = providersResponseModel.service_types.get(j).is_service;

                    ServiceTypeSnippet srvCatListObj = new ServiceTypeSnippet(id, st_code, st_name, img_flag, img_add, servTypeFlag, isService);
                    m_servTypeListDetailsFromServer.add(srvCatListObj);
                }

                getContentResolver().delete(
                        LazyContract.ServiceTypesEntry.CONTENT_URI,
                        null,
                        null
                );

                for (int i = 0; i < m_servTypeListDetailsFromServer.size(); i++) {
                    int id = m_servTypeListDetailsFromServer.get(i).getm_id();
                    int m_servTypeCode = m_servTypeListDetailsFromServer.get(i).getm_servTypeCode();
                    String m_servTypeName = m_servTypeListDetailsFromServer.get(i).getm_servTypeName();
                    int m_servTypeImgFlag = m_servTypeListDetailsFromServer.get(i).getm_servTypeImgFlag();
                    String m_servTypeImgAdd = m_servTypeListDetailsFromServer.get(i).getm_servTypeImgAdd();
                    int m_servTypeFlag = m_servTypeListDetailsFromServer.get(i).getm_servTypeFlag();
                    int isService = m_servTypeListDetailsFromServer.get(i).getM_isService();

                    ContentValues serviceTypeValues = new ContentValues();
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry._ID, id);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_CODE, m_servTypeCode);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_NAME, m_servTypeName);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_IMG_FLAG, m_servTypeImgFlag);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_IMG_ADD, m_servTypeImgAdd);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_FLAG, m_servTypeFlag);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_IS_SERVICE, isService);
                    Uri insertUri = getContentResolver().insert(LazyContract.ServiceTypesEntry.CONTENT_URI, serviceTypeValues);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(SplashScreen.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                Log.e(TAG, error.getMessage());
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        Log.i("tracker", "set");        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.SplashScreen");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }


    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }

        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainClass.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private int getStoredAppVersion(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        return registeredVersion;
    }

    private boolean doesStoredVersionChanged(Context context) {

        if (getStoredAppVersion(context) != getAppVersion(context))
            return true;
        else
            return false;
    }

    private void updateStoredAppVersion(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putInt(PreferencesStore.SHALL_VERSION_ALLOW_CONSTANT, PreferencesStore.DEFAULT_SHALL_VERSION_ALLOW_CONSTANT);
        editor.commit();
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private void registerInBackground() {
        new AsyncTask() {
            String registrationid;

            @Override
            protected Object doInBackground(Object... params) {
                String msg = "";
                try {
                    Context appContext = getApplicationContext();
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(appContext);
                    }
                    registrationid = gcm.register(SENDER_ID);
                    //Log.i("Resgitration Id Obtained", regid);
                    msg = "Device registered, registration ID=" + registrationid;

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(appContext, registrationid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(Object msg) {
                signInUser(m_userCode, registrationid);
                //new SignInUser().execute();
            }
        }.execute(null, null, null);

    }


    private void getConfig() {

        final SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        String cityCodeSelected = prefs.getString(PreferencesStore.CITY_CODE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_CODE_SELECTED_CONSTANT);
        String areaCodeSelected = prefs.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, "Not_Available");

        int currentVersion = getAppVersion(getApplicationContext());

        WifiManager wimanager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        String macAddress = wimanager.getConnectionInfo().getMacAddress();

        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String emeinummber = mngr.getDeviceId();

        int osversion = Build.VERSION.SDK_INT;
        String modelName = Build.MODEL;
        String manufacturerName = Build.MANUFACTURER;


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.ConfigRequestModel configRequestModel = requestModel.new ConfigRequestModel();
        configRequestModel.area_code_selected = areaCodeSelected;
        configRequestModel.city_code_selected = cityCodeSelected;
        configRequestModel.imei_number = emeinummber;
        configRequestModel.lazylad_version_code = currentVersion;
        configRequestModel.mac_address = macAddress;
        configRequestModel.model_name = modelName;
        configRequestModel.os_type = osversion;
        configRequestModel.phone_manufacturer = manufacturerName;
        configRequestModel.user_code = m_userCode;

        apiSuggestionsService.configDetailsAPICall(configRequestModel, new Callback<APIResonseModel.ConfigResponseModel>() {

            @Override
            public void success(APIResonseModel.ConfigResponseModel configResponseModel, retrofit.client.Response response) {


                int success = configResponseModel.error;
                Log.i("success on splashScreen", configResponseModel.lazylad_contact + "");
                if (success == 1) {

                    int shallNumberVerfiy = configResponseModel.number_verification.shall_number_verify;


                    String callUsNumber = configResponseModel.lazylad_contact.call_us_number;
                    Log.e("callUsNumber", callUsNumber);
                    int shall_version_allow = configResponseModel.version_allow;
                    int current_version = configResponseModel.current_version;

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(PreferencesStore.SHALL_NUMBER_VERIFY_CONSTANT, shallNumberVerfiy);
                    editor.putInt(PreferencesStore.CURRENT_APP_VERSION_CONSTANT, current_version);
                    editor.putString(PreferencesStore.CALL_US_PHONE_NUMBER_CONSTANT, callUsNumber);
                    editor.putInt(PreferencesStore.SHALL_VERSION_ALLOW_CONSTANT, shall_version_allow);
                    editor.commit();

                }

            }


            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
            }
        });
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.commit();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //finish();
                //GooglePlayServicesUtil.getErrorDialog(resultCode, this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void startMainActivity() {

        final SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        prefs.edit().putBoolean(PreferencesStore.APP_START_FLAG_CONSTANT, true).commit();
        final String selected_city_name = prefs.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT);

        new Handler().postDelayed((Runnable) (new Runnable() {

            public void run() {

                if (prefs.getBoolean("BannerLaunch_SplashScreen", true)) {
                    prefs.edit().putBoolean("BannerLaunch_SplashScreen", false).commit();
                    startActivity(new Intent(getApplicationContext(), (GeoLocation.class)));
                    SplashScreen.this.finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), (MainClass.class)));
                    SplashScreen.this.finish();
                }
            }
        }), 3000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }


    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void signInUser(int userCode, String regId) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.NewUserRequestModel newUserRequestModel = requestModel.new NewUserRequestModel();
        newUserRequestModel.user_code = Integer.toString(userCode);
        newUserRequestModel.reg_id = regId;

        apiSuggestionsService.signInUserAPICall(newUserRequestModel, new Callback<APIResonseModel.SignInUserResponseModel>() {

            @Override
            public void success(APIResonseModel.SignInUserResponseModel signInUserResponseModel, retrofit.client.Response response) {
                int success = signInUserResponseModel.error;
                if (success == 1) {
                    if (!(signInUserResponseModel.user_details == null) && !signInUserResponseModel.user_details.isEmpty()) {
                        int num = signInUserResponseModel.user_details.size();
                        for (int i = 0; i < num; i++) {
                            int user_code = signInUserResponseModel.user_details.get(i).user_code;
                            if (m_userCode == 0) {
                                m_userCode = user_code;
                                ContentValues values = new ContentValues();
                                values.put(UserDetailsEntry._ID, 1);
                                values.put(UserDetailsEntry.COLUMN_USER_CODE, user_code);
                                values.put(UserDetailsEntry.COLUMN_VERIFIED_FLAG, 0);
                                values.put(UserDetailsEntry.COLUMN_VERIFIED_NUMBER, "");
                                values.put(UserDetailsEntry.COLUMN_EMAIL_FLAG, 0);
                                values.put(UserDetailsEntry.COLUMN_EMAIL_ID, "");

                                Uri insertUri = getContentResolver().insert(UserDetailsEntry.CONTENT_URI, values);
                            }
                        }
                    }

                    getConfig();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
            }
        });
    }
}
