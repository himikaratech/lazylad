package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class NotificationCenterAdapter extends BaseAdapter {

    Activity m_activity;
    ArrayList<NotificationCenterSnippet> offersList;

    class ViewHolder {
        TextView offernumberTextView;
        TextView noti_textTextView;
        TextView noti_timestampTextView;
        TextView noti_endtimeTextView;
        ImageView noti_imageImageView;
        LinearLayout noti_headLinearLayout;
    }

    public NotificationCenterAdapter(Activity activity, ArrayList<NotificationCenterSnippet> offersList) {
        this.m_activity = activity;
        this.offersList = offersList;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (offersList != null) {
            count = offersList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (offersList != null) {
            return offersList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (offersList != null) {
            return offersList.get(position).m_offerCode;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (offersList == null)
            return null;
        else {
            String offerDescription = ((NotificationCenterSnippet) getItem(position)).m_offerDesc;
            Long offerLaunchTime = ((NotificationCenterSnippet) getItem(position)).m_launchTime;
            Long offerValiditiy = ((NotificationCenterSnippet) getItem(position)).m_offerValidity;
            int offerImageFlag = ((NotificationCenterSnippet) getItem(position)).m_offerImgFlag;
            String offerImageAdd = ((NotificationCenterSnippet) getItem(position)).m_offerImgAdd;

            int offerPosition = position + 1;

            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.notification_center_list, (ViewGroup) null);
                holder.offernumberTextView = (TextView) convertView.findViewById(R.id.offer_number);
                holder.noti_textTextView = (TextView) convertView.findViewById(R.id.notification_text);
                holder.noti_timestampTextView = (TextView) convertView.findViewById(R.id.notification_timestamp);
                holder.noti_endtimeTextView = (TextView) convertView.findViewById(R.id.notification_endtime);
                holder.noti_imageImageView = (ImageView) convertView.findViewById(R.id.image_notification);
                holder.noti_headLinearLayout = (LinearLayout) convertView.findViewById(R.id.head_notification);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
                holder.noti_endtimeTextView.setTextColor(Color.BLACK);
            }

            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            holder.noti_headLinearLayout.setBackgroundColor(color);

            holder.offernumberTextView.setText(Integer.toString(offerPosition));
            holder.noti_textTextView.setText(offerDescription);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String offerLaunchTimeStr = sdf.format(new Date(offerLaunchTime * 1000L));
            // no of seconds since epoch returned so converted to millis

            holder.noti_timestampTextView.setText(offerLaunchTimeStr);

            String remainingTimeStr;
            if (offerValiditiy == 0) {
                remainingTimeStr = "Offer Closed";
                holder.noti_endtimeTextView.setTextColor(Color.RED);
            } else {
                remainingTimeStr = "";
                Long minutes, hours, days, months;
                offerValiditiy = offerValiditiy / 60L; // seconds to minute validity
                minutes = offerValiditiy % 60L;
                hours = (offerValiditiy / 60) % 24;
                days = ((offerValiditiy / 60) / 24) % 30;
                months = ((offerValiditiy / 60) / 24) / 30;

                if (months != 0) remainingTimeStr += months + " Month";
                if (months != 0 && days != 0) remainingTimeStr += ", ";
                if (days != 0) remainingTimeStr += days + " days";
                if (months == 0) {
                    if (days != 0 && hours != 0) remainingTimeStr += ", ";
                    if (hours != 0) remainingTimeStr += hours + " hours";
                    if (days == 0) {
                        if (hours != 0 && minutes != 0) remainingTimeStr += ", ";
                        if (minutes != 0) remainingTimeStr += minutes + " minutes" + " left";
                    }
                }
            }

            holder.noti_endtimeTextView.setText(remainingTimeStr);
            WindowManager wm = (WindowManager) parent.getContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;

            if (offerImageFlag == 1) {
                if (offerImageAdd == null || offerImageAdd == "") offerImageAdd = "\"\"";
                Picasso.with(parent.getContext())
                        .load(offerImageAdd)
                        .placeholder(R.drawable.loading)
                        .resize( width,0)
                        .error(R.drawable.no_image)
                        .into(holder.noti_imageImageView);

                holder.noti_imageImageView.setVisibility(View.VISIBLE);
                holder.noti_headLinearLayout.setVisibility(View.GONE);
            } else {
                holder.noti_imageImageView.setVisibility(View.GONE);
                holder.noti_headLinearLayout.setVisibility(View.VISIBLE);
            }

            return convertView;
        }
    }
}
