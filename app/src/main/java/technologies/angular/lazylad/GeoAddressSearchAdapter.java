package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Amud on 23/11/15.
 */

    public class GeoAddressSearchAdapter extends RecyclerView.Adapter<GeoAddressSearchAdapter.ViewHolder> {

        private ArrayList<GeoAddress> geoAddressArrayList;
        private final Activity m_activity;
    private int itemLayout;
    private int  viewFlag;
    private String m_old_cityCodeSelected;
    private Double m_old_latSelected;
    private Double m_old_longSelected;



    public static final String TAG = "MyTag";
        public GeoAddressSearchAdapter(ArrayList<GeoAddress> items, int itemLayout, Context context, String oldCityCode, Double oldLat, Double oldLong) {
            m_activity = (Activity) context;
            this.geoAddressArrayList = items;
            this.itemLayout = itemLayout;
            this.m_old_cityCodeSelected=oldCityCode;
            m_old_latSelected = oldLat;
            m_old_longSelected = oldLong;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
            return new ViewHolder(v, viewFlag);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

                if (geoAddressArrayList != null) {
                    final String formatted_address = ((GeoAddress) getItem(position)).m_formattedAddress;
                    final Double latitude =((GeoAddress) getItem(position)).m_latitude;
                    final Double longitude =((GeoAddress) getItem(position)).m_longitude;
                    final String city_code =((GeoAddress) getItem(position)).m_cityCode;
                    final String city_name =((GeoAddress) getItem(position)).m_cityName;
                    final String area_name =((GeoAddress) getItem(position)).m_areaName;
                    final String city_image_address =((GeoAddress) getItem(position)).m_cityImageAddress;
                    final int coupon_in_city =((GeoAddress) getItem(position)).m_couponInCity;


                    Log.d("formatted_address",formatted_address+latitude+longitude+city_code+city_name+area_name);


                    holder.m_formattedAddressTV.setText(formatted_address);
                    holder.m_mainLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((GeoLocation)m_activity).saveParams(city_code,latitude,longitude,area_name,city_name,m_old_cityCodeSelected, m_old_latSelected, m_old_longSelected,city_image_address,coupon_in_city);
                        }

                    });
                }


        }

        private void startMainActivity() {
            Intent intent = new Intent(m_activity, MainClass.class);
            m_activity.startActivity(intent);
            m_activity.finish();
        }

        @Override
        public int getItemCount() {
            return geoAddressArrayList.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {
            TextView m_formattedAddressTV;
            View m_mainLayout;

            public ViewHolder(View itemView, int flag) {
                super(itemView);
                    m_formattedAddressTV = (TextView) itemView.findViewById(R.id.search_address_text);
                    m_mainLayout=itemView.findViewById(R.id.main_layout);
            }
        }

        public Object getItem(int position) {
            if (geoAddressArrayList != null) {
                return geoAddressArrayList.get(position);
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }



}
