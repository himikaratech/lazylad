package technologies.angular.lazylad.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import technologies.angular.lazylad.adaptors.DeliverySlotListAdaptor;
import technologies.angular.lazylad.DeliverySlot;
import technologies.angular.lazylad.R;
import technologies.angular.lazylad.common.Constants;


public class DeliverySlotFragment extends DialogFragment implements DeliverySlotListAdaptor.InSlotListCallBack {
    private static final String SHOP_OPEN_TIME = "SHOP_OPEN_TIME";
    private static final String SHOP_CLOSE_TIME = "SHOP_CLOSE_TIME";
    private static final String TIME_INTERVAL = "TIME_INTERVAL";
    private static final String SERVICE_CODE = "SERVICE_CODE";
    private static final String SERVICE_PICKUP_TIME = "SERVICE_PICKUP_TIME";
    private static final String SERVICE_PROCESSING_TIME = "SERVICE_PROCESSING_TIME";

    private long mShopOpenTime;
    private long mShopCloseTime;
    private long mTimeInterval;
    private int mServiceCode;
    private long mServicePickupTime;
    private long mServiceProcessingTime;

    private RecyclerView mRecyclerView;
    private Button mSetBtn;
    private Button mCancelBtn;
    private RadioButton mTodayRadioBtn;
    private RadioButton mTomorrowRadioBtn;
    private TextView mNoSlotAvailableTextView;

    private DeliverySlotListAdaptor mDeliveryAdaptor = null;

    private InDeliverySlotCallback mCallback;

    private int mDayFlag;
    private DeliverySlot mDeliverySlot = null;

    private static final int TODAY_SLOT = 0;
    private static final int TOMORROW_SLOT = 1;

    private ArrayList<String> mCurrentSlotList = null;
    private String mUserSelectedTimeSlot = "";

    private Calendar mDeliveryTodayCalendar;
    private Calendar mDeliveryTomoCalendar;
    private long mDeliveryCurrentTime;

    public DeliverySlotFragment() {
        // Required empty public constructor
    }

    public static DeliverySlotFragment newInstance(long shopOpenTime, long shopCloseTime, long timeInterval, int serviceCode, long pickupTime, long processingTime) {
        DeliverySlotFragment fragment = new DeliverySlotFragment();
        Bundle args = new Bundle();
        args.putLong(SHOP_OPEN_TIME, shopOpenTime);
        args.putLong(SHOP_CLOSE_TIME, shopCloseTime);
        args.putLong(TIME_INTERVAL, timeInterval);
        args.putInt(SERVICE_CODE, serviceCode);
        args.putLong(SERVICE_PICKUP_TIME, pickupTime);
        args.putLong(SERVICE_PROCESSING_TIME, processingTime);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mShopOpenTime = getArguments().getLong(SHOP_OPEN_TIME);
            mShopCloseTime = getArguments().getLong(SHOP_CLOSE_TIME);
            mTimeInterval = getArguments().getLong(TIME_INTERVAL);
            mServiceCode = getArguments().getInt(SERVICE_CODE);
            mServicePickupTime = getArguments().getLong(SERVICE_PICKUP_TIME);
            mServiceProcessingTime = getArguments().getLong(SERVICE_PROCESSING_TIME);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.fragment_delivery_slot);

        mSetBtn = (Button) dialog.findViewById(R.id.delivery_slot_set_btn);
        mCancelBtn = (Button) dialog.findViewById(R.id.delivery_slot_cancel_btn);
        mTodayRadioBtn = (RadioButton) dialog.findViewById(R.id.today_radio_btn);
        mTomorrowRadioBtn = (RadioButton) dialog.findViewById(R.id.tomorrow_radio_btn);
        mRecyclerView = (RecyclerView) dialog.findViewById(R.id.delivery_slot_recycler_view);
        mNoSlotAvailableTextView = (TextView) dialog.findViewById(R.id.no_slot_available_text_view);

        if (mServiceCode == Constants.LAUNDRY_DELIVERY_SLOT_CODE) {
            mDeliveryTodayCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            mDeliveryTodayCalendar.setTimeInMillis(mServicePickupTime);
            mDeliveryTodayCalendar.add(Calendar.DAY_OF_MONTH, (int) mServiceProcessingTime);
            mDeliveryCurrentTime = mDeliveryTodayCalendar.getTimeInMillis();
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM dd", Locale.US);
            mTodayRadioBtn.setText(dateFormat.format(mDeliveryTodayCalendar.getTime()));

            mDeliveryTodayCalendar.set(Calendar.HOUR_OF_DAY, 9);
            mDeliveryTodayCalendar.set(Calendar.MINUTE, 0);
            mDeliveryTodayCalendar.set(Calendar.SECOND, 0);
            mDeliveryTodayCalendar.set(Calendar.MILLISECOND, 0);
            mShopOpenTime = mDeliveryTodayCalendar.getTimeInMillis();

            mDeliveryTodayCalendar.set(Calendar.HOUR_OF_DAY, 22);
            mDeliveryTodayCalendar.set(Calendar.MINUTE, 0);
            mDeliveryTodayCalendar.set(Calendar.SECOND, 0);
            mDeliveryTodayCalendar.set(Calendar.MILLISECOND, 0);
            mShopCloseTime = mDeliveryTodayCalendar.getTimeInMillis();

            mDeliveryTomoCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            mDeliveryTomoCalendar.setTimeInMillis(mServicePickupTime);
            mDeliveryTomoCalendar.add(Calendar.DAY_OF_MONTH, (int) mServiceProcessingTime + 1);
            mTomorrowRadioBtn.setText(dateFormat.format(mDeliveryTomoCalendar.getTime()));



        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);

        mDeliverySlot = new DeliverySlot(mShopOpenTime, mShopCloseTime, mTimeInterval, mServiceProcessingTime);

        // Initially
        if(mServiceCode == Constants.LAUNDRY_DELIVERY_SLOT_CODE){
            mDeliveryAdaptor = new DeliverySlotListAdaptor(mDeliverySlot.getTimeSlotsForToday(mDeliveryCurrentTime), getActivity(), this);
        }else {
            mDeliveryAdaptor = new DeliverySlotListAdaptor(mDeliverySlot.getTimeSlotsForToday(System.currentTimeMillis()), getActivity(), this);
        }

        mRecyclerView.setAdapter(mDeliveryAdaptor);

        setTodaySlot();

        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mSetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = "";
                if (mDayFlag == TODAY_SLOT) {
                    date = mDeliverySlot.getTodayDate(mServiceCode);
                } else if (mDayFlag == TOMORROW_SLOT) {
                    date = mDeliverySlot.getTomorrowDate(mServiceCode);
                }
                if (mUserSelectedTimeSlot.isEmpty()) {
                    mCallback.onSetClicked("", mServiceCode);
                } else {
                    mCallback.onSetClicked(date + " " + mUserSelectedTimeSlot, mServiceCode);
                }
                dialog.dismiss();
            }
        });

        mTodayRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTodaySlot();
            }
        });

        mTomorrowRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTomorrowSlot();
            }
        });
        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            if (activity instanceof InDeliverySlotCallback) {
                mCallback = (InDeliverySlotCallback) activity;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLaundryFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    private void setTodaySlot() {
        mTodayRadioBtn.setChecked(true);
        mTomorrowRadioBtn.setChecked(false);
        mDayFlag = TODAY_SLOT;
        if(mServiceCode == Constants.LAUNDRY_DELIVERY_SLOT_CODE) {
            mDeliveryTodayCalendar.set(Calendar.HOUR_OF_DAY, 9);
            mDeliveryTodayCalendar.set(Calendar.MINUTE, 0);
            mDeliveryTodayCalendar.set(Calendar.SECOND, 0);
            mDeliveryTodayCalendar.set(Calendar.MILLISECOND, 0);
            mShopOpenTime = mDeliveryTodayCalendar.getTimeInMillis();

            mDeliveryTodayCalendar.set(Calendar.HOUR_OF_DAY, 22);
            mDeliveryTodayCalendar.set(Calendar.MINUTE, 0);
            mDeliveryTodayCalendar.set(Calendar.SECOND, 0);
            mDeliveryTodayCalendar.set(Calendar.MILLISECOND, 0);
            mShopCloseTime = mDeliveryTodayCalendar.getTimeInMillis();

            mCurrentSlotList = mDeliverySlot.getTimeSlotsForToday(mDeliveryCurrentTime);
        }else{
            mCurrentSlotList = mDeliverySlot.getTimeSlotsForToday(System.currentTimeMillis());
        }
        if (mCurrentSlotList != null && !mCurrentSlotList.isEmpty()) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mNoSlotAvailableTextView.setVisibility(View.GONE);
            mDeliveryAdaptor.updateSlotList(mCurrentSlotList);
            mDeliveryAdaptor.notifyDataSetChanged();
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mNoSlotAvailableTextView.setVisibility(View.VISIBLE);
        }

    }

    private void setTomorrowSlot() {
        mTodayRadioBtn.setChecked(false);
        mTomorrowRadioBtn.setChecked(true);
        mDayFlag = TOMORROW_SLOT;
        mRecyclerView.setVisibility(View.VISIBLE);
        mNoSlotAvailableTextView.setVisibility(View.GONE);
        if(mServiceCode == Constants.LAUNDRY_DELIVERY_SLOT_CODE){
            mDeliveryTomoCalendar.set(Calendar.HOUR_OF_DAY, 9);
            mDeliveryTomoCalendar.set(Calendar.MINUTE, 0);
            mDeliveryTomoCalendar.set(Calendar.SECOND, 0);
            mDeliveryTomoCalendar.set(Calendar.MILLISECOND, 0);
            mShopOpenTime = mDeliveryTomoCalendar.getTimeInMillis();
            mDeliveryTomoCalendar.set(Calendar.HOUR_OF_DAY, 22);
            mDeliveryTomoCalendar.set(Calendar.MINUTE, 0);
            mDeliveryTomoCalendar.set(Calendar.SECOND, 0);
            mDeliveryTomoCalendar.set(Calendar.MILLISECOND, 0);
            mShopCloseTime = mDeliveryTomoCalendar.getTimeInMillis();
        }
        mCurrentSlotList = mDeliverySlot.getTimeSlotsForTomorrow();
        mDeliveryAdaptor.updateSlotList(mCurrentSlotList);
        mDeliveryAdaptor.notifyDataSetChanged();

    }

    @Override
    public void onClicked(int position) {
        if (mCurrentSlotList != null && !mCurrentSlotList.isEmpty()) {
            mUserSelectedTimeSlot = mCurrentSlotList.get(position);
        }
    }

    public interface InDeliverySlotCallback {
        void onSetClicked(String timeSlot, int serviceCode);
    }
}
