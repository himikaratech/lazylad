package technologies.angular.lazylad.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import technologies.angular.lazylad.R;
import technologies.angular.lazylad.Snippets.LaundryCategoryItemSnippet;
import technologies.angular.lazylad.adaptors.LaundryItemListAdaptor;

public class LaundryCategoryFragment extends Fragment  implements LaundryItemListAdaptor.OnItemBtnClickedListener{
    private static final String LAUNDRY_CATEGORY_LIST = "CATEGORY_LIST";

    private ArrayList<LaundryCategoryItemSnippet> mCategoryItemList;

    private OnLaundryFragmentInteractionListener mListener;

    @Bind(R.id.laundry_category_recycler_view)
    RecyclerView mLaundryCategoryRecyclerView;

    private LaundryItemListAdaptor mLaundryItemListAdaptor;

    public LaundryCategoryFragment() {
        // Required empty public constructor
    }


    public static LaundryCategoryFragment newInstance(ArrayList<LaundryCategoryItemSnippet> categoryList) {
        LaundryCategoryFragment fragment = new LaundryCategoryFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(LAUNDRY_CATEGORY_LIST, categoryList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategoryItemList = getArguments().getParcelableArrayList(LAUNDRY_CATEGORY_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_laundry_category, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mLaundryCategoryRecyclerView.setLayoutManager(layoutManager);
        mLaundryItemListAdaptor = new LaundryItemListAdaptor(getActivity(), mCategoryItemList, this);
        mLaundryCategoryRecyclerView.setAdapter(mLaundryItemListAdaptor);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLaundryFragmentInteractionListener) {
            mListener = (OnLaundryFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLaundryFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPlusBtnClicked(String price, String tax) {
        mListener.onRateListPlusClicked(price, tax);
    }

    @Override
    public void onMinusBtnClicked(String price, String tax) {
        mListener.onRateListMinusClicked(price, tax);
    }


    public interface OnLaundryFragmentInteractionListener {
        void onRateListPlusClicked(String price, String tax);
        void onRateListMinusClicked(String price, String tax);
    }
}
