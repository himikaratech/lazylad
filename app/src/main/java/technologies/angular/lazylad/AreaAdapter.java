package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amud on 20/08/15.
 */

public class AreaAdapter extends RecyclerView.Adapter<AreaAdapter.ViewHolder> {

    private ArrayList<AreaSnippet> m_area_list;
    private int itemLayout;
    private final Activity m_activity;
    int viewFlag = 0;
    private String m_old_cityCodeSelected;
    private String m_old_areaCodeSelected;
    private List m_servTypeListDetails2 = new ArrayList<String>();
    private SharedPreferences prefs_location;

    private ArrayList<ServiceTypeSnippet> m_servTypeListDetailsFromServer;

    public static int flag = 0;
    public static final String TAG = "MyTag";

    int m_userCode;
    String m_cityCode;
    String m_cityNameSelected;
    String m_city_image_addSelected;
    int m_city_image_flagSelected;

    public AreaAdapter(ArrayList<AreaSnippet> items, int itemLayout, Context context, int viewFlag, String oldCityCode,
                       String oldAreaCode, String cityCode, String cityNameSelected, int city_image_flagSelected,
                       String city_image_addSelected, int userCode) {
        m_activity = (Activity) context;
        this.m_area_list = items;
        this.itemLayout = itemLayout;
        this.viewFlag = viewFlag;
        m_cityCode = cityCode;
        m_cityNameSelected = cityNameSelected;
        m_city_image_addSelected = city_image_addSelected;
        m_city_image_flagSelected = city_image_flagSelected;
        this.m_city_image_addSelected = city_image_addSelected;
        this.m_userCode = userCode;

        m_old_cityCodeSelected = oldCityCode;
        m_old_areaCodeSelected = oldAreaCode;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v, viewFlag);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (viewFlag == 0) {
            if (m_area_list != null) {
                final String area_name = ((AreaSnippet) getItem(position)).m_areaName;
                AreaSnippet item = m_area_list.get(position);

                holder.areaNameEditText.setText(area_name);
                holder.areaRelativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        prefs_location = m_activity.getSharedPreferences(
                                "technologies.angular.lazylad", Context.MODE_PRIVATE);

                        String area_name = m_area_list.get(holder.getAdapterPosition()).m_areaName;
                        String areaCode = m_area_list.get(holder.getAdapterPosition()).m_areaCode;
                        String cityCode = ((AreaSnippet) getItem(holder.getAdapterPosition())).m_cityCode;

                        SharedPreferences.Editor editor = prefs_location.edit();
                        editor.putString(PreferencesStore.AREA_SELECTED_CONSTANT, areaCode);
                        editor.putString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, area_name);

                        editor.putString(PreferencesStore.CITY_SELECTED_CONSTANT, m_cityCode);
                        editor.putString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, m_cityNameSelected);
                        editor.putString(PreferencesStore.CITY_IMAGE_ADD_CONSTANT, m_city_image_addSelected);
                        editor.putInt(PreferencesStore.CITY_IMAGE_ADD_FLAF_CONSTANT, m_city_image_flagSelected);
                        editor.putString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, m_cityNameSelected);
                        editor.commit();

                        if (!(cityCode.equals(m_old_cityCodeSelected)) && !(cityCode.equals("Not Available"))) {
                            updateServiceType(m_cityCode);
                        }

                        if (!(cityCode.equals(m_old_cityCodeSelected)) && !(cityCode.equals("Not Available"))
                                || !(areaCode.equals(m_old_areaCodeSelected)) && !(areaCode.equals("Not_Available"))
                                ) {
                            ShoppingCartUtils.destroyCart(m_activity);
                        }

                        startMainActivity();
                    }
                });
            }
            if (viewFlag == 1) {
                final String servProvName = ((ServiceProvidersListSnippet) getItem(position)).m_servProvName;
                holder.areaNameEditText.setText(servProvName);
            }
        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(m_activity, MainActivityWithBanner.class);
        m_activity.startActivity(intent);
        m_activity.finish();
    }

    @Override
    public int getItemCount() {
        return m_area_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView areaNameEditText;
        FrameLayout areaRelativeLayout;

        public ViewHolder(View itemView, int flag) {
            super(itemView);
            if (flag == 0) {
                areaNameEditText = (TextView) itemView.findViewById(R.id.area_name);
                areaRelativeLayout = (FrameLayout) itemView.findViewById(R.id.relative_layout_area_list);
            }
            if (flag == 1) {

                areaRelativeLayout = (FrameLayout) itemView.findViewById(R.id.relative_layout_area_list);
                areaNameEditText = (TextView) itemView.findViewById(R.id.area_name);
            }
        }
    }

    public Object getItem(int position) {
        if (m_area_list != null) {
            return m_area_list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void updateServiceType(String m_cityCodeSelected) {
 /*       String[] projection = {ServiceTypesEntry.COLUMN_ST_CODE};
        Cursor m_cursor2 = m_activity.getContentResolver().query(
                ServiceTypesEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);
        if (m_cursor2 != null && m_cursor2.moveToFirst()) {
            for (int i = 0; i < m_cursor2.getCount(); i++) {
                int id = m_cursor2.getInt(0);
                m_servTypeListDetails2.add(id + "");
                m_cursor2.moveToNext();
            }
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.updateServiceTypeAPICall(m_cityCodeSelected, new retrofit.Callback<APIResonseModel.ProvidersResponseModel>() {

            @Override
            public void success(APIResonseModel.ProvidersResponseModel providersResponseModel, retrofit.client.Response response) {

                int success = providersResponseModel.error;
                if (success == 1) {
                    int size = providersResponseModel.service_types.size();
                    int listSize = m_servTypeListDetails2.size();
                    if (listSize != size) {
                        flag = 1;
                    } else {
                        m_servTypeListDetailsFromServer = new ArrayList<ServiceTypeSnippet>(size);
                        for (int j = 0; j < size; j++) {
                            int id = providersResponseModel.service_types.get(j).id;
                            int st_code = providersResponseModel.service_types.get(j).st_code;
                            String st_name = providersResponseModel.service_types.get(j).st_name;
                            int img_flag = providersResponseModel.service_types.get(j).st_img_flag;
                            String img_add = providersResponseModel.service_types.get(j).st_img_add;
                            int servTypeFlag = providersResponseModel.service_types.get(j).seller_automated;

                            ServiceTypeSnippet srvCatListObj = new ServiceTypeSnippet(id, st_code, st_name, img_flag, img_add, servTypeFlag);
                            m_servTypeListDetailsFromServer.add(srvCatListObj);
                            int idComp = srvCatListObj.getm_servTypeCode();
                            String idComp2 = m_servTypeListDetails2.get(j) + "";
                            if (!((idComp + "").equals(idComp2))) {
                                flag = 1;
                            }
                        }
                    }
                    if (flag == 1) {
                        m_servTypeListDetailsFromServer = new ArrayList<ServiceTypeSnippet>(size);
                        for (int j = 0; j < size; j++) {
                            int id = providersResponseModel.service_types.get(j).id;
                            int st_code = providersResponseModel.service_types.get(j).st_code;
                            String st_name = providersResponseModel.service_types.get(j).st_name;
                            int img_flag = providersResponseModel.service_types.get(j).st_img_flag;
                            String img_add = providersResponseModel.service_types.get(j).st_img_add;
                            int servTypeFlag = providersResponseModel.service_types.get(j).seller_automated;

                            ServiceTypeSnippet srvCatListObj = new ServiceTypeSnippet(id, st_code, st_name, img_flag, img_add, servTypeFlag);
                            m_servTypeListDetailsFromServer.add(srvCatListObj);
                        }

                        m_activity.getContentResolver().delete(
                                ServiceTypesEntry.CONTENT_URI,
                                null,
                                null
                        );
                        for (int i = 0; i < m_servTypeListDetailsFromServer.size(); i++) {
                            int id = m_servTypeListDetailsFromServer.get(i).getm_id();
                            int m_servTypeCode = m_servTypeListDetailsFromServer.get(i).getm_servTypeCode();
                            String m_servTypeName = m_servTypeListDetailsFromServer.get(i).getm_servTypeName();
                            int m_servTypeImgFlag = m_servTypeListDetailsFromServer.get(i).getm_servTypeImgFlag();
                            String m_servTypeImgAdd = m_servTypeListDetailsFromServer.get(i).getm_servTypeImgAdd();
                            int servTypeFlag = m_servTypeListDetailsFromServer.get(i).getm_servTypeFlag();

                            ContentValues serviceTypeValues = new ContentValues();
                            serviceTypeValues.put(ServiceTypesEntry._ID, id);
                            serviceTypeValues.put(ServiceTypesEntry.COLUMN_ST_CODE, m_servTypeCode);
                            serviceTypeValues.put(ServiceTypesEntry.COLUMN_ST_NAME, m_servTypeName);
                            serviceTypeValues.put(ServiceTypesEntry.COLUMN_ST_IMG_FLAG, m_servTypeImgFlag);
                            serviceTypeValues.put(ServiceTypesEntry.COLUMN_ST_IMG_ADD, m_servTypeImgAdd);
                            serviceTypeValues.put(ServiceTypesEntry.COLUMN_ST_FLAG, servTypeFlag);

                            Uri insertUri = m_activity.getContentResolver().insert(ServiceTypesEntry.CONTENT_URI, serviceTypeValues);
                        }
                    }
                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
            }
        }); */
    }
}
