package technologies.angular.lazylad;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.moengage.addon.inbox.InboxManager;
import com.moengage.push.PushManager;

import java.util.HashMap;

/**
 * Created by Saurabh on 27/01/15.
 */
public class SessionManager extends Application {

    private Context m_context;
    SharedPreferences.Editor m_editor = null;
    private SharedPreferences m_pref;
    private int PRIVATE_MODE = 0;
    private static SessionManager m_sessMgr;

    public static final String USER_ID = "USER_ID";
    public static final String LOCAL_ADDRESS = "LOCAL_ADDRESS";

    /**
     * Log or request TAG
     */
    public static final String TAG = "VolleyPatterns";
    /**
     * Global request queue for Volley
     */


    private static String PROPERTY_ANALYTICS_ID = "UA-46943690-5";

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     * <p/>
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public SessionManager(Context context) {
        this.m_context = context;
        m_pref = m_context.getSharedPreferences("User_Login_Details", PRIVATE_MODE);
    }

    public SessionManager() {
        InboxManager.getInstance().setInboxAdapter(new CustomAdapter());
    }

    public static SessionManager getInstance(Context context) {
        if (SessionManager.m_sessMgr != null) return SessionManager.m_sessMgr;
        SessionManager.m_sessMgr = new SessionManager(context);
        return SessionManager.m_sessMgr;
    }

    public boolean isLoggedIn() {
        return this.m_pref.getBoolean("isActive", false);
    }

    public SharedPreferences getUserDetails() {
        return this.m_pref;
    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     */

    synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ANALYTICS_ID)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    : null;
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }
}
