package technologies.angular.lazylad;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Amud on 09/09/15.
 */
public class OfferPagerAdapter extends FragmentPagerAdapter {
    // This will Store the Titles of the Tabs which are Going to be passed when MainScreenPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the MainScreenPagerAdapter is created
    Context m_context;
    int flag;
    ViewPager viewPager;
    private int[] pageIDsArray;
    private int count;
    private ArrayList<offerSnippet> m_offer_details_list;

    // Build a Constructor and assign the passed Values to appropriate values in the class

    public OfferPagerAdapter(final ViewPager vp, Context ctxt, FragmentManager mgr, ArrayList<offerSnippet> offer_details_list) {
        super(mgr);
        this.m_offer_details_list = offer_details_list;
        this.viewPager = vp;
        this.m_context = ctxt;
        this.NumbOfTabs = offer_details_list.size();
        Log.e("OfferPagerAdapter", "OfferPagerAdapter");
        int actualNoOfIDs;
        count = NumbOfTabs + 2;
        pageIDsArray = new int[count];
        for (int i = 0; i < NumbOfTabs; i++) {
            pageIDsArray[i + 1] = i;
        }
        pageIDsArray[0] = NumbOfTabs - 1;
        pageIDsArray[count - 1] = 0;

        vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            public void onPageSelected(int position) {
                int pageCount = getCount();
                if (position == 0) {
                    vp.setCurrentItem(pageCount - 2, false);
                } else if (position == pageCount - 1) {
                    vp.setCurrentItem(1, false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        OfferFragment offerFragment = new OfferFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        String image_address = m_offer_details_list.get(position).offer_img_add;
        bundle.putString("offer_address", image_address);
        offerFragment.setArguments(bundle);
        Log.e("else", "working");
        return offerFragment;
    }

    // This method return the titles for the Tabs in the Tab Strip


    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}

