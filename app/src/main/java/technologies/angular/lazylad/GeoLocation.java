package technologies.angular.lazylad;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.moe.pushlibrary.utils.MoEHelperConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.lazylad.LazyContract.AddressSearchEntry;
import technologies.angular.lazylad.common.Constants;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;


/**
 * Created by Amud on 06/11/15.
 */
public class GeoLocation extends ActionBarActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, ResultCallback<LocationSettingsResult> {

    public static final String TAG = GeoLocation.class.getSimpleName();

    protected GoogleApiClient mGoogleApiClient;
    private GeoPlaceAutocompleteAdapter mAdapter;
    private AutoCompleteTextView mAutocompleteView;
    private View mCurrentLocation;
    public TextView mCityTV;
    private Location mLastLocation;
    private View mMainLayout;
    public LinearLayout fullLayout;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(6.7535159, 68.1623859), new LatLng(35.5087008, 97.395561));
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected LocationRequest mLocationRequest;
    private Toolbar toolbar;
    private SharedPreferences prefs_location;
    private ProgressDialog progressDialog;
    String mCityName;
    String mAreaName;
    double mLatitude;
    double mLongitude;
    String mCityCode;
    String mCityAliasName;
    int m_couponInCity;
    private String m_old_cityCodeSelected;
    private Double m_old_latSelected;
    private Double m_old_longSelected;
    private String mCityString;
    private String mCityNotLive;
    private int m_userCode;

    private ArrayList<ServiceTypeSnippet> m_servTypeListDetailsFromServer;
    private ArrayList<CitySnippet> m_cityList;
    private ArrayList<CitySnippet> m_showCityList;
    HashMap<String, CitySnippet> mCityMap;
    String formatted_address;
    RecyclerView recyclerView;
    ArrayList<GeoAddress> geoAddressArrayList;
    TextView m_recentText;
    String m_city_image_addSelected;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (checkPlayServices()) {
            buildGoogleApiClient();
        }

        prefs_location = getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        m_old_cityCodeSelected = prefs_location.getString(PreferencesStore.CITY_CODE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_CODE_SELECTED_CONSTANT);
        m_old_latSelected = Double.longBitsToDouble(prefs_location.getLong(PreferencesStore.LATITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LATITUDE_SELECTED_CONSTANT));
        m_old_longSelected = Double.longBitsToDouble(prefs_location.getLong(PreferencesStore.LONGITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LONGITUDE_SELECTED_CONSTANT));
        mCityMap = new HashMap<String, CitySnippet>();
        m_cityList = new ArrayList<CitySnippet>();
        m_showCityList = new ArrayList<CitySnippet>();
        mCityString = getResources().getString(R.string.currently_live);
        mCityNotLive = getResources().getString(R.string.select_another_area);
        progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        fullLayout = (LinearLayout) findViewById(R.id.layout_address);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mCityName = PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT;
        mAreaName = PreferencesStore.DEFAULT_AREA_SELECTED_CONSTANT;
        mLatitude = 0.0d;
        mLongitude = 0.0d;
        m_couponInCity = PreferencesStore.DEFAULT_COUPON_IN_CITY_CONSTANT;
        formatted_address = PreferencesStore.DEFAULT_ADDRESS_SELECTED_CONSTANT;
        mCityAliasName = "";
        m_city_image_addSelected = "";
        buildLocationSettingsRequest();
        setContentView(R.layout.geo_location_layout);

        m_recentText = (TextView) findViewById(R.id.recent_text);


        geoAddressArrayList = new ArrayList<GeoAddress>();
        Cursor m_cursor2 = getContentResolver().query(
                AddressSearchEntry.CONTENT_URI,
                null,
                null,
                null,
                AddressSearchEntry._ID + " DESC");
        if (m_cursor2 != null && m_cursor2.moveToFirst()) {
            m_recentText.setVisibility(View.VISIBLE);
            int size;
            if (m_cursor2.getCount() > 5)
                size = 5;
            else
                size = m_cursor2.getCount();
            if (m_cursor2.getCount() != 0) {
                for (int j = 0; j < size; j++) {
                    final String formatted_address = m_cursor2.getString(1);
                    final Double latitude = m_cursor2.getDouble(2);
                    final Double longitude = m_cursor2.getDouble(3);
                    final String city_code = m_cursor2.getString(4);
                    final String city_name = m_cursor2.getString(5);
                    final String area_name = m_cursor2.getString(6);
                    final String city_image_address = m_cursor2.getString(7);
                    final int coupon_in_city = m_cursor2.getInt(m_cursor2.getColumnIndex(AddressSearchEntry.COLUMN_COUPON_IN_CITY));


                    Log.d("formatted_address", formatted_address + latitude + longitude + city_code + city_name + area_name);


                    GeoAddress geoAddress = new GeoAddress(formatted_address, latitude, longitude, city_code, city_name, area_name, city_image_address, coupon_in_city);
                    geoAddressArrayList.add(geoAddress);
                    m_cursor2.moveToNext();
                }

                recyclerView = (RecyclerView) findViewById(R.id.address_search_view);
                final LinearLayoutManager layoutManager = new LinearLayoutManager(GeoLocation.this);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());

                recyclerView.setAdapter(new GeoAddressSearchAdapter(geoAddressArrayList, R.layout.geo_address_list, GeoLocation.this, m_old_cityCodeSelected, m_old_latSelected, m_old_longSelected));

            } else {
                //TODO Some UI Element asking to add address
            }


        } else
            Log.d("address", "no");

        mMainLayout = findViewById(R.id.main_layout);
        mMainLayout.setPadding(0, getStatusBarHeight(), 0, 0);
        mMainLayout.setVisibility(View.GONE);

        mCityTV = (TextView) findViewById(R.id.city_list_tv);

        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        mCurrentLocation = findViewById(R.id.gps_view);


        mAdapter = new GeoPlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY,
                null);
        mAutocompleteView.setAdapter(mAdapter);


        mCurrentLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mAutocompleteView.setText("");
                mCityTV.setText(mCityString);
                Log.e("current", "location");
                if (!progressDialog.isShowing())
                    progressDialog.show();
                checkLocationSettings();

            }
        });

        Cursor m_cursor = getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_userCode = m_cursor.getInt(1);
                    m_cursor.moveToNext();
                }
            } else {
                m_userCode = 0;
                //TODO Some UI Element asking to add address
            }
        } else {
            m_userCode = 0;
            //TODO Error Handling
        }


        loadCities();

        mHelper = new MoEHelper(this);
    }

    private void loadCities() {

        Log.e("city", "loading");

        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getCitiesAPICall(new retrofit.Callback<APIResonseModel.GetCityResponseModel>() {

            @Override
            public void success(APIResonseModel.GetCityResponseModel getCityResponseModel, retrofit.client.Response response) {

                int success = getCityResponseModel.error;
                if (success == 1) {
                    m_cityList = getCityResponseModel.cities;
                    StringBuilder sb = new StringBuilder();
                    sb.append(mCityString).append(" ");
                    if (!m_cityList.isEmpty()) {
                        for (int i = 0; i < m_cityList.size(); i++) {
                            if (m_cityList.get(i).m_showCityFlag == 1)
                                m_showCityList.add(m_cityList.get(i));
                            mCityMap.put(m_cityList.get(i).m_city_alias_name, m_cityList.get(i));
                        }

                        for (int i = 0; i < m_showCityList.size(); i++) {
                            if (i != 0 && i != m_showCityList.size() - 1)
                                sb.append(", ");
                            if (i == m_showCityList.size() - 1 && i != 0)
                                sb.append(" and ");
                            sb.append(m_showCityList.get(i).m_cityName);
                        }

                        mCityString = sb.toString();

                        mMainLayout.setVisibility(View.VISIBLE);
                        mCityTV.setText(mCityString);

                    }


                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("LoadCitiesRetro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    private void displayLocation() {

        new Handler().postDelayed((Runnable) (new Runnable() {
            public void run() {
                Log.e("displayLocation", "displayLocation");
                mLastLocation = LocationServices.FusedLocationApi
                        .getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    mLatitude = mLastLocation.getLatitude();
                    mLongitude = mLastLocation.getLongitude();
                    Log.e("mLatitude", mLatitude + "");
                    Log.e("mLongitude", mLongitude + "");

                    getAddressFromLocation(mLatitude, mLongitude,
                            getApplicationContext(), new GeocoderHandler());

                } else {
                    displayLocation();

                }


            }
        }), 800);
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(GeoLocation.this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (!progressDialog.isShowing())
                progressDialog.show();
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);


        }
    };

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                // GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                // PLAY_SERVICES_RESOLUTION_REQUEST).show();
                updateGoogleplay();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                updateGoogleplay();
            }
            return false;
        }
        return true;
    }

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }
            if (places != null) {
                final Place place = places.get(0);


                mLatitude = place.getLatLng().latitude;
                mLongitude = place.getLatLng().longitude;

                Log.d("mLatitude", String.valueOf(mLatitude));
                Log.d("mLongitude", String.valueOf(mLongitude));


                //  mPlaceDetailsText.setText(place.getLatLng().toString());
                getAddressFromLocation(mLatitude, mLongitude,
                        getApplicationContext(), new GeocoderHandler());
            }

            places.release();
        }
    };

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );

        result.setResultCallback(this);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("error", "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        mHelper.onStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
        mHelper.onResume(this);
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                displayLocation();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(GeoLocation.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        displayLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        progressDialog.dismiss();
                        break;
                }
                break;
        }
    }


    public void getAddressFromLocation(final double latitude, final double longitude,
                                       final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                if (geocoder.isPresent()) {

                    try {
                        List<Address> addressList = geocoder.getFromLocation(
                                latitude, longitude, 1);
                        if (addressList != null && addressList.size() > 0) {
                            Address address = addressList.get(0);

                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < address.getMaxAddressLineIndex() - 1; i++) {
                                sb.append(address.getAddressLine(i)).append(", ");
                            }


                            // sb.append(address.getLocality()).append("\n");
                            //sb.append(address.getPostalCode()).append("\n");
                            //  sb.append(address.getCountryName());


                            if (address.getLocality() != null)
                                mCityName = address.getLocality().toString().trim();
                            sb.append(mCityName);
                            if (address.getSubLocality() != null)
                                mAreaName = address.getSubLocality().toString().trim();
                            else
                                mAreaName = mCityName;
                            formatted_address = sb.toString();


                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Unable connect to Geocoder", e);
                    } finally {
                        Message message = Message.obtain();
                        message.setTarget(handler);
                        if (!(mCityName.equals(PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT)) && !(mAreaName.equals(PreferencesStore.DEFAULT_AREA_SELECTED_CONSTANT))) {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            bundle.putString("mCityName", mCityName);
                            bundle.putString("mAreaName", mAreaName);
                            bundle.putString("mAddress", formatted_address);
                            message.setData(bundle);
                        } else {
                            message.what = 0;
                            Bundle bundle = new Bundle();
                            bundle.putString("mCityName", mCityName);
                            bundle.putString("mAreaName", mAreaName);
                            bundle.putString("mAddress", formatted_address);
                            message.setData(bundle);
                        }
                        message.sendToTarget();
                    }
                } else {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    message.what = 0;
                    Bundle bundle = new Bundle();
                    bundle.putString("mCityName", mCityName);
                    bundle.putString("mAreaName", mAreaName);
                    bundle.putString("mAddress", formatted_address);
                    message.setData(bundle);

                }
            }
        };
        thread.start();
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationCity;
            String locationArea;
            String locationAddress;
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationCity = bundle.getString("mCityName");
                    locationArea = bundle.getString("mAreaName");
                    locationAddress = bundle.getString("mAddress");
                    if (!getCityCode(mCityName, locationAddress)) {
                        mCityTV.setText(mCityNotLive);
                        mAutocompleteView.setText("");
                    }
                    break;
                default:
                    locationCity = PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT;
                    locationArea = PreferencesStore.DEFAULT_AREA_SELECTED_CONSTANT;
                    requestCityName(mLatitude, mLongitude);
            }


        }
    }


    private boolean getCityCode(String city_name, String formatted_address) {

        for (String key : mCityMap.keySet()) {
            Log.d("formatted_address1", formatted_address);
            if (formatted_address.toLowerCase().contains(key.toLowerCase())) {
                mCityCode = (mCityMap.get(key)).m_cityCode.trim();
                mCityName = (mCityMap.get(key)).m_cityName.trim();
                m_couponInCity = mCityMap.get(key).m_coupon_in_city;
                m_city_image_addSelected = (mCityMap.get(key)).m_city_image_add.trim();


                int address_exist_flag = 0;
                Cursor m_cursor2 = getContentResolver().query(
                        AddressSearchEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        AddressSearchEntry._ID + " DESC");
                if (m_cursor2 != null && m_cursor2.moveToFirst()) {
                    if (m_cursor2.getCount() != 0) {
                        for (int j = 0; j < m_cursor2.getCount(); j++) {
                            final Double latitude = m_cursor2.getDouble(2);
                            final Double longitude = m_cursor2.getDouble(3);

                            if (latitude == mLatitude && longitude == mLongitude) {
                                address_exist_flag = 1;
                                break;
                            }
                            m_cursor2.moveToNext();
                        }
                    }
                }


                if (address_exist_flag == 0)
                    insertAddressIntoDB(mCityCode, formatted_address, mLatitude, mLongitude, mAreaName, mCityName, m_city_image_addSelected, m_couponInCity);

                saveParams(mCityCode, mLatitude, mLongitude, mAreaName, mCityName, m_old_cityCodeSelected, m_old_latSelected, m_old_longSelected, m_city_image_addSelected, m_couponInCity);
                return true;
            }
        }
        return false;
    }

    public void saveParams(String city_code, Double latitude, Double longitude, String area_name, String city_name, String old_city_code_selected, Double old_lat_selected, Double old_long_selected, String city_image_addSelected, int coupon_in_city) {
        SharedPreferences.Editor editor = prefs_location.edit();
        editor.putString(PreferencesStore.CITY_CODE_SELECTED_CONSTANT, city_code);
        editor.putLong(PreferencesStore.LATITUDE_SELECTED_CONSTANT, Double.doubleToLongBits(latitude));
        editor.putLong(PreferencesStore.LONGITUDE_SELECTED_CONSTANT, Double.doubleToLongBits(longitude));
        editor.putString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, area_name);
        editor.putString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, city_name);
        editor.putString(PreferencesStore.CITY_IMAGE_ADD_CONSTANT, city_image_addSelected);
        editor.putInt(PreferencesStore.COUPON_IN_CITY_CONSTANT, coupon_in_city);

        editor.commit();


       /* if (!(city_code.equals(old_city_code_selected)) && !(city_code.equals(PreferencesStore.DEFAULT_CITY_CODE_SELECTED_CONSTANT))) {
            updateLocation(city_code, latitude, longitude);
        }

        if (!(city_code.equals(old_city_code_selected)) && !(city_code.equals(PreferencesStore.DEFAULT_CITY_CODE_SELECTED_CONSTANT))) {
            ShoppingCartUtils.destroyCart(this);
        }
*/
        if (!(latitude.equals(old_lat_selected)) && !(latitude.equals(PreferencesStore.DEFAULT_LATITUDE_SELECTED_CONSTANT)) ||
                !(longitude.equals(old_long_selected)) && !(longitude.equals(PreferencesStore.DEFAULT_LONGITUDE_SELECTED_CONSTANT))) {
            updateLocation(city_code, latitude, longitude);
            ShoppingCartUtils.destroyCart(this);
        }

        if (m_userCode != 0) {
            MoEHelper.getInstance(this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_UNIQUE_ID, m_userCode);
            MoEHelper.getInstance(this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_LOCATION, city_code);
        }
        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("City Code", city_code);
        builder.putAttrDouble("OldLatitude", old_lat_selected);
        builder.putAttrDouble("OldLongitude", old_long_selected);
        builder.putAttrDouble("NewLatitude", latitude);
        builder.putAttrDouble("NewLongitude", longitude);
        builder.putAttrString("Area Name", area_name);
        builder.putAttrString("City Name", city_name);
        MoEHelper.getInstance(GeoLocation.this).trackEvent("GeoLocation", builder.build());

        startMainActivity();

    }


    private void updateLocation(String city_code, double latitude, double longitude) {


        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.ProvidersRequestModel getCityCodeRequestModel = requestModel.new ProvidersRequestModel();
        getCityCodeRequestModel.user_code = String.valueOf(m_userCode);
        getCityCodeRequestModel.city_code = city_code;
        getCityCodeRequestModel.lat = String.valueOf(latitude);
        getCityCodeRequestModel.longi = String.valueOf(longitude);


        RestAdapter restAdapter = Utils.providesRestAdapter(Constants.WEB_SERVICE_URL);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.updateServiceTypeAPICall(getCityCodeRequestModel, new retrofit.Callback<APIResonseModel.ProvidersResponseModel>() {

            @Override
            public void success(APIResonseModel.ProvidersResponseModel providersResponseModel, retrofit.client.Response response) {
                int size = providersResponseModel.service_types.size();
                m_servTypeListDetailsFromServer = new ArrayList<ServiceTypeSnippet>(size);
                for (int j = 0; j < size; j++) {
                    int id = providersResponseModel.service_types.get(j).id;
                    int st_code = providersResponseModel.service_types.get(j).st_code;
                    String st_name = providersResponseModel.service_types.get(j).st_name;
                    int img_flag = providersResponseModel.service_types.get(j).st_img_flag;
                    String img_add = providersResponseModel.service_types.get(j).st_img_add;
                    int servTypeFlag = providersResponseModel.service_types.get(j).seller_automated;
                    int isService = providersResponseModel.service_types.get(j).is_service;

                    ServiceTypeSnippet srvCatListObj = new ServiceTypeSnippet(id, st_code, st_name, img_flag, img_add, servTypeFlag, isService);
                    m_servTypeListDetailsFromServer.add(srvCatListObj);
                }

                getContentResolver().delete(
                        LazyContract.ServiceTypesEntry.CONTENT_URI,
                        null,
                        null
                );
                for (int i = 0; i < m_servTypeListDetailsFromServer.size(); i++) {
                    int id = m_servTypeListDetailsFromServer.get(i).getm_id();
                    int m_servTypeCode = m_servTypeListDetailsFromServer.get(i).getm_servTypeCode();
                    String m_servTypeName = m_servTypeListDetailsFromServer.get(i).getm_servTypeName();
                    int m_servTypeImgFlag = m_servTypeListDetailsFromServer.get(i).getm_servTypeImgFlag();
                    String m_servTypeImgAdd = m_servTypeListDetailsFromServer.get(i).getm_servTypeImgAdd();
                    int servTypeFlag = m_servTypeListDetailsFromServer.get(i).getm_servTypeFlag();
                    int isService = m_servTypeListDetailsFromServer.get(i).getM_isService();

                    ContentValues serviceTypeValues = new ContentValues();
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry._ID, id);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_CODE, m_servTypeCode);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_NAME, m_servTypeName);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_IMG_FLAG, m_servTypeImgFlag);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_IMG_ADD, m_servTypeImgAdd);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_FLAG, servTypeFlag);
                    serviceTypeValues.put(LazyContract.ServiceTypesEntry.COLUMN_ST_IS_SERVICE, isService);

                    Uri insertUri = getContentResolver().insert(LazyContract.ServiceTypesEntry.CONTENT_URI, serviceTypeValues);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(GeoLocation.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                Log.e("UpdateServiceTypeRetro", error + "");
            }
        });
    }


    private void startMainActivity() {
        int m_servTypeCode;
        int m_servTypeFlag;
        String parent;

        Intent intent = getIntent();
        m_servTypeCode = intent.getIntExtra("servTypeCode", -1);
        m_servTypeFlag = intent.getIntExtra("servTypeFlag", -1);
        parent = intent.getStringExtra("parent");

        Log.d("m_servTypeCode", m_servTypeCode + "");


        if (m_servTypeCode != -1) {

            SharedPreferences.Editor editor = prefs_location.edit();
            editor.putString("ServTypeCode", Integer.toString(m_servTypeCode));
            editor.commit();

            Intent i = new Intent(this, ServiceProvidersList.class);
            i.putExtra("servTypeFlag", m_servTypeFlag);
            this.startActivity(i);
            this.finish();
        }/* else if (parent != null) {
            onBackPressed();*/ else {
            Intent i = new Intent(this, MainClass.class);
            this.startActivity(i);
            this.finish();
        }
    }


    protected void requestCityName(final double latitude, final double longitude) {

        String latlng = latitude + "," + longitude;
        String key = "AIzaSyAevWz0y25b6b1kkdBfHl1ncDZGQU-8Qa4";
        String result_type = "sublocality_level_1";
        Log.e("city1", "loading");

        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://maps.googleapis.com")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getCityFromLatLong(latlng, key, result_type, new retrofit.Callback<APIResonseModel.ReverseGeoCodingResponseModel>() {

            @Override
            public void success(APIResonseModel.ReverseGeoCodingResponseModel reverseGeoCodingResponseModel, retrofit.client.Response response) {

                //  Log.d("reverseGeoCoding",reverseGeoCodingResponseModel.results+"");

                if (reverseGeoCodingResponseModel.results != null && !reverseGeoCodingResponseModel.results.isEmpty()) {


                    mAreaName = reverseGeoCodingResponseModel.results.get(0).address_components.get(0).short_name;
                    mCityName = reverseGeoCodingResponseModel.results.get(0).address_components.get(1).short_name;
                    formatted_address = reverseGeoCodingResponseModel.results.get(0).formatted_address;


                    if (!getCityCode(mCityName, formatted_address)) ;
                    {
                        mCityTV.setText(mCityNotLive);
                        mAutocompleteView.setText("");
                    }
                } else
                    mCityTV.setText(mCityNotLive);


                if (progressDialog.isShowing())
                    progressDialog.dismiss();


            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("LoadCitiesRetro", error + "");
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }


    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void insertAddressIntoDB(String city_code, String formatted_address, Double latitude, Double longitude, String area_name, String city_name, String city_image_addSelected, int coupon_in_city) {

      /*  Cursor m_cursor = getContentResolver().query(
                AddressSearchEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() > 5) {


                Log.d("number of address", m_cursor.getCount() + "");
                String[] projection = {AddressSearchEntry._ID};
                Cursor m_cursor2 = getContentResolver().query(
                        AddressSearchEntry.CONTENT_URI,
                        projection,
                        null,
                        null,
            AddressSearchEntry._ID + " ASC");

            if (m_cursor2 != null && m_cursor2.moveToFirst()) {
                if (m_cursor2.getCount() > 0) {
                    int id= m_cursor2.getInt(0);
                    Log.d("cursor",(id+1)+"");
                    getContentResolver().delete(
                            AddressSearchEntry.CONTENT_URI,
                            AddressSearchEntry._ID + " =? ",
                            new String[]{id+""});
                }
            }


        }
    } */

        ContentValues address_search_value = new ContentValues();
        address_search_value.put(AddressSearchEntry.COLUMN_FORMATTED_ADDRESS, formatted_address);
        address_search_value.put(AddressSearchEntry.COLUMN_LATITUDE, latitude);
        address_search_value.put(AddressSearchEntry.COLUMN_LONGITUDE, longitude);
        address_search_value.put(AddressSearchEntry.COLUMN_CITY_CODE, city_code);
        address_search_value.put(AddressSearchEntry.COLUMN_CITY_NAME, city_name);
        address_search_value.put(AddressSearchEntry.COLUMN_AREA_NAME, area_name);
        address_search_value.put(AddressSearchEntry.COLUMN_CITY_IMAGE_ADDRESS, city_image_addSelected);
        address_search_value.put(AddressSearchEntry.COLUMN_COUPON_IN_CITY, coupon_in_city);


        Uri insertUri = getContentResolver().insert(AddressSearchEntry.CONTENT_URI, address_search_value);

        Log.d("inserted", "successfully");

    }


    public void updateGoogleplay() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GeoLocation.this);
// set title
        alertDialogBuilder.setTitle("Update Google Play Services");
// set dialog message
        alertDialogBuilder
                .setMessage("This Application Want To Update You Google Play Services App")
                .setCancelable(false)
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callMarketPlace();
                                finish();
                            }
                        });
        alertDialogBuilder.show();
    }

    public void callMarketPlace() {
        try {
            startActivityForResult(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + "com.google.android.gms")), 1);
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivityForResult(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + "com.google.android.gms")), 1);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
