package technologies.angular.lazylad;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;

import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.moe.pushlibrary.MoEHelper;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

/**
 * Created by Amud on 09/09/15.
 */
public class AboutUs extends ActionBarActivity {

    public ArrayList<String> m_aboutNames;
    public ArrayList<Integer> m_aboutCode;
    private ViewPager m_viewPager;
    private Toolbar m_toolbar;
    private AboutUsPagerAdapter aboutUsPagerAdapter;
    private PagerSlidingTabStrip tabs;

    private String mAddressOutput;

    String TITLES[] = {"Home", "Notifications", "Addresses", "Order History", "Share & Earn", "Lazy Wallet", "", "Rate Us", "Feedback", "Call Us", "About Us"};
    int ICONS[] = {R.drawable.home, R.drawable.notification, R.drawable.address, R.drawable.orders, R.drawable.share, R.drawable.wallet, R.drawable.ic_launcher, R.drawable.rate, R.drawable.feedback, R.drawable.call, R.drawable.aboutus};
    String Name = "Current Location:";
    int PROFILE = R.drawable.pro;
    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;
    private View frame;
    private SharedPreferences prefs_location;
    private String areaName;
    private String cityName;

    private MoEHelper mHelper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.about_us_layout);
        m_toolbar = (Toolbar) findViewById(R.id.aboutus_toolbar);
        m_toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        setSupportActionBar(m_toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prefs_location = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        areaName = prefs_location.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, "Not_Available");
        cityName = prefs_location.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, "Not Available");

        mAddressOutput = areaName + "," + " " + cityName;

        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView); // Assigning the RecyclerView Object to the xml View
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayViewWidth = size.x;
        frame = (View) findViewById(R.id.content_frame);

        mRecyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, AboutUs.this, 10);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer object Assigned to the view
        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.width = 9 * (displayViewWidth / 10);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutParams(params);
        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, m_toolbar, R.string.opendrawer, R.string.closedrawer) {

            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (mRecyclerView.getWidth() * slideOffset);
                frame.setTranslationX(moveFactor);
            }
        };
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State

        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setShouldExpand(true);
        tabs.setVisibility(View.GONE);
        m_aboutCode = new ArrayList<Integer>();
        m_aboutNames = new ArrayList<String>();

        loadAboutUs();

        mHelper = new MoEHelper(this);
    }


    private void loadAboutUs() {

        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        apiSuggestionsService.aboutUsTabAPICall(new retrofit.Callback<APIResonseModel.AboutUsTabResponseModel>() {

            @Override
            public void success(APIResonseModel.AboutUsTabResponseModel aboutUsTabResponseModel, retrofit.client.Response response) {

                int success = aboutUsTabResponseModel.error;
                if (success == 1) {
                    if (!aboutUsTabResponseModel.info_list.isEmpty()) {
                        int length = aboutUsTabResponseModel.info_list.size();
                        for (int i = 0; i < length; i++) {
                            m_aboutCode.add(i, aboutUsTabResponseModel.info_list.get(i).info_id);
                            m_aboutNames.add(i, aboutUsTabResponseModel.info_list.get(i).info_name);
                        }
                        makeStrip();
                    }
                    progressDialog.dismiss();


                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();

            }
        });
    }


    void makeStrip() {

        m_viewPager = (ViewPager) findViewById(R.id.aboutus_pager);

        aboutUsPagerAdapter = new AboutUsPagerAdapter(getSupportFragmentManager(), m_aboutCode, this);

        m_viewPager.setAdapter(aboutUsPagerAdapter);
        setSupportActionBar(m_toolbar);
        getSupportActionBar().setTitle("About us");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tabs.setVisibility(View.VISIBLE);

        tabs.setViewPager(m_viewPager);

        m_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                tabs.setViewPager(m_viewPager);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }


    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Drawer.openDrawer(GravityCompat.START);  // OPEN DRAWER
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        Drawer.closeDrawer(Gravity.LEFT);
        mHelper.onStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}