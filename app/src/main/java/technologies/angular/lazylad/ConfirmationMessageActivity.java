package technologies.angular.lazylad;

/**
 * Created by Amud on 09/09/15.
 */

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Payu.PayuConstants;
import com.payu.payuui.PayUBaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazylad.common.Constants;
import technologies.angular.lazylad.fragments.DeliverySlotFragment;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

import technologies.angular.lazylad.LazyContract.ShoppingCartEntry;
import technologies.angular.lazylad.LazyContract.OrderDetailsEntry;


public class ConfirmationMessageActivity extends AppCompatActivity implements DeliverySlotFragment.InDeliverySlotCallback {

    private static final String TAG = ConfirmationMessageActivity.class.getSimpleName();

    private boolean m_couponApplied = false;

    private long epochExpDeliveryTime;
    int flag = 0;
    int totalcount = 0;
    private boolean isDeliverySlotSet;

    private Double m_totalItemsAmount = 0.0;
    private Double m_totalTaxes = 0.0;
    private Double m_totalPayableAmount;
    private Double m_totalOfferAmount;

    private Double userCODAmmount;
    private Double userWalletAmount;
    private Double userWalletApplied;
    private Double userWalletLeft;
    private Double m_codAmount;
    private int m_deliveryCharges = 0;
    private int m_discountAmount;

    private String m_addressCode;
    private String m_userCode;
    private String m_address_userCode;
    String couponCode = "";
    String m_number;
    private String expDeliveryTime;

    private TextView m_orderAmount;
    private TextView m_orderTaxApplied;
    private TextView m_deliveryAmount;
    private TextView m_totalAmountTextView;
    private EditText m_couponCodeEditText;
    private TextView m_discountAmountTextView;
    private TextView walletAmountTextView;
    private TextView walletAmountUptoTextView;
    private TextView codAmountTextView;
    private CheckBox userWalletAmountTV;
    private Button m_placeOrderButton;
    private ImageButton infoDeliveryCharge;
    private Button m_applyCouponCodeButton;
    protected TextView pickedTimeTextView;
    RelativeLayout.LayoutParams params;
    LinearLayout.LayoutParams redlineLayoutParams;
    LinearLayout.LayoutParams greylineLayoutParams;
    ScrollView scrollViewLayout;
    ViewPager pager;
    TextView totalItems;
    TextView showingItem;
    private Button mChangeDeliveryTimeBtn;

    private APIResonseModel.GetWalletDetailsResponseModel.UsageEntry wallet_usage_entry = null;


    Calendar FinalservercurrentCal;

    private int m_numberOfTimesCouponCodeApplied;
    Bundle bundle;

    CartPagerAdapter adapter;
    int Numboftabs = 0;

    private ArrayList<ItemSnippet> m_shoppingCartItemsList;
    private ArrayList<ItemSnippet> m_shoppingCartItemsCheckOutList;

    private ArrayList<String> m_spCodesList = null;
    private ArrayList<Integer> m_deliveryChargesList = null;
    private ArrayList<Integer> m_sellerAppliedTaxList = null;


    private MoEHelper mHelper;


    private String mTransId;

    int time_interval;
    int m_shop_open_time;
    int m_shop_open_hour;
    int m_shop_open_minute;
    int m_shop_close_time;
    int m_shop_close_hour;
    int m_shop_close_minute;


    private Calendar mShopOpeningCalendar;
    private Calendar mShopClosingCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.confirmation_message_new);
        setTheme(android.R.style.Theme_Holo);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        FinalservercurrentCal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));

        bundle = new Bundle();
        bundle = getIntent().getExtras();

        FacebookSdk.sdkInitialize(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Verify and Confirm");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        m_placeOrderButton = (Button) findViewById(R.id.place_order_button);
        m_couponCodeEditText = (EditText) findViewById(R.id.coupon_code_edittext);
        m_applyCouponCodeButton = (Button) findViewById(R.id.apply_code_button);
        m_orderAmount = (TextView) findViewById(R.id.order_amount);
        m_orderTaxApplied = (TextView) findViewById(R.id.tax_applied);
        m_deliveryAmount = (TextView) findViewById(R.id.delivery_amount);
        m_totalAmountTextView = (TextView) findViewById(R.id.total_amount);
        m_discountAmountTextView = (TextView) findViewById(R.id.discount_amount);
        infoDeliveryCharge = (ImageButton) findViewById(R.id.delivery_charge_info);

        totalItems = (TextView) findViewById(R.id.totalItems);
        showingItem = (TextView) findViewById(R.id.showingItem);
        pickedTimeTextView = (TextView) findViewById(R.id.picked_time_text_view);

        scrollViewLayout = (ScrollView) findViewById(R.id.scrollViewLayout);

        walletAmountTextView = (TextView) findViewById(R.id.wallet_amount);
        codAmountTextView = (TextView) findViewById(R.id.cod_amount);
        userWalletAmountTV = (CheckBox) findViewById(R.id.useWalletCheckBox);
        walletAmountUptoTextView = (TextView) findViewById(R.id.wallet_upto_amount);
        redlineLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 5);
        greylineLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);

        mChangeDeliveryTimeBtn = (Button) findViewById(R.id.change_delivery_time_button);

        LinearLayout coupon_view = (LinearLayout) findViewById(R.id.coupon_view);

        String[] data = bundle.getStringArray("data_send_to_confirm_activity");
        m_addressCode = data[0];
        m_address_userCode = data[1];
        m_userCode = data[2];

        m_discountAmount = 0;
        userWalletAmount = 0.0;
        userWalletApplied = 0.0;
        userWalletLeft = 0.0;
        m_totalOfferAmount = 0.0;


        SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        String[] mProjection = {OrderDetailsEntry.COLUMN_TIME_INTERVAL, OrderDetailsEntry.COLUMN_SHOP_OPEN_TIME, OrderDetailsEntry.COLUMN_SHOP_CLOSE_TIME};
        Cursor cursorResultForScheduling = getContentResolver().query(
                OrderDetailsEntry.CONTENT_URI,
                mProjection,
                null,
                null,
                null);


        if (cursorResultForScheduling != null && cursorResultForScheduling.getCount() == 1) {
            if (cursorResultForScheduling.moveToFirst()) {
                time_interval = cursorResultForScheduling.getInt(cursorResultForScheduling.getColumnIndex("time_interval"));
                m_shop_open_time = cursorResultForScheduling.getInt(cursorResultForScheduling.getColumnIndex("open_time"));
                m_shop_close_time = cursorResultForScheduling.getInt(cursorResultForScheduling.getColumnIndex("close_time"));
            }
        } else if (cursorResultForScheduling != null && cursorResultForScheduling.getCount() > 1) {
            time_interval = 90;
            m_shop_open_time = 540;
            m_shop_close_time = 1260;
        }

        if (cursorResultForScheduling != null) cursorResultForScheduling.close();


        m_shop_open_hour = m_shop_open_time / 60;
        m_shop_open_minute = m_shop_open_time % 60;


        m_shop_close_hour = m_shop_close_time / 60;
        m_shop_close_minute = m_shop_close_time % 60;

        String[] projectionArray = {OrderDetailsEntry.COLUMN_MIN_ORDER, OrderDetailsEntry.COLUMN_SERV_PROV_CODE};
        Cursor cursor1 = getContentResolver().query(
                OrderDetailsEntry.CONTENT_URI,
                projectionArray,
                null,
                null,
                OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " ASC");


        ArrayList<Integer> minDeliveryCharge = new ArrayList<Integer>();
        m_spCodesList = new ArrayList<String>();

        if (cursor1 != null && cursor1.moveToFirst()) {
            if (cursor1.getCount() > 0) {
                for (int i = 0; i < cursor1.getCount(); i++) {
                    int minOrderCursor = cursor1.getInt(cursor1.getColumnIndex("min_order"));
                    String spCodeCursor = cursor1.getString(cursor1.getColumnIndex("serv_prov_code"));
                    minDeliveryCharge.add(minOrderCursor);
                    m_spCodesList.add(spCodeCursor);
                    cursor1.moveToNext();
                }
            }
        }
        cursor1.close();

        String query = "select item_sp_code, SUM(item_cost*item_quantity_selected) AS total , SUM(item_offer_price*item_quantity_selected) AS total_offer_pice " +
                "from shopping_cart GROUP BY item_sp_code ORDER BY item_sp_code ASC";

        Cursor cursor = getContentResolver().query(
                LazyContract.RAW_CONTENT_URI,
                null,
                query,
                null,
                null);

        ArrayList<Integer> sellerTotalCostData = new ArrayList<Integer>();

        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    int itemCostCursor = (int) Math.round(cursor.getDouble(cursor.getColumnIndex("total")));
                    String spCursor = cursor.getString(cursor.getColumnIndex("item_sp_code"));
                    m_totalItemsAmount = m_totalItemsAmount + cursor.getDouble(cursor.getColumnIndex("total"));
                    m_totalOfferAmount = m_totalOfferAmount + cursor.getDouble(cursor.getColumnIndex("total_offer_pice"));
                    sellerTotalCostData.add(itemCostCursor);
                    cursor.moveToNext();
                }
                m_totalOfferAmount = m_totalItemsAmount - m_totalOfferAmount;
            }
        }

        m_sellerAppliedTaxList = new ArrayList<Integer>();

        String taxesQuery = "select ((CAST (tax_flag_1 AS DECIMAL(5,2)))*tax_cent_1) as tax1, ((CAST (tax_flag_2 AS DECIMAL(5,2)))*tax_cent_2) as tax2, " +
                "((CAST (tax_flag_3 AS DECIMAL(5,2)))*tax_cent_3) as tax3 from order_details ORDER BY serv_prov_code ASC";

        Cursor cursorTax = getContentResolver().query(
                LazyContract.RAW_CONTENT_URI,
                null,
                taxesQuery,
                null,
                null);

        if (cursorTax != null && cursorTax.moveToFirst()) {
            if (cursorTax.getCount() > 0) {
                for (int i = 0; i < cursorTax.getCount(); i++) {
                    if (cursorTax.getDouble(cursorTax.getColumnIndex("tax1")) > 0
                            || cursorTax.getDouble(cursorTax.getColumnIndex("tax2")) > 0
                            || cursorTax.getDouble(cursorTax.getColumnIndex("tax3")) > 0) {
                        double amountAfterTaxPerSeller = (sellerTotalCostData.get(i) * cursorTax.getDouble(cursorTax.getColumnIndex("tax1")))
                                + (sellerTotalCostData.get(i) * cursorTax.getDouble(cursorTax.getColumnIndex("tax2")))
                                + (sellerTotalCostData.get(i) * cursorTax.getDouble(cursorTax.getColumnIndex("tax3")));
                        m_totalTaxes = m_totalTaxes + (amountAfterTaxPerSeller / 100);
                        m_sellerAppliedTaxList.add((int) Math.round(amountAfterTaxPerSeller / 100));
                    } else {
                        m_sellerAppliedTaxList.add(0);
                    }
                    cursorTax.moveToNext();
                }
            }
        }

        m_deliveryChargesList = new ArrayList<Integer>();
        for (int k = 0; k < sellerTotalCostData.size(); k++) {
            if (minDeliveryCharge.get(k) > sellerTotalCostData.get(k)) {
                m_deliveryCharges = m_deliveryCharges + 20;
                m_deliveryChargesList.add(20);
            } else
                m_deliveryChargesList.add(0);
        }

        m_orderAmount.setText("Amount   " + "₹" + String.valueOf(m_totalItemsAmount - m_totalOfferAmount));
        m_orderTaxApplied.setText("Taxes   " + "₹" + String.valueOf(m_totalTaxes));

        m_deliveryAmount.setText("Delivery Charges   " + "₹" + String.valueOf(m_deliveryCharges));

        m_totalPayableAmount = m_totalItemsAmount + m_totalTaxes + m_deliveryCharges - m_totalOfferAmount;
        m_totalPayableAmount = (double) (Math.round(m_totalPayableAmount));

        m_totalAmountTextView.setText("TOTAL BILL   " + "₹" + String.valueOf(m_totalPayableAmount));
        userCODAmmount = m_totalPayableAmount;
        codAmountTextView.setText("AMOUNT PAYABLE   " + "₹ " + String.valueOf(Math.round(userCODAmmount)));


        SharedPreferences prefscouponcode = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
        m_numberOfTimesCouponCodeApplied = prefscouponcode.getInt("number_of_times_coupon_code_applied", 0);

        if (m_numberOfTimesCouponCodeApplied > 0) {
            m_applyCouponCodeButton.setVisibility(View.GONE);
        }


        m_applyCouponCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                couponCode = m_couponCodeEditText.getText().toString();
                if (couponCode.isEmpty() || couponCode == null)
                    Toast.makeText(ConfirmationMessageActivity.this, "Please add a coupon first", Toast.LENGTH_SHORT).show();
                else
                    checkAndApplyCouponCode(couponCode, m_userCode);
            }
        });

        mShopClosingCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        mShopClosingCalendar.set(Calendar.MINUTE, m_shop_close_minute);
        mShopClosingCalendar.set(Calendar.MILLISECOND, 0);
        mShopClosingCalendar.set(Calendar.HOUR_OF_DAY, m_shop_close_hour);

        mShopOpeningCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        mShopOpeningCalendar.set(Calendar.MINUTE, m_shop_open_minute);
        mShopOpeningCalendar.set(Calendar.MILLISECOND, 0);
        mShopOpeningCalendar.set(Calendar.HOUR_OF_DAY, m_shop_open_hour);

        Log.d(TAG, "mShopOpeningCalendar:" + mShopOpeningCalendar.getTimeInMillis());
        Log.d(TAG, "mShopClosingCalendar" + mShopClosingCalendar.getTimeInMillis());
        Log.d(TAG, "m_shop_open_time" + m_shop_open_time);
        Log.d(TAG, "m_shop_close_time" + m_shop_close_time);

        DeliverySlot deliverySlot = new DeliverySlot(mShopOpeningCalendar.getTimeInMillis(), mShopClosingCalendar.getTimeInMillis(), time_interval * 60000, Constants.GROCERIES_DELIVERY_SLOT_CODE);
        ArrayList<String> slotList = deliverySlot.getTimeSlotsForToday(System.currentTimeMillis());
        if (slotList != null) {
            isDeliverySlotSet = true;
            pickedTimeTextView.setText(deliverySlot.getTodayDate(Constants.GROCERIES_DELIVERY_SLOT_CODE) + "\n" + slotList.get(0));
        } else {
            isDeliverySlotSet = false;
            pickedTimeTextView.setText(R.string.no_slots_available);
        }

        Cursor m_cursor = getContentResolver().query(
                ShoppingCartEntry.CONTENT_URI,
                null,
                null,
                null,
                null);


        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() > 0) {
                m_shoppingCartItemsList = new ArrayList<ItemSnippet>(m_cursor.getCount());
                for (int i = 0; i < m_cursor.getCount(); i++) {

                    int itemsId = i + 1;
                    String m_itemCode = m_cursor.getString(1);
                    String m_itemName = m_cursor.getString(2);
                    int m_itemImgFlag = m_cursor.getInt(3);
                    String m_itemImgAddress = m_cursor.getString(4);
                    String m_itemUnit = m_cursor.getString(5);
                    double m_itemCost = m_cursor.getDouble(6);
                    String m_itemShortDesc = m_cursor.getString(7);  //Standard Weight
                    String m_itemDesc = m_cursor.getString(8);
                    String m_itemStatus = m_cursor.getString(9);  //Available and Not Available
                    int itemSelected = m_cursor.getInt(10);
                    int itemQuantitySelected = m_cursor.getInt(11);
                    String m_itemServiceType = m_cursor.getString(12);
                    String m_itemServiceTypeCategory = m_cursor.getString(13);
                    String m_itemServiceProvider = m_cursor.getString(14);
                    double m_itemOfferPrice = m_cursor.getDouble(15);
                    int m_itemInOffer = m_cursor.getInt(16);

                    String[] projection = {OrderDetailsEntry.COLUMN_SERV_PROV_NAME};
                    String whereClause = OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
                    String[] selectionArgs = new String[]{m_itemServiceProvider};
                    Cursor cursor2 = getContentResolver().query(
                            OrderDetailsEntry.CONTENT_URI,
                            projection,
                            whereClause,
                            selectionArgs,
                            null);

                    if (cursor2.moveToFirst()) {
                        String sellerName = cursor2.getString(cursor2.getColumnIndex("serv_prov_name"));

                        ItemSnippet currenOrderObj = new ItemSnippet(itemsId, m_itemCode, m_itemName, m_itemImgFlag, m_itemImgAddress,
                                m_itemUnit, m_itemCost, m_itemShortDesc, m_itemDesc, m_itemStatus, itemSelected, itemQuantitySelected,
                                m_itemServiceType, m_itemServiceTypeCategory, sellerName, m_itemOfferPrice, m_itemInOffer);
                        m_shoppingCartItemsList.add(currenOrderObj);
                    }
                    m_cursor.moveToNext();
                    cursor2.close();
                }
            }
        }

        if (!m_shoppingCartItemsList.isEmpty())
            Numboftabs = m_shoppingCartItemsList.size();

        String[] proj = {OrderDetailsEntry.COLUMN_SERV_PROV_CODE};
        Cursor cursor4 = getContentResolver().query(
                OrderDetailsEntry.CONTENT_URI,
                proj,
                null,
                null,
                null);

        totalcount = cursor4.getCount();
        totalItems.setText(Integer.toString(totalcount).concat(" Orders"));

        cursor4.close();
        adapter = new CartPagerAdapter(getSupportFragmentManager(), Numboftabs, this, m_shoppingCartItemsList);

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                int showingcount = position + 1;

                String[] mProj = {ShoppingCartEntry.COLUMN_ITEM_SP_CODE};
                Cursor cursorCountForSpCode = getContentResolver().query(
                        ShoppingCartEntry.CONTENT_URI,
                        mProj,
                        null,
                        null,
                        null);

                showingItem.setText("showing " + showingcount + " of " + cursorCountForSpCode.getCount());
            }

            @Override
            public void onPageSelected(int position) {
                Log.e("logging2", position + "");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

            // tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

            // Setting Custom Color for the Scroll bar indicator of the Tab View
            //    m_servTypeGridView.setAdapter((ListAdapter) m_servTypeAdapter);
        });


        infoDeliveryCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // custom dialog
                final Dialog dialog = new Dialog(ConfirmationMessageActivity.this);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Delivery Charge");

                // set the custom dialog components - text, image and button
                TextView text = (TextView) dialog.findViewById(R.id.text);
                text.setText("Android custom dialog example!jbjbhxbvhdbvhdbvhzsbhjbshcfbzhvzshjv" +
                        "bhdxbvhcvhdzbvdbvhdxbvjhdxhdxb");
                ImageView image = (ImageView) dialog.findViewById(R.id.image);
                image.setImageResource(R.drawable.info_del);

                Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        userWalletAmountTV.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                    applyWallet();
                else if (!isChecked)
                    unApplyWallet();

            }
        });
        getWalletDetails();

        setTracker();

        mHelper = new MoEHelper(this);

        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrDouble("TotalItemsAmount", m_totalItemsAmount);
        builder.putAttrInt("DeliveryCharge", m_deliveryCharges);
        builder.putAttrDouble("TotalBillAmount", m_totalPayableAmount);
        builder.putAttrInt("DiscountAmount", m_discountAmount);
        builder.putAttrString("CouponCode", couponCode);
        builder.putAttrString("Activity Name", this.getClass().getCanonicalName());
        MoEHelper.getInstance(ConfirmationMessageActivity.this).trackEvent("Order Details", builder.build());

        mChangeDeliveryTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeliverySlotFragment fragment = DeliverySlotFragment.newInstance(mShopOpeningCalendar.getTimeInMillis(), mShopClosingCalendar.getTimeInMillis(), time_interval * 60000, Constants.GROCERIES_DELIVERY_SLOT_CODE, 0, 0);
                fragment.show(getFragmentManager(), "DELIVERY_SLOT_FRAGMENT_TAG");
            }
        });
    }

    private double min3(double one, double two, double three) {

        return (one < two ? (one < three ? one : three) : (two < three ? two : three));
    }

    private double min2(double one, double two) {

        return (one < two ? one : two);
    }

    public double getWalletPayble() {
        double walletPayble = m_totalPayableAmount;
        if (wallet_usage_entry != null) {
            if (wallet_usage_entry.usage_type == 1) {
                walletPayble = m_totalPayableAmount * (wallet_usage_entry.usage_number / 100);
            }


            walletPayble = min3(walletPayble, userWalletAmount, wallet_usage_entry.usage_cap);
        } else
            walletPayble = min2(walletPayble, userWalletAmount);

        walletPayble = Long.valueOf(Math.round(walletPayble)).doubleValue();
        return walletPayble;
    }

    private void applyWallet() {

        userWalletApplied = getWalletPayble();
        userCODAmmount = m_totalPayableAmount - userWalletApplied;

        codAmountTextView.setText("AMOUNT PAYABLE   " + "₹ " + String.valueOf(Math.round(userCODAmmount)));
    }

    private void unApplyWallet() {
        userCODAmmount = m_totalPayableAmount;
        userWalletApplied = 0.0;
        userWalletLeft = userWalletAmount;

        codAmountTextView.setText("AMOUNT PAYABLE   " + "₹ " + String.valueOf(Math.round(userCODAmmount)));
    }


    private void getWalletDetails() {
        Cursor m_cursor = this.getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_userCode = String.valueOf(m_cursor.getInt(1));
                    m_number = m_cursor.getString(3);
                    m_cursor.moveToNext();
                }
            }
        }

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.GetWalletDetailsRequestModel getWalletDetailsRequestModel = requestModel.new GetWalletDetailsRequestModel();
        getWalletDetailsRequestModel.user_code = m_userCode;
        getWalletDetailsRequestModel.owner_type = 0;


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();
        apiSuggestionsService.getWalletDetailsAPICall(getWalletDetailsRequestModel, new Callback<APIResonseModel.GetWalletDetailsResponseModel>() {

            @Override
            public void success(APIResonseModel.GetWalletDetailsResponseModel getWalletDetailsResponseModel, Response response) {

                double usage_number = 0.0;
                double usage_cap = 0.0;
                boolean success = getWalletDetailsResponseModel.success;
                if (success == true) {
                    userWalletAmount = getWalletDetailsResponseModel.wallet.balance;
                    APIResonseModel.GetWalletDetailsResponseModel.UsageEntry usage_entry;
                    ArrayList<APIResonseModel.GetWalletDetailsResponseModel.UsageEntry> wallet_usage;

                    wallet_usage = getWalletDetailsResponseModel.wallet.wallet_usage;

                    if (null != wallet_usage) {
                        int len = wallet_usage.size();
                        for (int i = 0; i < len; i++) {
                            usage_entry = wallet_usage.get(i);
                            if (usage_entry.urid_type.equals("Order")) {
                                wallet_usage_entry = usage_entry;
                                usage_number = usage_entry.usage_number;
                                usage_cap = usage_entry.usage_cap;
                                break;
                            }
                        }
                    }

                    walletAmountTextView.setText(" PAY ₹" + String.valueOf(getWalletPayble() + " THROUGH WALLET"));
                    walletAmountUptoTextView.setText(" (Upto " + usage_number +
                            "% of order amount or ₹ " + usage_cap + ")");
                }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("RetrofitError", error.toString());
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    private void checkAndApplyCouponCode(final String couponCode, String userCode) {

        Tracker coupon = ((SessionManager) getApplicationContext()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        coupon.send(new HitBuilders.EventBuilder()
                .setCategory("coupon")
                .setAction(couponCode)
                .build());

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        APIRequestModel.CouponRequestModel coupon_model_param = requestModel.new CouponRequestModel();
        coupon_model_param.coupon_code = couponCode;
        coupon_model_param.total_amount = String.valueOf(m_totalPayableAmount);
        coupon_model_param.user_code = userCode;
        apiSuggestionsService.applyCoupon(coupon_model_param, new Callback<APIResonseModel.couponResultResponseModel>() {

            @Override
            public void success(APIResonseModel.couponResultResponseModel couponResult, retrofit.client.Response response) {

                int success = couponResult.error;
                if (success == 1) {

                    String couponTypeId = couponResult.coupon_type_id;
                    if (!m_couponApplied) {
                        if (userWalletAmountTV.isChecked())
                            unApplyWallet();
                        else
                            applyWallet();

                        m_couponCodeEditText.setEnabled(false);
                        m_couponApplied = true;


                        if (couponTypeId.equals("1")) {
                            double minAmountRequired = couponResult.minimum_order_amount;
                            if (m_totalPayableAmount > minAmountRequired) {
                                double discountAmount = couponResult.coupon_amount;
                                m_discountAmount = (int) Math.round(discountAmount);
                                if (m_discountAmount > m_totalPayableAmount)
                                    m_totalPayableAmount = 0.0;
                                else
                                    m_totalPayableAmount = m_totalPayableAmount - m_discountAmount;
                                String displayDiscount = "- ₹" + Double.toString(m_discountAmount);
                                m_discountAmountTextView.setText("Discount  " + displayDiscount);
                            } else {
                                String message = "Your total amount is less than " + minAmountRequired;
                                showToast(message);
                            }
                        } else if (couponTypeId.equals("2")) {
                            double minAmountRequired = couponResult.minimum_order_amount;
                            if (m_totalPayableAmount > minAmountRequired) {
                                double discountPercentage = couponResult.coupon_amount;
                                int discountPerc = (int) Math.round(discountPercentage);
                                m_discountAmount = (int) Math.round((m_totalPayableAmount * discountPerc) / 100);

                                if (m_discountAmount > m_totalPayableAmount)
                                    m_totalPayableAmount = 0.0;
                                else
                                    m_totalPayableAmount = m_totalPayableAmount - m_discountAmount;
                                String displayDiscount = "- ₹" + Double.toString(m_discountAmount);
                                m_discountAmountTextView.setText("Discount  " + displayDiscount);
                            } else {
                                String message = "Your total amount is less than " + minAmountRequired;
                                showToast(message);
                            }
                        } else if (couponTypeId.equals("3")) {
                            double discountAmount = couponResult.coupon_amount;
                            m_discountAmount = (int) Math.round(discountAmount);
                            if (m_discountAmount > m_totalPayableAmount)
                                m_totalPayableAmount = 0.0;
                            else
                                m_totalPayableAmount = m_totalPayableAmount - m_discountAmount;
                            String displayDiscount = "- ₹" + Double.toString(m_discountAmount);
                            m_discountAmountTextView.setText("Discount  " + displayDiscount);
                        } else {
                            showToast("Coupon Code expired or not valid");
                        }

                        m_totalAmountTextView.setText("TOTAL BILL   " + "₹" + String.valueOf(m_totalPayableAmount));

                        m_applyCouponCodeButton.setBackgroundColor(Color.parseColor("#ffffff"));
                        m_applyCouponCodeButton.setEnabled(false);

                        walletAmountTextView.setText(" PAY THROUGH WALLET   ₹ " + String.valueOf(getWalletPayble()));

                        if (userWalletAmountTV.isChecked())
                            applyWallet();
                        else
                            unApplyWallet();
                    }
                    PayloadBuilder builder = new PayloadBuilder();
                    builder.putAttrDouble("TotalBillAmount", m_totalPayableAmount);
                    builder.putAttrInt("DiscountAmount", m_discountAmount);
                    builder.putAttrString("CouponCode", couponCode);
                    builder.putAttrString("Activity Name", this.getClass().getCanonicalName());
                    MoEHelper.getInstance(ConfirmationMessageActivity.this).trackEvent("Order Details", builder.build());

                } else {
                    showToast("Coupon Code expired or not valid");
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.ConfirmationMessageActivity");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    public void viewCart(View v) {
        Intent intent = new Intent(ConfirmationMessageActivity.this, ShoppingCart.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        ConfirmationMessageActivity.super.onBackPressed();
        overridePendingTransition(R.anim.anim_zoom_out_back, R.anim.anim_slideup_back);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_confirmation_message, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
        mHelper.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
        mHelper.onResume(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }

    /**
     * Call for COD and PayU
     *
     * @param view - Button which is clicked(PayU or COD)
     */
    public void onClicked(View view) {

        if (!isDeliverySlotSet) {
            Toast.makeText(this, getString(R.string.select_delivery_time), Toast.LENGTH_SHORT).show();
            return;
        }
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = Utils.providesRestAdapter("http://54.169.62.100");
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);

        switch (view.getId()) {
            case R.id.place_order_button:
                APIRequestModel.SendOrderDetailsRequestModel sendOrderDetailsRequestModel = getOrderDetailsRequestModel("COD");

                APISuggestionsService sendorderObj = restAdapter.create(APISuggestionsService.class);
                sendorderObj.sendOrderDetailsAPI(sendOrderDetailsRequestModel, new Callback<APIResonseModel.SendOrderDetailsResponseModel>() {

                    @Override
                    public void success(APIResonseModel.SendOrderDetailsResponseModel orderSnippet, retrofit.client.Response response) {
                        boolean success = orderSnippet.success;
                        double totalAmount = 0.0;
                        ArrayList<LastActivitySnippet> postOrderArrayList = null;
                        Log.i("success", success + "");
                        if (success) {
                            m_placeOrderButton.setEnabled(true);
                            totalAmount = orderSnippet.total_amount;
                            Log.i("total amount on success", totalAmount + "");
                            postOrderArrayList = orderSnippet.postOrderResponseModelArrayList;
                            Log.i("total list on success", postOrderArrayList + "");

                            trackPayment("COD");

                            progressDialog.dismiss();
                            Intent intent = new Intent(ConfirmationMessageActivity.this, LastActivity.class);
                            intent.putParcelableArrayListExtra(LastActivity.FINAL_LIST_OF_ORDERS_TAG, postOrderArrayList);
                            intent.putExtra(LastActivity.TOTAL_AMOUNT_OF_ORDERS_TAG, totalAmount);
                            intent.putExtra(LastActivity.CALLING_ACTIVITY_TAG, 0);
                            startActivity(intent);
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(ConfirmationMessageActivity.this, "Some server error occurred", Toast.LENGTH_SHORT).show();
                            m_placeOrderButton.setEnabled(true);

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(TAG, error.toString());
                        progressDialog.dismiss();
                        m_placeOrderButton.setEnabled(true);

                    }
                });
                break;
            case R.id.pay_via_payU_button:
                APIRequestModel requestModel = new APIRequestModel();
                final APIRequestModel.PayUParamsRequestModel payUParamsRequestModel = requestModel.new PayUParamsRequestModel();
                payUParamsRequestModel.user_code = m_userCode;
                payUParamsRequestModel.amount = String.valueOf(m_totalPayableAmount);
                payUParamsRequestModel.orderDict = getOrderDetailsRequestString(getOrderDetailsRequestModel("Online"));
                payUParamsRequestModel.prod_info = "Groceries from LazyLad";

                APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
                apiSuggestionsService.getPaymentParameters(payUParamsRequestModel, new Callback<APIResonseModel.PayUResponseModel>() {
                    @Override
                    public void success(APIResonseModel.PayUResponseModel payUResponseModel, Response response) {

                        if (payUResponseModel.success) {
                            // MoEngage Tracking
                            trackPayment("Online");

                            PaymentParams paymentParams = new PaymentParams();
                            paymentParams.setKey(payUResponseModel.key);
                            paymentParams.setAmount(String.valueOf(m_totalPayableAmount));
                            paymentParams.setProductInfo("Groceries from LazyLad");
                            APIResonseModel.UserDetails userDetails = payUResponseModel.user_details;
                            paymentParams.setFirstName(userDetails.first_name);
                            paymentParams.setLastName(userDetails.last_name != null ? userDetails.last_name : "");
                            paymentParams.setEmail(userDetails.user_email);
                            paymentParams.setPhone(userDetails.phone_number);
                            paymentParams.setUdf1(payUResponseModel.udf1 != null ? payUResponseModel.udf1 : "");
                            paymentParams.setUdf2(payUResponseModel.udf2 != null ? payUResponseModel.udf2 : "");
                            paymentParams.setUdf3(payUResponseModel.udf3 != null ? payUResponseModel.udf3 : "");
                            paymentParams.setUdf4(payUResponseModel.udf4 != null ? payUResponseModel.udf4 : "");
                            paymentParams.setUdf5(payUResponseModel.udf5 != null ? payUResponseModel.udf5 : "");
                            paymentParams.setSurl(payUResponseModel.surl);
                            paymentParams.setFurl(payUResponseModel.furl);
                            mTransId = payUResponseModel.trans_id;
                            paymentParams.setTxnId(mTransId);
                            // TODO - For saved card
                            //paymentParams.setUserCredentials(payUResponseModel.user_credentials);

                            APIResonseModel.PayUResponseModel.Hash hash = payUResponseModel.payUHash;

                            PayuHashes payuHashes = new PayuHashes();
                            payuHashes.setPaymentHash(hash.payment_hash);
                            payuHashes.setMerchantIbiboCodesHash(hash.get_merchant_ibibo_codes_hash);
                            payuHashes.setVasForMobileSdkHash(hash.vas_for_mobile_sdk_hash);
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(hash.payment_related_details_for_mobile_sdk_hash);
                            payuHashes.setDeleteCardHash(hash.delete_user_card_hash);
                            payuHashes.setStoredCardsHash(hash.get_user_cards_hash);
                            payuHashes.setEditCardHash(hash.edit_user_card_hash);
                            payuHashes.setSaveCardHash(hash.save_user_card_hash);
                            PayuConfig payuConfig = new PayuConfig();
                            payuConfig.setEnvironment(PayuConstants.PRODUCTION_ENV);

                            String salt = payUResponseModel.salt;

                            progressDialog.dismiss();

                            navigateToPayUActivity(paymentParams, payuConfig, payuHashes, salt);

                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(ConfirmationMessageActivity.this, "Some server error occurred", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(TAG, error.toString());
                        progressDialog.dismiss();
                        showToast(getString(R.string.network_error));
                    }
                });
                break;
        }

    }

    /**
     * Get the order details request object
     *
     * @return - SendOrderDetailsRequestModel object
     */
    private APIRequestModel.SendOrderDetailsRequestModel getOrderDetailsRequestModel(String paymentMode) {
        epochExpDeliveryTime = Utils.getEpochTimeByTimeSlot(pickedTimeTextView.getText().toString());

        JSONArray sellerJsonArr = new JSONArray();
        for (int i = 0; i < m_deliveryChargesList.size(); i++) {
            JSONObject sellerJsonObj = new JSONObject();
            String seller_code = m_spCodesList.get(i);
            int delivery_charges = m_deliveryChargesList.get(i);
            int applied_tax = m_sellerAppliedTaxList.get(i);
            try {
                sellerJsonObj.put("sp_code", seller_code);
                sellerJsonObj.put("delivery_charges", delivery_charges);
                sellerJsonObj.put("taxes", applied_tax);
                sellerJsonArr.put(sellerJsonObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        String[] projection = {ShoppingCartEntry.COLUMN_ITEM_CODE, ShoppingCartEntry.COLUMN_ITEM_COST,
                ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED, ShoppingCartEntry.COLUMN_ITEM_SP_CODE, ShoppingCartEntry.COLUMN_ITEM_OFFER_PRICE, ShoppingCartEntry.COLUMN_ITEM_IN_OFFER};

        Cursor cursor = getContentResolver().query(
                ShoppingCartEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);

        JSONArray itemJsonArr = new JSONArray();

        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    JSONObject itemJsonObj = new JSONObject();
                    String itemCode = cursor.getString(0);
                    double itemCostSelected = cursor.getDouble(1);
                    int itemQuantitySelected = cursor.getInt(2);
                    String sp_code = cursor.getString(3);
                    String offer_price = cursor.getString(4);
                    int item_in_offer = cursor.getInt(5);
                    try {
                        itemJsonObj.put("itemCode", itemCode);
                        itemJsonObj.put("itemQuantitySelected", itemQuantitySelected);
                        itemJsonObj.put("itemCost", itemCostSelected);
                        itemJsonObj.put("sp_code", sp_code);
                        itemJsonObj.put("itemOfferCost", offer_price);
                        itemJsonObj.put("itemOfferFlag", item_in_offer);
                        itemJsonArr.put(itemJsonObj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    cursor.moveToNext();
                }
            }
        }
        if (cursor != null)
            cursor.close();

        JSONObject paymentJsonObject = new JSONObject();
        try {
            if (paymentMode.equalsIgnoreCase("Online")) {
                paymentJsonObject.put("COD", 0.0);
                paymentJsonObject.put("ONLINE", userCODAmmount);
            } else {
                paymentJsonObject.put("COD", userCODAmmount);
                paymentJsonObject.put("ONLINE", 0.0);
            }

            paymentJsonObject.put("WALLET", userWalletApplied);

            paymentJsonObject.put("PROMOTIONAL", m_totalOfferAmount);


        } catch (JSONException e) {
            e.printStackTrace();


        }

        APIRequestModel requestModel = new APIRequestModel();
        APIRequestModel.SendOrderDetailsRequestModel sendOrderDetailsRequestModel = requestModel.new SendOrderDetailsRequestModel();
        sendOrderDetailsRequestModel.seller_details = sellerJsonArr.toString();
        sendOrderDetailsRequestModel.JsonDataArray = itemJsonArr.toString();
        sendOrderDetailsRequestModel.address_code = m_addressCode;
        sendOrderDetailsRequestModel.user_exp_del_time = Long.toString(epochExpDeliveryTime);
        sendOrderDetailsRequestModel.total_items_amount = Double.toString(m_totalItemsAmount);
        sendOrderDetailsRequestModel.order_payment_details = paymentJsonObject.toString();
        sendOrderDetailsRequestModel.user_code = m_userCode;
        sendOrderDetailsRequestModel.couponCode = couponCode;
        sendOrderDetailsRequestModel.tot_amount = Double.toString(m_totalPayableAmount);
        sendOrderDetailsRequestModel.address_user_code = m_address_userCode;
        sendOrderDetailsRequestModel.discount_amount = Integer.toString(m_discountAmount);
        sendOrderDetailsRequestModel.delivery_charges = Integer.toString(m_deliveryCharges);

        return sendOrderDetailsRequestModel;
    }

    private void navigateToPayUActivity(PaymentParams paymentParams, PayuConfig payuConfig, PayuHashes payuHashes, String salt) {
        Intent intent = new Intent(this, PayUBaseActivity.class);
        intent.putExtra(PayuConstants.SALT, salt);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, paymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);
        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);

        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (data != null) {

                String merchantData = data.getStringExtra("result"); // Data received from surl/furl
                Log.d(TAG, "merchantData " + merchantData);
                String payuData = data.getStringExtra("payu_response"); // Response received from payu

                if (merchantData != null && merchantData.equalsIgnoreCase("Success")) {
                    Intent intent = new Intent(this, PaymentRedirectionActivity.class);
                    intent.putExtra(PaymentRedirectionActivity.TRANSACTION_ID_TAG, mTransId);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, getString(R.string.payment_unsuccessful), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.payment_unsuccessful), Toast.LENGTH_LONG).show();
            }
        }
    }

    private String getOrderDetailsRequestString(APIRequestModel.SendOrderDetailsRequestModel sendOrderDetailsRequestModel) {

        JSONObject sendOrderDicJsonObject = new JSONObject();
        try {
            sendOrderDicJsonObject.put("seller_details", sendOrderDetailsRequestModel.seller_details);
            sendOrderDicJsonObject.put("JsonDataArray", sendOrderDetailsRequestModel.JsonDataArray);
            sendOrderDicJsonObject.put("address_code", sendOrderDetailsRequestModel.address_code);
            sendOrderDicJsonObject.put("user_exp_del_time", sendOrderDetailsRequestModel.user_exp_del_time);
            sendOrderDicJsonObject.put("total_items_amount", sendOrderDetailsRequestModel.total_items_amount);
            sendOrderDicJsonObject.put("order_payment_details", sendOrderDetailsRequestModel.order_payment_details);
            sendOrderDicJsonObject.put("user_code", sendOrderDetailsRequestModel.user_code);
            sendOrderDicJsonObject.put("couponCode", sendOrderDetailsRequestModel.couponCode);
            sendOrderDicJsonObject.put("tot_amount", sendOrderDetailsRequestModel.tot_amount);
            sendOrderDicJsonObject.put("address_user_code", sendOrderDetailsRequestModel.address_user_code);
            sendOrderDicJsonObject.put("discount_amount", sendOrderDetailsRequestModel.discount_amount);
            sendOrderDicJsonObject.put("delivery_charges", sendOrderDetailsRequestModel.delivery_charges);


        } catch (JSONException e) {
            e.printStackTrace();

        }
        return sendOrderDicJsonObject.toString();
    }

    private void trackPayment(String paymentMode) {
        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("UserCode", m_userCode);
        builder.putAttrDouble("TotalItemsAmount", m_totalItemsAmount);
        builder.putAttrDouble("TotalBillAmount", m_totalPayableAmount);
        builder.putAttrInt("DiscountAmount", m_discountAmount);
        builder.putAttrInt("DeliveryCharge", m_deliveryCharges);
        builder.putAttrString("CouponCode", couponCode);
        builder.putAttrString("AddressUserCode", m_address_userCode);
        if (paymentMode.equalsIgnoreCase("COD")) {
            builder.putAttrDouble("CODAmount", userCODAmmount);
        } else {
            builder.putAttrDouble("CODAmount", 0);
        }
        builder.putAttrDouble("PaidThroughWallet", userWalletApplied);
        builder.putAttrLong("UserEpDeliveryTime", epochExpDeliveryTime);
        builder.putAttrString("PaymentMode", paymentMode);
        builder.putAttrString("Activity Name", this.getClass().getCanonicalName());
        MoEHelper.getInstance(ConfirmationMessageActivity.this).trackEvent("PlaceOrder", builder.build());
    }

    @Override
    public void onSetClicked(String timeSlot, int serviceCode) {
        if (serviceCode == Constants.GROCERIES_DELIVERY_SLOT_CODE && !timeSlot.isEmpty()) {
            isDeliverySlotSet = true;
            String[] splits = timeSlot.split(" ");
            String date = splits[0];
            String time = "";
            for (int i = 1; i < splits.length; i++) {
                time += splits[i] + " ";
            }

            pickedTimeTextView.setText(date + "\n" + time);
        }

    }

}