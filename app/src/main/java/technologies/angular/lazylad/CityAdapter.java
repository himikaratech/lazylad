package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Amud on 20/08/15.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

    private ArrayList<CitySnippet> m_city_list;
    private int itemLayout;
    private Activity m_activity;
    int viewFlag = 0;
    private SharedPreferences prefs_location;

    public CityAdapter(ArrayList<CitySnippet> items, int itemLayout, Context context, int viewFlag) {
        m_activity = (Activity) context;
        this.m_city_list = items;
        this.itemLayout = itemLayout;
        this.viewFlag = viewFlag;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v, viewFlag);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (viewFlag == 0) {
            if (m_city_list != null) {
                for (int i = 0; i < m_city_list.size(); i++) {
                    Log.e("name", m_city_list.get(i).m_cityName);
                    Log.e("address", m_city_list.get(i).m_city_image_add);
                }
                final String city_name = ((CitySnippet) getItem(position)).m_cityName;
                final String city_image_address = ((CitySnippet) getItem(position)).m_city_image_add;

                CitySnippet item = m_city_list.get(position);

                holder.cityNameEditText.setText(city_name);

                Picasso.with(m_activity)
                        .load(city_image_address)
                        .placeholder(R.drawable.cityimage)
                        .fit().centerCrop()
                        .error(R.drawable.cityimage)
                        .into(holder.cityImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.cityNameEditText.setTextColor(Color.parseColor("#ffffffff"));
                            }

                            @Override
                            public void onError() {

                            }
                        });

                holder.cityRelativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String m_cityCode = m_city_list.get(holder.getAdapterPosition()).m_cityCode;
                        String m_cityNameSelected = m_city_list.get(holder.getAdapterPosition()).m_cityName;
                        String m_city_image_addSelected = m_city_list.get(holder.getAdapterPosition()).m_city_image_add;
                        int m_city_image_flagSelected = m_city_list.get(holder.getAdapterPosition()).m_city_image_flag;
                        prefs_location = m_activity.getSharedPreferences(
                                "technologies.angular.lazylad", Context.MODE_PRIVATE);

                        Intent intent = new Intent(m_activity, AreaActivity.class);
                        intent.putExtra("cityCode", m_cityCode);
                        intent.putExtra("cityNameSelected", m_cityNameSelected);
                        intent.putExtra("city_image_addSelected", m_city_image_addSelected);
                        intent.putExtra("city_image_flagSelected", m_city_image_flagSelected);
                        intent.putExtra("m_cityNameSelected", m_cityNameSelected);
                        m_activity.startActivity(intent);
                        m_activity.finish();
                    }
                });
            }
            if (viewFlag == 1) {
                final String servProvName = ((ServiceProvidersListSnippet) getItem(position)).m_servProvName;
                holder.cityNameEditText.setText(servProvName);
            }
        }
    }

    @Override
    public int getItemCount() {
        return m_city_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView cityImageView;
        TextView cityNameEditText;
        FrameLayout cityRelativeLayout;

        public ViewHolder(View itemView, int flag) {
            super(itemView);
            if (flag == 0) {
                cityImageView = (ImageView) itemView.findViewById(R.id.city_image);
                cityNameEditText = (TextView) itemView.findViewById(R.id.city_name);
                cityRelativeLayout = (FrameLayout) itemView.findViewById(R.id.relative_layout_city_list);
            }
            if (flag == 1) {

                cityRelativeLayout = (FrameLayout) itemView.findViewById(R.id.relative_layout_city_list);
                cityNameEditText = (TextView) itemView.findViewById(R.id.city_name);
            }
        }
    }

    public Object getItem(int position) {
        if (m_city_list != null) {
            return m_city_list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
