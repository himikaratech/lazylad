package technologies.angular.lazylad;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ru.noties.scrollable.CanScrollVerticallyDelegate;

/**
 * Created by Amud on 09/09/15.
 */
public  class OfferFragment extends Fragment implements CanScrollVerticallyDelegate {

    private String str;
    TextView txtview;
    ImageView dotImage;
    ImageView dotImage2;
    int position;
    View mainView;
    String offer_address;

    public OfferFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        position = getArguments().getInt("position");
        offer_address = getArguments().getString("offer_address");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.offerfragment, container, false);
        Log.e("fianl fragment", "working");
        ImageView imageView = (ImageView) v.findViewById(R.id.offerImage);
        Picasso.with(getActivity())
                .load(offer_address)
                .placeholder(R.drawable.loading)
                .error(R.drawable.no_image)
                .into(imageView);

        return v;
    }

    @Override
    public boolean canScrollVertically(int i) {
        return false;
    }
}
