package technologies.angular.lazylad;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

/**
 * Created by Amud on 09/09/15.
 */

public class AboutUsFragment extends Fragment {
    private int m_aboutFragmentCode;
    private String currentURL;
    WebView wv1;
    String postUrl;

    public void init(String url) {
        currentURL = url;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        m_aboutFragmentCode = getArguments().getInt("m_aboutFragmentCode");
        currentURL = "http://54.169.62.100/task_manager/v1/aboutUsInfoforId";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.about_us_fragment_layout, container, false);
        wv1 = (WebView) rootView.findViewById(R.id.webview);
        aboutUsDetail();
        return rootView;
    }


    private void aboutUsDetail() {

        final ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.AboutUsDetailRequestModel aboutUsDetailRequestModel = requestModel.new AboutUsDetailRequestModel();
        aboutUsDetailRequestModel.info_id = m_aboutFragmentCode;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        apiSuggestionsService.aboutUsDetailAPICall(aboutUsDetailRequestModel, new retrofit.Callback<APIResonseModel.AboutUsDetailResponseModel>() {

            @Override
            public void success(APIResonseModel.AboutUsDetailResponseModel aboutUsDetailResponseModel, retrofit.client.Response response) {

                int success = aboutUsDetailResponseModel.error;
                if (success == 1) {
                    String aboutusDetail = aboutUsDetailResponseModel.info_text;
                    startWebView(aboutusDetail);
                    progressDialog.dismiss();
                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();

            }
        });
    }

    private void startWebView(String url) {
        Log.e("url", url);

        wv1.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

        });

        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.getSettings().setLoadWithOverviewMode(false);
        wv1.getSettings().setUseWideViewPort(false);
        WebSettings ws = wv1.getSettings();
        ws.setJavaScriptEnabled(true);
        //wv1.postUrl(url, EncodingUtils.getBytes(postUrl, "UTF-8"));
        wv1.loadData(url, "text/html; charset=UTF-8", null);
    }
}
