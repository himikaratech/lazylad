package technologies.angular.lazylad;

/**
 * Created by Amud on 30/09/15.
 */
public class NumberParameters {

    public static String HOME_SKIP_CONSTANT = "HOME_SKIP";
    public static String HOME_VERIFIED_CONSTANT = "HOME_VERIFIED";

    public static String ADDRESS_SKIP_CONSTANT = "ADDRESS _SKIP";
    public static String ADDRESS_VERIFIED_CONSTANT = "ADDRESS_VERIFIED";

    public static String REFERRAL_SKIP_CONSTANT = "REFERRAL_SKIP";
    public static String REFERRAL_VERIFIED_CONSTANT = "REFERRAL_VERIFIED";

    public static String WALLET_SKIP_CONSTANT = "WALLET_SKIP";
    public static String WALLET_VERIFIED_CONSTANT = "WALLET_VERIFIED";

    public static String SKIPPABLE_CONSTANT = "SKIPPABLE";
    public static String SITUATION_CONTANT = "SITUATION";
    public static String PREF_STRING_CONTANT = "PREF_STRING";
    public static String PREF_VERIFIED_CONSTANT = "PREF_VERIFIED";
}
