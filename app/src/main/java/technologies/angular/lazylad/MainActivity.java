package technologies.angular.lazylad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.moe.pushlibrary.utils.MoEHelperConstants;
import com.moengage.addon.inbox.InboxManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.ServiceTypesEntry;


public class MainActivity extends ActionBarActivity {
    public static String mAddressOutput;
    public static String mAddressCode;
    private int m_userCode;
    private SessionManager m_sessionManager;

    /**
     * Displays the location address.
     */

    String TITLES[] = {"Home", "Notifications", "Addresses", "Order History", "Share & Earn", "Lazy Wallet", "", "Rate Us", "Feedback", "Call Us", "About Us"};
    int ICONS[] = {R.drawable.home, R.drawable.notification, R.drawable.address, R.drawable.orders, R.drawable.share, R.drawable.wallet, R.drawable.ic_launcher, R.drawable.rate, R.drawable.feedback, R.drawable.call, R.drawable.aboutus};
    //Similarly we Create a String Resource for the
    // and email in the header view
    //And we also create a int resource for profile picture in the header view

    String Name = "Add number";
    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    public ActionBarDrawerToggle mDrawerToggle;
    protected TextView mLocationAddressTextView;
    private int m_numOfServiceTypes;
    private String m_servTypeCode;
    private String name = "test";
    private String email;
    private String m_name;
    private String m_email;
    private TextView m_location;
    private ImageButton cartImage;
    private ImageView dotImage;

    private ArrayList<ServiceTypeSnippet> m_servTypeListDetails;

    private ArrayList<AreaSnippet> m_areasListSnippet;
    private List<String> m_areaList;
    private SharedPreferences prefs;
    private SharedPreferences prefs_location;
    private String city_code;
    private String areaName;
    private String cityName;
    private String city_image_add;
    private int city_image_add_flag;
    private Double latitude;
    private Double longitude;

    Boolean isInternetPresent = true;
    ConnectionDetector cd;
    Toolbar toolbar;
    ViewPager pager;
    MainScreenPagerAdapter adapter;
    int Numboftabs = 0;
    private View frame;
    private boolean m_app_start_flag;
    private boolean activtyFlag = true;

    BroadcastReceiver resultReceiver;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(getApplicationContext());

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (isInternetPresent) {
            check();
        } else {
            Toast.makeText(MainActivity.this, "Sorry , Internet connection is not avaiable", Toast.LENGTH_SHORT).show();
        }
    }

    private void check() {

        prefs_location = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        int shall_version_allow = prefs_location.getInt(PreferencesStore.SHALL_VERSION_ALLOW_CONSTANT, PreferencesStore.DEFAULT_SHALL_VERSION_ALLOW_CONSTANT);
        if (shall_version_allow != PreferencesStore.DEFAULT_SHALL_VERSION_ALLOW_CONSTANT) {
            startActivity(new Intent(this, (MustUpdate.class)));
            this.finish();
        }

        setContentView(R.layout.main);
        m_sessionManager = new SessionManager(this);
        prefs = getPreferences(Context.MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Intent intent = getIntent();


        m_app_start_flag = prefs_location.getBoolean(PreferencesStore.APP_START_FLAG_CONSTANT, false);

        if (m_app_start_flag == true) {
            Log.e("app_start", m_app_start_flag + "");
            loadOffer();
            prefs_location.edit().putBoolean(PreferencesStore.APP_START_FLAG_CONSTANT, false).commit();
            m_app_start_flag = false;
        }

        city_code = prefs_location.getString(PreferencesStore.CITY_CODE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_CODE_SELECTED_CONSTANT);
        areaName = prefs_location.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, PreferencesStore.DEFAULT_AREA_SELECTED_CONSTANT);
        cityName = prefs_location.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT);
        city_image_add = prefs_location.getString(PreferencesStore.CITY_IMAGE_ADD_CONSTANT, "");
        city_image_add_flag = prefs_location.getInt(PreferencesStore.CITY_IMAGE_ADD_FLAF_CONSTANT, 0);
        latitude = Double.longBitsToDouble(prefs_location.getLong(PreferencesStore.LATITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LATITUDE_SELECTED_CONSTANT));
        longitude = Double.longBitsToDouble(prefs_location.getLong(PreferencesStore.LONGITUDE_SELECTED_CONSTANT, PreferencesStore.DEFAULT_LONGITUDE_SELECTED_CONSTANT));


        if (cityName.equals(PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT))
            mAddressOutput = this.getResources().getString(R.string.please_select_city);
        else
            mAddressOutput = areaName + "," + " " + cityName;

        m_location = (TextView) findViewById(R.id.location_text);
        cartImage = (ImageButton) findViewById(R.id.shopping_cart_image);

        m_location.setText(mAddressOutput);
        m_location.setPaintFlags(m_location.getPaintFlags());
        m_location.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                ShoppingCartUtils.alertDestroyCart(v.getContext(), new Utils.Handler() {
                    @Override
                    public void execute(Object data) {
                        Boolean b_proceed = (Boolean) data;
                        if (b_proceed) {
                            Intent intent = new Intent(getApplicationContext(), GeoLocation.class);
                            intent.putExtra("parent", MainActivity.class.getSimpleName());
                            startActivity(intent);
                              finish();
                        }
                    }
                });
            }
        });

        cartImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(getApplicationContext(), ShoppingCartOnMainScreen.class);
                startActivity(intent);
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView); // Assigning the RecyclerView Object to the xml View

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayViewWidth_ = size.x;

        frame = (View) findViewById(R.id.content_frame);

        mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, MainActivity.this, 1);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.width = 9 * (displayViewWidth_ / 10);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutParams(params);

        if (city_image_add == null || city_image_add == "") city_image_add = "\"\"";
        final ImageView bannerImage = (ImageView) findViewById(R.id.banner);
        Picasso.with(MainActivity.this)
                .load(city_image_add)
                .placeholder(R.drawable.cityimage)
                .error(R.drawable.cityimage)
                .fit().centerCrop()
                .into(bannerImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        m_location.setTextColor(Color.parseColor("#ffffff"));
                    }

                    @Override
                    public void onError() {
                    }
                });

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.opendrawer, R.string.closedrawer) {

            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (mRecyclerView.getWidth() * slideOffset);
                frame.setTranslationX(moveFactor);
            }
        };
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State

        Cursor m_cursor = getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    m_userCode = m_cursor.getInt(1);
                    Log.d("user_code", String.valueOf(m_userCode));
                    m_cursor.moveToNext();
                }
            } else {
                m_userCode = 0;
                //TODO Some UI Element asking to add address
            }
        } else {
            m_userCode = 0;
            //TODO Error Handling
        }

        setTracker();
        mHelper = new MoEHelper(this);
        if (m_userCode != 0) {
            MoEHelper.getInstance(this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_UNIQUE_ID, m_userCode);
            MoEHelper.getInstance(this).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_LOCATION, city_code);
        }
        InboxManager.getInstance().setInboxAdapter(new CustomAdapter());

        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("City", cityName);
        builder.putAttrString("Area", areaName);
        builder.putAttrDouble("Latitude", latitude);
        builder.putAttrDouble("Longitude", longitude);
        builder.putAttrString("Activity Name", this.getClass().getSimpleName());
        MoEHelper.getInstance(this).trackEvent("Main Activity Info", builder.build());

    }

    public void retry(View v) {
        isInternetPresent = isNetworkAvailable();
        if (isInternetPresent) {
            check();
        } else {
            Toast.makeText(MainActivity.this, "Sorry , Internet connection is not avaiable", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadOffer() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.loadOfferAPICall(new retrofit.Callback<APIResonseModel.LoadOfferResponseModel>() {

            @Override
            public void success(APIResonseModel.LoadOfferResponseModel loadOfferResponseModel, retrofit.client.Response response) {

                int success = loadOfferResponseModel.error;
                if (success == 1) {
                    ArrayList<offerSnippet> offer_details_list = new ArrayList<offerSnippet>();
                    offer_details_list = loadOfferResponseModel.offer_details;
                    if (!offer_details_list.isEmpty()) {
                        if (activtyFlag == true) {
                            MainScreenDialogFragment dialog = new MainScreenDialogFragment();
                            Bundle bundle = new Bundle();
                            bundle.putParcelableArrayList("offer_list", offer_details_list);
                            dialog.setArguments(bundle);
                            dialog.show(getSupportFragmentManager(), "Offer");
                        }
                    }

                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        activtyFlag = false;
        AppEventsLogger.deactivateApp(this);
        mHelper.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activtyFlag = true;
        Bundle bundle = new Bundle();
        Utils.verify_num(this, bundle, 1, true, NumberParameters.HOME_SKIP_CONSTANT, NumberParameters.HOME_VERIFIED_CONSTANT);

        getServiceTypesFromDatabase();
        AppEventsLogger.activateApp(this);
        mHelper.onResume(this);

        cityName = prefs_location.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT);
        areaName = prefs_location.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, PreferencesStore.DEFAULT_AREA_SELECTED_CONSTANT);
        if (cityName.equals(PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT))
            mAddressOutput = this.getResources().getString(R.string.please_select_city);
        else
            mAddressOutput = areaName + "," + " " + cityName;
        m_location = (TextView) findViewById(R.id.location_text);
        m_location.setText(mAddressOutput);


    }

    @Override
    public void onStop() {
        super.onStop();
        activtyFlag = false;
        Drawer.closeDrawer(Gravity.LEFT);
        mHelper.onStop(this);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.MainActivity");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void startServiceProviderListActivity() {
        Intent intent = new Intent(MainActivity.this, ServiceProvidersList.class);
        String[] details = new String[]{m_servTypeCode, mAddressOutput};
        intent.putExtra("ServiceProvidersList", details);
        MainActivity.this.startActivity(intent);
        MainActivity.this.finish();
    }

    private void getServiceTypesFromDatabase() {
        final int number_cat;
        Cursor m_cursor = getContentResolver().query(
                ServiceTypesEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {

            m_servTypeListDetails = new ArrayList<ServiceTypeSnippet>(m_cursor.getCount());
            number_cat = m_cursor.getCount();
            Numboftabs = (int) (Math.ceil(((double) number_cat) / 4.0));  //number of fragments
            LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.dotLinearLayout);

            LinearLayout.LayoutParams layoutParams;
            layoutParams = new LinearLayout.LayoutParams(8, 8);
            layoutParams.setMargins(10, 0, 0, 0);
            linearLayout1.removeAllViews();


            for (int x = 1; x <= Numboftabs; x++) {
                Log.e("Numboftabs1", Numboftabs + "");
                ImageView image = new ImageView(MainActivity.this);
                image.setId(x);
                Log.e("image", image + "");
                linearLayout1.addView(image, layoutParams);
                dotImage = (ImageView) findViewById(x);
                dotImage.setImageResource(R.drawable.inactive);

            }

            dotImage = (ImageView) findViewById(1);
            dotImage.setImageResource(R.drawable.active);

            for (int i = 0; i < m_cursor.getCount(); i++) {

                int id = m_cursor.getInt(0);
                int st_code = m_cursor.getInt(1);
                String st_name = m_cursor.getString(2);
                int img_flag = m_cursor.getInt(3);
                String img_add = m_cursor.getString(4);
                int servTypeFlag = m_cursor.getInt(5);
                int isService = m_cursor.getInt(6);

                ServiceTypeSnippet srvTypeSnipObj = new ServiceTypeSnippet(id, st_code, st_name, img_flag, img_add, servTypeFlag, isService);
                m_servTypeListDetails.add(srvTypeSnipObj);
                m_cursor.moveToNext();
            }

            // Creating The MainScreenPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
            adapter = new MainScreenPagerAdapter(getSupportFragmentManager(), Numboftabs, this, 0);

            // Assigning ViewPager View and setting the adapter
            pager = (ViewPager) findViewById(R.id.pager);

            pager.setAdapter(adapter);

        }
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.e("logging2", position + "");
                position = position + 1;
                for (int i = 1; i <= Numboftabs; i++) {
                    dotImage = (ImageView) findViewById(i);
                    dotImage.setImageResource(R.drawable.inactive);
                }
                dotImage = (ImageView) findViewById(position);
                dotImage.setImageResource(R.drawable.active);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
            // tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

            // Setting Custom Color for the Scroll bar indicator of the Tab View
            //    m_servTypeGridView.setAdapter((ListAdapter) m_servTypeAdapter);
        });
    }

    /**
     * Updates the address in the UI.
     */
    protected void displayAddressOutput() {
        mLocationAddressTextView.setText(mAddressOutput);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
