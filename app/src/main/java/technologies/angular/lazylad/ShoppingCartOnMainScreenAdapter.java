package technologies.angular.lazylad;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import technologies.angular.lazylad.LazyContract.ShoppingCartEntry;
import technologies.angular.lazylad.LazyContract.OrderDetailsEntry;

import java.util.ArrayList;

/**
 * Created by Saurabh on 12/02/15.
 */
public class ShoppingCartOnMainScreenAdapter extends BaseAdapter {
    private ArrayList<ItemSnippet> m_itemsDetails;
    private Context m_activity;
    private Context m_appContext;

    final private int TYPE_WITH_IMAGE = 0;
    final private int TYPE_WITHOUT_IMAGE = 1;


    public ShoppingCartOnMainScreenAdapter(Context activity,Context app_context, ArrayList<ItemSnippet> itemsDetails) {
        m_activity = activity;
        m_appContext=app_context;
        m_itemsDetails = itemsDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_itemsDetails != null) {
            count = m_itemsDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_itemsDetails != null) {
            return m_itemsDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_itemsDetails != null) {
            return m_itemsDetails.get(position).m_itemId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        if (m_itemsDetails != null) {
            final String itemName = ((ItemSnippet) getItem(position)).m_itemName;
            final String itemCode = ((ItemSnippet) getItem(position)).m_itemCode;
            int itemSelected = ((ItemSnippet) getItem(position)).m_itemSelected;
            final double itemCost = ((ItemSnippet) getItem(position)).m_itemCost;
            int itemQuantitySelected = ((ItemSnippet) getItem(position)).m_itemQuantitySelected;
            final int itemPosition = position;
            String itemImageAdd = ((ItemSnippet) getItem(position)).m_itemImgAddress;
            final String itemDesc = ((ItemSnippet) getItem(position)).m_itemDesc;
            final int itemImgFlag = ((ItemSnippet) getItem(position)).m_itemImgFlag;
            final String itemUnit = ((ItemSnippet) getItem(position)).m_itemUnit;
            final String itemShortDesc = ((ItemSnippet) getItem(position)).m_itemShortDesc;
            final String itemStatus = ((ItemSnippet) getItem(position)).m_itemStatus;
            final String itemStCode = ((ItemSnippet) getItem(position)).m_itemServiceType;
            final String itemScCode = ((ItemSnippet) getItem(position)).m_itemServiceCategory;
            final String itemSpCode = ((ItemSnippet) getItem(position)).m_itemServiceProvider;
            final String sellerName = ((ItemSnippet) getItem(position)).seller_Name;
            final double itemOfferPirce = ((ItemSnippet) getItem(position)).m_itemOfferPrice;
            final int itemInOffer = ((ItemSnippet) getItem(position)).m_itemInOffer;


            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (type == TYPE_WITH_IMAGE)
                    convertView = infalInflater.inflate(R.layout.shoping_cart_display_list_view, (ViewGroup) null);
                else
                    convertView = infalInflater.inflate(R.layout.shoping_cart_display_list_no_image_view, (ViewGroup) null);
            }
            TextView itemNameTextView = (TextView) convertView.findViewById(R.id.shopping_cart_item_name);
            final TextView itemQuantitySelectedTextView = (TextView) convertView.findViewById(R.id.shopping_cart_item_quantity_selected);
            Button m_shoppingCartItemsQuantityPlusButton = (Button) convertView.findViewById(R.id.shopping_cart_item_quantity_plus);
            Button m_shoppingCartItemsQuantityMinusButton = (Button) convertView.findViewById(R.id.shopping_cart_item_quantity_minus);
            TextView price = (TextView) convertView.findViewById(R.id.shopping_cart_item_price);
            TextView Description = (TextView) convertView.findViewById(R.id.shopping_cart_item_description);
            TextView sellerNameTextView = (TextView) convertView.findViewById(R.id.shopping_cart_item_seller_name);

            Description.setText(itemDesc);

            if (type == TYPE_WITH_IMAGE) {
                ImageView imageView = (ImageView) convertView.findViewById(R.id.shopping_cart_item_image);

                if (itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
                Picasso.with(parent.getContext())
                        .load(itemImageAdd)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.no_image)
                        .into(imageView);
            }

            price.setText("₹ " + itemOfferPirce);
            itemNameTextView.setText(itemName);
            sellerNameTextView.setText(sellerName);
            itemQuantitySelectedTextView.setText(Integer.toString(itemQuantitySelected));

            m_shoppingCartItemsQuantityPlusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int itemQuan = Integer.parseInt(itemQuantitySelectedTextView.getText().toString());
                    itemQuan = itemQuan + 1;
                    itemQuantitySelectedTextView.setText(Integer.toString(itemQuan));
                    ((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected = itemQuan;

                    SharedPreferences prefs = m_activity.getSharedPreferences(
                            "technologies.angular.lazylad", Context.MODE_PRIVATE);
                    double m_totalCost = Double.parseDouble(prefs.getString("totalCost", "0.0"));;
                    int m_numOfItemsInShoppingCart = prefs.getInt("noOfItemsInShoppingCart", 0);

                    m_totalCost = Math.round(m_totalCost + (itemOfferPirce));
                    m_numOfItemsInShoppingCart = m_numOfItemsInShoppingCart + 1;

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("totalCost", String.valueOf(m_totalCost));
                    editor.putInt("noOfItemsInShoppingCart", m_numOfItemsInShoppingCart);
                    editor.commit();

                    TextView cost = ((ShoppingCartOnMainScreen) m_activity).m_totalCostTextView;
                    cost.setText(Double.toString(m_totalCost));
                    TextView items = ((ShoppingCartOnMainScreen) m_activity).m_totalItemsTextView;
                    items.setText(Integer.toString(m_numOfItemsInShoppingCart));

                    if (((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected == 1) {
                        ContentValues cartValues = new ContentValues();

                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_CODE, itemCode);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_NAME, itemName);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_IMG_FLAG, itemImgFlag);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_IMG_ADDRESS, ((ItemSnippet) getItem(itemPosition)).m_itemImgAddress);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_UNIT, itemUnit);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_COST, itemCost);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_SHORT_DESC, itemShortDesc);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_DESC, itemDesc);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_STATUS, itemStatus);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_SELECTED, ((ItemSnippet) getItem(itemPosition)).m_itemSelected);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED, ((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_ST_CODE, itemStCode);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_SC_CODE, itemScCode);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_SP_CODE, itemSpCode);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_OFFER_PRICE, itemOfferPirce);
                        cartValues.put(ShoppingCartEntry.COLUMN_ITEM_IN_OFFER, itemInOffer);
                        Uri insertUri1 = m_activity.getContentResolver().insert(ShoppingCartEntry.CONTENT_URI, cartValues);

                    } else {
                        ContentValues cartUpdatedValues = new ContentValues();
                        cartUpdatedValues.put(ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED, itemQuan);

                        String whereClause = ShoppingCartEntry.COLUMN_ITEM_CODE + " = ? " + " and " +
                                ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = ? ";
                        String[] selectionArgs = new String[]{itemCode, itemSpCode};
                        m_activity.getContentResolver().update(ShoppingCartEntry.CONTENT_URI, cartUpdatedValues, whereClause, selectionArgs);
                    }
                    Log.d("itemSpCode",itemSpCode);
                    new StoreClicksInSqlite(m_appContext).execute(itemCode,itemSpCode);
                }
            });

            m_shoppingCartItemsQuantityMinusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(itemQuantitySelectedTextView.getText().toString()) != 0) {
                        int sel;
                        if (Integer.parseInt(itemQuantitySelectedTextView.getText().toString()) == 1) {
                            ((ItemSnippet) getItem(itemPosition)).m_itemSelected = 0;
                        }
                        int itemQuan = Integer.parseInt(itemQuantitySelectedTextView.getText().toString());
                        itemQuan = itemQuan - 1;
                        itemQuantitySelectedTextView.setText(Integer.toString(itemQuan));
                        ((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected = itemQuan;     //Shopping Cart increased

                        if (((ItemSnippet) getItem(itemPosition)).m_itemQuantitySelected == 0) {
                            String whereClause = ShoppingCartEntry.COLUMN_ITEM_CODE + " = ? " + " and " +
                                    ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = ? ";
                            String[] selectionArgs = new String[]{itemCode, itemSpCode};
                            m_activity.getContentResolver().delete(
                                    ShoppingCartEntry.CONTENT_URI,
                                    whereClause,
                                    selectionArgs
                            );

                            String whereCondition = ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = ? ";
                            String[] selectionArgument = new String[]{itemSpCode};
                            Cursor cursor = m_activity.getContentResolver().query(
                                    ShoppingCartEntry.CONTENT_URI,
                                    null,
                                    whereCondition,
                                    selectionArgument,
                                    null);


                            if (cursor == null || cursor.getCount() == 0) {
                                String whereCond = OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
                                String[] selArg = new String[]{itemSpCode};
                                m_activity.getContentResolver().delete(
                                        OrderDetailsEntry.CONTENT_URI,
                                        whereCond,
                                        selArg
                                );
                            }
                        } else {
                            ContentValues itemUpdateValue = new ContentValues();
                            itemUpdateValue.put(ShoppingCartEntry.COLUMN_ITEM_QUANTITY_SELECTED, itemQuan);

                            String where = ShoppingCartEntry.COLUMN_ITEM_CODE + " = ? " + " and " +
                                    ShoppingCartEntry.COLUMN_ITEM_SP_CODE + " = ? ";
                            String[] Args = new String[]{itemCode, itemSpCode};

                            m_activity.getContentResolver().update(ShoppingCartEntry.CONTENT_URI, itemUpdateValue, where, Args);
                        }

                        SharedPreferences prefs = m_activity.getSharedPreferences(
                                "technologies.angular.lazylad", Context.MODE_PRIVATE);
                        double m_totalCost = Double.parseDouble(prefs.getString("totalCost", "0.0"));;
                        int m_numOfItemsInShoppingCart = prefs.getInt("noOfItemsInShoppingCart", 0);

                        m_totalCost = Math.round(m_totalCost - (itemOfferPirce));
                        m_numOfItemsInShoppingCart = m_numOfItemsInShoppingCart - 1;

                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("totalCost", String.valueOf(m_totalCost));
                        editor.putInt("noOfItemsInShoppingCart", m_numOfItemsInShoppingCart);
                        editor.commit();

                        TextView cost = ((ShoppingCartOnMainScreen) m_activity).m_totalCostTextView;
                        cost.setText(Double.toString(m_totalCost));
                        TextView items = ((ShoppingCartOnMainScreen) m_activity).m_totalItemsTextView;
                        items.setText(Integer.toString(m_numOfItemsInShoppingCart));

                        if (itemQuan == 0) {
                            m_itemsDetails.remove(itemPosition);
                            notifyDataSetChanged();
                        }
                    }
                }
            });
        }
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (((ItemSnippet) getItem(position)).m_itemImgFlag == 0)
            return TYPE_WITHOUT_IMAGE;
        else
            return TYPE_WITH_IMAGE;
    }


    private class StoreClicksInSqlite extends AsyncTask<String, Void, Void> {
        Context myactivity;

        public StoreClicksInSqlite(Context myactivity) {
            this.myactivity = myactivity;
        }

        @Override
        protected Void doInBackground(String... param) {

            int clicks_count = 0;
            String[] clicksSelectionValues = new String[]{LazyContract.ItemsClicksDetails.COLUMN_COUNT};
            String clicksWhereClause = LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE + " = ? and " +
                    LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE + " = ? and " +
                    LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC + " = ? ";
            String[] clicksSelectionArgs = new String[]{param[0],param[1], String.valueOf(0)};



            Cursor m_cursor = myactivity.getContentResolver().query(
                    LazyContract.ItemsClicksDetails.CONTENT_URI,
                    clicksSelectionValues,
                    clicksWhereClause,
                    clicksSelectionArgs,
                    null);

            if (m_cursor != null && m_cursor.moveToFirst()) {
                clicks_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.ItemsClicksDetails.COLUMN_COUNT));
            }
            clicks_count = clicks_count + 1;
            ContentValues clicksValues = new ContentValues();
            clicksValues.put(LazyContract.ItemsClicksDetails.COLUMN_COUNT, clicks_count);
            int clicks_update = myactivity.getContentResolver().update(LazyContract.ItemsClicksDetails.CONTENT_URI, clicksValues, clicksWhereClause, clicksSelectionArgs);
            if (clicks_update < 1) {
                clicksValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE, param[0]);
                clicksValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE, param[1]);
                clicksValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC, 0);
                myactivity.getContentResolver().insert(LazyContract.ItemsClicksDetails.CONTENT_URI, clicksValues);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }
}