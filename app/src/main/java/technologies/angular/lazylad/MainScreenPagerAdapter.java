package technologies.angular.lazylad;

/**
 * Created by Amud on 09/09/15.
 */

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;

class MainScreenPagerAdapter extends FragmentPagerAdapterExt {

    // This will Store the Titles of the Tabs which are Going to be passed when MainScreenPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this

    private ArrayList<ServiceTypeSnippet> m_servTypeListDetails;
    Context m_context;

    int flag;
    private int[] pageIDsArray;
    private int count;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public MainScreenPagerAdapter(FragmentManager fm, int mNumbOfTabsumb, Context context, int flag) {
        super(fm);
        this.NumbOfTabs = mNumbOfTabsumb;
        this.m_context = context;
        this.flag = flag;
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

            Cursor m_cursor = m_context.getContentResolver().query(
                    LazyContract.ServiceTypesEntry.CONTENT_URI,
                    null,
                    null,
                    null,
                    LazyContract.ServiceTypesEntry._ID + " LIMIT 4 OFFSET " + ((4 * position))
            );

            if (m_cursor != null && m_cursor.moveToFirst()) {
                m_servTypeListDetails = new ArrayList<ServiceTypeSnippet>(m_cursor.getCount());
                for (int i = 0; i < m_cursor.getCount(); i++) {

                    int id = m_cursor.getInt(0);
                    int st_code = m_cursor.getInt(1);
                    String st_name = m_cursor.getString(2);
                    int img_flag = m_cursor.getInt(3);
                    String img_add = m_cursor.getString(4);
                    int servTypeFlag = m_cursor.getInt(5);
                    int isService = m_cursor.getInt(6);

                    ServiceTypeSnippet srvTypeSnipObj = new ServiceTypeSnippet(id, st_code, st_name, img_flag, img_add, servTypeFlag, isService);
                    m_servTypeListDetails.add(srvTypeSnipObj);
                    m_cursor.moveToNext();
                }
            }

            if (!m_servTypeListDetails.isEmpty()) {
                MainActivityFragment mainActivityFragment = new MainActivityFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("servTypeListDetails", m_servTypeListDetails);
                bundle.putInt("position", position);
                mainActivityFragment.setArguments(bundle);
                return mainActivityFragment;
            } else
                return null;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return ((MainActivityFragment)getItem(position)).getTitle();
    }

    // This method return the Number of tabs for the tabs Strip
    @Override
    public int getCount() {
        return NumbOfTabs;
    }


    boolean canScrollVertically(int position, int direction) {
        return ((MainActivityFragment)getItem(position)).canScrollVertically(direction);
    }


}
