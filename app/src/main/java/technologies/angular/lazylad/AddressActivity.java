package technologies.angular.lazylad;

/**
 * Created by Ankur on 19-Jul-2015.
 */

import android.animation.ObjectAnimator;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.network.APISuggestionsService;
import technologies.angular.lazylad.LazyContract.UserAddressDetailsEntry;
import technologies.angular.lazylad.LazyContract.UserDetailsEntry;


public class AddressActivity extends ActionBarActivity {

    private static RecyclerView mRecyclerView;
    private static RecyclerView.Adapter mAdapter;
    private static RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    public static int i;
    int m_userCode;
    private View frame;
    private View topLayout;
    Bundle bundle = new Bundle();
    int sourceFlag = 0;

    private ArrayList<AddressSnippet> m_addressList;

    View linear_layout_address_View;
    private Toolbar toolbar;
    RecyclerView mRecyclerViewNav;
    private ArrayList<AddressSnippet> m_addressList_server;

    String TITLES[] = {"Home", "Notifications", "Addresses", "Order History", "Share & Earn", "Lazy Wallet", "", "Rate Us", "Feedback", "Call Us", "About Us"};
    int ICONS[] = {R.drawable.home, R.drawable.notification, R.drawable.address, R.drawable.orders, R.drawable.share, R.drawable.wallet, R.drawable.ic_launcher, R.drawable.rate, R.drawable.feedback, R.drawable.call, R.drawable.aboutus};
    String Name = "Add number";
    private SharedPreferences prefs;
    private SharedPreferences prefs_location;
    private String areaName;
    private String cityName;
    public static String mAddressOutput;
    DrawerLayout Drawer;
    ActionBarDrawerToggle mDrawerToggle;
    int animated_flag = 0;
    FragmentManager fragmentManager;
    boolean loadedFlag;
    View m_fragment_layout;
    LinearLayout addressFragment;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("Activity", "create");
        super.onCreate(savedInstanceState);
        loadedFlag = false;
        bundle = getIntent().getExtras();

        sourceFlag = bundle.getInt("sourceFlag");

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (sourceFlag == 0)
            setContentView(R.layout.address_layout);
        else
            setContentView(R.layout.checkout_address);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Addresses");
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerViewNav = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView);
        frame = (View) findViewById(R.id.topLayout);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        linear_layout_address_View = (View) findViewById(R.id.overLayView);
        addressFragment = (LinearLayout) findViewById(R.id.linear_layout_address);

        m_fragment_layout = findViewById(R.id.new_fragment_container);
        m_fragment_layout.setVisibility(View.GONE);

        setTracker();
        mHelper = new MoEHelper(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);

        Utils.verify_num(this, bundle, 2, false, NumberParameters.ADDRESS_SKIP_CONSTANT, NumberParameters.ADDRESS_VERIFIED_CONSTANT);
        boolean prefVerified = prefs.getBoolean(NumberParameters.ADDRESS_VERIFIED_CONSTANT, false);
        if (prefVerified) {
            if (sourceFlag == 0) {

                prefs_location = this.getSharedPreferences(
                        "technologies.angular.lazylad", Context.MODE_PRIVATE);
                areaName = prefs_location.getString(PreferencesStore.AREA_NAME_SELECTED_CONSTANT, "Not_Available");
                cityName = prefs_location.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, "Not Available");

                mAddressOutput = areaName + "," + " " + cityName;
                // Assigning the RecyclerView Object to the xml View

                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int displayViewWidth_ = size.x;

                mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, AddressActivity.this, 3);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
                mRecyclerViewNav.setAdapter(mAdapter);
                mLayoutManager = new LinearLayoutManager(this);
                mRecyclerViewNav.setLayoutManager(mLayoutManager);

                ViewGroup.LayoutParams params = mRecyclerViewNav.getLayoutParams();
                params.width = 9 * (displayViewWidth_ / 10);
                mRecyclerViewNav.setHasFixedSize(true);
                mRecyclerViewNav.setLayoutParams(params);

                mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.opendrawer, R.string.closedrawer) {

                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        mgr.hideSoftInputFromWindow(mRecyclerViewNav.getWindowToken(), 0);
                        float moveFactor = (mRecyclerViewNav.getWidth() * slideOffset);
                        frame.setTranslationX(moveFactor);
                    }
                };
                Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
                mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State
            }

            linear_layout_address_View.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    View frame;
                    int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
                    ViewGroup.LayoutParams params = view.getLayoutParams();
                    params.height = height;
                    linear_layout_address_View.setLayoutParams(params);
                    frame = (View) findViewById(R.id.new_fragment_container);
                    view.setBackgroundColor(000000);
                    ObjectAnimator mover = ObjectAnimator.ofFloat(frame, "translationY", 0f);
                    mover.setDuration(300);
                    mover.start();
                    View v = getCurrentFocus();
                    v.clearFocus();
                    animated_flag = 0;
                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            });

            m_addressList = null;

            // Code to Add an item with default animation
            fragmentManager = getFragmentManager();
            Cursor m_cursor = this.getContentResolver().query(
                    UserDetailsEntry.CONTENT_URI,
                    null,
                    null,
                    null,
                    null);
            if (m_cursor != null && m_cursor.moveToFirst()) {
                if (m_cursor.getCount() != 0) {
                    m_userCode = m_cursor.getInt(1);


                } else {
                    m_userCode = 0;
                }
            } else {
                m_userCode = 0;
                //TODO error handling
            }






            if (!loadedFlag)
                loadAddressfromServer();
        }
        mHelper.onResume(this);
    }

    private void loadAddressfromServer() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.169.62.100")
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();
        apiSuggestionsService.addressAPICall(m_userCode, new Callback<AddressSnippet>() {


            @Override
            public void success(AddressSnippet addressSnippet, Response response) {
                m_fragment_layout.setVisibility(View.VISIBLE);
                loadedFlag = true;
                m_addressList_server = new ArrayList<AddressSnippet>();
                m_addressList_server = addressSnippet.user_addresses;
                getContentResolver().delete(
                        LazyContract.UserAddressDetailsEntry.CONTENT_URI,
                        null,
                        null
                );

                for (int i = 0; i < m_addressList_server.size(); i++) {
                    int user_code = m_addressList_server.get(i).m_userCode;
                    int user_address_code = m_addressList_server.get(i).m_addressCode;
                    String m_addressText = m_addressList_server.get(i).m_addressText;
                    String m_customerName = m_addressList_server.get(i).m_userName;
                    String m_customerEmail = m_addressList_server.get(i).m_userEmail;
                    String m_customerNumber = m_addressList_server.get(i).m_userPhone;
                    String m_customerFlat = m_addressList_server.get(i).m_addressFlat;
                    String m_customerCity = m_addressList_server.get(i).m_userCity;
                    String m_customerSubArea = m_addressList_server.get(i).m_userSubarea;
                    String m_customerArea = m_addressList_server.get(i).m_userArea;
                    Double m_lat = m_addressList_server.get(i).m_lat;
                    Double m_longi = m_addressList_server.get(i).m_longi;

                    ContentValues addressValues = new ContentValues();
                    addressValues.put(UserAddressDetailsEntry.COLUMN_USER_CODE, user_code);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_USER_ADDRESS_CODE, user_address_code);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_USER_ADDRESS, m_customerCity);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_USER_NAME, m_customerName);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_USER_EMAIL_ID, m_customerEmail);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_USER_PHONE_NUMBER, m_customerNumber);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_USER_FLAT, m_customerFlat);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_USER_SUBAREA, m_customerSubArea);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_USER_AREA, m_customerArea);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_LATITUDE, m_lat);
                    addressValues.put(UserAddressDetailsEntry.COLUMN_LONGITUDE, m_longi);

                    Uri insertUri = getContentResolver().insert(UserAddressDetailsEntry.CONTENT_URI, addressValues);
                }
                progressDialog.dismiss();

                Cursor m_cursor = getContentResolver().query(
                        UserAddressDetailsEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);
                ArrayList<AddressSnippet> addressList = new ArrayList<AddressSnippet>();
                if (m_cursor != null && m_cursor.moveToFirst()) {
                    m_addressList = new ArrayList<AddressSnippet>(m_cursor.getCount());
                    if (m_cursor.getCount() != 0) {
                        for (int j = 0; j < m_cursor.getCount(); j++) {
                            int m_userCode = m_cursor.getInt(1);
                            int m_userAddressCode = m_cursor.getInt(2);
                            String userCity = m_cursor.getString(3);
                            String userName = m_cursor.getString(4);
                            String userEmail = m_cursor.getString(5);
                            String userPhoneNum = m_cursor.getString(6);
                            String user_flat = m_cursor.getString(7);
                            String user_subarea = m_cursor.getString(8);
                            String user_area = m_cursor.getString(9);
                            Double lat= m_cursor.getDouble(10);
                            Double longi=m_cursor.getDouble(11);
                            Log.d("latlong",lat+"  "+longi);

                            String userAddress = "";
                            if (user_flat != null && user_flat != "")
                                userAddress += user_flat + ", ";
                            if (user_subarea != null && user_subarea != "")
                                userAddress += user_subarea + ", ";
                            if (user_area != null && user_area != "")
                                userAddress += user_area + ", ";

                            userAddress += userCity;

                            AddressSnippet m_addSnippet = new AddressSnippet(j + 1, m_userCode, m_userAddressCode, userAddress, userName, userEmail, userPhoneNum,lat,longi);
                            addressList.add(m_addSnippet);
                            m_cursor.moveToNext();
                        }
                    } else {
                        //TODO Some UI Element asking to add address
                    }
                } else {
                    if (m_cursor == null || m_cursor.getCount() == 0) {
                        addressFragment.setVisibility(View.GONE);
                    }
                }

                m_addressList = addressList;
                mAdapter = new AddressAdapter(AddressActivity.this, m_addressList, fragmentManager, sourceFlag);
                mRecyclerView.setAdapter(mAdapter);
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                final AddressActivityFragment addressActivityFragment = new AddressActivityFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("flag", 0);
                addressActivityFragment.setArguments(bundle);
                fragmentTransaction.add(R.id.new_fragment_container, addressActivityFragment, "HELLO");
                fragmentTransaction.commit();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void addItem(AddressSnippet addressSnippet) {
        int index = m_addressList.size();
        if (index == 0)
            addressSnippet.m_addressId = 1;
        else
            addressSnippet.m_addressId = m_addressList.get(index - 1).m_addressId + 1;
        m_addressList.add(addressSnippet);
        mAdapter.notifyItemInserted(index);
    }

    public void editItem(String ustomerName, String customerNumber, String customerFlat, String customerSubArea, String customerCity, String customerArea, int addressCode) {

        String userAddress = "";
        if (customerFlat != null && customerFlat != "")
            userAddress += customerFlat + ", ";
        if (customerSubArea != null && customerSubArea != "")
            userAddress += customerSubArea + ", ";
        if (customerArea != null && customerArea != "")
            userAddress += customerArea + ", ";

        userAddress += customerCity;
        int index = getIndex(addressCode);
        m_addressList.get(index).m_userPhone = customerNumber;
        m_addressList.get(index).m_userName = ustomerName;
        m_addressList.get(index).m_addressText = userAddress;
        mAdapter.notifyItemChanged(index);
    }

    private void setTracker() {
        Tracker t = ((SessionManager) getApplication()).getTracker(SessionManager.TrackerName.APP_TRACKER);
        t.enableAdvertisingIdCollection(true);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName("technologies.angular.lazylad.CheckOut");

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }


    public int getIndex(int indexofCode) {
        for (int i = 0; i < m_addressList.size(); i++) {
            int code = m_addressList.get(i).m_addressCode;
            if (code == indexofCode) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onBackPressed() {
        if (animated_flag == 1) {
            View frame;
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
            ViewGroup.LayoutParams params = linear_layout_address_View.getLayoutParams();
            params.height = height;
            linear_layout_address_View.setLayoutParams(params);
            frame = (View) findViewById(R.id.new_fragment_container);
            linear_layout_address_View.setBackgroundColor(000000);
            ObjectAnimator mover = ObjectAnimator.ofFloat(frame, "translationY", 0f);
            mover.setDuration(300);
            mover.start();
            View v = getCurrentFocus();
            v.clearFocus();
            animated_flag = 0;
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (sourceFlag == 0)
            Drawer.closeDrawer(Gravity.LEFT);
        mHelper.onStop(this);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
