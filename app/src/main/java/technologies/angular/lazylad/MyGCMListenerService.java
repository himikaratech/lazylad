package technologies.angular.lazylad;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.moengage.push.MoEngageNotificationUtils;
import com.moengage.push.PushManager;

/**
 * Created by sakshigupta on 18/12/15.
 */
public class MyGCMListenerService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        if( null == data)return;
        if( MoEngageNotificationUtils.isFromMoEngagePlatform(data)){
            //If the message is not sent from MoEngage it will be rejected
            PushManager.getInstance(getApplicationContext()).getPushHandler().handlePushPayload(getApplicationContext(), data);
        }else{
            //your logic
            Log.i("No Message from", "MoEngage");
        }
    }
}
