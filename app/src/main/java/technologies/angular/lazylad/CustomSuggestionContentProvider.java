package technologies.angular.lazylad;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Saurabh on 03/03/15.
 */
public class CustomSuggestionContentProvider extends ContentProvider {

    public static final String AUTHORITY = "technologies.angular.lazylad.provider";
    public static Uri CONTENT_URI_SQL;
    public static Uri CONTENT_URI_INSERT;

    private static UriMatcher uriMatcher;
    private newDBHelper m_dbHelper;
    private SQLiteDatabase m_db;
    private Cursor m_cursor;

    static {
        CustomSuggestionContentProvider.CONTENT_URI_SQL = Uri.parse((String) ("content://technologies.angular.lazylad.provider/SQL"));
        CustomSuggestionContentProvider.CONTENT_URI_INSERT = Uri.parse((String) ("content://technologies.angular.lazylad.provider/search_suggest_query"));
        CustomSuggestionContentProvider.uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        CustomSuggestionContentProvider.uriMatcher.addURI("technologies.angular.lazylad.provider", "SQL", 1);
        CustomSuggestionContentProvider.uriMatcher.addURI("technologies.angular.lazylad.provider", "SQL/#", 2);
        CustomSuggestionContentProvider.uriMatcher.addURI("technologies.angular.lazylad.provider", "search_suggest_query", 3);
        CustomSuggestionContentProvider.uriMatcher.addURI("technologies.angular.lazylad.provider", "search_suggest_query/#", 4);

    }

    private static final String[] COLUMNS = {
            "_id", // must include this column
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_INTENT_DATA};

    @Override
    public boolean onCreate() {
        m_dbHelper = new newDBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = uri.getLastPathSegment();


        if (query.length() > 2) {
            Tracker search = ((SessionManager) getContext()).getTracker(SessionManager.TrackerName.APP_TRACKER);
            search.send(new HitBuilders.EventBuilder()
                    .setCategory("Search")
                    .setAction(query)
                    .build());
            if (SearchManager.SUGGEST_URI_PATH_QUERY.equals(query)) {
                return null;
            } else {
                m_db = m_dbHelper.getReadableDatabase();

                String sqlquery = "select * from search_result where search_string like '" + Utils.strForSqlite(query) + "'  COLLATE NOCASE ";

                this.m_cursor = this.m_db.rawQuery(sqlquery, (String[]) (null));

                this.m_cursor.moveToFirst();
                int n = 0;
                MatrixCursor cursor = new MatrixCursor(COLUMNS);
                for (int i = 0; i < this.m_cursor.getCount(); i++) {
                    cursor.addRow(createRow(new Integer(n), this.m_cursor.getString(2), this.m_cursor.getString(1)));
                    n++;
                    this.m_cursor.moveToNext();
                }
                return cursor;
            }
        }
        return null;
    }

    private Object[] createRow(Integer id, String text1, String text2) {
        return new Object[]{id, // _id
                text1, text2};
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}