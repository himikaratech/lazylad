package technologies.angular.lazylad;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import technologies.angular.lazylad.R;

/**
 * Created by Amud on 18/01/16.
 */
public class SelectCityCustomDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Dialog d;
    public Button yes, no;
    public Activity c;
    int m_servTypeCode;
    int m_servTypeFlag;
    public SelectCityCustomDialog(Context context,int servTypeCode,int servTypeFlag) {
        super(context);
        this.c=(Activity)context;
         this.m_servTypeCode=servTypeCode;
        this.m_servTypeFlag=servTypeFlag;

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.select_city_custom_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                Intent intent = new Intent(c, GeoLocation.class);
                intent.putExtra("servTypeCode", m_servTypeCode);
                intent.putExtra("servTypeFlag", m_servTypeFlag);
                c.startActivity(intent);
                break;
            case R.id.btn_no:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}

