package technologies.angular.lazylad;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;

/**
 * Created by sakshigupta on 03/11/15.
 */
public class ShoppingCartUtils {
    public static void destroyCart(Context context) {
        SharedPreferences cart = context.getSharedPreferences("technologies.angular.lazylad", context.MODE_PRIVATE);
        SharedPreferences.Editor editorCart = cart.edit();
        editorCart.remove("totalCost");
        editorCart.remove("noOfItemsInShoppingCart");
        editorCart.apply();

        context.getContentResolver().delete(
                LazyContract.ShoppingCartEntry.CONTENT_URI,
                null,
                null
        );

        context.getContentResolver().delete(
                LazyContract.OrderDetailsEntry.CONTENT_URI,
                null,
                null
        );
    }

    public static void alertDestroyCart(final Context context, final Utils.Handler alertHandler) {
        Cursor cursor = context.getContentResolver().query(
                LazyContract.ShoppingCartEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (cursor != null && cursor.getCount() > 0) {

            AlertDialog.Builder alertDialogBuilder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertDialogBuilder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                alertDialogBuilder = new AlertDialog.Builder(context);
            }
            alertDialogBuilder.setMessage("Changing location will empty your cart. Continue ?");
            alertDialogBuilder.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int arg1) {
                            destroyCart(context);
                            dialog.dismiss();
                            alertHandler.execute(true);
                        }
                    });
            alertDialogBuilder.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            alertHandler.execute(false);
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else
            alertHandler.execute(true);
    }

}
