package technologies.angular.lazylad;

import android.app.ProgressDialog;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import technologies.angular.lazylad.Snippets.LaundryCategoryItemSnippet;
import technologies.angular.lazylad.Snippets.LaundryCategorySnippet;
import technologies.angular.lazylad.adaptors.LaundryItemListAdaptor;
import technologies.angular.lazylad.adaptors.LaundryRateListPagerAdaptor;
import technologies.angular.lazylad.common.Constants;
import technologies.angular.lazylad.fragments.LaundryCategoryFragment;
import technologies.angular.network.APIResonseModel;
import technologies.angular.network.APISuggestionsService;

public class RateListActivity extends AppCompatActivity implements LaundryItemListAdaptor.OnItemBtnClickedListener, LaundryCategoryFragment.OnLaundryFragmentInteractionListener {

    private static final String TAG = RateListActivity.class.getSimpleName();
    @Bind(R.id.items_pager)
    ViewPager mViewPager;
    @Bind(R.id.tabs)
    PagerSlidingTabStrip mTabStrip;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.item_estimated_cost)
    TextView mItemEstimatedPriceTextView;

    private int mItemEstimatedCost = 0;

    private LaundryRateListPagerAdaptor mPagerAdapter;

    private ProgressDialog mProgressDialog;

    private ArrayList<LaundryCategorySnippet> mLaundryCategoryList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_list);
        ButterKnife.bind(this);
        toolbar.setTitle("Rate List");
        toolbar.setPadding(0, Utils.getStatusBarHeight(this), 0, 0);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        getServiceCategories();

    }

    private void getServiceCategories() {
        mProgressDialog = new ProgressDialog(this, R.style.MyTheme);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        mProgressDialog.show();

        RestAdapter restAdapter = Utils.providesRestAdapter(Constants.WEB_SERVICE_URL);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getLaundryCategories(Utils.getUserCode(this), new Callback<APIResonseModel.LaundryCategoriesResponseModel>() {
            @Override
            public void success(APIResonseModel.LaundryCategoriesResponseModel laundryCategoriesResponseModel, Response response) {
                boolean success = laundryCategoriesResponseModel.success;
                if (success) {
                    mLaundryCategoryList = laundryCategoriesResponseModel.categories;
                    setUpUI();
                } else {
                    mProgressDialog.dismiss();
                    Toast.makeText(RateListActivity.this, getString(R.string.server_error_occured), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, error.toString());
                Toast.makeText(RateListActivity.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });

    }

    private void setUpUI() {
        HashMap<String, ArrayList<LaundryCategoryItemSnippet>> categoryMap = new HashMap<>();
        for (LaundryCategorySnippet category : mLaundryCategoryList) {
            categoryMap.put(category.category_name, category.items);
        }
        ArrayList<String> categoryTypeList = new ArrayList<>(categoryMap.keySet());

        ArrayList<ArrayList<LaundryCategoryItemSnippet>> itemList = new ArrayList<>(categoryMap.values());
        mPagerAdapter = new LaundryRateListPagerAdaptor(getSupportFragmentManager(), itemList, categoryTypeList);
        mViewPager.setAdapter(mPagerAdapter);
        mTabStrip.setViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTabStrip.setViewPager(mViewPager);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mProgressDialog.dismiss();
    }

    @Override
    public void onPlusBtnClicked(String price, String tax) {
        mItemEstimatedCost += Double.parseDouble(price);
        if (mItemEstimatedCost == 0) {
            mItemEstimatedPriceTextView.setText("₹ " + mItemEstimatedCost);
        } else {
            mItemEstimatedPriceTextView.setText("₹ " + mItemEstimatedCost + " + tax");
        }

    }

    @Override
    public void onMinusBtnClicked(String price, String tax) {
        mItemEstimatedCost -= Double.parseDouble(price);
        if (mItemEstimatedCost == 0) {
            mItemEstimatedPriceTextView.setText("₹ " + mItemEstimatedCost);
        } else {
            mItemEstimatedPriceTextView.setText("₹ " + mItemEstimatedCost + " + tax");
        }

    }

    @Override
    public void onRateListPlusClicked(String price, String tax) {
        onPlusBtnClicked(price, tax);
    }

    @Override
    public void onRateListMinusClicked(String price, String tax) {
        onMinusBtnClicked(price, tax);
    }
}
