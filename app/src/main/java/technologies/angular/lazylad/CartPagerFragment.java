package technologies.angular.lazylad;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Amud on 24/08/15.
 */

public class CartPagerFragment extends Fragment {

    int position;
    private ArrayList<ItemSnippet> m_shoppingCartItemsList;

    private static RecyclerView cartRecycleView;
    private static RecyclerView.Adapter mAdapter;
    private static RecyclerView.LayoutManager mLayoutManager;

    public CartPagerFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        position = getArguments().getInt("position");
        m_shoppingCartItemsList = getArguments().getParcelableArrayList("shoppingCartItemsList");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.cart_list_layout, container, false);

        TextView itemName;
        TextView itemAmount;
        TextView itemRate;
        ImageView itemImage;
        TextView sellerNameTextView;

        itemName = (TextView) v.findViewById(R.id.itemName);
        itemRate = (TextView) v.findViewById(R.id.itemRate);
        itemAmount = (TextView) v.findViewById(R.id.itemAmount);
        itemImage = (ImageView) v.findViewById(R.id.itemImage);
        sellerNameTextView = (TextView) v.findViewById(R.id.sellername);

        String name = m_shoppingCartItemsList.get(position).m_itemName;
        int count = m_shoppingCartItemsList.get(position).m_itemQuantitySelected;
        int quantity = m_shoppingCartItemsList.get(position).m_itemQuantitySelected;
        String imageURL = m_shoppingCartItemsList.get(position).m_itemImgAddress;
        String sellerName = m_shoppingCartItemsList.get(position).seller_Name;
        String spCode = m_shoppingCartItemsList.get(position).m_itemServiceProvider;
        double itemOfferPrice = m_shoppingCartItemsList.get(position).m_itemOfferPrice;

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("technologies.angular.lazylad", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("spCodeOnConfirmationPage", String.valueOf(spCode));
        editor.commit();

        itemName.setText(name);
        itemAmount.setText("Quantity " + quantity + "");
        itemRate.setText("₹" + itemOfferPrice + "");
        sellerNameTextView.setText(sellerName);

        Picasso.with(getActivity())
                .load(imageURL)
                .placeholder(R.drawable.loading)
                .error(R.drawable.no_image)
                .fit().centerCrop()
                .into(itemImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.e("image", "loaded");
                    }

                    @Override
                    public void onError() {
                    }
                });
        return v;
    }
}

