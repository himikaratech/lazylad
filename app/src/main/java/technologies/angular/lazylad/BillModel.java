package technologies.angular.lazylad;

import java.util.ArrayList;

/**
 * Created by Saurabh on 08/04/15.
 */
public class BillModel {

    public int error;
    public String order_code;
    public String user_name;
    public String user_address;
    public String order_date;
    public double order_tot_amount;
    public double order_total_cost;
    public double order_delivery_cost;
    public double order_discount_cost;
    public String sp_code;

    public ArrayList<BillItemsSnippet> order_item_details;
}
