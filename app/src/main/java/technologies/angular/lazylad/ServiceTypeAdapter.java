package technologies.angular.lazylad;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Saurabh on 18/02/15.
 */
public class ServiceTypeAdapter extends BaseAdapter {

    private Activity m_activity;
    private ArrayList<ServiceTypeSnippet> m_servTypeDetailsList;


    public ServiceTypeAdapter(Activity activity, ArrayList<ServiceTypeSnippet> servTypeDetailsList) {
        m_activity = activity;
        m_servTypeDetailsList = servTypeDetailsList;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_servTypeDetailsList != null) {
            count = m_servTypeDetailsList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_servTypeDetailsList != null) {
            return m_servTypeDetailsList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_servTypeDetailsList != null) {
            return m_servTypeDetailsList.get(position).m_id;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        if (m_servTypeDetailsList != null) {
            final String servTypeName = ((ServiceTypeSnippet) getItem(position)).m_servTypeName;
            int imgFlag = ((ServiceTypeSnippet) getItem(position)).m_servTypeImgFlag;
            final String imgAdd = ((ServiceTypeSnippet) getItem(position)).m_servTypeImgAdd;
            final int servTypeCode = ((ServiceTypeSnippet) getItem(position)).m_servTypeCode;
            final int servTypeFlag = ((ServiceTypeSnippet) (getItem(position))).m_servTypeFlag;
            final int isService = ((ServiceTypeSnippet) (getItem(position))).m_isService;

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.service_types_lists, (ViewGroup) null);
            }

            final FrameLayout m_servTypeRelativeLayout = (FrameLayout) convertView.findViewById(R.id.service_type_relative_layout);
            final ImageView servTypeImageView = (ImageView) convertView.findViewById(R.id.serv_type_image);
            servTypeImageView.buildDrawingCache();

            Picasso.with(parent.getContext())
                    .load(imgAdd)
                    .placeholder(servTypeImageView.getDrawable())
                    .fit().centerCrop()
                    .into(servTypeImageView);

            TextView servTypeNameTextView = (TextView) convertView.findViewById(R.id.serv_type_name_textview);
            servTypeNameTextView.setText(servTypeName);

            m_servTypeRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences prefs = m_activity.getSharedPreferences(
                            "technologies.angular.lazylad", Context.MODE_PRIVATE);

                    final String selected_city_name = prefs.getString(PreferencesStore.CITY_NAME_SELECTED_CONSTANT, PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT);
                    if (selected_city_name.equals(PreferencesStore.DEFAULT_CITY_SELECTED_CONSTANT)) {

                        SelectCityCustomDialog cdd = new SelectCityCustomDialog(m_activity, servTypeCode, servTypeFlag);
                        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        cdd.show();
                    } else {
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("ServTypeCode", Integer.toString(servTypeCode));
                        editor.apply();

                        PayloadBuilder builder = new PayloadBuilder();
                        builder.putAttrInt("Selected StCode", servTypeCode);
                        builder.putAttrString("Activity Name", "MainActivityWithBanner");
                        MoEHelper.getInstance(m_activity).trackEvent("Service Type", builder.build());

                        if (isService == 1) {
                            final Cursor cursor = m_activity.getContentResolver().query(
                                    LazyContract.ShoppingCartEntry.CONTENT_URI,
                                    null,
                                    null,
                                    null,
                                    null);

                            if (cursor != null && cursor.getCount() > 0) {
                                AlertDialog.Builder alertDialogBuilder;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    alertDialogBuilder = new AlertDialog.Builder(m_activity, android.R.style.Theme_Material_Dialog_Alert);
                                } else {
                                    alertDialogBuilder = new AlertDialog.Builder(m_activity);
                                }
                                alertDialogBuilder.setMessage("Choosing Laundry Service will empty your cart. Continue ?");
                                alertDialogBuilder.setPositiveButton("Yes",
                                        new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int arg1) {
                                                ShoppingCartUtils.destroyCart(m_activity);
                                                Intent intent = new Intent(m_activity, LaundryServicesActivity.class);
                                                m_activity.startActivity(intent);
                                                cursor.close();
                                                dialog.dismiss();
                                            }
                                        });
                                alertDialogBuilder.setNegativeButton("No",
                                        new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                                cursor.close();
                                            }
                                        }).create().show();
                            } else {
                                Intent intent = new Intent(m_activity, LaundryServicesActivity.class);
                                intent.putExtra(LaundryServicesActivity.SERVICE_TYPE_CODE_TAG, servTypeCode);
                                m_activity.startActivity(intent);
                            }
                        } else {
                            Intent intent = new Intent(m_activity, ServiceProvidersList.class);
                            m_activity.startActivity(intent);
                        }

                    }
                }
            });
        }
        return convertView;
    }
}
