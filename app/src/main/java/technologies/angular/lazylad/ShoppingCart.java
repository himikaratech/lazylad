package technologies.angular.lazylad;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.moe.pushlibrary.MoEHelper;

import technologies.angular.lazylad.LazyContract.ShoppingCartEntry;
import technologies.angular.lazylad.LazyContract.OrderDetailsEntry;

import java.util.ArrayList;


public class ShoppingCart extends ActionBarActivity {

    private ArrayList<ItemSnippet> m_itemsList;
    private ArrayList<ItemSnippet> m_shoppingCartFinalList;
    private ShoppingCartConfirmationPageAdapter m_itemsAdapter;
    private ListView m_itemsDetails;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Shopping Cart");

        m_shoppingCartFinalList = new ArrayList<ItemSnippet>();
        m_itemsDetails = (ListView) findViewById(R.id.shoping_cart_list_view);

        Cursor m_cursor = getContentResolver().query(
                ShoppingCartEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            for (int i = 0; i < m_cursor.getCount(); i++) {
                int itemsId = i + 1;
                String m_itemCode = m_cursor.getString(1);
                String m_itemName = m_cursor.getString(2);
                m_itemName = m_itemName.replace("'", "''");
                int m_itemImgFlag = m_cursor.getInt(3);
                String m_itemImgAddress = m_cursor.getString(4);
                m_itemImgAddress = m_itemImgAddress.replace("'", "''");
                String m_itemUnit = m_cursor.getString(5);
                m_itemUnit = m_itemUnit.replace("'", "''");
                double m_itemCost = m_cursor.getDouble(6);
                String m_itemShortDesc = m_cursor.getString(7);  //Standard Weight
                m_itemShortDesc = m_itemShortDesc.replace("'", "''");
                String m_itemDesc = m_cursor.getString(8);
                m_itemDesc = m_itemDesc.replace("'", "''");
                String m_itemStatus = m_cursor.getString(9);  //Available and Not Available
                m_itemStatus = m_itemStatus.replace("'", "''");
                int itemSelected = m_cursor.getInt(10);
                int itemQuantitySelected = m_cursor.getInt(11);
                String m_itemServiceType = m_cursor.getString(12);
                String m_itemServiceTypeCategory = m_cursor.getString(13);
                String m_itemServiceProvider = m_cursor.getString(14);
                double m_itemOfferPrice = m_cursor.getDouble(15);
                int m_itemInOffer = m_cursor.getInt(16);



                String[] mProjection = {OrderDetailsEntry.COLUMN_SERV_PROV_NAME};
                String whereClause = OrderDetailsEntry.COLUMN_SERV_PROV_CODE + " = ? ";
                String[] selectionArgs = new String[]{m_itemServiceProvider};
                m_cursor = getContentResolver().query(
                        OrderDetailsEntry.CONTENT_URI,
                        mProjection,
                        whereClause,
                        selectionArgs,
                        null);

                if (m_cursor != null && m_cursor.moveToFirst()) {

                    String sellerName = m_cursor.toString();
                    ItemSnippet currenOrderObj = new ItemSnippet(itemsId, m_itemCode, m_itemName, m_itemImgFlag, m_itemImgAddress,
                            m_itemUnit, m_itemCost, m_itemShortDesc, m_itemDesc, m_itemStatus, itemSelected, itemQuantitySelected, m_itemServiceType, m_itemServiceTypeCategory, m_itemServiceProvider, sellerName,m_itemOfferPrice,m_itemInOffer);
                    m_shoppingCartFinalList.add(currenOrderObj);
                    m_cursor.moveToNext();
                }
            }
            m_itemsAdapter = new ShoppingCartConfirmationPageAdapter(this, m_shoppingCartFinalList);
            m_itemsDetails.setAdapter((ListAdapter) m_itemsAdapter);
        }
        mHelper = new MoEHelper(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shopping_cart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHelper.onStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHelper.onPause(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mHelper.onNewIntent(this, intent);
    }
}
