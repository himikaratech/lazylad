package technologies.angular.network;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import technologies.angular.lazylad.AddressSnippet;
import technologies.angular.lazylad.ItemModel;

public interface APISuggestionsService {


    @GET("/task_manager/v1/searchItemsSPCategoryWiseLimited/{serv_prov_code}/{serv_type_code}/{serv_category_code}")
    void searchFeeds(@Path("serv_prov_code") String serv_prov_code, @Path("serv_type_code") String serv_type_code,
                     @Path("serv_category_code") String serv_category_code,
                     @Query("search") String searchString, @Query("previous_item_code") String previousItemCode,
                     Callback<ItemModel> cb);


    @GET("/task_manager/v1/getAllUserAddresses/{m_userCode}")
    void addressAPICall(@Path("m_userCode") int m_userCode,
                        Callback<AddressSnippet> cb);

    @POST("/newserver/oms/placeBulkOrderFromCart")
    void sendOrderDetailsAPI(@Body APIRequestModel.SendOrderDetailsRequestModel params,
                             Callback<APIResonseModel.SendOrderDetailsResponseModel> callback);


    @POST("/task_manager/v1/checkCouponValidityFinal_v2")
    void applyCoupon(@Body APIRequestModel.CouponRequestModel param,
                     Callback<APIResonseModel.couponResultResponseModel> callback);


    @POST("/task_manager/v1/serviceCategoriesOfferedBySP")
    void postServiceCategory(@Body APIRequestModel.ServiceCategoryRequestModel param,
                             Callback<APIResonseModel.ServiceCategoryResponseModel> callback);

    @POST("/newserver/location/get_seller_list")
    void loadProvidersForAreaAndType(@Body APIRequestModel.ServiceTypeRequestModel param,
                                     Callback<APIResonseModel.ServiceTypeResponseModel> cb);

    //SplashScreen
    @POST("/newserver/location/v2_update_user_delivery_location")
    void updateServiceTypeAPICall(@Body APIRequestModel.ProvidersRequestModel param,
                                  Callback<APIResonseModel.ProvidersResponseModel> cb);

    @POST("/task_manager/v1/configDetails_v2")
    void configDetailsAPICall(@Body APIRequestModel.ConfigRequestModel param,
                              Callback<APIResonseModel.ConfigResponseModel> callback);

    @POST("/task_manager/v1/addNewUser")
    void signInUserAPICall(@Body APIRequestModel.NewUserRequestModel param,
                           Callback<APIResonseModel.SignInUserResponseModel> callback);


    //AreaActivity
    @GET("/task_manager/v1/getCities")
    void getCitiesAPICall(Callback<APIResonseModel.GetCityResponseModel> cb);


    //CityActivity

    @GET("/task_manager/v1/getAreas/{m_cityCode}")
    void getAreasAPICall(@Path("m_cityCode") String m_cityCode,
                         Callback<APIResonseModel.GetAreaResponseModel> cb);


    //AddressActivity
    @POST("/task_manager/v1/deleteUserAddressSynced")
    void deleteAddressAPICall(@Body APIRequestModel.DeleteAddressRequestModel param,
                              Callback<APIResonseModel.DeleteAddressResponseModel> callback);

    @POST("/task_manager/v1/{url}")
    void postNewAddForUserAPICall(@Path("url") String url,
                                  @Body APIRequestModel.PostNewAddForUserRequestModel param,
                                  Callback<APIResonseModel.PostNewAddForUserResponseModel> callback);


    //PreviosOrders

    @GET("/newserver/userapp/getUserPreviousOrdersGSONLimited_v2/{user_code}/{page_number}")
    void getPreviousOrderHistoryAPICall(@Path("user_code") String userCode,
                                        @Path("page_number") int page_number,
                                        Callback<APIResonseModel.GetPreviousOrderResponseModel> cb);

    @GET("/task_manager/v1/getOrderBillForUser/{m_orderCode}")
    void getOrderBillAPICall(@Path("m_orderCode") String m_orderCode,
                             Callback<APIResonseModel.GetOrderBillResponseModel> cb);

    @GET("/task_manager/v1/itemsServiceProviderCategoryWiseFinalWithMRPLimited/{m_spCode}/{m_stCode}/{m_scCode}")
    void getItemsFromServerLimitedAPICall(@Path("m_spCode") String m_spCode,
                                          @Path("m_stCode") String m_stCode,
                                          @Path("m_scCode") String m_scCode,
                                          @Query("previous_item_code") String previous_item_code,
                                          Callback<APIResonseModel.GetItemsFromServerLimitedResponseModel> cb);


    @POST("/task_manager/v1/cancelUsersPreviousOrder")
    void cancelOrderAPICall(@Body APIRequestModel.CancelOrderRequestModel param,
                            Callback<APIResonseModel.CancelOrderResponseModel> callback);


    @GET("/task_manager/v1/getConfirmedPreviousOrderDetails/{m_orderCode}/{m_userCode}")
    void loadPreviousOrderDetails1APICall(@Path("m_orderCode") String m_orderCode,
                                          @Path("m_userCode") String m_userCode,
                                          Callback<APIResonseModel.LoadPreviousOrderDetails1ResponseModel> cb);


    @GET("/task_manager/v1/getUserPreviousOrderDetails/{m_orderCode}/{m_userCode}")
    void loadPreviousOrderDetails2APICall(@Path("m_orderCode") String m_orderCode,
                                          @Path("m_userCode") String m_userCode,
                                          Callback<APIResonseModel.LoadPreviousOrderDetails2ResponseModel> cb);


    // OfferAcitivty


    @GET("/task_manager/v1/offersListLimited/{before_offer_code}")
    void getOffersListReadyAPICall(@Path("before_offer_code") int before_offer_code,
                                   Callback<APIResonseModel.GetOffersListReadyResponseModel> cb);


    //MainActivityWithBanner

    @GET("/task_manager/v1/activeOffersListLatest")
    void loadOfferAPICall(Callback<APIResonseModel.LoadOfferResponseModel> cb);


    //Profile
    @POST("/task_manager/v1/addNumberWithUserv2")
    void sendNumberToserverAPICall(@Body APIRequestModel.SendNumberToserverRequestModel param,
                                   Callback<APIResonseModel.SendNumberToserverResponseModel> cb);


    @POST("/task_manager/v1/getUserProfileDetails")
    void getUserProfileAPICall(@Body APIRequestModel.GetUserProfileRequestModel param,
                               Callback<APIResonseModel.GetUserProfileResponseModel> cb);


    @POST("/task_manager/v1/userProfileDetailsv2")
    void loadUserProfileAPICall(@Body APIRequestModel.LoudUserProfileRequestModel param,
                                Callback<APIResonseModel.LoadUserProfileResponseModel> cb);


    @GET("/task_manager/v1/aboutUsCategoriesforSeller")
    void aboutUsTabAPICall(Callback<APIResonseModel.AboutUsTabResponseModel> cb);

    @POST("/task_manager/v1/aboutUsInfoforId")
    void aboutUsDetailAPICall(@Body APIRequestModel.AboutUsDetailRequestModel param,
                              Callback<APIResonseModel.AboutUsDetailResponseModel> cb);


    @POST("/newserver/account/getWalletDetails")
    void getWalletDetailsAPICall(@Body APIRequestModel.GetWalletDetailsRequestModel param,
                                 Callback<APIResonseModel.GetWalletDetailsResponseModel> cb);

    @FormUrlEncoded
    @POST("/newserver/account/getAllTransactions")
    void getAllTransactionsAPICall(@Field("user_code") String user_code, @Field("owner_type") int owner_type,
                                   Callback<APIResonseModel.GetAllTransactionsResponseModel> cb);


    @POST("/newserver/referral/getReferralCodeForOwner")
    void getReferralCodeForOwnerAPICall(@Body APIRequestModel.GetReferralCodeForOwnerRequestModel param,
                                        Callback<APIResonseModel.GetReferralCodeForOwnerResponseModel> cb);

    @POST("/newserver/userapp/getOrderDetailsUser")
    void loadPaymentDetailsAPICall(@Body APIRequestModel.LoadPaymentDetailsRequestModel param,
                                   Callback<APIResonseModel.LoadPaymentDetailsResponseModel> cb);


    @POST("/newserver/rating/saverating")
    void ratingAndReviewAPICall(@Body APIRequestModel.RatingAndReviewRequestModel param,
                                Callback<APIResonseModel.RatingAndReviewResponseModel> cb);


    @GET("/maps/api/geocode/json")
    void getCityFromLatLong(@Query("latlng") String latlng,
                            @Query("key") String key,
                            @Query("result_type") String result_type,
                            Callback<APIResonseModel.ReverseGeoCodingResponseModel> cb);

    @POST("/task_manager/v1/getOrderDetailsUser")
    void geoTagAddressAPICall(@Body APIRequestModel.GeoTagAddressRequestModel param,
                              Callback<APIResonseModel.GeoTagAddressResponseModel> cb);

    @FormUrlEncoded
    @POST("/newserver/account/walletCashbackByCoupon")
    void postCouponCodeAPICall(@Field("user_code") String user_code, @Field("coupon_code") String coupon_code,
                               Callback<APIResonseModel.CouponPostResponseModel> cb);


    @GET("/newserver/coupon/get_all_coupons")
    void loadCouponAPICall(@Query("city_code") String city_code,
                           Callback<APIResonseModel.GetCouponResponseModel> cb);

    @POST("/newserver/payu/pay_online")
    void getPaymentParameters(@Body APIRequestModel.PayUParamsRequestModel payUParamsRequestModel, Callback<APIResonseModel.PayUResponseModel> cb);

    @POST("/newserver/payu/post_order")
    void getOrderDetailsResponse(@Body APIRequestModel.PaymentRequestModel paymentRequestModel, Callback<APIResonseModel.SendOrderDetailsResponseModel> cb);

    @GET("/newserver/services/get_services")
    void getLaunderyServices(@Query("user_code") int userCode, Callback<APIResonseModel.LaunderServiceDetailsResponseModel> cb);

    @POST("/newserver/services/post_order")
    void sendRequestedServices(@Body APIRequestModel.LaundryServicesRequestModel laundryServicesRequestModel, Callback<APIResonseModel.LaundryRequestedServicesResponseModel> cb);

    @GET("/newserver/services/get_categories")
    void getLaundryCategories(@Query("user_code") int userCode, Callback<APIResonseModel.LaundryCategoriesResponseModel> cb);

    @POST("/newserver/services/coupon_validity")
    void getCouponVailidity(@Body APIRequestModel.ServiceCouponRequestModel couponRequestModel, Callback<APIResonseModel.LaundryServiceCouponResponseModel> cb);

}
