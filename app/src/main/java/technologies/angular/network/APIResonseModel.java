package technologies.angular.network;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import technologies.angular.lazylad.LastActivitySnippet;
import technologies.angular.lazylad.AccountTransactionsDetailSnippet;
import technologies.angular.lazylad.AreaSnippet;
import technologies.angular.lazylad.BillItemsSnippet;
import technologies.angular.lazylad.CitySnippet;
import technologies.angular.lazylad.ItemSnippet;
import technologies.angular.lazylad.NotificationCenterSnippet;
import technologies.angular.lazylad.PreviousOrdersSnippet;
import technologies.angular.lazylad.ServiceProvidersListSnippet;
import technologies.angular.lazylad.Snippets.CouponBannerSnippet;
import technologies.angular.lazylad.Snippets.LaundryCategorySnippet;
import technologies.angular.lazylad.Snippets.LaundryServiceSnippet;
import technologies.angular.lazylad.offerSnippet;

/**
 * Created by Amud on 02/09/15.
 */
public class APIResonseModel {

    public class SendOrderDetailsResponseModel {
        @SerializedName("error_code")
        public int error;
        @SerializedName("success")
        public boolean success;
        @SerializedName("orders_details")
        public ArrayList<LastActivitySnippet> postOrderResponseModelArrayList;
        @SerializedName("tot_amount")
        public double total_amount;

        public String message;
    }

    public class couponResultResponseModel {
        public int error;
        public String coupon_type_id;
        public Double coupon_amount;
        public Double minimum_order_amount;
    }

    public class ServiceCategoryResponseModel {

        @SerializedName("error")
        public int error;
        @SerializedName("service_categories")
        public ArrayList<ServiceCategoryArrayResponseModel> serviceCategoryArrayResponseModelArrayList;

        public class ServiceCategoryArrayResponseModel {
            @SerializedName("st_code")
            public String st_code;
            @SerializedName("sc_code")
            public String sc_code;
            @SerializedName("sc_name")
            public String sc_name;
        }
    }

    public class ServiceTypeResponseModel {
        @SerializedName("error")
        public int error;
        @SerializedName("service_providers")
        public ArrayList<ServiceProvidersListSnippet> serviceProvidersListSnippetArrayList;
    }

    //SplashScreen
    public class ProvidersResponseModel {
        public String city_code;
        public ArrayList<ProviderArrayResponseModel> service_types;

        public class ProviderArrayResponseModel {
            public int id;
            public int st_code;
            public String st_name;
            public int st_img_flag;
            public String st_img_add;
            public int seller_automated;
            public int is_service;
        }
    }

    public class ConfigResponseModel {
        public int error;
        public int current_version;
        public int version_allow;

        public Number_verification number_verification;

        public class Number_verification {
            public int shall_number_verify;

        }

        public CallUsPhoneNumber lazylad_contact;

        public class CallUsPhoneNumber {
            public String call_us_number;
        }
    }

    public class SignInUserResponseModel {
        public int error;
        public ArrayList<UserDetailsObject> user_details;

        public class UserDetailsObject {
            public int user_code;
        }
    }

    //CityActivity
    public class GetCityResponseModel {
        public int error;
        public ArrayList<CitySnippet> cities;
    }

    public class GetAreaResponseModel {
        public ArrayList<AreaSnippet> areas;

    }

    public class DeleteAddressResponseModel {
        public int error;
    }

    public class PostNewAddForUserResponseModel {
        public int error;
        public ArrayList<NewUserArrayResponseModel> user_details;

        public class NewUserArrayResponseModel {
            public int user_code;
            public int user_address_code;
        }
    }

    public class GetPreviousOrderResponseModel {
        public ArrayList<PreviousOrdersSnippet> previous_orders_details;
    }

    public class GetOrderBillResponseModel {
        public int error;
        public String order_code;
        public String user_name;
        public String user_address;
        public String order_date;
        public double order_tot_amount;
        public double order_total_cost;
        public double taxes;
        public double order_delivery_cost;
        public double order_discount_cost;
        public String sp_code;

        public ArrayList<BillItemsSnippet> order_item_details;

    }

    public class GetItemsFromServerLimitedResponseModel {

        public ArrayList<ItemSnippet> items_service_providers;
    }

    public class GetOffersListReadyResponseModel {
        public int error;
        public ArrayList<NotificationCenterSnippet> offer_details;
    }

    public class CancelOrderResponseModel {
        public int error;
    }

    public class LoadPreviousOrderDetails1ResponseModel {
        public int error;
        public ArrayList<LoadPreviousOrderDetailsArrayResponseModel> previous_orders_confirmed_details;

        public class LoadPreviousOrderDetailsArrayResponseModel {
            public String item_code;
            public String item_name;
            public int item_img_flag;
            public String item_img_address;
            public String item_short_desc;
            public int item_quantity;
            public double item_cost;
            public String item_description;
        }
    }

    public class LoadPreviousOrderDetails2ResponseModel {
        public int error;
        public ArrayList<LoadPreviousOrderDetailsArrayResponseModel> previous_orders_details;

        public class LoadPreviousOrderDetailsArrayResponseModel {
            public String item_code;
            public String item_name;
            public int item_img_flag;
            public String item_img_address;
            public String item_short_desc;
            public int item_quantity;
            public double item_cost;
            public String item_description;
        }
    }

    public class LoadOfferResponseModel {
        @SerializedName("error")
        public int error;
        @SerializedName("offer_details")
        public ArrayList<offerSnippet> offer_details;
    }

    public class SendNumberToserverResponseModel {
        public int error;
    }

    public class LoadUserProfileResponseModel {
        public int error;
    }

    public class GetUserProfileResponseModel {
        public int error;

        public UserProfile user_profile_details;

        public class UserProfile {
            public String user_email;
            public String user_first_name;
            public String user_last_name;
        }
    }

    public class AboutUsTabResponseModel {
        public int error;
        public ArrayList<AboutUsArrayResponseModel> info_list;

        public class AboutUsArrayResponseModel {
            public int info_id;
            public String info_name;
        }
    }

    public class AboutUsDetailResponseModel {
        public int error;
        public String info_text;
    }

    public static class GetWalletDetailsResponseModel {
        public boolean success;

        public WalletDetail wallet;

        public class WalletDetail {
            public int w_id;
            public String owner_id;
            public String owner_type;
            public Double balance;
            public ArrayList<UsageEntry> wallet_usage;
        }

        public class UsageEntry {
            public String urid_type;
            public int usage_type;
            public double usage_number;
            public double usage_cap;
        }
    }

    public class GetAllTransactionsResponseModel {
        public boolean success;
        public String description;
        public String message;
        public ArrayList<AccountTransactionsDetailSnippet> transactions;
    }

    public class GetReferralCodeForOwnerResponseModel {
        public int error_code;
        public boolean success;
        public String referral_code;
    }

    public class LoadPaymentDetailsResponseModel {
        public int error_code;
        public OrderDeliveryDetails order_delivery_details;

        public OrderPaymentDetails order_payment_details;

        public class OrderDeliveryDetails {
            public String user_name;
            public String user_address;
            public String user_phone_number;

        }

        public class OrderPaymentDetails {
            public Double wallet_paid;
            public Double to_be_paid;
            public Double discount_amount;
            public String coupon_code;
            public Double paid;
        }
    }

    public class RatingAndReviewResponseModel {
        public int error_code;
        public boolean success;
    }


    public class GetCityCodeResponseModel {
        public int success;
    }


    public class GeoTagAddressResponseModel {
        public int success;
    }


    public class ReverseGeoCodingResponseModel {
        public ArrayList<AddressComponents> results;


        public class AddressComponents {
            public ArrayList<Addresses> address_components;
            public String formatted_address;
        }

        public class Addresses {
            public String short_name;
        }
    }

    public class CouponPostResponseModel {
        public int error;
        public boolean success;
        public String message;
        public Double wallet_balance;
        public Double amount_credited;
    }

    public class CouponsClicksResponseModel {
        public boolean success;
    }

    public class CouponsViewsResponseModel {
        public boolean success;
    }

    public class ItemsClicksResponseModel {
        public boolean success;
    }

    public class ItemsViewsResponseModel {
        public boolean success;
    }


    public class GetCouponResponseModel {
        public boolean coupon_exist_in_city;
        public ArrayList<CouponBannerSnippet> coupons;
    }

    //PayU Payment related responses
    public class PayUResponseModel {
        public boolean success;
        public int error_code;
        public String error_details;
        public String trans_id;
        public String key;
        public String surl;
        public String furl;
        public String salt;
        public UserDetails user_details;
        // public String user_credentials;
        @SerializedName("payu_hashes")
        public Hash payUHash;
        public String udf1;
        public String udf2;
        public String udf3;
        public String udf4;
        public String udf5;

        public class Hash {
            public String delete_user_card_hash;
            public String payment_related_details_for_mobile_sdk_hash;
            public String get_user_cards_hash;
            public String save_user_card_hash;
            public String vas_for_mobile_sdk_hash;
            public String get_merchant_ibibo_codes_hash;
            public String payment_hash;
            public String edit_user_card_hash;
        }

    }

    public class UserDetails {
        public String user_email;
        public String first_name;
        public String last_name;
        public String phone_number;

    }

    public class LaunderServiceDetailsResponseModel {
        public boolean success;
        public int error_code;
        @SerializedName("services")
        public ArrayList<LaundryServiceSnippet> laundryServiceList;


    }

    public class LaundryRequestedServicesResponseModel {
        public boolean success;
        public int error_code;
        public String order_code;
        public String pickup_time;
        public String delivery_time;
    }

    public class LaundryCategoriesResponseModel {
        public boolean success;
        public int error_code;
        public String error_details;
        public ArrayList<LaundryCategorySnippet> categories;
    }

    public class LaundryServiceCouponResponseModel{
        public boolean success;
        public int error_code;
        public String discount_amount;
        public String coupon_type;
        public double max_discount;
    }
}

