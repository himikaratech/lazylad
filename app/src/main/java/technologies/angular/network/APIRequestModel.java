package technologies.angular.network;

import com.google.gson.annotations.SerializedName;
import com.payu.india.Model.PaymentParams;

import java.util.ArrayList;

import technologies.angular.lazylad.ItemsStaticsListSnippet;
import technologies.angular.lazylad.Snippets.CouponsClicksListSnippet;
import technologies.angular.lazylad.Snippets.CouponsViewsListSnippet;
import technologies.angular.lazylad.Snippets.LaundryServicesRequestedSnippet;

/**
 * Created by Amud on 02/09/15.
 */
public class APIRequestModel {

    public class CouponRequestModel {

        public CouponRequestModel() {
        }

        public String coupon_code;
        public String user_code;
        public String total_amount;
    }


    public class ServiceCategoryRequestModel {
        public ServiceCategoryRequestModel() {
        }

        public String sp_code;
        public String st_code;
    }

    public class ServiceTypeRequestModel
    {
        public String lat;
        @SerializedName("long")
        public String longi;
        public String city_code;
        public String st_code;
    }

    public class ConfigRequestModel {
        public String city_code_selected;
        public String area_code_selected;
        public int lazylad_version_code;
        public String imei_number;
        public String mac_address;
        public int os_type;
        public String model_name;
        public String phone_manufacturer;
        public int user_code;
    }

    public class NewUserRequestModel {
        public String user_code;
        public String reg_id;
    }

    public class DeleteAddressRequestModel {
        public String user_code;
        public String changing_user_code;
        public String address_code;
    }

    public class PostNewAddForUserRequestModel {

        public String user_code;
        public String customer_name;
        public String customer_number;
        public String customer_email;
        public String customer_flat;
        public String customer_subarea;
        public String customer_area;
        public String customer_city;
        public String address_code;
        public String changing_user_code;
        public Double address_lat;
        public Double address_long;
    }

    public class CancelOrderRequestModel {
        public String order_code;
    }

    public class SendNumberToserverRequestModel {
        public String user_code;
        public String phone_number;
        public String verification_source;
//        public String referral_code;
    }

    public class LoudUserProfileRequestModel {
        public String user_code;
        public String user_email;
        public String user_first_name;
        public String user_last_name;
        public String referralCode;
    }

    public class GetUserProfileRequestModel {
        public String user_code;
    }

    public class AboutUsDetailRequestModel {
        public int info_id;
    }

    public class GetWalletDetailsRequestModel {
        public String user_code;
        public int owner_type;
    }

    public class GetAllTransactionsRequestModel {

        @SerializedName("user_code")
        public String user_code;
        @SerializedName("owner_type")
        public int owner_type;
        @SuppressWarnings("previous_tid")
        public int previous_tid;
    }

    public class GetReferralCodeForOwnerRequestModel {
        public String user_code;
        public int owner_type;
    }

    public class LoadPaymentDetailsRequestModel {
        public String user_code;
        public String order_code;
    }

    public class RatingAndReviewRequestModel{
        public String urid_type;
        public String urid;
        public String feedback;
        public String user_code;
        public String owner_type;

        @SerializedName("rating")
        public RatingStar rating = new RatingStar();

        public class RatingStar{
            public FactorData Factor1 = new FactorData();
            public FactorData Factor2 = new FactorData();
            public FactorData Factor3 = new FactorData();
            public FactorData Factor4 = new FactorData();

            public class FactorData{
                public int score;
                public int out_of;
            }
        }
    }

    public class GetCityFromLatLongRequestModel {
        public String user_code;
        public int owner_type;
    }

    public class GeoTagAddressRequestModel {
        public String user_code;
        public String changing_user_code;
        public String address_code;
        public Double latitude;
        public Double longitude;
    }

    public class ProvidersRequestModel {
        public String user_code;
        public String city_code;
        public String lat;
        @SerializedName("long")
        public String longi;
    }

    public class CouponPostModel {

        public String coupon_code;
        public String user_code;
    }


    public class CouponsClicksRequestModel {
        public ArrayList<CouponsClicksListSnippet> click_logs;
    }

    public class CouponsViewsRequestModel {
        public ArrayList<CouponsViewsListSnippet> view_logs;
    }


    public class ItemsClicksRequestModel {
        public ArrayList<ItemsStaticsListSnippet> click_logs;
    }

    public class ItemsViewsRequestModel {
        public ArrayList<ItemsStaticsListSnippet> view_logs;
    }

    // PayU Related Request Model
    public class PayUParamsRequestModel{

        public String user_code;
        public String prod_info;
        public String amount;
        @SerializedName("json_obj")
        public String orderDict;

    }

    public class SendOrderDetailsRequestModel {

        public String seller_details;
        public String JsonDataArray;
        public String address_code;
        public String user_exp_del_time;
        public String total_items_amount;
        public String order_payment_details;
        public String user_code;
        public String couponCode;
        public String tot_amount;
        public String address_user_code;
        public String discount_amount;
        public String delivery_charges;
    }

    public class PaymentRequestModel{
        public String trans_id;
    }

    public class LaundryServicesRequestModel{
        public String user_code;
        public String address_code;
        public String address_user_code;
        public String coupon_code;
        public String phone;
        public ArrayList<LaundryServicesRequestedSnippet>services_requested;

    }

    public class ServiceCouponRequestModel {
        public String coupon_code;
        public String user_code;
    }
}

