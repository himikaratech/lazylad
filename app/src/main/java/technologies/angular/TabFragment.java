package technologies.angular;

import android.app.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import technologies.angular.lazylad.LazyContract;
import technologies.angular.lazylad.R;
import technologies.angular.lazylad.Snippets.CouponBannerSnippet;
import technologies.angular.lazylad.Snippets.TabSnippet;

/**
 * Created by Amud on 03/02/16.
 */


public class TabFragment extends Fragment {

    int position;
    private ArrayList<String> m_tabSnippet;

    private static RecyclerView cartRecycleView;
    private static RecyclerView.Adapter mAdapter;
    private static RecyclerView.LayoutManager mLayoutManager;

    public TabFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        position = getArguments().getInt("position");
        m_tabSnippet = getArguments().getStringArrayList("m_tabSnippet");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_tab_item, container, false);

        String city_image_add=m_tabSnippet.get(position);

           if (city_image_add == null || city_image_add == "") city_image_add = "\"\"";
        final ImageView bannerImage = (ImageView) v.findViewById(R.id.city_image);
        Picasso.with(getContext())
                .load(city_image_add)
                .placeholder(R.drawable.cityimage)
                .error(R.drawable.cityimage)
                .fit().centerCrop()
                .into(bannerImage);

        /*final String coupon_name = m_tabSnippet.get(position).coupon_id;
        final String coupon_sort_desc = m_tabSnippet.get(position).coupon_short_desc;
        final String coupon_image = m_tabSnippet.get(position).coupon_img_add;
        final int coupon_flag = m_tabSnippet.get(position).coupon_img_flag;*/

        /*TextView name = (TextView) v.findViewById(R.id.coupon_name);
        TextView short_desc = (TextView) v.findViewById(R.id.coupon_short_desc);
        ImageView coupon_imageView = (ImageView) v.findViewById(R.id.coupon_image);*/

        /*name.setText(coupon_name);
        short_desc.setText(coupon_sort_desc);

        if (coupon_flag ==1 && coupon_image != null && !coupon_image.isEmpty()) {
            Picasso.with(getActivity())
                    .load(coupon_image)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.no_image)
                    .fit().centerCrop()
                    .into(coupon_imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.e("image", "loaded");
                        }

                        @Override
                        public void onError() {
                        }
                    });*/

        return v;
    }


}

