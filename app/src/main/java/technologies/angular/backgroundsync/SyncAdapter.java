package technologies.angular.backgroundsync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

/*import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;*/

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;


import technologies.angular.lazylad.ItemsStaticsListSnippet;
import technologies.angular.lazylad.LazyContract;
import technologies.angular.lazylad.R;
import technologies.angular.lazylad.Snippets.CouponsClicksListSnippet;
import technologies.angular.lazylad.Snippets.CouponsViewsListSnippet;
import technologies.angular.network.APIRequestModel;
import technologies.angular.network.APIResonseModel;


/**
 * Created by Amud on 29/01/16.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    public final String LOG_TAG = SyncAdapter.class.getSimpleName();
    public static final int SYNC_INTERVAL = 300;
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        // TODO Auto-generated constructor stub
    }

    @SuppressLint("NewApi")
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        //TODO connect to the server
        //TODO To write code to download and upload data from server to local content provider
        //TODO Handle data conflicts
        //TODO cleanup

        Log.e("syncing1", "viewsssstarted");


        int user_code;
        Cursor m_cursorUser = getContext().getContentResolver().query(
                LazyContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        if (m_cursorUser != null && m_cursorUser.moveToFirst()) {
            if (m_cursorUser.getCount() != 0) {
                user_code = m_cursorUser.getInt(1);


            } else {
                user_code = 0;
            }
        } else {
            user_code = 0;
            //TODO error handling
        }


        //Coupon Views

        ArrayList<CouponsViewsListSnippet> couponsViewsList = new ArrayList<CouponsViewsListSnippet>();
        String couponViewsWhereClause = LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " = ? and " +
                LazyContract.CouponsStatistics.COLUMN_COUPON_ID + " != ? ";
        String[] couponViewsSelectionArgs = new String[]{String.valueOf(0), String.valueOf(0)};


        Cursor m_cursor = getContext().getContentResolver().query(
                LazyContract.CouponsViewsDetails.CONTENT_URI,
                null,
                couponViewsWhereClause,
                couponViewsSelectionArgs,
                null);


        if (m_cursor != null && m_cursor.moveToFirst()) {

            if (m_cursor.getCount() != 0) {
                for (int j = 0; j < m_cursor.getCount(); j++) {


                    String coupon_id = m_cursor.getString(m_cursor.getColumnIndex(LazyContract.CouponsStatistics.COLUMN_COUPON_ID));
                    int views_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT));

                    CouponsViewsListSnippet couponsViewsListSnippet = new CouponsViewsListSnippet(coupon_id, views_count, String.valueOf(user_code));
                    couponsViewsList.add(couponsViewsListSnippet);
                    m_cursor.moveToNext();

                }

            }
        }

        if (!couponsViewsList.isEmpty()) {


            APIRequestModel requestModel = new APIRequestModel();
            final APIRequestModel.CouponsViewsRequestModel couponsViewsRequestModel = requestModel.new CouponsViewsRequestModel();
            couponsViewsRequestModel.view_logs = couponsViewsList;

            Gson gson = new Gson();
            String json = gson.toJson(couponsViewsRequestModel);


            try {

                Log.e("json", json);

                URL url = new URL("http://54.169.62.100/newserver/coupon/save_coupon_views");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(json);
                wr.flush();
                wr.close();

                conn.connect();


                Log.d("getResponseCode", conn.getResponseCode() + "");

                if (conn.getResponseCode() == 200) {


                    StringBuilder sb = new StringBuilder();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    String responseStr = sb.toString();
                    Log.d("responseCouponsViews", responseStr);
                    APIResonseModel.CouponsViewsResponseModel couponsViewsResponseModel = gson.fromJson(responseStr, APIResonseModel.CouponsViewsResponseModel.class);
                    boolean success = couponsViewsResponseModel.success;
                    if (success == true) {
                        m_cursor.moveToFirst();
                        if (m_cursor != null && m_cursor.moveToFirst()) {
                            if (m_cursor.getCount() != 0) {
                                for (int i = 0; i < m_cursor.getCount(); i++) {
                                    Log.d("_id", i + "");
                                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.CouponsViewsDetails._ID));
                                    String whereClause = LazyContract.CouponsViewsDetails._ID + " = ? ";
                                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                                    getContext().getContentResolver().delete(LazyContract.CouponsViewsDetails.CONTENT_URI, whereClause, selectionArgs);
                                    m_cursor.moveToNext();
                                }
                                markCouponsViewsSynced(couponViewsWhereClause, couponViewsSelectionArgs);
                            }
                        }


                    } else {
                        makeCouponViewsUpsyncToUnSync(m_cursor);
                    }
                } else {
                    makeCouponViewsUpsyncToUnSync(m_cursor);

                }

                conn.disconnect();


                if (m_cursor != null) m_cursor.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        //Coupon clicks


        ArrayList<CouponsClicksListSnippet> couponsClickList = new ArrayList<CouponsClicksListSnippet>();
        String couponClicksWhereClause = LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " = ? and " +
                LazyContract.CouponsStatistics.COLUMN_COUPON_ID + " != ? ";
        String[] couponClicksSelectionArgs = new String[]{String.valueOf(0), String.valueOf(0)};


        m_cursor = getContext().getContentResolver().query(
                LazyContract.CouponsClicksDetails.CONTENT_URI,
                null,
                couponClicksWhereClause,
                couponClicksSelectionArgs,
                null);


        if (m_cursor != null && m_cursor.moveToFirst()) {

            if (m_cursor.getCount() != 0) {
                for (int j = 0; j < m_cursor.getCount(); j++) {
                    Log.e("logging", "here");


                    String coupon_id = m_cursor.getString(m_cursor.getColumnIndex(LazyContract.CouponsStatistics.COLUMN_COUPON_ID));
                    int click_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.CouponsClicksDetails.COLUMN_COUNT));

                    CouponsClicksListSnippet couponsClicksListSnippet = new CouponsClicksListSnippet(coupon_id, click_count, String.valueOf(user_code));
                    couponsClickList.add(couponsClicksListSnippet);
                    m_cursor.moveToNext();

                }
                markCouponsClicksSynced(couponClicksWhereClause, couponClicksSelectionArgs);

            }
        }

        if (!couponsClickList.isEmpty()) {


            APIRequestModel requestModel = new APIRequestModel();
            final APIRequestModel.CouponsClicksRequestModel couponsClicksRequestModel = requestModel.new CouponsClicksRequestModel();
            couponsClicksRequestModel.click_logs = couponsClickList;

            Gson gson = new Gson();
            String json = gson.toJson(couponsClicksRequestModel);


            try {

                URL url = new URL("http://54.169.62.100/newserver/coupon/save_coupon_clicks");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(json);
                wr.flush();
                wr.close();

                conn.connect();


                if (conn.getResponseCode() == 200) {
                    String responseStr = null;

                    StringBuilder sb = new StringBuilder();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    responseStr = sb.toString();

                    Log.d("responseCouponClicks", responseStr);
                    APIResonseModel.CouponsClicksResponseModel couponsClicksResponseModel = gson.fromJson(responseStr, APIResonseModel.CouponsClicksResponseModel.class);
                    boolean success = couponsClicksResponseModel.success;
                    if (success == true) {
                        m_cursor.moveToFirst();
                        if (m_cursor != null && m_cursor.moveToFirst()) {
                            if (m_cursor.getCount() != 0) {
                                for (int i = 0; i < m_cursor.getCount(); i++) {
                                    Log.d("_id", i + "");
                                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.CouponsClicksDetails._ID));
                                    String whereClause = LazyContract.CouponsClicksDetails._ID + " = ? ";
                                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                                    getContext().getContentResolver().delete(LazyContract.CouponsClicksDetails.CONTENT_URI, whereClause, selectionArgs);
                                    m_cursor.moveToNext();
                                }
                            }
                        }


                    } else {
                        makeCouponClicksUpsyncToUnSync(m_cursor);
                    }
                } else {
                    makeCouponClicksUpsyncToUnSync(m_cursor);

                }


                if (m_cursor != null) m_cursor.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        //Items Clicks


        ArrayList<ItemsStaticsListSnippet> itemsClickList = new ArrayList<ItemsStaticsListSnippet>();
        String itemsClicksWhereClause = LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC + " = ? ";
        String[] itemsClicksSelectionArgs = new String[]{String.valueOf(0)};


        m_cursor = getContext().getContentResolver().query(
                LazyContract.ItemsClicksDetails.CONTENT_URI,
                null,
                itemsClicksWhereClause,
                itemsClicksSelectionArgs,
                null);


        if (m_cursor != null && m_cursor.moveToFirst()) {

            if (m_cursor.getCount() != 0) {
                for (int j = 0; j < m_cursor.getCount(); j++) {
                    Log.e("logging", "here");


                    String item_code = m_cursor.getString(m_cursor.getColumnIndex(LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE));
                    String item_sp_code = m_cursor.getString(m_cursor.getColumnIndex(LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE));
                    int click_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.ItemsClicksDetails.COLUMN_COUNT));

                    ItemsStaticsListSnippet itemsStaticsListSnippet = new ItemsStaticsListSnippet(item_code, click_count, String.valueOf(user_code), item_sp_code);
                    itemsClickList.add(itemsStaticsListSnippet);
                    m_cursor.moveToNext();

                }
                markItemsClicksSynced(itemsClicksWhereClause, itemsClicksSelectionArgs);

            }
        }

        if (!itemsClickList.isEmpty()) {


            APIRequestModel requestModel = new APIRequestModel();
            final APIRequestModel.ItemsClicksRequestModel itemsClicksRequestModel = requestModel.new ItemsClicksRequestModel();
            itemsClicksRequestModel.click_logs = itemsClickList;

            Gson gson = new Gson();
            String json = gson.toJson(itemsClicksRequestModel);


            try {

                URL url = new URL("http://54.169.62.100/newserver/item/save_item_clicks");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(json);
                wr.flush();
                wr.close();

                conn.connect();
                if (conn.getResponseCode() == 200) {
                    String responseStr = null;
                    StringBuilder sb = new StringBuilder();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    responseStr = sb.toString();

                    Log.d("responseItemsClicks", responseStr);
                    APIResonseModel.ItemsClicksResponseModel itemsClicksResponseModel = gson.fromJson(responseStr, APIResonseModel.ItemsClicksResponseModel.class);
                    boolean success = itemsClicksResponseModel.success;
                    if (success == true) {
                        m_cursor.moveToFirst();
                        if (m_cursor != null && m_cursor.moveToFirst()) {
                            if (m_cursor.getCount() != 0) {
                                for (int i = 0; i < m_cursor.getCount(); i++) {
                                    Log.d("_id", i + "");
                                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.ItemsClicksDetails._ID));
                                    String whereClause = LazyContract.ItemsClicksDetails._ID + " = ? ";
                                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                                    getContext().getContentResolver().delete(LazyContract.ItemsClicksDetails.CONTENT_URI, whereClause, selectionArgs);
                                    m_cursor.moveToNext();
                                }
                            }
                        }


                    } else {
                        makeItemsClicksUpsyncToUnSync(m_cursor);
                    }
                } else {
                    makeItemsClicksUpsyncToUnSync(m_cursor);

                }


                if (m_cursor != null) m_cursor.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


//Items Views

        ArrayList<ItemsStaticsListSnippet> itemsViewsList = new ArrayList<ItemsStaticsListSnippet>();
        String itemsViewsWhereClause = LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC + " = ? ";
        String[] itemsViewsSelectionArgs = new String[]{String.valueOf(0)};


        m_cursor = getContext().getContentResolver().query(
                LazyContract.ItemsViewsDetails.CONTENT_URI,
                null,
                itemsViewsWhereClause,
                itemsViewsSelectionArgs,
                null);


        if (m_cursor != null && m_cursor.moveToFirst()) {

            if (m_cursor.getCount() != 0) {
                for (int j = 0; j < m_cursor.getCount(); j++) {
                    Log.e("logging", "here");


                    String item_code = m_cursor.getString(m_cursor.getColumnIndex(LazyContract.ItemsStatistics.COLUMN_ITEMS_CODE));
                    String sp_code = m_cursor.getString(m_cursor.getColumnIndex(LazyContract.ItemsStatistics.COLUMN_ITEMS_SP_CODE));

                    int click_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.ItemsViewsDetails.COLUMN_VIEWS_COUNT));

                    ItemsStaticsListSnippet itemsStaticsListSnippet = new ItemsStaticsListSnippet(item_code, click_count, String.valueOf(user_code), sp_code);
                    itemsViewsList.add(itemsStaticsListSnippet);
                    m_cursor.moveToNext();

                }
                markItemsViewsSynced(itemsViewsWhereClause, itemsViewsSelectionArgs);

            }
        }

        if (!itemsViewsList.isEmpty()) {


            APIRequestModel requestModel = new APIRequestModel();
            final APIRequestModel.ItemsViewsRequestModel itemsViewsRequestModel = requestModel.new ItemsViewsRequestModel();
            itemsViewsRequestModel.view_logs = itemsViewsList;

            Gson gson = new Gson();
            String json = gson.toJson(itemsViewsRequestModel);


            try {

                URL url = new URL("http://54.169.62.100/newserver/item/save_item_views");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(json);
                wr.flush();
                wr.close();

                conn.connect();


                if (conn.getResponseCode() == 200) {

                    String responseStr = null;
                    StringBuilder sb = new StringBuilder();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    responseStr = sb.toString();

                    Log.d("responseItemViews", responseStr);
                    APIResonseModel.ItemsViewsResponseModel itemsViewsResponseModel = gson.fromJson(responseStr, APIResonseModel.ItemsViewsResponseModel.class);
                    boolean success = itemsViewsResponseModel.success;
                    if (success == true) {
                        m_cursor.moveToFirst();
                        if (m_cursor != null && m_cursor.moveToFirst()) {
                            if (m_cursor.getCount() != 0) {
                                for (int i = 0; i < m_cursor.getCount(); i++) {
                                    Log.d("_id", i + "");
                                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.ItemsViewsDetails._ID));
                                    String whereClause = LazyContract.ItemsViewsDetails._ID + " = ? ";
                                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                                    getContext().getContentResolver().delete(LazyContract.ItemsViewsDetails.CONTENT_URI, whereClause, selectionArgs);
                                    m_cursor.moveToNext();
                                }
                            }
                        }


                    } else {
                        makeItemsViewsUpsyncToUnSync(m_cursor);
                    }
                } else {
                    makeItemsViewsUpsyncToUnSync(m_cursor);

                }


                if (m_cursor != null) m_cursor.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return;
    }

    private void makeCouponClicksUpsyncToUnSync(Cursor m_cursor) {
        m_cursor.moveToFirst();
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    Log.d("_id", i + "");
                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.CouponsClicksDetails._ID));
                    String whereClause = LazyContract.CouponsClicksDetails._ID + " = ? ";
                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                    ContentValues values = new ContentValues();
                    values.put(LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 0);
                    getContext().getContentResolver().update(LazyContract.CouponsClicksDetails.CONTENT_URI, values, whereClause, selectionArgs);
                    m_cursor.moveToNext();
                }
            }
        }
    }


    private void makeCouponViewsUpsyncToUnSync(Cursor m_cursor) {
        m_cursor.moveToFirst();
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    Log.d("_id", i + "");
                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.CouponsViewsDetails._ID));
                    String whereClause = LazyContract.CouponsViewsDetails._ID + " = ? ";
                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                    ContentValues values = new ContentValues();
                    values.put(LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 0);
                    getContext().getContentResolver().update(LazyContract.CouponsViewsDetails.CONTENT_URI, values, whereClause, selectionArgs);
                    m_cursor.moveToNext();
                }
            }
        }
    }

    private void makeItemsViewsUpsyncToUnSync(Cursor m_cursor) {
        m_cursor.moveToFirst();
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    Log.d("_id", i + "");
                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.ItemsViewsDetails._ID));
                    String whereClause = LazyContract.ItemsViewsDetails._ID + " = ? ";
                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                    ContentValues values = new ContentValues();
                    values.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC, 0);
                    getContext().getContentResolver().update(LazyContract.ItemsViewsDetails.CONTENT_URI, values, whereClause, selectionArgs);
                    m_cursor.moveToNext();
                }
            }
        }
    }


    private void makeItemsClicksUpsyncToUnSync(Cursor m_cursor) {
        m_cursor.moveToFirst();
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    Log.d("_id", i + "");
                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyContract.ItemsClicksDetails._ID));
                    String whereClause = LazyContract.ItemsClicksDetails._ID + " = ? ";
                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                    ContentValues values = new ContentValues();
                    values.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC, 0);
                    getContext().getContentResolver().update(LazyContract.ItemsClicksDetails.CONTENT_URI, values, whereClause, selectionArgs);
                    m_cursor.moveToNext();
                }
            }
        }
    }

    private void markCouponsClicksSynced(String clicksWhereClause, String[] clicksSelectionArgs) {
        ContentValues updateUpSyncValues = new ContentValues();
        updateUpSyncValues.put(LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 1);
        getContext().getContentResolver().update(LazyContract.CouponsClicksDetails.CONTENT_URI, updateUpSyncValues, clicksWhereClause, clicksSelectionArgs);
    }


    private void markItemsClicksSynced(String itemsClicksWhereClause, String[] itemsClicksSelectionArgs) {
        ContentValues updateUpSyncValues = new ContentValues();
        updateUpSyncValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC, 1);
        getContext().getContentResolver().update(LazyContract.ItemsClicksDetails.CONTENT_URI, updateUpSyncValues, itemsClicksWhereClause, itemsClicksSelectionArgs);
    }


    private void markCouponsViewsSynced(String viewsWhereClause, String[] viewsSelectionArgs) {
        ContentValues updateUpSyncValues = new ContentValues();
        updateUpSyncValues.put(LazyContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 1);
        getContext().getContentResolver().update(LazyContract.CouponsViewsDetails.CONTENT_URI, updateUpSyncValues, viewsWhereClause, viewsSelectionArgs);
    }

    private void markItemsViewsSynced(String itemsViewsWhereClause, String[] itemsViewsSelectionArgs) {
        ContentValues updateUpSyncValues = new ContentValues();
        updateUpSyncValues.put(LazyContract.ItemsStatistics.COLUMN_ITEMS_UP_SYNC, 1);
        getContext().getContentResolver().update(LazyContract.ItemsViewsDetails.CONTENT_URI, updateUpSyncValues, itemsViewsWhereClause, itemsViewsSelectionArgs);
    }


    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = LazyContract.CONTENT_AUTHORITY;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(), syncInterval);
        }
    }


    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context),
                LazyContract.CONTENT_AUTHORITY, bundle);

        Log.d("views", "syncing");
    }


    public static Account getSyncAccount(Context context) {
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        if (null == accountManager.getPassword(newAccount)) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            Log.d("account", "created");
            onAccountCreated(newAccount, context);
        } else {
            Log.d("account", "not created");
        }

        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {


        /*
         * Since we've created an account
         */
        SyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);

        /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount, LazyContract.CONTENT_AUTHORITY, true);

        /*
         * Finally, let's do a sync to get things started
         */
        //  syncImmediately(context);
    }

    public static void initializeSyncAdapter(Context context) {

        getSyncAccount(context);
    }

}